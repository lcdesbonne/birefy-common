package com.birefy.common.repo;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.birefy.common.CommonIntegrationTest;
import com.birefy.common.entity.QTag;

public class QTagRepositoryTest extends CommonIntegrationTest {
	@Autowired
	private QTagRepository tagRepository;
	
	private QTag testTag;
	
	@Before
	public void init() {
		this.testTag = dataGenerator.createQTag();
	}
	
	@Test
	public void testFindAllTags() {
		List<QTag> tags = tagRepository.findAll();
		assertFalse(tags.isEmpty());
		
		assertTrue(tags.stream().anyMatch(tag -> {
			return tag.equals(testTag);
		}));
	}
	
	@Test
	public void testFindTagsByName() {
		List<QTag> tags = tagRepository.searchByTitle("Test");
		
		assertFalse(tags.isEmpty());
		assertTrue(tags.stream().anyMatch(tag -> {
			return tag.equals(testTag);
		}));
		
		List<QTag> alternateTagSearch = tagRepository.searchByTitle("zx");
		if(!alternateTagSearch.isEmpty()) {
			assertFalse(alternateTagSearch.stream().anyMatch(tag -> {
				return tag.equals(testTag);
			}));
		}
	}
}
