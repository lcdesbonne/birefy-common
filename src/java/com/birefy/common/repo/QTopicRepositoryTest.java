package com.birefy.common.repo;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.birefy.common.CommonIntegrationTest;
import com.birefy.common.entity.QTag;
import com.birefy.common.entity.QTopic;
import com.birefy.common.entity.QTopicTag;

public class QTopicRepositoryTest extends CommonIntegrationTest {
	@Autowired
	private QTopicRepository topicRepository;
	@Autowired
	private QTopicTagRepository topicTagRepository;
	
	private QTopic testTopic;
	
	@Before
	public void init() {
		testTopic = dataGenerator.createTopic();
	}
	
	@Test
	public void testFindAll() {
		List<QTopic> topics = topicRepository.findAll(true);
		assertFalse(topics.isEmpty());
		assertTrue(topics.stream().anyMatch(topic -> {
			return topic.equals(testTopic);
		}));
	}
	
	@Test
	public void testFindByTitle() {
		List<QTopic> topics = topicRepository.findByTitle(true, testTopic.getTitle().substring(1));
		assertFalse(topics.isEmpty());
		assertTrue(topics.stream().anyMatch(topic -> {
			return topic.equals(testTopic);
		}));
		
		List<QTopic> differentTopics = topicRepository.findByTitle(true, "zx");
		if(!differentTopics.isEmpty()) {
			assertTrue(differentTopics.stream().noneMatch(topic -> {
				return topic.equals(testTopic);
			}));
		}
	}
	
	@Test
	public void testFindByTags() throws Exception {
		//Create test tag
		QTag testTag = dataGenerator.createQTag();
		//Index the tag with the topic
		QTopicTag topicTag = new QTopicTag();
		topicTag.setTagId(testTag.getId());
		topicTag.setTopicId(testTopic.getId());
		
		//Save the new index
		topicTagRepository.addNewRecord(topicTag);
		
		//Query for the topic by tag
		List<QTopic> topics = topicRepository.findByTags(true, Arrays.asList(testTag.getId()));
		assertFalse(topics.isEmpty());
		assertTrue(topics.stream().anyMatch(topic -> {
			return topic.equals(testTopic);
		}));
		
		//Query for a different tag
		List<QTopic> topicsDifferentTag = topicRepository.findByTags(true, Arrays.asList(testTag.getId() + 1l));
		if (!topicsDifferentTag.isEmpty()) {
			assertTrue(topicsDifferentTag.stream().noneMatch(topic -> {
				return topic.equals(testTopic);
			}));
		}
		
	}
	
	@Test
	public void testFindByTitleAndTags() throws Exception {
		//Create test tag
		QTag testTag = dataGenerator.createQTag();
		//Link tag and topic
		QTopicTag link = new QTopicTag();
		link.setTopicId(testTopic.getId());
		link.setTagId(testTag.getId());
		topicTagRepository.addNewRecord(link);
		//Test search by valid title but invalid tag
		List<QTopic> topics = topicRepository.findByTitleAndTags(true, testTopic.getTitle().substring(1), Arrays.asList(testTag.getId() + 1));
		assertTrue(topics.stream().noneMatch(topic -> {
			return topic.equals(testTopic);
		}));
		
		//Test search by valid title and tag
		List<QTopic> validTagTitleTopics = topicRepository.findByTitleAndTags(true, testTopic.getTitle().substring(1), Arrays.asList(testTag.getId()));
		assertTrue(validTagTitleTopics.stream().anyMatch(topic -> {
			return topic.equals(testTopic);
		}));
		
		//Test search by invalid title and valid tag
		List<QTopic> invalidTitleTopics = topicRepository.findByTitleAndTags(true, "zx", Arrays.asList(testTag.getId()));
		assertTrue(invalidTitleTopics.stream().noneMatch(topic -> {
			return topic.equals(testTopic);
		}));
		
	}

}
