package com.birefy.common;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.birefy.common.testUtils.DataGenerator;


/**
 * Base integration test class
 * @author Lennon
 *
 */
@ContextConfiguration(classes = Application.class)
@WebAppConfiguration
@SqlGroup({
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD,
			scripts = "classpath:initialTables.sql"),
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD,
			scripts = "classpath:dropTables.sql")
})
@RunWith(SpringRunner.class)
public class CommonIntegrationTest {
	@Autowired
	protected DataGenerator dataGenerator;
	@Autowired
	protected JdbcTemplate jdbcTemplate;
	
	@Test
	@Ignore
	public void stub() {}

}
