CREATE DATABASE tidings;

USE tidings;

CREATE TABLE member_channel (
	id bigint AUTO_INCREMENT,
	member_id bigint,
	tiding_id varchar(50) NOT NULL UNIQUE,
	channel_url varchar(200),
	active boolean NOT NULL DEFAULT TRUE,
	member_authority int NOT NULL DEFAULT 0,
	PRIMARY KEY (id)
);

CREATE TABLE message (
	content text,
	creator bigint NOT NULL,
	recipient bigint NOT NULL,
	creation_date datetime NOT NULL,
	archived boolean NOT NULL DEFAULT FALSE,
	FOREIGN KEY (creator) REFERENCES member_channel(id),
	FOREIGN KEY (recipient) REFERENCES member_channel(id)
);

CREATE TABLE blocked_list (
	blocked_by bigint UNIQUE,
	blocked bigint NOT NULL,
	CONSTRAINT pk_blocked_list PRIMARY KEY (blocked_by, blocked),
	FOREIGN KEY (blocked_by) REFERENCES member_channel(id),
	FOREIGN KEY (blocked) REFERENCES member_channel(id)
);
