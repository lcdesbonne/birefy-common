CREATE TABLE member (
	id bigint NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
    first_name varchar(255) NOT NULL,
    family_name varchar(255) NOT NULL,
    email varchar(255) NOT NULL UNIQUE,
    phone varchar(255),
    authority smallint,
    active boolean,
    enabled boolean DEFAULT = FALSE,
    PRIMARY KEY (id)
);

CREATE TABLE q_topic (
	id bigint NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
    title varchar(255) NOT NULL UNIQUE,
    detail varchar(1000),
    active boolean DEFAULT FALSE,
    PRIMARY KEY (id)
);

CREATE TABLE q_question (
	id bigint NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
    title varchar(255) NOT NULL UNIQUE,
    detail varchar(1000),
    topic_id bigint NOT NULL,
    active boolean DEFAULT FALSE,
    app_url varchar(255) NOT NULL,
    source varchar(250),
    port INTEGER DEFAULT 8080,
    review boolean DEFAULT FALSE,
    inception date
    PRIMARY KEY (id),
    FOREIGN KEY (topic_id) REFERENCES q_topic(id)
);

CREATE TABLE q_tag (
	id bigint NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
    tag_name varchar(50) NOT NULL UNIQUE,
    PRIMARY KEY (id)
);

CREATE TABLE technical_tag (
	id bigint NOT NULL GENERATED ALWAYS AS IDENTY (START WITH 1, INCREMENT BY 1),
	name varchar(250) NOT NULL UNIQUE,
	PRIMARY KEY (id)
);

CREATE TABLE q_question_technical_tag (
	question_id bigint NOT NULL,
	tag_id bigint NOT NULL,
	CONSTRAINT pk_question_technical_tag PRIMARY KEY (question_id, tag_id),
	FOREIGN KEY (question_id) REFERENCES q_question(id),
	FOREIGN KEY (tag_id) REFERENCES technical_tag(id)
);

CREATE TABLE q_topic_tag (
	topic_id bigint NOT NULL,
    tag_id bigint NOT NULL,
    CONSTRAINT pk_topic_tag PRIMARY KEY (topic_id, tag_id),
    FOREIGN KEY (topic_id) REFERENCES q_topic(id),
    FOREIGN KEY (tag_id) REFERENCES q_tag(id)
);

CREATE TABLE q_member_topic (
	member_id bigint NOT NULL,
    topic_id bigint NOT NULL,
    CONSTRAINT pk_member_topic PRIMARY KEY (member_id, topic_id),
    FOREIGN KEY (member_id) REFERENCES member(id),
    FOREIGN KEY (topic_id) REFERENCES q_topic(id)
);

CREATE TABLE q_member_question (
	member_id bigint NOT NULL,
    question_id bigint NOT NULL,
    CONSTRAINT pk_member_question PRIMARY KEY (member_id, question_id),
    FOREIGN KEY (member_id) REFERENCES member(id),
    FOREIGN KEY (question_id) REFERENCES q_question(id)
);