CREATE DATABASE common;

USE common;

CREATE TABLE member (
	id bigint NOT NULL AUTO_INCREMENT,
    first_name varchar(255) NOT NULL,
    family_name varchar(255) NOT NULL,
    email varchar(255) NOT NULL UNIQUE,
    phone varchar(255),
    access_key varchar(250),
    authority tinyint,
    active boolean DEFAULT false,
    enabled boolean DEFAULT FALSE,
    subscription_customer_id varchar(255),
    host_service boolean DEFAULT FALSE,
    last_paid datetime,
    PRIMARY KEY (id)
);

CREATE TABLE q_topic (
	id bigint NOT NULL AUTO_INCREMENT,
    title varchar(255) NOT NULL UNIQUE,
    detail varchar(1000),
    active boolean DEFAULT FALSE,
    PRIMARY KEY (id)
);

CREATE TABLE q_question (
	id bigint NOT NULL AUTO_INCREMENT,
    title varchar(255) NOT NULL UNIQUE,
    detail varchar(1000),
    topic_id bigint NOT NULL,
    active boolean DEFAULT FALSE,
    app_url varchar(255) NOT NULL,
    source varchar(250),
    review boolean DEFAULT FALSE,
    inception date,
    subscription boolean DEFAULT FALSE,
    port integer DEFAULT 8080,
    delete_requested boolean DEFAULT FALSE,
    launch_requested boolean DEFAULT FALSE,
    PRIMARY KEY (id),
    FOREIGN KEY (topic_id) REFERENCES q_topic(id)
);

CREATE TABLE q_tag (
	id bigint NOT NULL AUTO_INCREMENT,
    tag_name varchar(50) NOT NULL UNIQUE,
    PRIMARY KEY (id)
);

CREATE TABLE technical_tag (
	id bigint NOT NULL AUTO_INCREMENT,
	name varchar(250) NOT NULL UNIQUE,
	PRIMARY KEY (id)
);

CREATE TABLE q_topic_tag (
	topic_id bigint NOT NULL,
    tag_id bigint NOT NULL,
    CONSTRAINT pk_topic_tag PRIMARY KEY (topic_id, tag_id),
    FOREIGN KEY (topic_id) REFERENCES q_topic(id),
    FOREIGN KEY (tag_id) REFERENCES q_tag(id)
);

CREATE TABLE q_question_technical_tag (
	question_id bigint NOT NULL,
	tag_id bigint NOT NULL,
	CONSTRAINT pk_question_technical_tag PRIMARY KEY (question_id, tag_id),
	FOREIGN KEY (question_id) REFERENCES q_question(id),
	FOREIGN KEY (tag_id) REFERENCES technical_tag(id)
);

CREATE TABLE q_member_topic (
	member_id bigint NOT NULL,
    topic_id bigint NOT NULL,
    CONSTRAINT pk_member_topic PRIMARY KEY (member_id, topic_id),
    FOREIGN KEY (member_id) REFERENCES member(id),
    FOREIGN KEY (topic_id) REFERENCES q_topic(id)
);

CREATE TABLE q_member_question (
	member_id bigint NOT NULL,
    question_id bigint NOT NULL,
    details text,
    CONSTRAINT pk_member_question PRIMARY KEY (member_id, question_id),
    FOREIGN KEY (member_id) REFERENCES member(id),
    FOREIGN KEY (question_id) REFERENCES q_question(id)
);

CREATE TABLE member_verification (
	member_id bigint NOT NULL UNIQUE,
	token varchar(36) NOT NULL,
	creation_date date NOT NULL,
	change_password boolean NOT NULL DEFAULT FALSE,
	FOREIGN KEY (member_id) REFERENCES member(id)
);

CREATE TABLE bulletin (
	id bigint NOT NULL AUTO_INCREMENT,
	description text NOT NULL,
	open boolean NOT NULL DEFAULT TRUE,
	targeted boolean NOT NULL DEFAULT TRUE,
	creation_date datetime NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE member_bulletin (
	bulletin_id bigint NOT NULL,
	member_id bigint NOT NULL,
	CONSTRAINT pk_member_bulletin PRIMARY KEY (bulletin_id, member_id)
);

CREATE TABLE bulletin_tech_tag (
	bulletin_id bigint NOT NULL,
	tag_id bigint NOT NULL,
	FOREIGN KEY (bulletin_id) REFERENCES bulletin(id),
	FOREIGN KEY (tag_id) REFERENCES technical_tag(id)
);

CREATE TABLE sound (
	id bigint NOT NULL AUTO_INCREMENT,
	member_id bigint,
	location varchar(255) UNIQUE,
    category varchar(50),
    name varchar(255),
    creation_date datetime NOT NULL,
    price DECIMAL(13,2),
    software TEXT NOT NULL,
    review BOOLEAN DEFAULT TRUE,
    PRIMARY KEY (id),
    FOREIGN KEY (member_id) REFERENCES member(id)
);

CREATE TABLE painting (
	id BIGINT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    location VARCHAR(255) UNIQUE,
    member_id BIGINT REFERENCES member(id),
    creation_date DATETIME,
    price DECIMAL(13,2),
    category VARCHAR(25),
    software TEXT NOT NULL,
    review BOOLEAN DEFAULT TRUE
);

CREATE TABLE sound_tag (
	id BIGINT PRIMARY KEY AUTO_INCREMENT,
	tag VARCHAR(255) UNIQUE
);

CREATE TABLE painting_tag (
	id BIGINT PRIMARY KEY AUTO_INCREMENT,
	tag VARCHAR(255) UNIQUE
);

CREATE TABLE painting_descriptive_tag (
	painting_id BIGINT REFERENCES painting(id),
	painting_tag_id BIGINT REFERENCES painting_tag(id),
	CONSTRAINT pk_painting_descriptive_tag PRIMARY KEY (painting_id, painting_tag_id)
);

CREATE TABLE sound_descriptive_tag (
	sound_id BIGINT REFERENCES sound(id),
	sound_tag_id BIGINT REFERENCES sound_tag(id),
	CONSTRAINT pk_sound_descriptive_tag PRIMARY KEY (sound_id, sound_tag_id)
);

CREATE TABLE member_technical_tag (
	member_id BIGINT REFERENCES member(id),
    technical_tag_id BIGINT REFERENCES technical_tag(id),
    CONSTRAINT pk_member_technical_tag PRIMARY KEY (member_id, technical_tag_id)
);

CREATE TABLE sound_purchases (
	member_id BIGINT REFERENCES member(id),
	sound_id BIGINT REFERENCES sound(id),
	CONSTRAINT pk_sound_perchases PRIMARY KEY (member_id, sound_id)
);

CREATE TABLE image_purchases (
	member_id BIGINT REFERENCES member(id),
	painting_id BIGINT REFERENCES painting(id),
	CONSTRAINT pk_image_purchases PRIMARY KEY (member_id, painting_id)
);

CREATE TABLE deal (
	id BIGINT PRIMARY KEY AUTO_INCREMENT,
    customer_id BIGINT NOT NULL REFERENCES member(id),
    supplier_id BIGINT NOT NULL REFERENCES member(id),
    supplier_confirm BOOLEAN DEFAULT FALSE,
    customer_confirm BOOLEAN DEFAULT FALSE,
    supplier_details TEXT NOT NULL,
    customer_details TEXT NOT NULL,
    creation_date DATETIME NOT NULL,
    active BOOLEAN DEFAULT TRUE,
    paid DATETIME,
    price DECIMAL(13,2)
);

CREATE TABLE inventory_type (
	id SMALLINT PRIMARY KEY AUTO_INCREMENT,
    type VARCHAR(50)
);

CREATE TABLE deal_inventory (
	id BIGINT PRIMARY KEY AUTO_INCREMENT,
	title VARCHAR(255) NOT NULL,
	deal_id BIGINT NOT NULL REFERENCES deal(id),
	author_id BIGINT NOT NULL REFERENCES member(id),
    inventory_type_id SMALLINT REFERENCES inventory_type(id),
    location VARCHAR(255),
	can_preview BOOLEAN DEFAULT FALSE,
    app_url VARCHAR(255),
    run_commands TEXT,
    can_download BOOLEAN DEFAULT FALSE,
    active BOOLEAN DEFAULT FALSE
);

CREATE TABLE revenue (
	id BIGINT PRIMARY KEY AUTO_INCREMENT,
	member_id BIGINT NOT NULL REFERENCES member(id),
	product_id BIGINT NOT NULL,
	description VARCHAR(255),
	category VARCHAR(255) NOT NULL,
	inventory_request_price INTEGER NOT NULL,
	birefy_charge INTEGER NOT NULL,
	created DATETIME NOT NULL,
	resolved BOOLEAN DEFAULT FALSE,
	payment_id VARCHAR(255),
	payment_confirmed BOOLEAN DEFAULT FALSE
);

INSERT INTO inventory_type (type) VALUES ('Image'), ('Audio'), ('Runnable Programme'), ('Other');

CREATE TABLE member_beneficiary (
	member_id BIGINT NOT NULL REFERENCES member(id),
	beneficiary_id VARCHAR(255) NOT NULL,
	CONSTRAINT member_beneficiary_pk PRIMARY KEY (member_id, beneficiary_id)
);


