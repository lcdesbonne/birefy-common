package com.birefy.common.repo;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.birefy.common.CommonIntegrationTest;
import com.birefy.common.entity.Member;

public class MemberRepositoryTest extends CommonIntegrationTest {
	@Autowired
	private MemberRepository memberRepository;
	
	private Member testMember;
	
	@Before
	public void init() {
		testMember = dataGenerator.createMemberInDatabase();
	}
	
	@Test
	public void testFindMemberById() {
		Member member = memberRepository.selectFullMemberDetailsById(testMember.getId());
		assertTrue(member.equals(testMember));
	}
	
	@Test
	public void testFindMemberByEmail() {
		Member member = memberRepository.findMemberByEmail(testMember.getEmail());
		assertTrue(member.equals(testMember));
	}
	
	@Test
	public void testCountMembers() {
		long count = memberRepository.countMembers();
		
		assertTrue(count > 0);
	}
	
	@Test
	public void testFindAllMembers() {
		List<Member> members = memberRepository.selectAllMembers();
		assertTrue(members.stream().anyMatch(member -> {
			return member.equals(testMember);
		}));
	}
	
	@Test
	public void testFindMembersByName() {
		//Search with a valid first name
		String validFirstName = testMember.getFirstName().substring(1);
		List<Member> membersByFirstName = memberRepository.findMembersByName(validFirstName);
		assertTrue(membersByFirstName.stream().anyMatch(m -> {
			return m.equals(testMember);
		}));
		//Search with a valid last name
		String validLastName = testMember.getFamilyName().substring(1);
		List<Member> membersByLastName = memberRepository.findMembersByName(validLastName);
		assertTrue(membersByLastName.stream().anyMatch(m -> {
			return m.equals(testMember);
		}));
	}

}
