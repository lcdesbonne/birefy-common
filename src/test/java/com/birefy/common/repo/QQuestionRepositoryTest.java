package com.birefy.common.repo;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.springframework.beans.factory.annotation.Autowired;

import com.birefy.common.CommonIntegrationTest;
import com.birefy.common.entity.QQuestion;
import com.birefy.common.entity.QTopic;

public class QQuestionRepositoryTest extends CommonIntegrationTest {
	@Autowired
	private QQuestionRepository questionRepository;
	@Autowired
	private QTopicRepository topicRepository;
	
	private QQuestion question;
	
	@Before
	public void init() {
		question = dataGenerator.createQQuestion();
	}
	
	@Test
	public void testSearchQuestions() {
		List<QQuestion> activeQs = questionRepository.searchQuestions(true);
		
		Predicate<QQuestion> containsQuestion = q -> {
			return q.equals(question);
		};
		
		assertTrue(activeQs.stream().anyMatch(containsQuestion));
		
		List<QQuestion> pendingQs = questionRepository.searchQuestions(false);
		assertFalse(pendingQs.stream().anyMatch(containsQuestion));
	}
	
	@Test
	public void testCountQuestions() {
		long expectedCount = 1l;
		long expectedPendingCount = 0l;
		long activeCount = questionRepository.countQuestions(true);
		assertTrue(activeCount == expectedCount);
		long pendingCount = questionRepository.countQuestions(false);
		assertTrue(pendingCount == expectedPendingCount);
		
	}
	
	@Test
	public void testSearchQuestionsByTitle() throws Exception{
		//Add an extra question
		QQuestion newQuestion = new QQuestion();
		newQuestion.setActive(true);
		newQuestion.setAppUrl("http:\\test2.co");
		newQuestion.setDetail("2nd test question");
		newQuestion.setTitle("Test 2 Question");
		newQuestion.setTopicId(question.getTopicId());
		
		//Add the new question to the database
		questionRepository.addNewRecord(newQuestion);
		
		List<QQuestion> approxMatchResult = questionRepository.searchQuestionsByTitle("tesT", true);
		Predicate<QQuestion> nameMatch = q -> q.getTitle().toLowerCase().contains("test");
		assertTrue(approxMatchResult.stream().allMatch(nameMatch));
		Predicate<QQuestion> exactMatch = q -> q.getTitle()
				.toLowerCase().equals(newQuestion.getTitle().toLowerCase());
		List<QQuestion> accurateMatch = questionRepository.searchQuestionsByTitle(newQuestion.getTitle(), true);
		assertTrue(accurateMatch.stream().allMatch(exactMatch));
	}
	
	@Test
	public void testSearchQuestionsByTopic() {
		List<QQuestion> results = questionRepository.searchQuestionsByTopic(Arrays.asList(question.getTopicId()), true);
		Predicate<QQuestion> topicIdMatch = q -> q.getTopicId().equals(question.getTopicId());
		assertTrue(results.stream().allMatch(topicIdMatch));
	}
	
	@Test
	public void testSearchQuestionsByTopicAndTitle() throws Exception {
		QQuestion question = new QQuestion();
		long newTopicId = createNewTopicId();
		question.setActive(true);
		question.setAppUrl("http:\\redirecttest.co");
		question.setDetail("Search topic and title");
		question.setTitle("Search Topic and Title");
		question.setTopicId(newTopicId);
		questionRepository.addNewRecord(question);
		
		Predicate<QQuestion> containsNewId = q -> q.getTopicId().equals(new Long(newTopicId));
		List<QQuestion> haveNewTopicId = questionRepository.searchQuestionsByTopicAndTitle(Arrays.asList(newTopicId), "Search", true);
		assertFalse(haveNewTopicId.isEmpty());
		assertTrue(haveNewTopicId.stream().allMatch(containsNewId));
		Predicate<QQuestion> containDefaultTopicId = q -> q.getTopicId().equals(this.question.getTopicId());
		List<QQuestion> questionsWithDefaultTopics = questionRepository.searchQuestionsByTopicAndTitle(Arrays.asList(this.question.getTopicId()), "Test", true);
		assertFalse(questionsWithDefaultTopics.isEmpty());
		assertTrue(questionsWithDefaultTopics.stream().allMatch(containDefaultTopicId));
		
	}
	
	private long createNewTopicId() {
		QTopic topic = new QTopic();
		topic.setActive(true);
		topic.setDetail("Topic for QQuestionRepository test");
		topic.setTitle("QQuestionRepository test Topic");
		try{
			topicRepository.addNewRecord(topic);
		} catch (Exception e){}
		String queryForTopicId = "SELECT id FROM "+QTopic.TABLE_NAME+" WHERE "+QTopic.TITLE_COLUMN+" = '"+topic.getTitle()+"'";
		long id = jdbcTemplate.queryForObject(queryForTopicId, Long.class);
		return id;
	}

}
