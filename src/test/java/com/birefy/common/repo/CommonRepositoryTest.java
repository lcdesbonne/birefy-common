package com.birefy.common.repo;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertNotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.birefy.common.CommonIntegrationTest;
import com.birefy.common.entity.Member;
import com.birefy.common.entity.utils.database.MemberRowMapper;


public class CommonRepositoryTest extends CommonIntegrationTest {
	//Test functionality against the Member table
	@Autowired
	private CommonRepository<Member> repository;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Test
	public void testAddNewRecord() throws Exception {
		Member member = dataGenerator.createMember();
		repository.addNewRecord(member);
		String querySavedEntity = "SELECT * FROM "+ Member.TABLE_NAME + " WHERE email='"+member.getEmail()+"'";
		Member result = jdbcTemplate.queryForObject(querySavedEntity, new MemberRowMapper());
		
		//Compare the result with what was saved
		assertNotNull(result.getId());
		assertEquals(member.getEmail(), result.getEmail());
	}
	
	@Test
	public void testUpdate() throws Exception {
		Member member = dataGenerator.createMemberInDatabase();
		assertNotNull(member.getId());
		//Modify one of the member fields
		member.setFamilyName("NewTestName");
		repository.update(member);
		
		String queryUpdatedObject = "SELECT * FROM " + Member.TABLE_NAME + " WHERE id="+member.getId().toString();
		Member result = jdbcTemplate.queryForObject(queryUpdatedObject, new MemberRowMapper());
		assertEquals(result.getId(), member.getId());
		assertEquals(member.getFamilyName(), "NewTestName");
	}

}
