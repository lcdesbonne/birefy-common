package com.birefy.common.testUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.birefy.common.entity.Member;
import com.birefy.common.entity.QQuestion;
import com.birefy.common.entity.QTag;
import com.birefy.common.entity.QTopic;

/**
 * Creates objects in the database
 * @author Lennon
 *
 */
@Component
public class DataGenerator {
	private static final String INSERT_STATEMENT = "INSERT INTO %s (%s) VALUES (%s)";
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public Member createMemberInDatabase() {
		Member member = new Member();
		member.setFirstName("TestName");
		member.setFamilyName("Testname");
		member.setEmail("test.testname@test.com");
		member.setPhone("09052458760");
		
		String columns = Member.FIRST_NAME_COLUMN+","+Member.FAMILY_NAME_COLUMN+","+Member.EMAIL_COLUMN+","+Member.PHONE_COLUMN;
		String values = "'"+member.getFirstName()+"',"
				+"'"+member.getFamilyName()+"','"+member.getEmail()+"','"+member.getPhone()+"'";
		String query = String.format(INSERT_STATEMENT, Member.TABLE_NAME, columns, values);
		try {
		jdbcTemplate.execute(query);
		} catch (Exception e) {}
		String queryForId = "SELECT id FROM " + Member.TABLE_NAME + " WHERE email = 'test.testname@test.com'";
		Long id = jdbcTemplate.queryForObject(queryForId, Long.class);
		member.setId(id);
		return member;
	}
	
	/**
	 * Creates a member without id
	 * @return
	 */
	public Member createMember() {
		Member member = new Member();
		member.setFirstName("TestName");
		member.setFamilyName("Testname");
		member.setEmail("test.testname@test.com");
		member.setPhone("09052450987");
		//member.setId(1l);
		return member;
	}
	
	public QTopic createTopic() {
		QTopic topic = new QTopic();
		topic.setTitle("Test Topic");
		topic.setDetail("Topic for testing");
		topic.setActive(true);
		
		String columns = QTopic.TITLE_COLUMN+","+QTopic.DETAIL_COLUMN+","+QTopic.ACTIVE_COLUMN;
		String values = "'"+topic.getTitle()+"','"+topic.getDetail()+"',"+topic.getActive();
		try {
			jdbcTemplate.execute(String.format(INSERT_STATEMENT, QTopic.TABLE_NAME, columns, values));
		} catch (Exception e){}
		String queryForId = "SELECT id FROM "+QTopic.TABLE_NAME+" WHERE "+QTopic.TITLE_COLUMN+" = '"+topic.getTitle()+"'";
		Long id = jdbcTemplate.queryForObject(queryForId, Long.class);
		topic.setId(id);
		return topic;
	}
	
	/**
	 * Creates an active question
	 * @return QQuestion
	 */
	public QQuestion createQQuestion() {
		QTopic topic = createTopic();
		QQuestion question = new QQuestion();
		question.setTitle("Test question");
		question.setDetail("Question for testing");
		question.setActive(true);
		question.setAppUrl("http:\\test.co");
		question.setTopicId(topic.getId());
		
		String columns = QQuestion.TITLE_COLUMN + "," + QQuestion.DETAIL_COLUMN + "," + QQuestion.TOPIC_COLUMN + ","
				+ QQuestion.ACTIVE_COLUMN + "," + QQuestion.APP_URL_COLUMN;
		
		String values = "'" + question.getTitle() + "','" + question.getDetail() + "'," + question.getTopicId() + ","
				+ question.getActive() + ",'" + question.getAppUrl()+"'";
		try{
			jdbcTemplate.execute(String.format(INSERT_STATEMENT, QQuestion.TABLE_NAME, columns, values));
		} catch (Exception e){}
		String queryForId = "SELECT id FROM "+QQuestion.TABLE_NAME+" WHERE "+QQuestion.TITLE_COLUMN+" = '"+question.getTitle()+"'";
		Long questionId = jdbcTemplate.queryForObject(queryForId, Long.class);
		question.setId(questionId);
		
		return question;
	}
	
	public QTag createQTag() {
		QTag tag = new QTag();
		tag.setTagName("Test Tag");
		try{
			jdbcTemplate.execute(String.format(INSERT_STATEMENT, QTag.TABLE_NAME, QTag.TAG_NAME_COLUMN, "'"+tag.getTagName()+"'"));
		} catch (Exception e) {}
		String queryForId = "SELECT id FROM "+QTag.TABLE_NAME+" WHERE "+QTag.TAG_NAME_COLUMN+" = '"+tag.getTagName()+"'";
		Long tagId = jdbcTemplate.queryForObject(queryForId, Long.class);
		tag.setId(tagId);
		return tag;
	}
	
}
