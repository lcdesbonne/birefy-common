Hello from Birefy.

We have received a request to update your passphrase.

If you would like to do so, please click the link below.

${webAddress}/revenue-payable/passphrase-update?token=${token}&member=${memberId}

If you didn't request this please contact support@birefy.com

