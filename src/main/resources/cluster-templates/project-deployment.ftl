kind: Service
apiVersion: v1
metadata:
  name: question/q-${qId}
spec:
  selector:
    app: question/q-${qId}
    tier: app
  ports:
  - protocol: TCP
    port: ${port}
    targetPort: http
  type: NodePort  
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: question/q-${qId}
spec:
  selector:
    matchLabels:
      app: question/q-${qId}
      tier: app
      track: stable
  replicas: 1
  template:
    metadata:
      labels:
        app: question/q-${qId}
        tier: app
        track: stable
    spec:
      containers:
        - name: question/q-${qId}
          image: "${dockerImage}"
          ports:
            - name: http
              containerPort: ${port}