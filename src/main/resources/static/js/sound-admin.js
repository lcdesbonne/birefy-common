window.onload = function () {
	populateSoundTable(recentSounds);
};

var foundArtists = [];

var playingSoundId;
var currentTidingTarget;

function populateSoundTable(soundList) {
	let tableContents = "";
	
	soundList.forEach(function (sound) {
		tableContents += soundRow.replace(/:id/g,sound.id)
			.replace(/:tidingId/g, sound.tidingId)
			.replace(":title", sound.name)
			.replace(":category", sound.category);
	});
	
	document.getElementById("sound-table").innerHTML = tableContents;
}

function deleteSound(id) {
	showYesNoConfirm("Are you sure you want to delete this sound?", function () {
		$.ajax({
			url: 'sound-admin/delete',
			method: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(id),
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function (result){
			if(result === true) {
				operationCompleteAlert();
				//Populate the page with all paintings
				window.location.reload();
			} else {
				openFailureDialogue();
			}
		});
	});
}

function approveSound(id) {
	$.ajax({
		url: 'sound-admin/approve',
		method: 'POST',
		contentType: 'application/json',
		data: JSON.stringify(id),
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_CSRF']").attr("content")
		}
	}).done(function (result){
		if(result === true) {
			operationCompleteAlert();
			window.location.reload();
		} else {
			openFailureDialogue();
		}
	});
}

function openSearchDialogue() {
	document.getElementById("searchDialogue").style.display = "block";
}

function cancelSearchDialogue() {
	document.getElementById("searchDialogue").style.display = "none";
}

function searchForArtists() {
	let approxTidingId = document.getElementById("tiding-id-search").value;
	
	if(approxTidingId.length < 3) {
		return;
	}
	
	$.ajax({
		url:"/sound/search-artists?name="+approxTidingId,
		type: 'GET',
		success: function(members) {
			foundArtists = members;
			populateMatchingArtists(foundArtists);
		}
	})
}

function populateMatchingArtists(memberList) {
	let matchingArtistList = document.getElementById("artist-list");
	let option = "<option value=\":name\">";
	let options = "";
	memberList.forEach(function (member) {
		options += option.replace(":name", member.tidingId);
	});
	matchingArtistList.innerHTML = options;
}

function openMessageDialogueForArtist(tidingId) {
	document.getElementById("contactMusicianDialogue").style.display = "block";
	currentTidingTarget = tidingId;
}

function searchForSounds() {
	let tidingId = document.getElementById("tiding-id-search").value;
	let category = document.getElementById("category-select").value;
	let title = document.getElementById("image-title-search").value;
	
	category = category == 'ALL' ? null : category;
	
	//Identify member Id from the tidingId
	let memberId = foundArtists.find(function (artist) {
		artist.tidingId == tidingId;
	}).memberId;
	
	if (!category && !memberId && !title) {
		searchAllrecentSounds();
	}
	if (category && !memberId && !title) {
		searchRecentSoundsByCategory(category);
	}
	if (category && memberId && !title) {
		searchRecentSoundsByMemberAndCategory(memberId, category);
	}
	if (category && memberId && title) {
		searchSoundsByMemberTitleCategory(memberId, title, category)
	}
	if (!category && !memberId && title) {
		searchSoundsByTitle(title);
	}
	if (!category && memberId && title) {
		searchSoundsByArtistAndTitle(memberId, title);
	}
	if (!category && memberId && !title) {
		searchSoundsByArtist(memberId);
	}
}

function searchRecentSoundsByCategory(category) {		
	//Load recent instrumentals for the category
	$.ajax({
		url: "/sound/find-recent-sounds?category="+category,
		type: 'GET',
		success: function (results) {
			//Populate the sound table
			recentSounds = results;
			populateSoundTable(recentSounds);
		}
	});
}

function searchRecentSoundsByMemberAndCategory(memberId, category) {
	$.ajax({
		url: "/sound/all-music-by-artist-category?artist="+memberId+"&category="+category,
		type: 'GET',
		success: function (results) {
			recentSounds = results;
			populateSoundTable(recentSounds);
		}
	});
}

function searchAllrecentSounds() {
	$.ajax({
		url: "/sound/all-recent-sounds",
		type: 'GET',
		success: function (results) {
			recentSounds = results;
			populateSoundTable(recentSounds);
		}
	});
}

function searchSoundsByMemberTitleCategory(memberId, title, category) {
	$.ajax({
		url: "/sound/search-by-songName-category-and-artist?songName="+title+"&artist="+memberId+"&category="+category,
		type: 'GET',
		success: function (results) {
			recentSounds = results;
			populateSoundTable(recentSounds);
		}
	});
}

function searchSoundsByTitle(title) {
	$.ajax({
		url: "/sound/sounds-by-title?title="+title,
		type: 'GET',
		success: function (results) {
			recentSounds = results;
			populateSoundTable(recentSounds);
		}
	});
}

function searchSoundsByArtistAndTitle(memberId, title) {
	$.ajax({
		url: "/sound/sounds-by-member-title?memberId="+memberId+"&title="+title,
		type: 'GET',
		success: function (results) {
			recentSounds = results;
			populateSoundTable(recentSounds);
		}
	});
}

function searchSoundsByArtist(memberId) {
	$.ajax({
		url: "/sound/all-music-by-artist?memberId="+memberId,
		type: 'GET',
		success: function (results) {
			recentSounds = results;
			populateSoundTable(recentSounds);
		}
	});
}

function searchAllReviewSounds() {
	$.ajax({
		url: "/sound-admin/review",
		type: 'GET',
		success: function (results) {
			recentSounds = results;
			populateSoundTable(recentSounds);
		}
	});
}

function playTrack(id) {
	let audioPlayer = document.getElementById("audio-player-" + id);
	let srcUrl = soundUrlTemplate.replace(":id", id);
	
	if(playingSoundId && playingSoundId != id) {
		stopTrack(playingSoundId);
	}
	
	if(audioPlayer.src != srcUrl) {
		audioPlayer.src = srcUrl;
		audioPlayer.load();
	}
	
	document.getElementById("audio-player-"+id).play();
	playingSoundId = id;
}

function stopTrack(id) {
	document.getElementById("audio-player-"+id).pause();
}

function closeMessageDialogue() {
	document.getElementById("contactMusicianDialogue").style.display = "none";
}

function sendMessageToMusician() {
	if(currentTidingTarget) {
		let tidingId = currentTidingTarget;
		let socket = new SockJS('/chatsocket');
		let stompClient = Stomp.over(socket);
		
		let saveMessageUrl = "/routing/saveMessage";
		let urlTemplate = "/direct/tidings/:recipient";
		
		let tiding = {
				'type': 'CHAT',
				'content': "",
				'sender': ""
		}
		
		let message = {
				'content': '',
				'creator': '',
				'recipient': ''
		}
		
		stompClient.connect({}, function () {
			let messageContent = document.getElementById("messageContent").innerHTML;
			message.content = messageContent;
			message.creator = userTidingId;
			message.recipient = tidingId;
			
			tiding.content = messageContent;
			tiding.sender = userTidingId;
			
			stompClient.send(urlTemplate.replace(":recipient",tidingId, {}, JSON.stringify(tiding)));
			stompClient.send(saveMessageUrl, {}, JSON.stringify(message));
			
			operationCompleteAlert();
			document.getElementById("messageContent").innerHTML = "";
		});
	}
}

const soundUrlTemplate = applicationDomain + "sound/stream-sound?id=:id";

const soundRow = 
`<tr id="row-:id"><td>
	<div class="form-group">
		<label>Title</label>
		<input id="title-:id" type="text" class="form-control" value=":title" />
	</div>
	<div class="form-group">
		<label>Category</label>
		<input id="category-:id" type="text" class="form-control" value=":category" />
	</div>
	<div class="form-group">
		<button class="btn btn-success" onclick="approveSound(:id)">Approve</button>
		<button class="btn btn-warning" onclick="deleteSound(:id)">Delete</button>
		<button class="btn btn-default" type="button" onclick="openMessageDialogueForArtist(':tidingId')">Contact :tidingId</button>
	</div>
</td>
<td>
	<audio id="audio-player-:id">
		<source src="#" type="audio/wav">
	</audio>
	<button id="play-:id" class="btn btn-info" onclick="playTrack(:id)">Play/Pause</button>
	<button id="stop-:id" class="btn btn-warning" onclick="stopTrack(:id)">Stop</button>
</td>
</tr>
`;
