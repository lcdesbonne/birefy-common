window.onload = function() {
	populateQuestionDetails();
	populateQuestionContributors();
	ceaseLoadingPage();
}

var currentTidingTarget;

function contactDevelopers() {
	document.getElementById("contact-develper-dialogue").style.display = "block";
}

function sendMessageToProgrammer() {
	if(currentTidingTarget) {
		tidingId = currentTidingTarget;
		let socket = new SockJS('/chatsocket');
		let stompClient = Stomp.over(socket);
		
		let saveMessageUrl = "/routing/saveMessage";
		let urlTemplate = "/direct/tidings/:recipient";
		
		let tiding = {
				'type': 'CHAT',
				'content': "",
				'sender': ""
		}
		
		let message = {
				'content': '',
				'creator': '',
				'recipient': ''
		}
		
		stompClient.connect({}, function () {
			let messageContent = document.getElementById("messageContent").innerHTML;
			message.content = messageContent;
			message.creator = userTidingId;
			message.recipient = tidingId;
			
			tiding.content = messageContent;
			tiding.sender = userTidingId;
			
			stompClient.send(urlTemplate.replace(":recipient",tidingId, {}, JSON.stringify(tiding)));
			stompClient.send(saveMessageUrl, {}, JSON.stringify(message));
			
			operationCompleteAlert();
			document.getElementById("messageContent").innerHTML = "";
		});
	}
}

function populateQuestionContributors() {
	if(contributorDetails && userTidingId) {
		let contributors = "";
		let firstOne = true;
		//Cycle through the keys of the contributor details list
		for(let tidingId in contributorDetails) {
			let contDetails = contributorDetails[tidingId].details ? contributorDetails[tidingId].details : "Unspecified";
			
			if(firstOne) {
				contributors += selectedContributorTemplate.replace(/:tidingId/g, tidingId);
				
				if(contributorDetails[tidingId].length) {
					contributors = contributors
						.replace(":skillsList", contributorDetails[tidingId].skillsList.join(", "))
						.replace(":details", contDetails);
				} else {
					contributors = contributors
						.replace(":skillsList", getQuestionSkills())
						.replace(":details", contDetails);
				}
								
				currentTidingTarget = tidingId;
				let contactDeveloperButton = document.getElementById("sendMessage");
				contactDeveloperButton.innerHTML = "Send to " + tidingId;
				firstOne = false;
			} else {
				if(contributorDetails[tidingId].length) {
					contributors += contributorDetailsTemplate.replace(/:tidingId/g, tidingId)
					.replace(":skillsList", contributorDetails[tidingId].skillsList.join(", "))
					.replace(":details", contDetails);
				} else {
					contributors += contributorDetailsTemplate.replace(/:tidingId/g, tidingId)
					.replace(":skillsList", getQuestionSkills())
					.replace(":details", contDetails);
				}
			}
		}
		
		let contributorList = document.getElementById("contributor-list");
		contributorList.innerHTML = contributors;
	}
}

function closeMessageDialogue() {
	document.getElementById("contact-develper-dialogue").style.display = "none";
}

function selectContributorToMessage(tidingId) {
	let contactDeveloperButton = document.getElementById("sendMessage");
	contactDeveloperButton.innerHTML = "Send to " + tidingId;
	let previousTarget = currentTidingTarget;
	
	if(previousTarget) {
		//Remove the selected highlighting
		document.getElementById("contact-"+previousTarget).classList.remove("info");
	}
	
	currentTidingTarget = tidingId;
	document.getElementById("contact-"+currentTidingTarget).setAttribute("class","info");
}

function populateQuestionDetails() {
	let questionDetails = document.getElementById("skillsAndTechnologiesForQuestion");
	
	questionDetails.innerHTML = getQuestionSkills();
}

function getQuestionSkills() {
	let skillsList = "";
	
	for (let index in questionSkills) {
		skillsList += questionSkills[index].tagName;
		skillsList += ", ";
	}
	return skillsList;
}

const contributorDetailsTemplate = 
	`<tr id="contact-:tidingId" onclick="selectContributorToMessage(':tidingId')">
		<td>:tidingId</td>
		<td>:details</td>
		<td>:skillsList</td>
	</tr>`;

const selectedContributorTemplate = 
	`<tr id="contact-:tidingId" class="info" onclick="selectContributorToMessage(':tidingId')">
		<td>:tidingId</td>
		<td>:details</td>
		<td>:skillsList</td>
	</tr>
	`;
