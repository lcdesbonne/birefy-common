var stompClient = null;

var selectedBulletinId;
var bulletinViewIndex = 0;
var selectedConversation;
var selectedConversationIsGroup;

//To be filled with the tiding ids for the new group
var newGroupList = new Set();

var skillsForNewBulletin = [];

//To be filled with updates to any bulletins
var updatedSkillsMap = {};

//To be filled with updated bulletins
var updatedBulletins = [];

var userBulletins = [];
var userBulletinTechTags = {};

var foundContacts = [];

window.onload = function () {
	ceaseLoadingPage();
	
	//Populate the bulletin board
	populateBulletinBoard();
	
	//Populate all the contacts
	populateContacts();
	if(recentConversations.length) {
		populateConversation(recentConversations[0].tidings);
	}
	
	connectToTidings();
	
	populateTechnicalSkillsList();
};

function populateBulletinBoard() {
	var bulletinBoard = $("#bulletinBoard");
	var content = "";
	if (!bulletins.length) {
		//Add the default content
		content += defaultBulletinTemplate;
	} else {
		var bulletin = bulletins[bulletinViewIndex];
		var techList = bulletinAndTechTags[bulletin.id];
		content += bulletinTemplate.replace(/:bulletinId/g, bulletin.id)
		.replace(":description", bulletin.description)
		.replace(":date", bulletin.date)
		.replace(":skillsList", techList.join());
	}		
	
	//Add the content to the table
	bulletinBoard.html(content);
}

function nextBulletin() {
	if (bulletinViewIndex == (bulletins.length - 1)) {
		//Reset the index to 0
		bulletinViewIndex = 0;
	} else {
		bulletinViewIndex += 1;
	}
	populateBulletinBoard();
}

function previousBulletin() {
	if (bulletinViewIndex == 0) {
		//Set the index to the length of the bulletins
		bulletinViewIndex = (bulletins.length - 1);
	} else {
		bulletinViewIndex -= 1;
	}
	populateBulletinBoard();
}

function populateTechnicalSkillsList() {
	let optionTemplate = "<option>:value</option>";
	let techListContent = "";
	technicalSkills.forEach(function (skill) {
		techListContent += optionTemplate.replace(":value", skill.name);
	});
	
	document.getElementById("technicalSkillsList").innerHTML = techListContent;
}

function contactBulletinCreator() {
	if(!selectedBulletinId) {
		return;
	}
	
	//Get the channel for the owner of the bulletin
	$.ajax({
		url: '/tidings/channelForBulletin?bulletinId='+selectedBulletinId,
		type: 'GET',
		success: function (data) {
			if (data.length) {
				for (var i = 0; i < data.length; i++) {
					let convo = {};
					convo.withTidingUser = data[i];
					convo.group = false;
					addToTopOfContacts(convo);
				}
								
				openGeneralInfoDialogue("The creator of this bulletin is: "+data[0].tidingId +"\n"
						+"They have been added to your contacts list.");
			} else {
				openFailureDialogue("Failed to retrieve contact information for this bulletin");
			}
		}
	});
}

function openTidings() {
	var contact = $("#findContact").val();
	
	if (!contact) {
		return;
	}
	
	//Check if this conversation is already loaded
	var conversationLoaded = false;
	
	for (var x = 0; x < recentConversations.length; x++) {
		if (contact == recentConversations[x].withTidingUser.tidingId) {
			selectedConversation = contact;
			selectedConversationIsGroup = recentConversations[x].group;
			if (recentConversations[x].hasOwnProperty('removed')) {
				recentConversations[x].removed = undefined;
			}
			populateContacts();
			populateConversation(recentConversations[x].tidings);
			conversationLoaded = true;
		}
	}
	
	if (!conversationLoaded) {
		let contactChannelId = foundContacts.find(function (foundContact) {
			return foundContact.tidingId == contact;
		}).channelId;
		
		$.ajax({
			url: '/tidings/openConversation?tidingId='+currentUserTidingId.channelId+'&receiverId='+contactChannelId,
			type: 'GET',
			success: function (conversation) {
				if (conversation) {
					recentConversations.push(conversation);
					addToTopOfContacts(conversation);
				}
			}
		});
	}
	
	$("#findContact").val("");
}

function searchForContacts() {
	var tidingId = $("#findContact").val();
	
	if (tidingId.length < 3) {
		return;
	}
	
	$.ajax({
		url: '/tidings/searchUsers?tidingId='+tidingId,
		type: 'GET',
		success: function (data) {
			populateContactSearchList(data);
		}
	});
}

function populateContactSearchList(contactList) {
	var optionTemplate = "<option value=\":value\">";
	
	var optionList = "";
	
	for (var x = 0; x < contactList.length; x++) {
		optionList += optionTemplate.replace(":value", contactList[x].tidingId);
	}
	
	$("#contactList").html(optionList);
	
	//Assign the results to the found contacts
	foundContacts = contactList;
}


function populateContacts() {
	var foundContacts = "";
	var firstOne = true;
	
	for(var i = 0; i < recentConversations.length; i++) {
		if (recentConversations[i].removed) {
			continue;
		}
		
		let contact = recentConversations[i].withTidingUser.tidingId;
		let hasNewMessages = recentConversations[i].newMessages;
		let isGroup = recentConversations[i].group;
		
		if (!selectedConversation) {
			selectedConversation = contact;
		}
		
		if (firstOne) {
			//Identify the selected contact and add it first
			for (var g = 0; g < recentConversations.length; g++) {
				if(recentConversations[g].removed) {
					continue;
				}
														
				let selContact = recentConversations[g].withTidingUser;
				let selIsGroup = recentConversations[g].group;
				let selContactType = selIsGroup ? "G" : "P";
				selectedConversationIsGroup = selIsGroup;
				let selectedConversationActionDropdownDisplay = selIsGroup ? "inherit" : "none";
				
				if(recentConversations[g].withTidingUser.tidingId == selectedConversation) {
					foundContacts += contactSelectedTemplate.replace(":contactType", selContactType)
					.replace(/:contactId/g, selContact.tidingId).replace(":display", selectedConversationActionDropdownDisplay);
					break;
				}
			}
			firstOne = false;
		}
		
		let contactType = isGroup ? "G" : "P";
		let conversationActionDropdownDisplay = isGroup ? "inherit" : "none";
		
		if (contact != selectedConversation) {
			if(!hasNewMessages) {
				foundContacts += contactTemplate.replace(":contactType", contactType)
				.replace(/:contactId/g, contact).replace(":display", conversationActionDropdownDisplay);
			} else {
				foundContacts += newMessageContactTemplate.replace(":contactType", contactType)
				.replace(/:contactId/g, contact).replace(":display", conversationActionDropdownDisplay);
			}
		}
	}
	
	if (!foundContacts) {
		document.getElementById('contacts').innerHTML = defaultContactsTemplate;
	} else {
		document.getElementById('contacts').innerHTML = foundContacts;	
	}	
}

function addToTopOfContacts(conversation) {
	var contactType = conversation.isGroup ? "G" : "P";
	let conversationActionDropdownDisplay = conversation.isGroup ? "inherit" : "none";
	
	var contactToAdd = contactSelectedTemplate.replace(":contactType", contactType)
	.replace(/:contactId/g, conversation.withTidingUser.tidingId).replace(":display", conversationActionDropdownDisplay);
	
	if(!selectedConversation) {
		//This is the first conversation
		selectedConversation = conversation.withTidingUser.tidingId;
		selectedConversationIsGroup = conversation.group;
		$("#contacts").html(contactToAdd);
	} else {
		//Change the id and class of the previously selected contact
		var previouslySelectedContact = document.getElementById(selectedConversation);
		previouslySelectedContact.classList.remove("birefy-fixed-view-selected");
		selectedConversation = conversation.withTidingUser.tidingId;
		selectedConversationIsGroup = conversation.group;
		
		$("#contacts").prepend(contactToAdd);
	}
	
	populateConversation(conversation.tidings);
}

function removeContactFromList(tidingId) {
	selectedConversation = null;
	
	for(var i = 0; i < recentConversations.length; i++) {
		if(recentConversations[i].withTidingUser.tidingId == tidingId) {
			recentConversations[i].removed = true;
		}
	}
	
	populateContacts();
	
	let convolist = recentConversations.find(function (convo) {
		return convo.withTidingUser.tidingId == selectedConversation;
	}).tidings;
		
	populateConversation(convolist);
}

function selectContactFromList(tidingId) {

	selectedConversation = tidingId;
	
	for(var i = 0; i < recentConversations.length; i++) {
		if(recentConversations[i].withTidingUser.tidingId == tidingId) {
			selectedConversationIsGroup = recentConversations[i].group;
			
			if (recentConversations[i].hasOwnProperty('removed')) {
				recentConversations[i].removed = undefined;
			}
			
			if(recentConversations[i].newMessages) {
				recentConversations[i].newMessages = false;
			}
			
			populateContacts();
			populateConversation(recentConversations[i].tidings);
			break;
		}
	}
}


function populateConversation(tidingsList) {
	if(!tidingsList || !tidingsList.length) {
		//Add the default conversation list
		$("#correspondence").html(defaultCorrespondenceTemplate);
	} else {
		var messageList = "";
		for (var i = 0; i < tidingsList.length; i++) {
			var sender = tidingsList[i].sender.tidingId;
			var content = tidingsList[i].content;
			
			
			messageList += correspondenceTemplate.replace(":creator", sender)
			.replace(":content", content);
			
			//TODO add more styling here
		}
		
		$("#correspondence").html(messageList);
	}
}

function setConnected(connected) {
	$("#connect").prop("disabled", connected);
	$("#disconnect").prop("disabled", !connected);
	
	if(connected) {
		$("#conversation").show();
	} else {
		$("#conversation").hide();
	}
	$("#tidings").html("");
}

function connectToTidings() {
	var socket = new SockJS('/chatsocket');
	stompClient = Stomp.over(socket);
	stompClient.connect({}, function (frame) {
		setConnected(true);
		stompClient.subscribe('/direct/tidings/'+currentUserTidingId.tidingId, messageReceivedAction);
		
		//Subscribe to any groups present in the users contacts
		var groupConversations = recentConversations.filter(convo => convo.group);
		groupConversations.forEach(function (convo) {
			stompClient.subscribe('/direct/tidings/'+convo.withTidingUser.tidingId, messageReceivedAction);
		});
	});
}

var messageReceivedAction = function (tiding) {
	let parsedTiding = JSON.parse(tiding.body);
	let destination = tiding.headers.destination;
	let tidingChannel = destination.substring(destination.lastIndexOf('/') + 1);
	
	addToConversationThread(parsedTiding, tidingChannel);
	
	if(tidingChannel == selectedConversation) {
		//Add to the conversation in view
		for (let x = 0; x < recentConversations.length; x++) {
			if (recentConversations[x].withTidingUser.tidingId == selectedConversation) {
				populateConversation(recentConversations[x].tidings);
				break;
			}
		}
	}
}

function addToConversationThread(tiding, tidingChannelId) {
	let hasNoMessages = false;
	
	//Add the message to the conversation
	for(var x = 0; x < recentConversations.length; x++) {
		if (tiding.sender.tidingId == recentConversations[x].withTidingUser.tidingId 
				|| tidingChannelId == recentConversations[x].withTidingUser.tidingId) {
			recentConversations[x].tidings.unshift(tiding);
			hasNoMessages = recentConversations[x].tidings.length == 0;
			
			if(tiding.sender.tidingId != selectedConversation && 
					tidingChannelId != selectedConversation) {
				
				recentConversations[x].newMessages = true;
			}
		}
	}
		
	if (hasNoMessages) {	
		//TODO perhaps add a wait function here
		//Find out if the channel is a group
		var channelToVerify = tidingChannelId ? tidingChannelId : tiding.sender.tidingId;
		$.ajax({
			url: '/tidings/conversationIsGroup?tidingId='+channelToVerify,
			type: 'GET',
			success: function (result) {
				let convoStub = {
						'withTidingUser':channelToVerify,
						'tidings': [tiding],
						'isGroup': result,
						'newMessages': true
				};
				
				recentConversations.push(convoStub);
			}
		});
	} 
}

function selectBulletin(bulletinId) {
	if(selectedBulletinId === bulletinId) {
		var bulletin = $('#'+bulletinId);
		//Remove the selection of the bulletin
		bulletin.removeClass("selectedBulletin");
		selectedBulletinId = null;
		$('#linkBulletinAuthor').prop('disabled', true);
	} else {
		selectedBulletinId = bulletinId;
		//Modify the colour of the bulletin
		var bulletin = $('#'+bulletinId);
		bulletin.addClass("selectedBulletin");
		$('#linkBulletinAuthor').prop('disabled', false);
	}
}

function disconnectTidings() {
	if(stompClient !== null) {
		stompClient.disconnect();
	}
	setConnected(false);
	console.log("Disconnected");
}

function sendDirectMessage() {
	var urlTemplate = "/direct/tidings/:recipient";
	var saveMessageUrl = "/routing/saveMessage";
	
	let currentTargetConversation = recentConversations.find(function(convo) {
		return convo.withTidingUser.tidingId == selectedConversation;
	});
	
	var tiding = {
		'type': 'CHAT',
		'content': "",
		'sender': ""
	};
	
	//Matches the format used to persist messages
	var message = {
		'content': '',
		'creator': '',
		'recipient': ''
	};
	message.content = $("#myTiding").val();
	message.creator = currentUserTidingId.channelId;
	message.recipient = currentTargetConversation.withTidingUser.channelId;
	
	tiding.content = $("#myTiding").val();
	tiding.sender = currentUserTidingId;
	
	stompClient.send(urlTemplate.replace(":recipient",selectedConversation),{}, JSON.stringify(tiding));
	stompClient.send(saveMessageUrl, {}, JSON.stringify(message));
	
	if(!selectedConversationIsGroup) {
		addToConversationThread(tiding, selectedConversation);
		populateConversation(currentTargetConversation.tidings);
	}
	
	$("#myTiding").val("");
}

//Create Group functionality 
function openCreateGroupDialogue() {
	addToGroupList();
	//Show the dialogue
	document.getElementById("edit-group-overlay").style.display = "block";
}

function closeCreateGroupDialogue() {
	//Clear the members from the group creation
	newGroupList.clear();
	document.getElementById("edit-group-overlay").style.display = "none";
}

function searchForGroupContacts() {
	var tidingId = $("#groupCreateSearch").val();
	
	if (tidingId.length < 3) {
		return;
	}
	
	$.ajax({
		url: '/tidings/searchUsers?tidingId='+tidingId,
		type: 'GET',
		success: function (data) {
			if(data.length) {
				data.forEach(function (contact) {
					foundContacts.push(contact);
				});
			}
			populateGroupContactSearchList(data);
		}
	});
}

function populateGroupContactSearchList(contactList) {
	var optionTemplate = "<option value=\":value\">";
	
	var optionList = "";
	
	for (var x = 0; x < contactList.length; x++) {
		optionList += optionTemplate.replace(":value", contactList[x].tidingId);
	}
	
	$("#groupContactsSelect").html(optionList);
}

function removeFromGroup(tidingId) {
	newGroupList.delete(tidingId);
	addToGroupList();
}

function addToGroupList() {
	let groupTidingMember = $("#groupCreateSearch").val().trim();
	
	if (groupTidingMember) {
		//Add the member to the group list
		newGroupList.add(groupTidingMember);	
	}
	
	let groupListAsArray = Array.from(newGroupList);
	
	let newRow = "<div class=\"row\">";
	let endRow = "</div>";
	let emptyColumn = "<div class=\"col-sm-4\"></div>";
	
	let groupListContent = "" + newRow;
	//Populate the display with the selected members
	for (let x = 0; x < groupListAsArray.length; x++) {
		if ((x % 3 == 0 || x == 0) && (x != (groupListAsArray.length - 1))) {
			//Begin a new row
			groupListContent += newRow;
			groupListContent += selectedGroupMemberTemplate.replace(/:tidingId/g, groupListAsArray[x]);
		} else if (x % 3 == 2 && (x != (groupListAsArray.length - 1))) {
			groupListContent += selectedGroupMemberTemplate.replace(/:tidingId/g, groupListAsArray[x]);
			//End the row
			groupListContent += endRow;
		} else if (x == (groupListAsArray.length - 1)) {
			groupListContent += selectedGroupMemberTemplate.replace(/:tidingId/g, groupListAsArray[x]);
			//Populate empty columns
			for (let y = 1; y < (3 - x % 3); y++) {
				groupListContent += emptyColumn;
			}
			//Close the row
			groupListContent += endRow;
		} else {
			groupListContent += selectedGroupMemberTemplate.replace(/:tidingId/g, groupListAsArray[x]);
		}
	}
	var defaultGroupListMessage = "Added members here ...";
	groupListContent = newGroupList.size === 0 ? defaultGroupListMessage : groupListContent;
	$("#selectedGroupMembers").html(groupListContent);
	
	//Remove the value from the search box
	document.getElementById("groupCreateSearch").value = "";
}

function validateAndCreateGroup() {
	//Check to see if the group name is valid
	let newGroupName = $("#newGroupName").val();
	
	$.ajax({
		url: '/tidings/checkTidingIdExists?tidingIds='+newGroupName,
		type: 'GET',
		success: function (data) {
			if (data[newGroupName] === false) {
				//Send a message to all the group participants
				createGroup(newGroupName);
				
			} else {
				openGeneralInfoDialogue("Group Name already exists please choose another.");
			}
		}
	});	
}

function createGroup(newGroupName) {
	$.ajax({
		url: '/tidings/createGroup',
		contentType: 'application/json',
		method: 'PUT',
		data: newGroupName,
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
		}
	}).done(function (successful) {
		if (successful.success === true) {
			initializeGroup(successful.tidingId);
			//Close the modal
			closeCreateGroupDialogue();
		} else {
			openFailureDialogue("Error creating new group. Please try again.");
		}
	});
}

function initializeGroup(groupName) {
	var urlTemplate = "/direct/tidings/:recipient";
	var saveMessageUrl = "/routing/saveMessage";
	var tiding = {
			'type': 'JOIN',
			'content': "Joined group.",
			'sender': ""
		};
	
	var message = {
			'content': 'Joined group',
			'creator': '',
			'recipient': ''
		};
	
	//Send join message to each recipient
	for (var x = 0; x < newGroupList.length; x++) {
		message.creator = groupName.channelId;
		message.recipient = foundContacts.find(function (contact) {
			return contact.tidingId == newGroupList[x];
		}).channelId;
		
		tiding.sender = groupName;
		
		stompClient.send(urlTemplate.replace(":recipient",newGroupList[x]),{}, JSON.stringify(tiding));
		stompClient.send(saveMessageUrl, {}, JSON.stringify(message));
	};
	
	message.creator = groupName.channelId;
	message.recipient = currentUserTidingId.channelId;
	
	tiding.sender = groupName;
	//Send a join message to the creator of the group also
	stompClient.send(urlTemplate.replace(":recipient", currentUserTidingId.tidingId),{}, JSON.stringify(tiding));
	stompClient.send(saveMessageUrl, {}, JSON.stringify(message));
	
	//Reset the message subscriptions
	connectToTidings();
}


/*
 * Bulletin creation functions
 */

function openCreateBulletinDialogue() {
	document.getElementById("create-bulletin-overlay").style.display = "block";
}

function closeCreateBulletinDialogue() {
	//Remove all bulletin skills listed.
	skillsForNewBulletin.length = 0;
	
	document.getElementById("create-bulletin-overlay").style.display = "none";
}

function createBulletin() {
	//Retrieve the details for the bulletin
	let bulletinDetails = document.getElementById("newBulletinDetails").value;
	//Retrieve the required skills for the bulletin
	skillsForNewBulletin;
	//Retrieve the targeting information
	let targeted  = document.getElementById("targeted").checked;
	
	//Save the bulletin
	let newBulletin = {
			"description": bulletinDetails,
			"targeted": targeted,
			"technicalTags": skillsForNewBulletin
	}
	
	$.ajax({
		url: '/tidings/saveNewBulletin',
		contentType: 'application/json',
		method: 'POST',
		data: JSON.stringify(newBulletin),
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
		}
	}).done(function (successful) {
		if (successful === true) {
			operationCompleteAlert(closeDialogueAndReload);
		} else {
			openFailureDialogue("Error creating new bulletin. Please try again.");
		}
	});
}

var closeDialogueAndReload  = function () {
	closeCreateBulletinDialogue();
	window.location.reload();
}


function removeSkillFromBulletin(techSkillName) {
	for (let x = 0; x < skillsForNewBulletin.length; x++) {
		if (skillsForNewBulletin[x].name == techSkillName) {
			skillsForNewBulletin.splice(x, 1);
			break;
		}
	}
	visualiseBulletinTechnicalSkillsList();
}

function addSkillToBulletin() {
	let skillName = $("#technicalSkillsSearch").val();
	
	//Identify if the skill exists already
	
	var existingTag = technicalSkills.find(function (skill) {
		return skill.name === skillName;
	});
	
	if (existingTag) {
		//Add the existing tag to the tech for bulletin list
		skillsForNewBulletin.push(existingTag);
	} else {
		let techTag = {
				"id": null,
				"name": skillName
		}
		
		skillsForNewBulletin.push(techTag);
	}
	
	visualiseBulletinTechnicalSkillsList();
	
	//Clear the dialogue box
	$("#technicalSkillsSearch").val("");
}

function visualiseBulletinTechnicalSkillsList() {
	
	let newRow = "<div class=\"row\">";
	let endRow = "</div>";
	let emptyColumn = "<div class=\"col-sm-4\"></div>";
	
	let techListContent = "" + newRow;
	//Populate the display with the selected members
	for (let x = 0; x < skillsForNewBulletin.length; x++) {
		if ((x % 3 == 0 || x == 0) && (x != (skillsForNewBulletin.length - 1))) {
			//Begin a new row
			techListContent += newRow;
			techListContent += selectedTechSkillTemplate.replace(/:techSkillName/g, skillsForNewBulletin[x].name);
		} else if (x % 3 == 2 && (x != (skillsForNewBulletin.length - 1))) {
			techListContent += selectedTechSkillTemplate.replace(/:techSkillName/g, skillsForNewBulletin[x].name);
			//End the row
			techListContent += endRow;
		} else if (x == (skillsForNewBulletin.length - 1)) {
			techListContent += selectedTechSkillTemplate.replace(/:techSkillName/g, skillsForNewBulletin[x].name);
			//Populate empty columns
			for (let y = 1; y < (3 - x % 3); y++) {
				techListContent += emptyColumn;
			}
			//Close the row
			techListContent += endRow;
		} else {
			techListContent += selectedTechSkillTemplate.replace(/:techSkillName/g, skillsForNewBulletin[x].name);
		}
	}
	var defaultSkillsListMessage = "List of desired skills displayed here...";
	techListContent = skillsForNewBulletin.length === 0 ? defaultSkillsListMessage : techListContent;
	$("#skillsForBulletin").html(techListContent);
	
	//Remove the value from the search box
	document.getElementById("technicalSkillsSearch").value = "";
}

function visualiseUpdateBulletinTechnicalSkillsList(bulletinId, skillsList) {
	let newRow = "<div class=\"row\">";
	let endRow = "</div>";
	let emptyColumn = "<div class=\"col-sm-6\"></div>";
	
	let techListContent = "" + newRow;
	//Populate the display with the selected members
	for (let x = 0; x < skillsList.length; x++) {
		if ((x % 2 == 0 || x == 0) && (x != (skillsList.length - 1))) {
			//Begin a new row
			techListContent += newRow;
			techListContent += selectedUpdatedSkillTemplate.replace(/:techSkillName/g, skillsList[x])
			.replace(":bulletinId",bulletinId);
		} else if (x % 2 == 1 && (x != (skillsList.length - 1))) {
			techListContent += selectedUpdatedSkillTemplate.replace(/:techSkillName/g, skillsList[x])
			.replace(":bulletinId",bulletinId);
			//End the row
			techListContent += endRow;
		} else if (x == (skillsList.length - 1)) {
			techListContent += selectedUpdatedSkillTemplate.replace(/:techSkillName/g, skillsList[x])
			.replace(":bulletinId",bulletinId);
			//Populate empty columns
			for (let y = 1; y < (2 - x % 2); y++) {
				techListContent += emptyColumn;
			}
			//Close the row
			techListContent += endRow;
		} else {
			techListContent += selectedUpdatedSkillTemplate.replace(/:techSkillName/g, skillsList[x].name)
			.replace(":bulletinId",bulletinId);
		}
	}
	var defaultSkillsListMessage = "No skills identified for this bulletin";
	techListContent = skillsList.length === 0 ? defaultSkillsListMessage : techListContent;
	$("#" + bulletinId + "-updatedSkillsForBulletin").html(techListContent);
	
	//Remove the value from the search box
	document.getElementById(bulletinId + "-updateTechnicalSkillsSearch").value = "";
}

//Update bulletin functions
function viewUserBulletins() {
	//Populate the table with the bulletins created by the user
	$.ajax({
		url: 'tidings/bulletinsByUser',
		type: 'GET',
		success: function (bulletins) {
			userBulletins = bulletins.bulletins;
			userBulletinTechTags = bulletins.techTags;
			
			//Populate the user bulletin table
			populateUserBulletinsTable(userBulletins, userBulletinTechTags);
			
			document.getElementById("view-user-bulletins-overlay").style.display = "block";
		}
	});
}

function populateUserBulletinsTable(userBulletins, techTagMap) {
	if (userBulletins.length) {
		let tableContent = "";
		userBulletins.forEach(function (userBulletin) {
			tableContent += viewCreatedBulletinTemplate.replace(":creationDate", formatLocalDateTime(userBulletin.creationDate))
			.replace(/:bulletinId/g, userBulletin.id)
			.replace(":detail", userBulletin.description)
			.replace(":skills", techTagMap[userBulletin.id].join())
			.replace(":targeted", userBulletin.targeted)
			.replace(":open", userBulletin.open)
		});
		
		document.getElementById("user-bulletins").innerHTML = tableContent;
	} else {
		document.getElementById("user-bulletins").innerHTML = noBulletinsMade;
	}
}

function updateBulletin(bulletinId) {
	let bulletin = {
			id: '',
			description: '',
			open: '',
			targeted: '',
			date: ''
	}
	
	//Retrieve all the values for update
	bulletin.id = bulletinId;
	bulletin.description = document.getElementById(bulletinId + "-details").value;
	bulletin.open = document.getElementById(bulletinId + "-open").innerHTML == 'true' ? true : false;
	bulletin.targeted = document.getElementById(bulletinId + "-targeted").innerHTML == 'true' ? true: false;
	
	$.ajax({
		url: '/tidings/updateBulletin',
		contentType: 'application/json',
		method: 'POST',
		data: JSON.stringify(bulletin),
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
		}
	}).done(function (result) {
		if (result) {
			var updatedBulletin = userBulletins.find(function (b) {
				return b.id == result.id;
			});
			updatedBulletin.description = document.getElementById(bulletinId + "-details").value;
			updatedBulletin.open = result.open;
			updatedBulletin.targeted = result.targeted;
			updatedBulletin.creationDate = formatLocalDateTime(result.creationDate);
			
			openGeneralInfoDialogue("Successfully updated bulletin.")
		} else {
			openFailureDialogue("Error updating bulletin. Please try again.");
		}
	});
}

function revertChangesToBulletin(bulletinId) {
	//Revert any skills modification
	cancelChangesToSkills(bulletinId);
	
	let revertingBulletin = userBulletins.find(function (b) {
		return b.id == bulletinId;
	});
	
	//Reset the details
	document.getElementById(bulletinId + "-details").value = revertingBulletin.description;
	
	//Reset targeted
	document.getElementById(bulletinId + "-targeted").innerHTML = revertingBulletin.targeted;
	
	//Reset open
	document.getElementById(bulletinId + "-open").innerHTML = revertingBulletin.open;
}

function toggleTargeted(bulletinId) {
	let targetedElement = document.getElementById(bulletinId+"-targeted");
	
	if(targetedElement.innerHTML == 'true') {
		targetedElement.innerHTML = false;
	} else {
		targetedElement.innerHTML = true;
	}
}

function toggleOpen(bulletinId) {
	let openElement = document.getElementById(bulletinId+"-open");
	
	if(openElement.innerHTML == 'true') {
		openElement.innerHTML = false;
	} else {
		openElement.innerHTML = true;
	}
}

var updateBulletinSkills = function (bulletinId) {
	//Launch the skills template
	let skillsLocationId = bulletinId + "-skills";
	let idForSkillsDisplay = bulletinId + "-updatedSkillsForBulletin";
	
	let skillsEdit = updateBulletinSkillsTemplate.replace(/:bulletinId/g, bulletinId);
	
	let skillsElement = document.getElementById(skillsLocationId);
	//Embed the update template
	skillsElement.innerHTML = skillsEdit;
	
	//Check for the presence of the bulletin Id in the updated map
	if (updatedSkillsMap.hasOwnProperty(bulletinId)) {
		let updatedSkillsList = updatedSkillsMap[bulletinId];
		
		visualiseUpdateBulletinTechnicalSkillsList(bulletinId, updatedSkillsList)
	} else {
		visualiseUpdateBulletinTechnicalSkillsList(bulletinId, userBulletinTechTags[bulletinId]);
	}
	
}

function addSkillToUpdatedBulletin(bulletinId) {
	let addedSkill = $("#"+bulletinId+"-updateTechnicalSkillsSearch").val();
	
	//Add to the updated map
	if (!updatedSkillsMap.hasOwnProperty(bulletinId)) {
		//Copy the value from the existing techtag map
		updatedSkillsMap[bulletinId] = userBulletinTechTags[bulletinId].slice();
	}
	
	updatedSkillsMap[bulletinId].push(addedSkill);
	
	visualiseUpdateBulletinTechnicalSkillsList(bulletinId, updatedSkillsMap[bulletinId]);
}

function changeSkillsUpdatedBulletin(bulletinId) {
	//Close the edit skills dialogue
	let skillList = "";
	
	if(updatedSkillsMap.hasOwnProperty(bulletinId)) {
		skillList = updatedSkillsMap[bulletinId].join();
	} else {
		skillList = userBulletinTechTags[bulletinId].join();
	}
	
	let skillsLocationId = bulletinId + "-skills";
	document.getElementById(skillsLocationId).innerHTML = "<p onclick=\"updateBulletinSkills(:bulletinId)\">:skills</p></td>".replace(":skills",skillList)
	.replace(":bulletinId", bulletinId);
}

function leaveGroup(groupId) {
	showYesNoConfirm("Are you sure you want to leave this group?", function (){
		//Send leave message
		
		var urlTemplate = "/direct/tidings/:recipient";
		var saveMessageUrl = "/routing/saveMessage";
		var tiding = {
				'type': 'LEAVE',
				'content': "Left group.",
				'sender': ""
			};
		
		var message = {
				'content': 'Left group',
				'creator': '',
				'recipient': ''
			};
		
		//Send join message to each recipient
		message.creator = currentUserTidingId;
		message.recipient = groupId;
		
		tiding.sender = currentUserTidingId;
		
		stompClient.send(urlTemplate.replace(":recipient",groupId),{}, JSON.stringify(tiding));
		stompClient.send(saveMessageUrl, {}, JSON.stringify(message));
		
		
		$.ajax({
			url: '/tidings/leaveGroup',
			contentType: 'application/json',
			method: 'PUT',
			data: groupId,
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function (result) {
			if (result === true) {
				//Reload the entire page
				location.reload();
			} else {
				openFailureDialogue("Error executing the procedure please try again.");
			}
		});
	});
}

function cancelChangesToSkills(bulletinId) {
	let bulletin = userBulletins.find(function (b) {
		return b.id == bulletinId;
	});
	
	//Get the original skills list
	let originalSkills = userBulletinTechTags[bulletinId].join();
	
	//Remove all updates for skills for this bulletin
	delete updatedSkillsMap[bulletinId];
	
	let skillsLocationId = bulletinId + "-skills";
	
	document.getElementById(skillsLocationId).innerHTML = "<p onclick=\"updateBulletinSkills(:bulletinId)\">:skills</p></td>".replace(":skills",originalSkills)
	.replace(":bulletinId", bulletinId);
}

function closeUpdateBulletins() {
	document.getElementById("view-user-bulletins-overlay").style.display = "none";
}

function removeSkillFromUpdatedBulletin(bulletinId, skillName) {
	//Check for the bulletin in the updated map
	if (!updatedSkillsMap.hasOwnProperty(bulletinId)) {
		//Copy the value from the existing skills map
		updatedSkillsMap[bulletinId] = userBulletinTechTags[bulletinId].slice();
	}
	let updatedTech = updatedSkillsMap[bulletinId];
	updatedTech.splice(updatedTech.indexOf(skillName),1);
	updatedSkillsMap[bulletinId] = updatedTech;
	
	//Redraw the tag list
	visualiseUpdateBulletinTechnicalSkillsList(bulletinId, updatedSkillsMap[bulletinId]);
}

/*
 * Colour luminance adjustment
 * To increase luminance by 10% use 0.1
 * To decrease luminance by 10% use -0.1
 */
function ColourLuminance(hexa, lum) {
	let hex = String(hexa).replace(/[^0-9a-f]/gi,'');
	
	if(hex.length < 6) {
		hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
	}
	
	lum = lum || 0;
	
	let rgb = "#";
	let c;
	let i;
	
	for(i = 0; i < 3; i++) {
		c = parseInt(hex.substr(i*2,2), 16);
		c = Math.round(Math.min(Math.max(0,c + (c*lum)),255)).toString(c.length);
		rgb += ("00"+c).substr(c.length);
	}
	
	return rgb;
}

//Apply the correct init state for the contact bulletin creator button
(function() {
	if (!selectedBulletinId) {
		$('#linkBulletinAuthor').prop('disabled', true);
	}
})();

//Templates
/*
 * bulletin id is represented by :bulletinId
 * date is represented by :date
 * The description of the bulletin is in the place of :description
 * The tag list needs to be applied to the inner html of the div element
 * with id taglist:bulletinId
 */
const bulletinTemplate = 
`<div class="row text-center" id=":bulletinId" onclick="selectBulletin(':bulletinId')" style="cursor: pointer">
	<p><i>:description</i></p>
	<div class="col-md-6">
		<label>Created<p><i>:date</i></p></label>
	</div>
	<div class="col-md-6">
		<label for="tagList:bulletinId">Skills</label>
		<div id="taglist:bulletinId">:skillsList</div>
	</div>
</div>`;

/*
 * To replace the colour of the row replace colorValue with the correct
 * hash value.
 * To style the sender and receiver names replace :creatorClass
 * To add the name of the creator replace :creator
 * To add the message content replace :content
 */
const correspondenceTemplate = 
`<tr style="color: colorValue">
	<td><p class=":initiatorClass">:creator</p></td>
	<td>:content</td>
</tr>`;

const defaultCorrespondenceTemplate = "<tr><td colspan=\"2\">No new tidings..</td></tr>";

const defaultBulletinTemplate = "<tr><td>There are currently no bulletins</td></tr>";
const defaultContactsTemplate = "<tr><td>No recent contacts...</td></tr>";

/*
 * Contact contains contact type :contactType
 * Contact Id :contactId
 */
const contactSelectedTemplate = 
`<tr id=":contactId" class="birefy-fixed-view-selected">
	<td>:contactType</td>
	<td style="cursor: pointer" onclick="selectContactFromList(':contactId')">:contactId</td>
	<td><button class="btn btn-default" onclick="removeContactFromList(':contactId')">Remove</button></td>
	<td><div class="dropdown" style="display: :display">
			<button class="caretButton" type="button" id=":contactId-contact-context-menu" 
					data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<span class="caret"></span>
			</button>
		 	<div class="dropdown-menu shift-left-100" aria-lebelledby=":contactId-contact-context-menu">
    			<button class="dropdown-item dropDownButton" type="button" onclick="leaveGroup(':contactId')">Leave Group</button>
			</div>
		</div>
	</td>
</tr>`;

const contactTemplate = 
`<tr id=":contactId">
	<td>:contactType</td>
	<td style="cursor: pointer" onclick="selectContactFromList(':contactId')">:contactId</td>
	<td><button class="btn btn-default" onclick="removeContactFromList(':contactId')">Remove</button></td>
	<td><div class="dropdown" style="display: :display">
			<button class="caretButton" type="button" id=":contactId-contact-context-menu" 
					data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<span class="caret"></span>
			</button>
		 	<div class="dropdown-menu" aria-lebelledby=":contactId-contact-context-menu">
    			<button class="dropdown-item dropDownButton" type="button" onclick="leaveGroup(':contactId')">Leave Group</button>
			</div>
		</div>
	</td>
 </tr>`;

const newMessageContactTemplate = 
`<tr id=":contactId" class="warning">
	<td>:contactType</td>
	<td onclick="selectContactFromList(':contactId')">:contactId</td>
	<td><button class="btn btn-default" onclick="removeContactFromList(':contactId')">Remove</button></td>
	<td><div class="dropdown" style="display: :display">
			<button class="caretButton" type="button" id=":contactId-contact-context-menu" 
					data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<span class="caret"></span>
			</button>
		 	<div class="dropdown-menu" aria-lebelledby=":contactId-contact-context-menu">
    			<button class="dropdown-item dropDownButton" type="button" onclick="leaveGroup(':contactId')">Leave Group</button>
			</div>
		</div>
	</td>
 </tr>`;

const selectedGroupMemberTemplate = 
`<div class="col-sm-4">
	<label>
		:tidingId
		<button type="button" class="btn" onclick="removeFromGroup(':tidingId')">X</button>
	</label>
 </div>`;

const selectedTechSkillTemplate = 
`<div class="col-sm-4">
	<label>
		:techSkillName
		<button type="button" class="btn" onclick="removeSkillFromBulletin(':techSkillName')">X</button>
	</label>
</div>`;

const viewCreatedBulletinTemplate = 
`<tr>
	<td>:creationDate</td>
	<td><input id=":bulletinId-details" value=":detail" class="form-control"></input></td>
	<td id=":bulletinId-skills"><p onclick="updateBulletinSkills(:bulletinId)">:skills</p></td>
	<td id=":bulletinId-targeted" class="clickableThing" onclick="toggleTargeted(:bulletinId)" title="Click to toggle">:targeted</td>
	<td id=":bulletinId-open" class="clickableThing" onclick="toggleOpen(:bulletinId)" title="Click to toggle">:open</td>
	<td><input class="btn btn-default" type="button" onclick="updateBulletin(:bulletinId)" value="Save"></input></td>
	<td><input class="btn btn-default" type="button" onclick="revertChangesToBulletin(:bulletinId)" value="Revert"></input></td>
</tr>`;

const selectedUpdatedSkillTemplate =
`<div class="col-sm-6">
	<label>
		:techSkillName
		<button type="button" class="btn" onclick="removeSkillFromUpdatedBulletin(:bulletinId,':techSkillName')">X</button>
	</label>
</div>`;

const updateBulletinSkillsTemplate = 
`<div>
	<form>
		<div class="form-group" id=":bulletinId-updatedSkillsForBulletin">
			No skills identified for this bulletin
		</div>
		<div class="form-group">
			<input class="form-control" type="list" list="technicalSkillsList" id=":bulletinId-updateTechnicalSkillsSearch"
			placeholder="Search for technical skills here"></input>
		</div>
		<div class="form-inline">
			<button class="btn btn-default" type="button" onclick="addSkillToUpdatedBulletin(:bulletinId)">Add Skill</button>
			<button class="btn btn-success" type="button" onclick="changeSkillsUpdatedBulletin(:bulletinId)">Done</button>
			<button class="btn btn-warning" type="button" onclick="cancelChangesToSkills(:bulletinId)">Cancel</button>
		</div>
	</form>
</div>`;

const noBulletinsMade = "<td colspan='6'>You have no open bulletins</td>";

