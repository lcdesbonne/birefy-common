var contributorsToAdd = [];
var contributorsToRemove = [];
var contributorsToUpdate = [];
var foundContributors = [];
//List of tiding ids
//var availableMembers = [];

/*
 *Format of member details
 *  {
 *  memberId: 1,
 *  questionId: 1,
 *  details: "I did this",
 *  tidingId: "thisOne"
 *  }
 */

function revertAllChanges() {
	contributorsToAdd.length = 0;
	contributorsToRemove.length = 0;
	contributorsToUpdate.length = 0;
	
	populateContributorsForQuestion(selectedQuestion.id);
}

function populateContributorsForQuestion(questionId) {
	let questionContributors = contributors[questionId];
	let tableContent = "";
	
	let contributorsForDisplay = [];
	questionContributors.forEach(function (contributor) {
		if(!contributorsToRemove.find(function(ctbr) {
			return ctbr.memberId == contributor.memberId && ctbr.questionId == questionId;
		}) && !contributorsToUpdate.find(function(ctr) {
			return ctr.memberId == contributor.memberId && ctr.questionId == questionId;
		})) {
			contributorsForDisplay.push(contributor);
		}
	});
	
	contributorsToUpdate.forEach(function (conbr) {
		if(conbr.questionId == questionId) {
			contributorsForDisplay.push(conbr);
		}
	});
	
	contributorsToAdd.forEach(function (contrbtr) {
		if(contrbtr.questionId == questionId) {
			contributorsForDisplay.push(contrbtr);
		}
	});
	
	contributorsForDisplay.forEach(function (contributor) {
		let details = contributor.details == null ? "" : contributor.details;
		
		tableContent += contributorTableTemplate.replace(/:tidingId/g, contributor.tidingId)
		.replace(":details", details)
		.replace(/:memberId/g, contributor.memberId)
		.replace(":questionId", questionId);
	});	
	
	document.getElementById("contributor-table-content").innerHTML = tableContent;
}

function addNewContributorInView() {
	//Retrieve the value of the tiding ID to add
	let tidingId = document.getElementById("tidingToAdd").value;
	
	//Check to see if the member is in the removed list
	let removedContributor = contributorsToRemove.find(function (cont) {
		return cont.tidingId == tidingId;
	});
	
	if (removedContributor) {
		//Remove from the removed list
		let index = contributorsToRemove.indexOf(removedContributor);
		contributorsToRemove.splice(index, 1);
		populateContributorsForQuestion(selectedQuestion.id);
		return;
	}
	
	//Check to see if the contributor is already added
	let existingContributor = contributors[selectedQuestion.id].find(function (cont) {
		return cont.tidingId == tidingId;
	});
	
	if (existingContributor) {
		return;
	}
	
	let memberDetails = availableMembers.find(function (tId) {
		return tId.tidingId == tidingId;
	});
	
	if(memberDetails) {
		//Build the member details object
		let memberQuestion = {
			memberId: memberDetails.memberId,
			questionId: selectedQuestion.id,
			details: "",
			tidingId: tidingId
		}
		
		contributorsToAdd.push(memberQuestion);
	}
	populateContributorsForQuestion(selectedQuestion.id);
}

function removeContributorFromView(tidingId) {
	if(allowRemove(tidingId)) {
		//Check if the contributor is in the new members list
		let newContributor = contributorsToAdd.find(function (cont) {
			return cont.tidingId == tidingId;
		});
		
		if(newContributor) {
			let index = contributorsToAdd.indexOf(newContributor);
			contributorsToAdd.splice(index, 1);
		}
		
		//Check if the question already has this contributor
		let questionContributors = contributors[selectedQuestion.id];
		
		let targetMember = questionContributors.find(function (contributor) {
			return contributor.tidingId == tidingId;
		});
		
		if(targetMember) {
			if(!contributorsToRemove.some(function (contributor) {
				return contributor.tidingId == tidingId;
			})) {
				targetMember.questionId = selectedQuestion.id;
				//Add the member to the removed list
				contributorsToRemove.push(targetMember);
			}
		}
		populateContributorsForQuestion(selectedQuestion.id);
	}
}

function detailsUpdated(questionId, tidingId, memberId) {
	let updatedDetails = document.getElementById("contributor-"+memberId).value;
	
	let existingUpdatedMember;
	
	//Check current updated list
	existingUpdatedMember = contributorsToUpdate.find(function(contributor) {
		return contributor.memberId == memberId;
	});
	
	if(!existingUpdatedMember) {
		existingUpdatedMember = contributors[questionId].find(function(contributor) {
			return contributor.memberId == memberId;
		});
		
		let memberQuestion = {
				memberId: memberId,
				questionId: questionId,
				details: updatedDetails,
				tidingId: tidingId
		}
		
		contributorsToUpdate.push(memberQuestion);
	} else {
		existingUpdatedMember.details = updatedDetails;
	}
}

function populatePotentialMembersToAdd() {
	let optionTemplate = "<option>:value</option>";
	let memberChoiceOptions = "";
	
	availableMembers.forEach(function (member) {
		memberChoiceOptions += optionTemplate.replace(":value", member.tidingId);
	});
	
	let memberOptionsList = document.getElementById("tidingsList");
	memberOptionsList.innerHTML = memberChoiceOptions;
}

function saveChangesToContributors() {
	if (contributorsToAdd.length) {
		saveNewContributors();
	}
	
	if(contributorsToRemove.length) {
		deleteContributors();
	}
	
	if(contributorsToUpdate.length) {
		updateContributors();
	}
	
	contributorsToAdd.length = 0;
	contributorsToRemove.length = 0;
	contributorsToUpdate.length = 0;
	
	closeUpdateTagsForQuestion();
}

function saveNewContributors() {
	$.ajax({
		url: '/tech-estate/add-contributors',
		contentType: 'application/json',
		method: 'POST',
		data: JSON.stringify(contributorsToAdd),
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
		}
	}).done(function () {
		location.reload();
	});
}

function deleteContributors() {
	$.ajax({
		url: '/tech-estate/delete-contributors',
		contentType: 'application/json',
		method: 'POST',
		data: JSON.stringify(contributorsToRemove),
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
		}
	}).done(function () {
		location.reload();
	});
}

function updateContributors() {
	$.ajax({
		url: '/tech-estate/update-contributor-details',
		contentType: 'application/json',
		method: 'POST',
		data: JSON.stringify(contributorsToUpdate),
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf'").attr("content")
		}
	}).done(function () {
		location.reload();
	});
}

function searchForAvailableMembers() {
	let tidingSearch = document.getElementById("tidingToAdd").value;
	
	if (!tidingSearch || tidingSearch.length < 3) {
		return;
	}
	
	$.ajax({
		url: '/tech-estate/search-members?tiding='+tidingSearch,
		type: 'GET',
		success: function (data) {
			availableMembers = data;
			populatePotentialMembersToAdd();
		}
	});
}

function allowRemove(tidingId) {
	let questionContributors = contributors[selectedQuestion.id];
	
	let allCurrentContributors = questionContributors.concat(contributorsToAdd);
	
	let remainingContributors = allCurrentContributors.filter(function(contrb) {
		return !contributorsToRemove.some(function (cb) {
			return cb.tidingId == contrb.tidingId || contrb.tidingId == tidingId;
		});
	});
	
	remainingContributors = remainingContributors.filter(function (ctb) {
		return ctb.tidingId != tidingId;
	});
	
	return remainingContributors.length > 0;
}

const contributorTableTemplate = 
`
<tr>
	<td>:tidingId</td>
	<td><input class="form-control" id="contributor-:memberId" type="text" value=":details" 
		onchange="detailsUpdated(:questionId, ':tidingId', :memberId)" placeholder="Details of contribution"></td>
	<td><button class="btn btn-warning" onclick="removeContributorFromView(':tidingId')">Remove</button></td>
</tr>
`;

const newContributorTemplate = 
`
<tr>
	<td>:tidingId</td>
	<td><input class="form-control" id="contributor-:memberId" type="text" value=":details" onchange="newDetailsUpdated(:questionId, ':tidingId', :memberId)"></td>
	<td><input class="btn btn-warning" type="button" onclick="removeContributorFromView(':tidingId')" value="Remove"></td>
</tr>
`;


