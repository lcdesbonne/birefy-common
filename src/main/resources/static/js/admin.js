window.onload = function() {
	$(".srchQuestions").hide();
	$(".pendingData").hide();

	// Populate the tags list
	findAllTags();

	// Call a search for active topics
	findTopicByTitle();

};

var listOfTopics = [];
var listOfQuestions = [];
var questionsForTopic = [];
var foundTags = [];
var tagsForTopic = [];
var deletedTags = [];
var selectedTags = [];
var selectedTopic;
var selectedQuestion;
/*
 * Below is the format for an updatedTag object within the updatedQuestionTags
 * list var updatedTag = { questionId: selectedQuestion.id, 
 * 							 tagNames: [],
 * 						  removedTags: []
 * 						 }
 */
var updatedQuestionTags = [];

/*
 * Below is the format for updating question urls
 * var urlUpdate = {
 * 					 questionId: id,
 * 					 url: 'url/here'
 * 				   }
 */
var urlUpdate;

var tagsForQuestions;

function generateTopicTable(topicId) {
	// Set the selected topic
	if (topicId) {
		selectedTopic = topicId;
	} else {
		selectedTopic = listOfTopics[0] ? listOfTopics[0].id : null;
	}

	setQuestionsForTopic();

	var tableBody = $('#topicTable');
	var tableRow = '<tr onclick=\"generateTopicAndQuestionTable(:topId)\"><th>:topicName</th></tr>\n';
	var tableRowHighlighted = '<tr class=\"info\"><th>:topicName</th></tr>\n';

	var content = "";
	for (var i = 0; i < listOfTopics.length; i++) {
		if (listOfTopics[i].id == topicId) {
			// Highlighted row
			content += tableRowHighlighted.replace(":topicName",
					listOfTopics[i].topicName);
		} else {
			content += tableRow
					.replace(":topicName", listOfTopics[i].topicName).replace(
							":topId", listOfTopics[i].id);
		}
	}
	tableBody.html(content);

	// Update the tag list
	findTagsForTopic();
}

function generateTopicAndQuestionTable(topicId) {
	generateTopicTable(topicId);
	generateQuestionTable();
}

function generateQuestionTable(questionId) {
	// Set the selected question
	if (questionId) {
		selectedQuestion = questionId;
	} else {
		if (questionsForTopic.length > 0) {
			selectedQuestion = questionsForTopic[0].id;
		} else {
			selectedQuestion = null;
		}
	}

	var tableBody = $('#questionTable');
	var tableRow = '<tr onclick=\"generateQuestionTable(:qId)\"><th>:questionName</th></tr>\n';
	var tableRowHighlighted = '<tr class=\"info\"><th>:questionName</th></tr>\n';
	var tableRowReview = '<tr class=\"warning\" onclick=\"generateQuestionTable(:qId)\"><th>:questionName</th></tr>\n';

	var content = "";
	for (var i = 0; i < questionsForTopic.length; i++) {
		if (questionsForTopic[i].id == selectedQuestion) {
			// Highlight the row
			content += tableRowHighlighted.replace(":questionName",
					questionsForTopic[i].title);
		} else if (questionsForTopic[i].review) {
			// Highlight the row as review
			content += tableRowReview.replace(":questionName",
					questionsForTopic[i].title).replace(":qId",
					questionsForTopic[i].id);
		} else {
			content += tableRow.replace(":questionName",
					questionsForTopic[i].title).replace(":qId",
					questionsForTopic[i].id);
		}
	}
	tableBody.html(content);

	setTopicDetail();
	setQuestionDetail();
}

function setQuestionsForTopic() {
	// Clear questions list
	questionsForTopic.length = 0;

	for (var x = 0; x < listOfQuestions.length; x++) {
		if (listOfQuestions[x].topicId == selectedTopic) {
			questionsForTopic.push(listOfQuestions[x]);
		}
	}
}

function setQuestionDetail() {
	var questionDetail = document.getElementById("qDetail");
	questionDetail.innerHTML = "No Details";

	for (var i = 0; i < listOfQuestions.length; i++) {
		if (listOfQuestions[i].id == selectedQuestion) {
			// Set the question detail
			questionDetail.innerHTML = listOfQuestions[i].detail;
			break;
		}
	}
	
	//Populate the tags for the selected question
	populateTagsForQuestion();
	setQuestionUrl();
	setSourceUrl();
}

function setTopicDetail() {
	var topicDetail = document.getElementById("tDetail");
	topicDetail.innerHTML = "No Details";

	for (var i = 0; i < listOfTopics.length; i++) {
		if (listOfTopics[i].id == selectedTopic) {
			// Set the topic detail
			topicDetail.innerHTML = listOfTopics[i].topicDetail;
			break;
		}
	}
}

function nextTopic() {
	var topicFound = false;

	for (var i = 0; i < listOfTopics.length; i++) {
		if (topicFound) {
			// Call generate table for the current Id
			generateTopicTable(listOfTopics[i].id);
			generateQuestionTable();
			break;
		}
		if (listOfTopics[i].id == selectedTopic) {
			topicFound = true;
		}
	}
	if (!topicFound) {
		// Generate topic table for the 1st topic
		generateTopicTable(listOfTopics[0].id);
		generateQuestionTable();
	}
}

function prevTopic() {
	var topicFound = false;

	for (var i = listOfTopics.length - 1; i >= 0; i--) {
		if (topicFound) {
			generateTopicTable(listOfTopics[i].id);
			generateQuestionTable();
			break;
		}
		if (listOfTopics[i].id == selectedTopic) {
			topicFound = true;
		}
	}
	if (!topicFound) {
		// Generate the topic list for the last topic in the list
		generateTopicTable(listOfTopics[listOfTopics.length - 1].id);
		generateQuestionTable();
	}
}

function nextQuestion() {
	var questionFound = false;

	for (var i = 0; i < questionsForTopic.length; i++) {
		if (questionFound) {
			generateQuestionTable(questionsForTopic[i].id);
			break;
		}
		if (questionsForTopic[i].id == selectedQuestion) {
			questionFound = true;
		}
	}

	if (!questionFound) {
		// Generate the question list for the 1st question in the list
		generateQuestionTable(questionsForTopic[0].id);
	}
}

function prevQuestion() {
	var questionFound = false;

	for (var i = questionsForTopic.length - 1; i >= 0; i--) {
		if (questionFound) {
			generateQuestionTable(questionsForTopic[i].id);
			break;
		}
		if (questionsForTopic[i].id == selectedQuestion) {
			questionFound = true;
		}
	}
	if (!questionFound) {
		// Generate question list for the last in the list
		generateQuestionTable(questionsForTopic[questionsForTopic.length - 1].id);
	}
}

(function() {
	$('input[type="radio"]').click(function() {
		var inputValue = $(this).attr("value");

		// Toggle the pending and active page view
		if (inputValue == "activeData") {
			// Hide all elements of class pendingData
			$(".pendingData").hide();
			// Show all elements of class activeData
			$(".activeData").show();

			// Populate the page with active data
			findTopicByTitle();
		} else if (inputValue == "pendingData") {
			// Hide all elements of class activeData
			$(".activeData").hide();
			// Show all elements of class pendingData
			$(".pendingData").show();

			// Populate the page with pending data
			findPendingQuestionsByTitle();
		} else if (inputValue == "reviewData") {
			// Hide all elements of class activeData
			$(".activeData").hide();
			// Show all elements of class pendingData
			$(".pendingData").show();

			// Populate the page with pending data with
			// Review status
			findPendingQuestionsWithStatusReview();
		} else {
			var targetForm = $("." + inputValue);
			$(".qSearch").not(targetForm).hide();
			$(targetForm).show();
		}
	});
})();

function findTopicByTitle() {
	var title = document.getElementById("topValue").value;
	title = title ? title : "";
	$.ajax({
		url : '/questions/filterByTopicTitle?title=' + title,
		type : 'GET',
		success : function(data) {
			listOfTopics = data.topics;
			listOfQuestions = data.questions;

			if (listOfTopics.length > 0) {
				generateTopicTable(listOfTopics[0].id);
			} else {
				generateTopicTable();
			}

			if (questionsForTopic.length > 0) {
				var questionIds = [];
				for (var i = 0; i < listOfQuestions.length; i++) {
					questionIds.push(listOfQuestions[i].id);
				}
				// Retrieve questions tags by id
				$.ajax({
						url : '/admin/questionTagsByIds?questionIds='
								+ questionIds,
						type : 'GET',
						success : function(data) {
							tagsForQuestions = data;
							generateQuestionTable(questionsForTopic[0].id);
						}
					});
			} else {
				generateQuestionTable();
			}
		}
	});
}

function findPendingTopicsByTitle() {
	var title = $("#topValue").attr("value");
	title = title ? title : "";
	$.ajax({
		url : '/admin/pendingTopicsByTitle?title=' + title,
		type : 'GET',
		success : function(data) {
			listOfTopics = data.topics;
			listOfQuestions = data.questions;

			if (listOfTopics.length > 0) {
				generateTopicTable(listOfTopics[0].id);
			} else {
				generateTopicTable();
			}

			if (questionsForTopic.length > 0) {
				generateQuestionTable(questionsForTopic[0].id);
			} else {
				generateQuestionTable();
			}

		}
	})
}

// Search active questions
function findQuestionsByTitle() {
	var title = document.getElementById("qstValue").value;
	title = title ? title : "";
	$.ajax({
		url : '/questions/filterQuestionByTitle?title=' + title,
		type : 'GET',
		success : function(data) {
			listOfTopics = data.topics;
			listOfQuestions = data.questions;

			generateTopicTable(listOfTopics[0].id);
			generateQuestionTable(questionsForTopic[0].id);

		}
	})
}

// Search pending questions
function findPendingQuestionsByTitle() {
	var title = $("#qstValue").attr("value");
	title = title ? title : "";
	$.ajax({
		url : '/admin/pendingQuestionsByTitle?title=' + title,
		type : 'GET',
		success : function(data) {
			listOfTopics = data.topics;
			listOfQuestions = data.questions;
			
			setQuestionsForTopic();
			if (listOfTopics.length > 0) {
				generateTopicTable(listOfTopics[0].id);
			} else {
				generateTopicTable();
			}

			if (questionsForTopic.length > 0) {
				// Retrieve questions tags by id
				let questionIds = [];
				listOfQuestions.forEach(function(question) {
					questionIds.push(question.id);
				});
				
				$.ajax({
					url : '/admin/questionTagsByIds?questionIds='
							+ questionIds,
					type : 'GET',
					success : function(data) {
						tagsForQuestions = data;
						generateQuestionTable(questionsForTopic[0].id);
					}
				});
			} else {
				generateQuestionTable();
			}
		}
	});
}

// Search pending questions with status review
function findPendingQuestionsWithStatusReview() {
	var title = $("#qstValue").attr("value");
	title = title ? title : "";
	$.ajax({
		url : '/admin/reviewQuestionsByTitle?title=' + title,
		type : 'GET',
		success : function(data) {
			listOfTopics = data.topics;
			listOfQuestions = data.questions;

			if (listOfTopics.length > 0) {
				generateTopicTable(listOfTopics[0].id);
			} else {
				generateTopicTable();
			}

			if (questionsForTopic.length > 0) {
				generateQuestionTable(questionsForTopic[0].id);
			} else {
				generateQuestionTable();
			}
		}
	});
}

function findAllTags() {
	//Retrieves all the tags associated with the topic
	$.ajax({
		url : '/questions/findTags?tagName=',
		type : 'GET',
		success : function(data) {
			foundTags = data;
			// Populate the tag datalist
			var options = "";
			for (var i = 0; i < foundTags.length; i++) {
				options += "<option value=\"" + foundTags[i].tagName + "\">";
			}
			$("#tagList").html(options);
		}
	});
	
	//Retrieve all the technical tags
	$.ajax({
		url : '/admin/allTechTags',
		type : 'GET',
		success : function(data) {
			var foundTechTags = data;
			// Populate the tech tag datalist
			var options = "";
			for (var i = 0; i < foundTechTags.length; i++) {
				options += "<option value=\"" + foundTechTags[i] +"\">";
			}
			$("#techTagList").html(options);
		}
	});
}

function selectTag(tagId) {
	if (!tagId) {
		// Find the tag with this name
		var tagName = $("#tagName").val();

		for (var i = 0; i < selectedTags.length; i++) {
			if (selectedTags[i].tagName == tagName) {
				return;
			}
		}

		for (var i = 0; i < foundTags.length; i++) {
			if (foundTags[i].tagName == tagName) {
				selectedTags.push(foundTags[i]);
				break;
			}
		}
	} else {
		for (var i = 0; i < selectedTags.length; i++) {
			if (selectedTags[i].id == tagId) {
				return;
			}
		}

		for (var i = 0; i < foundTags.length; i++) {
			if (foundTags[i].id == tagId) {
				selectedTags.push(foundTags[i]);
				break;
			}
		}
	}

	// add to the table for selected tags with 3 options
	// Add to search list, Add to topic, Remove
	var row = '<tr><td>tagName</td>'
			+ '<td><input class="adminButtons" type="button" value="Add To Search" onclick="searchForTopicsMatchingTags(:id)"/></td>'
			+ '<td><input class="adminButtons" type="button" value="Add To Topic" onclick="addTagToTopic(:id)"/></td>'
			+ '<td><input class="adminButtons" type="button" value="Remove" onclick="removeTagFromSearchList(:id)"/></td></tr>';

	var content = "";

	for (var i = 0; i < selectedTags.length; i++) {
		content += row.replace("tagName", selectedTags[i].tagName).replace(
				/:id/g, selectedTags[i].id);
	}

	$("#selectedTags").html(content);
}

//Adds a new technical tag to the question
function selectTechTag() {
	var techTag = $("#techTag").val();
	
	//Identify if the question already has this technical tag
	for(var x = 0; x < tagsForQuestions.length; x++) {
		if(tagsForQuestions[x].questionId == selectedQuestion) {
			//Search through the tag list to see matching tags
			var tagList = tagsForQuestions[x].tagNames;
			for(var y = 0; y < tagList.length; y++) {
				if(tagList[y] == techTag) {
					return;
				}
			}
			break;
		}
	}
	
	var updated = false;
	//Identify if the question already has updated tags
	for(var x = 0; x < updatedQuestionTags.length; x++) {
		if(updatedQuestionTags[x].questionId == selectedQuestion) {
			var updatedTechTags = updatedQuestionTags[x].tagNames;
			for(var y = 0; y < updatedTechTags.length; y++) {
				if(updatedTechTags[y] == techTag) {
					return;
				}
			}
			//Add the tag to the updated question list
			updatedQuestionTags[x].tagNames.push(techTag);
			updated = true;
			break;
		}
	}
	
	if(!updated) {
		var newTagList = [];
		newTagList.push(techTag);
		
		//Create a new update question tag object
		var questionTagUpdate = { "questionId": selectedQuestion, 
								    "tagNames": newTagList,
								 "removedTags": []
								};
		
		updatedQuestionTags.push(questionTagUpdate);
	}
	
	//Reconstruct the tag tables
	populateTagsForQuestion();
}

function removeTagFromSearchList(tagId) {
	for (var i = 0; i < selectedTags.length; i++) {
		if (selectedTags[i].id == tagId) {
			selectedTags.splice(i, 1);
		}
	}
	// Clear the search box
	$("#tagName").val(null);

	selectTag();
}

function searchForTopicsMatchingTags(tagId) {
	for (var i = 0; i < selectedTags.length; i++) {
		if (selectedTags[i].id == tagId) {
			tagsForTopic.push(selectedTags[i]);

			// Remove the tag from selected tags
			selectedTags.splice(i, 1);
			break;
		}
	}

	// Update the topic list
	findTopicsByTag();
}

// TODO think about the UI implementation a bit more
// Search topics by tag
function findTopicsByTag() {
	var tagIds = [];
	for (var x = 0; x < tagsForTopic.length; x++) {
		tagIds.push(tagsForTopic[x].id);
	}
	$.ajax({
		url : '/questions/filterByTags?tags=' + tagIds,
		type : 'GET',
		success : function(data) {
			listOfTopics = data.topics;
			listOfQuestions = data.questions;

			// Regenerate the topic and questions tables
			generateTopicTable(listOfTopics[0].id);
			generateQuestionTable(questionsForTopic[0].id);

			// Update the tag list
			populateTagsForTopic();
		}
	});
}

// Remove tag from topic
function deleteTagFromTopic() {
	var topicAndTags = {
		topicId : selectedTopic,
		tagIds : deletedTags
	};

	$.ajax({
		url : '/admin/deleteTagFromTopic',
		type : 'POST',
		data : JSON.stringify(topicAndTags),
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
		}
	}).done(function() {
		// Clear the deleted tags list
		deletedTags.length = 0;
		// Refresh the tag lists
		findTagsForTopic();
	});
}

// Create a new tag
function addTagToTopic(tagId) {
	if (selectedTopic && selectedTags.length > 0) {
		var topicId = selectedTopic;
		var tagIds = [];

		tagIds.push(tagId);

		$.ajax({
			url : '/admin/addTagsToTopic',
			type : 'POST',
			data : {
				topicId : topicId,
				tagIds : tagIds
			},
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function() {
			// Clear the deleted tags list
			deletedTags.length = 0;
			// Refresh the tag lists
			findTagsForTopic();
		});
	}
}

// Makes a topic active
function activateTopic() {
	if (selectedTopic) {
		var topicId = selectedTopic;

		$.ajax({
			url : '/admin/activateTopic/',
			type : 'POST',
			contentType : 'application/json',
			data : JSON.stringify(topicId),
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function() {
			// Update the active topic list
			findPendingTopicsByTitle()
		});
	}
}

// Deactivates topic
function deactivateTopic() {
	if (selectedTopic) {
		var topicId = selectedTopic;

		$.ajax({
			url : '/admin/deactivateTopic',
			type : 'POST',
			contentType : 'application/json',
			data : JSON.stringify(topicId),
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function() {
			findTopicByTitle();
		});
	}
}

// Activate a question
function activateQuestion() {
	if (selectedQuestion) {
		$.ajax({
			url : '/admin/activateQuestion',
			type : 'POST',
			contentType : 'application/json',
			data : JSON.stringify(selectedQuestion),
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function() {
			findPendingQuestionsByTitle();
		});
	}
}

// Deactivate Question
function deactivateQuestion() {
	if (selectedQuestion) {
		$.ajax({
			url : '/admin/deactivateQuestion',
			type : 'POST',
			contentType : 'application/json',
			data : JSON.stringify(selectedQuestion),
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function() {
			findTopicByTitle();
		});
	}
}

function findTagsForTopic() {
	if (selectedTopic) {
		var topicId = selectedTopic;

		$.ajax({
			url : '/admin/tagsForTopic?topicId=' + topicId,
			type : 'GET',
			success : function(data) {
				tagsForTopic = data;
				populateTagsForTopic();
			}
		});
	} else {
		// Clear the tag list
		$("#tagsForTopic").html("No Tags");
	}
}

function populateTagsForTopic() {
	// Populate the tags for topic using a table format
	var row = '<tr id="topTag:id"><th>tagName <input class="adminButtons" type="button"'
			+ 'value="Remove Tag" onclick="moveTagToDeleteList(:id)"/></tr>';
	var content = "";
	for (var x = 0; x < tagsForTopic.length; x++) {
		var id = tagsForTopic[x].id;
		var tagName = tagsForTopic[x].tagName;

		content += row.replace(/:id/g, id).replace("tagName", tagName);
	}

	$("#tagsForTopic").html(content);
}

function populateTagsForQuestion() {
	// Populate the tags for the question using a table format
	var row = '<tr><th><p class="labelsConfined"> :tagName </p></th><th><input class="adminButtons" type="button"'
			+ 'value="Remove Tag" onclick="moveTagToTechDeleteList(\':tagName\')"/></th></tr>';

	var tags = [];
	var deletedTags = []
	if(updatedQuestionTags.length) {
		for(var x = 0; x <updatedQuestionTags.length; x++) {
			if(updatedQuestionTags[x].questionId == selectedQuestion) {
				//Add any new tags to the list
				tags = updatedQuestionTags[x].tagNames.slice();
				deletedTags = updatedQuestionTags[x].removedTags.slice();
				break;
			}
		}
	}
	
	if (tagsForQuestions) {
		//Identify the tags for the selected question
		for(var x = 0; x < tagsForQuestions.length; x++) {
			if(tagsForQuestions[x].questionId == selectedQuestion) {
				tags = tags.concat(uniques(tagsForQuestions[x].tagNames, deletedTags));
				break;
			}
		}
	}
	
	var content = "";
	
	if(!tags.length) {
		content += "No Technical Tags";
	}

	for (var x = 0; x < tags.length; x++) {
		var tagName = tags[x];

		content += row.replace(/:tagName/g, tagName);
	}

	$("#tagsForQuestion").html(content);
}

function populateDeletedTagsForTopic() {
	// Populate the deleted tags list using tabular format
	var row = '<tr id="delTag:id"><th>tagName</th><th><input class="adminButtons" type="button" '
			+ 'value="Remove Tag" onclick="returnTagToTagsForTopicList(:id)"/></th></tr>';
	var content = "";
	for (var x = 0; x < deletedTags.length; x++) {
		var id = deletedTags[x].id;
		var tagName = deletedTags[x].tagName;

		content += row.replace(/:id/g, id).replace("tagName", tagName);
	}

	$("#removeTags").html(content);
}

function populateDeletedTagsForQuestion() {
	var row = '<tr><th><p class="labelsConfined">:tagName</p></th><th><input class="adminButtons" type="button"'
		+ ' value="Remove Tag" onclick="returnTagToTechTagsForQuestionList(\':tagName\')"/></th></tr>';
	var content = "";
	
	var tagsToBeRemoved = [];
	
	//Identify the tags to be removed from the updatedQuestionTags object
	for(var x = 0; x < updatedQuestionTags.length; x++) {
		if (updatedQuestionTags[x].questionId == selectedQuestion) {
			tagsToBeRemoved = updatedQuestionTags[x].removedTags;
			break;
		}
	}
	
	for(var x = 0; x < tagsToBeRemoved.length; x++) {
		content += row.replace(/:tagName/g, tagsToBeRemoved[x]);
	}
	
	$("#removeQuestionTags").html(content);
}

function moveTagToDeleteList(tagId) {
	for (var x = 0; x < tagsForTopic.length; x++) {
		if (tagsForTopic[x].id == tagId) {
			deletedTags.push(tagsForTopic[x]);
			break;
		}
	}
	// Hide the tag from view in the tags for topic section
	document.getElementById("topTag" + tagId).hidden = true;

	populateDeletedTagsForTopic();
}

function moveTagToTechDeleteList(tagName) {	
	// Check to see if there are already updates
	for (var x = 0; x < updatedQuestionTags.length; x++) {
		if (updatedQuestionTags[x].questionId = selectedQuestion) {
			//Check if the tag is in the updated list
			for (var j = 0; j < updatedQuestionTags[x].tagNames.length; j++) {
				var existingTag = updatedQuestionTags[x].tagNames[j];
				if (existingTag == tagName) {
					//Remove the tag from the updated list
					updatedQuestionTags[x].tagNames.splice(j,1);
					populateDeletedTagsForQuestion();
					populateTagsForQuestion();
					return;
				}
			}
			
			// Check that the tag is not already in the delete list
			for (var i = 0; i < updatedQuestionTags[x].removedTags.length; x++) {
				var existingTag = updatedQuestionTags[x].removedTags[i];
				if (existingTag == tagName) {
					return;
				}
			}
			// Add the tag to the removed tags list
			updatedQuestionTags[x].removedTags.push(tagName);
			break;
		}
	}

	var removedTags = [];
	removedTags.push(tagName);

	var updatedTag = {
		'questionId' : selectedQuestion,
		'tagNaes' : [],
		'removedTags' : removedTags
	}
	updatedQuestionTags.push(updatedTag);
	
	populateDeletedTagsForQuestion();
}

function returnTagToTagsForTopicList(tagId) {
	document.getElementById("topTag" + tagId).hidden = false;

	// Remove from the deleted tags list
	for (var x = 0; x < deletedTags.length; x++) {
		if (deletedTags[x].id == tagId) {
			deletedTags.splice(x, 1);
		}
	}

	// Reconstruct the deleted tags list
	populateDeletedTagsForTopic();
}

function returnTagToTechTagsForQuestionList(tagName) {
	//Remove the tag from the updatedQuestionTag object
	for(var x = 0; x < updatedQuestionTags.length; x++) {
		if(updatedQuestionTags[x].questionId == selectedQuestion) {
			//Cycle through the removed tags list
			var rTags = updatedQuestionTags[x].removedTags;
			
			for(var y = 0; y < rTags.length; y++) {
				if(rTags[y] == tagName) {}
				rTags.splice(y,1);
				break;
			}
			break;
		}
	}
	
	//Reconstruct the remove tags list
	populateDeletedTagsForQuestion();
}

function updateQuestionTags() {
	if(updatedQuestionTags && updatedQuestionTags.length) {
		//Apply the updates of the question tags
		$.ajax({
			url: '/tech-estate/update-question-tags',
			contentType: 'application/json',
			method: 'POST',
			data: JSON.stringify(updatedQuestionTags),
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function(data) {
			if (data === true) {
				//Reload all the data related to the question tags
				location.reload();
			} else {
				openFailureDialogue("An error occurred. Unable to apply tech tag updates");
			}
			
		});
	}
}

function setQuestionUrl() {
	var linkElement = document.getElementById("goToQuestion");
	var urlText = document.getElementById("qstUrl");
	
	var url = "";
	for (var x = 0; x < questionsForTopic.length; x++) {
		if (questionsForTopic[x].id == selectedQuestion) {
			url = questionsForTopic[x].appUrl;
			break;
		}
	}
	
	linkElement.href = url;
	urlText.value = url;
}

function setSourceUrl() {
	var linkElement = document.getElementById("goToSource");
	var urlText = document.getElementById("srcUrl");
	
	var url = "";
	for (var x = 0; x < questionsForTopic.length; x++) {
		if (questionsForTopic[x].id == selectedQuestion) {
			url = questionsForTopic[x].source;
			break;
		}
	}
	
	linkElement.href = url;
	urlText.value = url;
}

function modifyQuestionUrl() {
	showYesNoConfirm("Modifying the URL will overwrite the existing one. " +
	"\n Would you like to proceed?", function () {
			var newUrl = $("#qstUrl").val();
			var urlUpdate = {
					'questionId': selectedQuestion,
					'url': newUrl
			}
			
			$.ajax({
				url: '/admin/update-question-url',
				contentType: 'application/json',
				method: 'POST',
				data: JSON.stringify(urlUpdate),
				headers: {
					'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
				}
			}).done(function (data) {
				if (data === true) {
					operationCompleteAlert();
				} else {
					openFailureDialogue("Failed to apply updates");
				}
			});
	});
}

function modifySourceUrl() {
	showYesNoConfirm("Modifying the URL will overwrite the existing one. "+
	"\n Would you like to proceed?", function () {
		var newUrl = $("#srcUrl").val();
		var urlUpdate = {
				'questionId': selectedQuestion,
				'url': newUrl
		}
		
		$.ajax({
			url: '/admin/update-question-source',
			contentType: 'application/json',
			data: JSON.stringify(urlUpdate),
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function (data) {
			if (data === true) {
				operationCompleteAlert();
			} else {
				openFailureDialogue("Failed to apply updates");
			}
		})
	});
}