window.onload = function () {
	ceaseLoadingPage();
	
	populateDealTable();
}

var reloadPage = function() {
	window.location.reload();
}

function populateDealTable() {
	let dealEntries = "";
	
	let confirmValue = "Confirm";
	let unconfirmValue = "Unconfirm";
	let confirmFunction = "confirmDeal";
	let unconfirmFunction = "unconfirmDeal";
	deals.forEach(function (deal) {
		dealEntries += dealTableRowTemplate.replace(/:id/g, deal.id)
			.replace(/:customerTidingId/g, deal.customerId.tidingId)
			.replace(/:customerDetails/g, deal.customerDetails)
			.replace(/:supplierTidingId/g, deal.supplierId.tidingId)
			.replace(/:supplierDetails/g, deal.supplierDetails)
			.replace(/:price/g, deal.price)
			.replace(/:confirmValue/g, hasUserConfirmedDeal(deal.id) ? unconfirmValue : confirmValue)
			.replace(/:confirmFunction/g, hasUserConfirmedDeal(deal.id) ? unconfirmFunction : confirmFunction)
			.replace(/:customerConfirmed/g, deal.customerConfirm)
			.replace(/:supplierConfirmed/g, deal.supplierConfirm)
			.replace(/:status/g, dealStatus(deal.id))
			.replace(/:creationDate/g, deal.creationDate)
			.replace(":isCustomer", deal.customerId.memberId == userId ? "" : "hidden")
			.replace(":canPay", deal.customerConfirm && deal.supplierConfirm ? "" : "disabled");
			
	});
	
	document.getElementById("deals-table").innerHTML = dealEntries;
}

function hasUserConfirmedDeal(dealId) {
	let deal = deals.find(function (d) {
		return d.id == dealId;
	});
	
	let confirmedStatus;
	
	if(deal.customerId.memberId == userId) {
		confirmedStatus = deal.customerConfirm;
	} else if (deal.supplierId.memberId == userId) {
		confirmedStatus = deal.supplierConfirm;
	}
	
	return confirmedStatus;
}

function dealStatus(dealId) {
	let deal = deals.find(function (d) {
		return d.id == dealId;
	});
	
	let status;
	
	if(deal.paid) {
		status = "Resolved on " + deal.paid;
	} else if (!deal.active) {
		status = "Rejected";
	} else {
		status = "Ongoing";
	}
	return status;
}

function updateDeal(dealId) {
	let dealToUpdate = deals.find(function (deal) {
		return deal.id == dealId;
	});
	
	if ((dealToUpdate.customerConfirm == true || dealToUpdate.supplierConfirm == true) && dealToUpdate.active) {
		openFailureDialogue("Updates cannot be made if a deal member has confirmed or if a deal has been rejected.\n" +
				"Both members will need to unconfirm before further updates are made.\n" +
				"No further actions can be taken for rejected deals, or deals that have been resolved.");
	} else {
		let dealUpdate = {};
		
		dealUpdate.dealId = dealId;
		dealUpdate.customerDetails = document.getElementById("customerDetails-"+dealId).value;
		dealUpdate.supplierDetails = document.getElementById("supplierDetails-"+dealId).value;
		dealUpdate.price = document.getElementById("price-"+dealId).value;
		
		$.ajax({
			url: 'deals/update-deal',
			method: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(dealUpdate),
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function (result) {
			if (result === true) {
				operationCompleteAlert(reloadPage);
				
			} else {
				openFailureDialogue("Error updating deal. Perhaps contact support.");
			}
		}); 
	}
}

function viewInventory(dealId) {
	let inventories = inventory.filter(function (inv) {
		return inv.dealId == dealId;
	});
	
	let invContent = "";
	inventories.forEach(function (invent) {
		invContent += inventoryRowTemplate.replace(":title", invent.title)
			.replace(/:id/g, invent.inventoryId)
			.replace(/:download-disabled/g, (invent.canDownload || invent.authorId == userId) ? "" : "disabled")
			.replace(/:preview-disabled/g, invent.canPreview ? "" : "disabled")
			.replace(/:delete-disabled/g, invent.authorId == userId ? "" : "disabled")
			.replace(/:canPreview/g, invent.canPreview)
			.replace(/:commands/g, invent.runCommands ? invent.runCommands : "")
			.replace(/:type/g, invent.type.type)
			.replace(/:isAuthor/g, invent.authorId == userId ? "" : "hidden")
			.replace(/:hideRunCommands/g, invent.type.type == "Runnable Programme" ? "" : "hidden")
			.replace(/:fileExtension/g, "." + invent.fileExtension);
	});
	
	document.getElementById("inventory-list").innerHTML = invContent ? invContent : "<tr><td>None found.</td></tr>";
	document.getElementById("inventory-dialogue").style.display = "block";
}

function closeInventoryView() {
	document.getElementById("inventory-dialogue").style.display = "none";
}

function confirmDeal(dealId) {
	let deal = deals.find(function (deal) {
		return deal.id == dealId;
	});
	
	if (deal.customerId.memberId == userId) {
		$.ajax({
			url: 'deals/customer-confirm-deal',
			method: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(dealId),
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function (result) {
			if (result === true) {
				operationCompleteAlert(reloadPage);
			} else {
				openFailureDialogue("Operation Failed.");
			}
		});
	} else if(deal.supplierId.memberId == userId) {
		$.ajax({
			url: 'deals/supplier-confirm-deal',
			method: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(dealId),
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function (result) {
			if (result === true) {
				operationCompleteAlert(reloadPage);
			} else {
				openFailureDialogue("Operation Failed.");
			}
		});
	}
}

function unconfirmDeal(dealId) {
	let deal = deals.find(function (deal) {
		return deal.id == dealId;
	});
	
	if (deal.customerId.memberId == userId) {
		$.ajax({
			url: 'deals/customer-unconfirm-deal',
			method: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(dealId),
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function (result) {
			if (result === true) {
				operationCompleteAlert(reloadPage);
			} else {
				openFailureDialogue("A deal cannot be unconfirmed once both members have confirmed.");
			}
		});
	} else if(deal.supplierId.memberId == userId) {
		$.ajax({
			url: 'deals/supplier-unconfirm-deal',
			method: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(dealId),
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function (result) {
			if (result === true) {
				operationCompleteAlert(reloadPage);
			} else {
				openFailureDialogue("Updates cannot be made if a deal member has confirmed or if a deal has been rejected.\n" +
						"Both members will need to unconfirm before further updates are made.\n" +
						"No further actions can be taken for rejected deals, or deals that have been resolved.");
			}
		});
	}
}

function rejectDeal(dealId) {
	let dealToReject = deals.find(function (d) {
		return d.id == dealId;
	});
	
	if(dealToReject.paid != null) {
		openFailureDialogue("It is not possible to reject deals that have already been resolved.");
	} else {
		$.ajax({
			url: 'deals/reject',
			method: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(dealId),
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function (result) {
			if (result === true) {
				operationCompleteAlert(reloadPage);
			} else {
				openFailureDialogue("If both members have accepted a deal, the deal cannot be rejected.");
			}
		});
	}
}

function downloadInventory(inventoryId) {
	window.location = "/deals/download-inventory?inventoryId=" + inventoryId;
}

function previewInventory(inventoryId) {
	window.location = "/deals/inventory/preview?id="+inventoryId;
}

function deleteInventory(inventoryId) {
	$.ajax({
		url: 'deals/delete-inventory',
		method: 'POST',
		contentType: 'application/json',
		data: JSON.stringify(inventoryId),
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
		}
	}).done(function (result) {
		if (result === true) {
			operationCompleteAlert(reloadPage);
		} else {
			openFailureDialogue("Operation Failed.");
		}
	});
}

function purchaseDeal(dealId) {	
	window.location = '/purchase?id='+dealId+'&category=DEAL';
}

const dealTableRowTemplate =
`
<tr>
	<td>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div>
						<label>Customer</label>
						<input class="form-control" type="text" value=":customerTidingId" disabled>
					</div>
					<div>
						<label>Customer expectations</label>
						<textarea rows="5" cols="255" class="form-control" type="text" id="customerDetails-:id">:customerDetails</textarea>
					</div>
					<div class="form-inline">
						<label>Confirmed :</label>
						<p class="form-control" style="border: none">:customerConfirmed</p>
					</div>
				</div>
				<div class="col-md-6">
					<div>
						<label>Supplier</label>
						<input class="form-control" type="text" value=":supplierTidingId" disabled>
					</div>
					<div>
						<label>Supplier deliveries</label>
						<textarea rows="5" cols="255" class="form-control" type="text" id="supplierDetails-:id">:supplierDetails</textarea>
					</div>
					<div class="form-inline">
						<label>Confirmed :</label>
						<p class="form-control" style="border: none">:supplierConfirmed</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 form-inline">
					<label>Created :</label>
					<p class="form-control" style="border: none">:creationDate</p>
				</div>
				<div class="col-md-4 form-inline">
					<label>Price (£)</label>
					<input class="form-control" id="price-:id" type="number" min="0" step="0.01" value=":price"/>
				</div>
				<div class="col-md-4 form-inline">
					<label>Status :</label>
					<p class="form-control" style="border: none">:status</p>
				</div>
			</div>
			<div class="row">
				<input class="btn btn-success" type="button" value="Update" onclick="updateDeal(:id)">
				<a href="/inventory-upload?dealId=:id"><input class="btn btn-default" type="button" value="Add Inventory"></a>
				<input class="btn btn-success" type="button" value=":confirmValue" onclick=":confirmFunction(:id)">
				<input class="btn btn-danger" type="button" value="Reject" onclick="rejectDeal(:id)">
				<input class="btn" type="button" value="View Inventory" onclick="viewInventory(:id)">
				<input class="btn btn-success :isCustomer" type="button" value="Purchase" onclick="purchaseDeal(:id)" title="Purchases can be made once both members confirm the deal" :canPay>
			</div>
		</div>
	</td>
</tr>
`;

const inventoryRowTemplate = 
`<tr>
	<td>
		<div class="container">
			<div class="row">
				<div class="col-md-3 form-group">
					<label>Title</label>
					<div class="form-inline">
						<input id="invTitle-:id" type="text" class="form-control" value=":title" readonly>
						<p>:fileExtension</p>
					</div>
				</div>
				<div class="col-md-3 form-group">
					<label>Can Preview</label>
					<h5 id="canPreview-:id">:canPreview</h5>
				</div>
				<div class="col-md-3 form-group">
					<label class=":hideRunCommands">Run Commands</label>
					<textarea id="invCommands-:id" type="text" class="form-control :hideRunCommands">:commands</textarea>
				</div>
				<div class="col-md-3 form-group">
					<label>Type</label>
					<h5>:type</h5>
				</div>
			</div>
			<div class="row">
				<div class="form-group form-inline text-center">
					<button class="btn btn-success" onclick="downloadInventory(:id)" :download-disabled>Download</button>
					<button class="btn btn-default" onclick="previewInventory(:id)" :preview-disabled>Preview</button>
					<button class="btn btn-danger :isAuthor" onclick="deleteInventory(:id)" :delete-disabled>Delete</button>
				</div>
			</div>
		</div>
	</td>
</tr>
`;


