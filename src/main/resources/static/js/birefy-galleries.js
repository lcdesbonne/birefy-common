window.onload = function () {
	ceaseLoadingPage();
	
	initializePage();
};

var graphicsList = [];
var selectedImage;
var currentSearchCriteria = {}; //Object to hold current search criteria
var foundTags = [];
var userDefinedTagList = [];
var foundPainters = [];

function initializePage() {
	graphicsList = activePaintings;
	
	if(graphicsList.length) {
		selectedImage = graphicsList[0].id;
		populateImageDetails(graphicsList[0]);
	}
	
	populateImageCategories();
	populateArtistSuggestions();
}

function downloadImage() {
	window.location = '/image-download/redirect?imageId='+selectedImage;
}

function populateImageCategories() {
	let optionsTemplate = "<option>:value</option>";
	let allOption = "<option>ALL</option>";
	
	let imageCategoryList = document.getElementById("search-category");
	
	let optionList = "";
	
	galleryCategories.forEach(function (category) {
		optionList += optionsTemplate.replace(":value", category);
	});
	
	optionList += allOption;
	imageCategoryList.innerHTML = optionList;
}

function previousImage() {
	if(graphicsList.length && selectedImage) {
		//Identify the index of the currently selected image
		let currentIndex;
		for(let x = 0; x < graphicsList.length; x++) {
			if(graphicsList[x].id == selectedImage) {
				currentIndex = x;
				break;
			}
		}
		
		currentIndex -= 1;
		
		let newIndex = currentIndex < 0 ? graphicsList.length - 1 : currentIndex;
		
		selectedImage = graphicsList[newIndex].id;
		
		populateImageDetails(graphicsList[newIndex]);
	}
}

function nextImage() {
	if(graphicsList.length && selectedImage) {
		//Identify the index of the currently selected image
		let currentIndex;
		for(let x = 0; x < graphicsList.length; x++) {
			if(graphicsList[x].id == selectedImage) {
				currentIndex = x;
				break;
			}
		}
		
		currentIndex += 1;
		
		let newIndex = currentIndex > (graphicsList.length - 1) ? 0 : currentIndex;
		
		selectedImage = graphicsList[newIndex].id;
		
		populateImageDetails(graphicsList[newIndex]);
	}
}

function showCurrentSearchCriteria(hide) {
	if(!hide) {
		document.getElementById("current-search-criteria").style.display = "block";
		document.getElementById("toggle-search-criteria").value = "Hide Current Search Criteria";
	} else {
		document.getElementById("current-search-criteria").style.display = "none";
		document.getElementById("toggle-search-criteria").value = "Show Current Search Criteria";
	}
	
	document.getElementById("toggle-search-criteria").onclick = (function (show) {
		return function() {
			showCurrentSearchCriteria(!hide);
		};
	})(hide);
}

function searchPaintings() {
	//Retrieve values from the form
	let tidingId = document.getElementById("search-tiding").value;
	let imageTitle = document.getElementById("search-title").value;
	let category = document.getElementById("search-category").value;
	
	category = category == 'ALL' ? null : category;
	
	let foundPainter = foundPainters.find(function (painter) {
		return painter.tidingId === tidingId;
	});
	
	let memberId = foundPainter ? foundPainter.memberId : null;
	
	if (!memberId && !imageTitle && category && !userDefinedTagList.length) {
		findImagesByCategory(category);
		
	} else if (memberId && !imageTitle && category && !userDefinedTagList.length) {
		findImagesByPainterAndCategory(tidingId, memberId, category);
		
	} else if (!memberId && imageTitle && category && !userDefinedTagList.length) {
		findImageByTitleAndCategory(imageTitle, category);
		
	} else if (memberId && imageTitle && category && !userDefinedTagList.length) {
		findImagesByTitleCategoryAndPainter(imageTitle, category,tidingId, memberId);
		
	} else if (!memberId && !imageTitle && !category && !userDefinedTagList.length) {
		findAllPaintings();
		
	} else if (memberId && !imageTitle && !category && !userDefinedTagList.length) {
		findImagesByPainter(memberId, tidingId);
		
	} else if (!memberId && imageTitle && !category && !userDefinedTagList.length) {
		findPaintingByTitle(imageTitle);
		
	} else if (memberId && imageTitle && !category && !userDefinedTagList.length) {
		findPaintingsByArtistWithTitle(tidingId, imageTitle);
		
	} else if (!memberId && !imageTitle && category && userDefinedTagList.length) {
		findImagesByCategoryTags(category);
		
	} else if (memberId && !imageTitle && category && userDefinedTagList.length) {
		findImagesByPainterAndCategoryTags(tidingId, memberId, category);
		
	} else if (!memberId && imageTitle && category && userDefinedTagList.length) {
		findImageByTitleAndCategoryTags(imageTitle, category);
		
	} else if (memberId && imageTitle && category && userDefinedTagList.length) {
		findImagesByTitleCategoryAndPainterTags(imageTitle, category,tidingId, memberId);
		
	} else if (!memberId && !imageTitle && !category && userDefinedTagList.length) {
		findAllPaintingsTags();
		
	} else if (memberId && !imageTitle && !category && userDefinedTagList.length) {
		findImagesByPainterTags(memberId, tidingId);
		
	} else if (!memberId && imageTitle && !category && userDefinedTagList.length) {
		findPaintingByTitleTags(imageTitle);
		
	} else if (memberId && imageTitle && !category && userDefinedTagList.length) {
		findPaintingsByArtistWithTitleTags(tidingId, imageTitle);
		
	}
	
	cancelSearchDialogue();
}

function extractSelectedTagIds() {
	let tagIds = [];
	
	userDefinedTagList.forEach(function (tag) {
		tagIds.push(tag.id);
	});
	
	return tagIds;
}

function findAllPaintings() {
	fillCurrentSearchCriteria(null, null, 'ALL');
	
	$.ajax({
		url: "birefy-galleries/all-paintings",
		method: 'GET',
		success: function (paintings) {
			applyPaintingSearchResults(paintings);
		}
	});
}

function findAllPaintingsTags() {
	let tagIds = extractSelectedTagIds();
	fillCurrentSearchCriteria(null, null, 'ALL');
	
	$.ajax({
		url: "/birefy-galleries/all-paintings-with-tags?tagIds="+tagIds,
		method: 'GET',
		success: function (paintings) {
			applyPaintingSearchResults(paintings);
		}
	});
}

function findPaintingsByArtistWithTitle(memberId, imageTitle) {
	fillCurrentSearchCriteria(tidingId, title, null);
	
	$.ajax({
		url: "/birefy-galleries/paintings-by-artist-title?painter="+memberId+"&title="+imageTitle,
		method: 'GET',
		success: function (paintings) {
			applyPaintingSearchResults(paintings);
		}
	});
}

function findPaintingsByArtistWithTitleTags(memberId, imageTitle) {
	let tagIds = extractSelectedTagIds();
	fillCurrentSearchCriteria(tidingId, title, null);
	
	$.ajax({
		url: "/birefy-galleries/paintings-by-artist-title-tags?painter="+memberId+"&title="+imageTitle+"&tagIds="+tagIds,
		method: 'GET',
		success: function (paintings) {
			applyPaintingSearchResults(paintings);
		}
	});
}

function findPaintingByTitle(title) {
	fillCurrentSearchCriteria(null, title, null);
	
	$.ajax({
		url: "/birefy-galleries/paintings-by-title?title="+title,
		method: 'GET',
		success: function (paintings) {
			applyPaintingSearchResults(paintings);
		}
	});
}

function findPaintingByTitleTags(title) {
	fillCurrentSearchCriteria(null, title, null);
	let tagIds = extractSelectedTagIds();
	
	$.ajax({
		url: "/birefy-galleries/paintings-by-title-tags?title="+title+"&tagIds="+tagIds,
		method: 'GET',
		success: function (paintings) {
			applyPaintingSearchResults(paintings);
		}
	});
}

function findImagesByPainter(memberId, tidingId) {
	fillCurrentSearchCriteria(tidingId, null, null);
	
	$.ajax({
		url: "/birefy-galleries/artwork-by-painter?painter="+memberId,
		method: 'GET',
		success: function (paintings) {
			applyPaintingSearchResults(paintings);
		}
	});
}

function findImagesByPainterTags(memberId, tidingId) {
	fillCurrentSearchCriteria(tidingId, null, null);
	let tagIds = extractSelectedTagIds();
	
	$.ajax({
		url: "/birefy-galleries/artwork-by-painter?painter="+memberId+"&tagIds="+tagIds,
		method: 'GET',
		success: function (paintings) {
			applyPaintingSearchResults(paintings);
		}
	});
}

function findImagesByCategory(category) {
	fillCurrentSearchCriteria(null, null, category);
	
	$.ajax({
		url: "/birefy-galleries/paintings-by-category?category="+category,
		method: 'GET',
		success: function (paintings) {
			applyPaintingSearchResults(paintings);
		}
	});
}

function findImagesByCategoryTags(category) {
	fillCurrentSearchCriteria(null, null, category);
	let tagIds = extractSelectedTagIds();
	
	$.ajax({
		url: "/birefy-galleries/paintings-by-category-tags?category="+category+"&tagIds="+tagIds,
		method: 'GET',
		success: function (paintings) {
			applyPaintingSearchResults(paintings);
		}
	});
}

function findImagesByTitleCategoryAndPainter(title, category, tidingId, memberId) {
	fillCurrentSearchCriteria(tidingId, title, category);
	
	$.ajax({
		url: "/birefy-galleries/search-by-title-category-and-artist?title="+title+"&category="+category+"&painter="+memberId,
		method: 'GET',
		success: function (paintings) {
			applyPaintingSearchResults(paintings);
		}
	});
}

function findImagesByTitleCategoryAndPainterTags(title, category, tidingId, memberId) {
	fillCurrentSearchCriteria(tidingId, title, category);
	let tagIds = extractSelectedTagIds();
	
	$.ajax({
		url: "/birefy-galleries/search-by-title-category-and-artist-tags?title="+title+"&category="+category+"&painter="+memberId+"&tagIds="+tagIds,
		method: 'GET',
		success: function (paintings) {
			applyPaintingSearchResults(paintings);
		}
	});
}

function findImagesByPainterAndCategory(tidingId, memberId, category) {
	fillCurrentSearchCriteria(tidingId, null, category);
	
	$.ajax({
		url: "/birefy-galleries/all-images-by-painter-category?painter="+memberId+"&category="+category,
		method: 'GET',
		success: function (paintings) {
			applyPaintingSearchResults(paintings);
		}
	});	
}

function findImagesByPainterAndCategoryTags(tidingId, memberId, category) {
	fillCurrentSearchCriteria(tidingId, null, category);
	let tagIds = extractSelectedTagIds();
	
	$.ajax({
		url: "/birefy-galleries/all-images-by-painter-category-tags?painter="+memberId+"&category="+category+"&tagIds="+tagIds,
		method: 'GET',
		success: function (paintings) {
			applyPaintingSearchResults(paintings);
		}
	});
}

function findImageByTitleAndCategory(title, category) {
	//Populate the search criteria
	fillCurrentSearchCriteria(null, title, category);
	
	$.ajax({
		url:"/birefy-galleries/find-image-by-title-and-category?title="+title+"&category="+category,
		method: 'GET',
		success: function (paintings) {
			applyPaintingSearchResults(paintings);
		}
	});
}

function findImageByTitleAndCategoryTags(title, category) {
	fillCurrentSearchCriteria(null, title, category);
	let tagIds = extractSelectedTagIds();
	
	$.ajax({
		url:"/birefy-galleries/find-image-by-title-and-category-tags?title="+title+"&category="+category+"&tagIds="+tagIds,
		method: 'GET',
		success: function (paintings) {
			applyPaintingSearchResults(paintings);
		}
	});
}

function findPainters() {
	let tidingId = document.getElementById("search-tiding").value;
	
	if(tidingId.length < 3) {
		return;
	}
	
	$.ajax({
		url: '/birefy-galleries/search-painters?name='+tidingId,
		method: 'GET',
		success: function (painters) {
			foundPainters = painters;
			populateArtistSuggestions();
		}
	});
}


function searchForDescriptiveTags() {
	let tagName = document.getElementById("descriptive-tag-search").value;
	
	if(tagName.length < 3) {
		return;
	}
	
	let foundTag = foundTags.find(function (tag) {
		return tag.tag == tagName;
	});
	
	if(!foundTag) {
		$.ajax({
			url: '/birefy-galleries/search-painting-tags?tag='+tagName,
			method: 'GET',
			success: function(tagListResults) {
				foundTags = tagListResults;
				let foundTagNames = [];
				foundTags.forEach(function(t) {
					foundTagNames.push(t.tag);
				});
				
				let tagOptions = "";
				foundTagNames.forEach(function (tName) {
					tagOptions += dataListOptionTemplate.replace(":value", tName);
				});
				
				document.getElementById("descriptive-tag-list").innerHTML = tagOptions;
			}
		});
	}
}

function displaySelectedTagsForSearch() {
	elementId = "selected-tag-display";
	if (!userDefinedTagList || !userDefinedTagList.length) {
		document.getElementById(elementId).innerHTML = "<p class=\"text-center\"><label>None Selected</label></p>";
		return;
	}
	
	var blankColumns = userDefinedTagList.length % 3;
	var row = "<div class=\"row\">";
	var endRow = "</div>";
	var column = "<div class=\"col-sm-4\"><label>--tag--</label><button class=\"btn\" onclick=\"removeTagFromSearch(--id--)\">X</button></div>";
	var emptyColumn = "<div class=\"col-sm-4\"></div>";
	var content = row;
	var endOfRow = false;
	
	for(var i = 0; i < userDefinedTagList.length; i++) {
		if (i % 3 == 0 && i != 0) {
			content += column.replace(/--id--/g,userDefinedTagList[i].id).replace(/--tag--/g, userDefinedTagList[i].tag);
			
			//End the row
			content += endRow;
			endOfRow = true;
			continue;
		}
		if (endOfRow) {
			content += row;
			endOfRow = false;
		}
		
		content += column.replace(/--tag--/g, userDefinedTagList[i].tag).replace(/--id--/g, userDefinedTagList[i].id);	
	}
	
	if (blankColumns > 0) {
		for (var i = 0; i < blankColumns; i++) {
			content += emptyColumn;
		}
		//End the row
		content += endRow;
	}
	
	document.getElementById(elementId).innerHTML = content;
}

function addDescriptiveTagToSearch() {
	let tagName = document.getElementById("descriptive-tag-search").value;
	
	let foundTag = foundTags.find(function (tag) {
		return tag.tag == tagName;
	});
	
	if(foundTag) {
		userDefinedTagList.push(foundTag);
		
		displaySelectedTagsForSearch();
	}
}

function removeTagFromSearch(tagId) {
	let tagIndex;
	
	for(let index in userDefinedTagList) {
		if (userDefinedTagList[index].id == tagId) {
			tagIndex = index;
			break;
		}
	}
	
	userDefinedTagList.splice(tagIndex, 1);
	displaySelectedTagsForSearch();
}

function populateArtistSuggestions() {
	let options = "";
	for(let painterIndex in foundPainters) {
		options += dataListOptionTemplate.replace(":value", foundPainters[painterIndex].tidingId);
	}
	document.getElementById("artist-list").innerHTML = options;
}

function applyPaintingSearchResults(paintings) {
	graphicsList = paintings;
	selectedImage = graphicsList.length ? graphicsList[0].id : null;
	
	populateImageDetails(graphicsList[0]);
}

function fillCurrentSearchCriteria(painter, title, category) {
	currentSearchCriteria.artist = painter ? painter : "";
	currentSearchCriteria.title = title ? title : "";
	currentSearchCriteria.category = category ? category : "ALL";
	
	if(userDefinedTagList.length) {
		let tagNames = [];
		userDefinedTagList.forEach(function (tag) {
			tagNames.push(tag.tag);
		});
		currentSearchCriteria.tagList = tagNames.join(", ");
	} else {
		currentSearchCriteria.tagList = "";
	}
	
	//Apply the search criteria to the page
	document.getElementById("searchCriteriaPainter").value = currentSearchCriteria.artist;
	document.getElementById("title-search-criteria").value = currentSearchCriteria.title;
	document.getElementById("category-search-criteria").value = currentSearchCriteria.category;
	document.getElementById("taglist-search-criteria").value = currentSearchCriteria.tagList;
}

function populateImageDetails(graphic) {
	document.getElementById("image-name").value = graphic ? graphic.name : "N/A";
	document.getElementById("artist").value = graphic ? graphic.tidingId : "N/A";
	document.getElementById("image-type").value = graphic ? graphic.category : "N/A";
	document.getElementById("contact-artist").value = graphic ? "Contact "+graphic.tidingId : "No Artist Found";
	document.getElementById("selected-image").src = graphic ? paintingLocationUrlTemplate.replace(":id",graphic.id) : "#";
	document.getElementById("fullScreenImage").src = graphic ? paintingLocationUrlTemplate.replace(":id",graphic.id) : "#";
	populateImageResultNumber();
}

function populateImageResultNumber() {
	const resultsTemplate = "Showing :imageNumber of :totalResults";
	const noResultsTemplate = "No Images Found";
	
	let element = document.getElementById("result-number");
	
	if(graphicsList.length && selectedImage) {
		for(let i = 0; i < graphicsList.length; i++) {
			if(graphicsList[i].id == selectedImage) {
				element.innerHTML = resultsTemplate.replace(":imageNumber", i+1)
					.replace(":totalResults", graphicsList.length);
			}
		}
		
	} else {
		element.innerHTML = noResultsTemplate;
	}
}

function openMessageDialogue() {
	document.getElementById("contactAndSearch").style.display = "none";
	document.getElementById("sendMessageForm").style.display = "block";
}

function closeMessageDialogue() {
	document.getElementById("messageContent").value = "";
	document.getElementById("contactAndSearch").style.display = "block";
	document.getElementById("sendMessageForm").style.display = "none";
}

function sendMessageToArtist() {
	let socket = new SockJS('/chatsocket');
	let stompClient = Stomp.over(socket);
	
	let saveMessageUrl = "/routing/saveMessage";
	let urlTemplate = "/direct/tidings/:recipient";
	
	let tiding = {
			'type': 'CHAT',
			'content': "",
			'sender': ""
	}
	
	let message = {
			'content': '',
			'creator': '',
			'recipient': ''
	}
	
	stompClient.connect({}, function () {
		let messageContent = document.getElementById("messageContent").value;
		message.content = messageContent;
		message.creator = userTidingId.id;
		
		let graphicDetails = graphicsList.find(function (graphic) {
			return graphic.id == selectedImage;
		});
		
		message.recipient = graphicDetails.channelId;
		
		tiding.content = messageContent;
		tiding.sender = userTidingId.tidingId;
		
		stompClient.send(urlTemplate.replace(":recipient",graphicDetails.tidingId, {}, JSON.stringify(tiding)));
		stompClient.send(saveMessageUrl, {}, JSON.stringify(message));
		
		operationCompleteAlert();
		document.getElementById("messageContent").value = "";
	});
}

function openSearchDialogue() {
	document.getElementById("search-form-container").style.display = "block";
}

function cancelSearchDialogue() {
	document.getElementById("search-tiding").innerHTML = "";
	document.getElementById("search-title").innerHTML = "";
	document.getElementById("search-form-container").style.display = "none";
}

function openFullScreenImage() {
	document.getElementById("fullScreenImageContainer").style.display = "block";
}

function closeFullScreenImage() {
	document.getElementById("fullScreenImageContainer").style.display = "none";
}

const paintingLocationUrlTemplate = applicationDomain+"/birefy-galleries/stream-painting?id=:id";
const dataListOptionTemplate = "<option value=\":value\">";

(function () {
	$('img').bind('contextmenu', function(e) {
		return false;
	});
})();
