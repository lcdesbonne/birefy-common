window.onload = function () {
	ceaseLoadingPage();
	populateSoundCategories();
	populatePlayList(recentInstrumentals);
	populateArtistOptions();
};

var selectedMember;
var highlightedSongIndex = 0;
var foundTags = [];
var selectedTags = [];

const soundUrlTemplate = applicationDomain + "/sound/stream-sound?id=:id";

function populateSoundCategories() {
	let dataOption = "<option>:value</option>";
	
	let soundCategoryList = document.getElementById("sound-categories");
	
	let optionList = "";
	
	soundCategories.forEach(function(category) {
		optionList += dataOption.replace(":value", category);
	});
	
	soundCategoryList.innerHTML = optionList;
}

function selectTrackToPlay(id) {
	stopMusic();
	
	//Get the audio player and assign the new track
	let audioPlayer = document.getElementById("audio-player");
	audioPlayer.src = soundUrlTemplate.replace(":id",id);
	audioPlayer.load();
	audioPlayer.play();
	
	//Set the playing name
	let selectedSong = recentInstrumentals.find(function (song){
		return song.id == id;
	});
	
	let songTitle = selectedSong.name;
	let songArtist = selectedSong.tidingId;
	
	document.getElementById("playing-name").value = songTitle;
	document.getElementById("playing-artist").value = songArtist;
	
	selectArtistForSearch(selectedSong.tidingId);
}

function populatePlayList(sounds) {
	let soundList = sounds.slice(0);
	
	let playListTable = document.getElementById("visible-playlist");
	
	let content = birefyFixedViewTable(0, 5,soundList, soundRowTemplate, selectedSoundRowTemplate);
	
	playListTable.innerHTML = content;
}

function highlightNextSong(sounds) {
	let soundList = sounds.slice(0);
	highlightedSongIndex += 1;
	
	if(highlightedSongIndex >= soundList.length) {
		highlightedSongIndex = 0;
	}
	
	let playListTable = document.getElementById("visible-playlist");
	
	let content = birefyFixedViewTable(highlightedSongIndex, 5,soundList, soundRowTemplate, selectedSoundRowTemplate);
	
	playListTable.innerHTML = content;
}

function highLightPreviousSong(sounds) {
	let soundList = sounds.slice(0);
	highlightedSongIndex -= 1;
	
	if(highlightedSongIndex < 0) {
		highlightedSongIndex = highlightedSongIndex + soundList.length;
	}
	
	let playListTable = document.getElementById("visible-playlist");
	
	let content = birefyFixedViewTable(highlightedSongIndex, 5,soundList, soundRowTemplate, selectedSoundRowTemplate);
	
	playListTable.innerHTML = content;
}

function downloadSound(id){
	window.location = '/sound-download/redirect?soundId='+id;
}

function searchForSoundTags() {
	let tagName = document.getElementById("soundTagSearch").value;
	
	if(tagName.length < 3) {
		return;
	}
	
	$.ajax({
		url: '/sound/search-tags-by-name?tagName='+tagName,
		type: 'GET',
		success: function (tags) {
			foundTags = tags;
			populateTagSuggestions(foundTags);
		}
	});
}

function selectTagForSearch() {
	let tagName = document.getElementById("soundTagSearch").value;
	
	let tagToAdd = foundTags.find(function (tag) {
		return tag.tag == tagName;
	});
	
	if(tagToAdd) {
		selectedTags.push(tagToAdd);
		search();
	}
	
	let tagList = populateModifyableTagsList();
	document.getElementById("modifiable-tag-display").innerHTML = tagList;
	
	document.getElementById("soundTagSearch").value = "";
}

function populateTagSuggestions(tagList) {
	let optionTemplate = "<option>:tagName</option>";
	
	let tagOptions = "";
	tagList.forEach(function (tag) {
		tagOptions += optionTemplate.replace(":tagName", tag.tag);
	});
	
	document.getElementById("tagSearchSuggestions").innerHTML = tagOptions;
}

function selectedTagIds() {
	let tagIds = [];
	
	selectedTags.forEach(function (tag) {
		tagIds.push(tag.id);
	});
	
	return tagIds;
}

function removeTagFromSearch(tagId) {
	for(let index in selectedTags) {
		if(selectedTags[index].id == tagId) {
			selectedTags.splice(index, 1);
			break;
		}
	}
	
	search();
	let tagList = populateModifyableTagsList();
	document.getElementById("modifiable-tag-display").innerHTML = tagList;
}

function populateModifyableTagsList() {
	let modTagList = "";
	let newRow = '<div class="row">';
	let endRow = '</div>';
	
	if(selectedTags.length) {
		let tagsPerRow = 4;
		
		let tagTemplate = `<div class="col-sm-3">
		<label>:tagName <button class="btn" onclick="removeTagFromSearch(:tagId)">X</button>
		</div>`;
		let emptyColumn = `<div class="col-sm-3"></div>`;
		
		let emptyRows = selectedTags.length < tagsPerRow ? tagsPerRow - selectedTags.length 
				: selectedTagslength % tagsPerRow;
		
		let entryCounter = 0;
		selectedTags.forEach(function (tag) {			
			if(entryCounter == tagsPerRow) {
				modTagList += endRow;
				entryCounter = 0;
			} 
			if (entryCounter == 0) {
				modTagList += newRow;
				
				modTagList += tagTemplate.replace(":tagName", tag.tag)
					.replace(":tagId", tag.id);
			} else {
				modTagList += tagTemplate.replace(":tagName", tag.tag)
					.replace(":tagId", tag.id);
			}
			
			entryCounter += 1;
		});
		
		let emptyRowCounter = 0;
		while(emptyRowCounter < emptyRows) {
			modTagList += emptyColumn;
			emptyRowCounter += 1;
		}
	} else {
		modTagList += newRow;
		modTagList += "<i>No tags selected for search</i>";
		modTagList += endRow;
	}
	
	return modTagList;
}

function search() {
	selectArtistForSearch();
	let selectedCategory = document.getElementById("sound-categories").value;
	let memberId = selectedMember ? selectedMember.memberId : null;
	let songTitle = document.getElementById("search-song-title").value;
	let tagIds = selectedTagIds();
	
	if(selectedCategory && memberId && songTitle.length && !tagIds.length) {
		searchSoundsByTitleCategoryAndArtist(songTitle, memberId, selectedCategory);
		
	} else if(selectedCategory && !memberId && songTitle.length && !tagIds.length) {
		searchSoundsByTitleAndCategory(songTitle, selectedCategory);
		
	} else if(selectedCategory && !memberId && !songTitle.length && !tagIds.length) {
		searchRecentSoundsByCategory();
		
	} else if(selectedCategory && memberId && !songTitle.length && !tagIds.length) {
		searchSoundsByArtistAndCategory(memberId, selectedCategory);
		
	}
	
	else if(selectedCategory && !memberId && !songTitle.length && tagIds.length) {
		searchRecentSoundsByCategoryAndTags(tagIds);
		
	} else if(selectedCategory && !memberId && songTitle.length && tagIds.length) {
		searchSoundsByTitleAndCategoryTags(songTitle, selectedCategory, tagIds);
		
	} else if(selectedCategory && memberId && !songTitle.length && tagIds.length) {
		searchSoundsByArtistAndCategoryAndTags(memberId, selectedCategory, tagIds);
		
	}
}

function searchRecentSoundsByCategoryAndTags(tagIds) {
	let selectedCategory = document.getElementById("sound-categories").value;
	
	$.ajax({
		url: "/sound/find-recent-sounds-tags?category="+selectedCategory+"&tagIds="+tagIds,
		type: 'GET',
		success: function (sounds) {
			recentInstrumentals = sounds;
			populatePlayList(recentInstrumentals);
			//Clear artist and song search fields
			document.getElementById("artist-search").value = "ALL";
		}
	});
}

function searchRecentSoundsByCategory() {
	let selectedCategory = document.getElementById("sound-categories").value;
		
	//Load recent instrumentals for the category
	$.ajax({
		url: "/sound/find-recent-sounds?category="+selectedCategory,
		type: 'GET',
		success: function (recentSounds) {
			//Populate the sound table
			recentInstrumentals = recentSounds;
			populatePlayList(recentInstrumentals);
			//Clear artist and song search fields
			document.getElementById("artist-search").value = "ALL";
			//Clear the tag list
			selectedTags.length = 0;
			let tagList = populateModifyableTagsList();
			document.getElementById("modifiable-tag-display").innerHTML = tagList;
			document.getElementById("soundTagSearch").value = "";
			//Clear the song title field
			document.getElementById("search-song-title").value = "";
		}
	});
}

function searchSoundsByArtistAndCategory(memberId, selectedCategory) {
	if (selectedMember && selectedMember.memberId !== null) {
		let selectedCategory = document.getElementById("sound-categories").value;
		let memberId = selectedMember.memberId;
		selectArtistForSearch();
		
		$.ajax({
			url: "/sound/all-music-by-artist-category?artist="+memberId+"&category="+selectedCategory,
			type: 'GET',
			success: function (soundsByArtist) {
				recentInstrumentals = soundsByArtist;
				populatePlayList(recentInstrumentals);
			}
		});
	}
}

function searchSoundsByArtistAndCategoryAndTags(memberId, selectedCategory, tagIds) {	
	$.ajax({
		url: "/sound/all-music-by-artist-category-tags?artist="+memberId+"&category="+selectedCategory+"&tagIds="+tagIds,
		type: 'GET',
		success: function (soundsByArtist) {
			recentInstrumentals = soundsByArtist;
			populatePlayList(recentInstrumentals);
		}
	});
}

function searchSoundsByTitleCategoryAndArtist(songTitle, memberId, selectedCategory) {
	$.ajax({
		url: "/sound/search-by-songName-category-and-artist?songName="+songTitle+"&artist="+memberId+"&category="+selectedCategory,
		type: 'GET',
		success: function (sounds){
		recentInstrumentals = sounds;
		populatePlayList(recentInstrumentals);
		}
	});
}

function searchSoundsByTitleAndCategory(songTitle, category) {
	$.ajax({
		url: "/sound/find-sound-by-title-and-category?title="+songTitle+"&category="+category,
		type: 'GET',
		success: function (sounds){
		recentInstrumentals = sounds;
		populatePlayList(recentInstrumentals);
		}
	});
}

function searchSoundsByTitleAndCategoryTags(songTitle, category, tagIds) {
	$.ajax({
		url: "/sound/find-sound-by-title-and-category-tags?title="+songTitle+"&category="+category+"&tagIds="+tagIds,
		type: 'GET',
		success: function (sounds){
		recentInstrumentals = sounds;
		populatePlayList(recentInstrumentals);
		}
	});
}

function selectArtistForSearch(definedArtist) {	
	if(!definedArtist) {
		let tidingId = document.getElementById("artist-search").value;
		if(tidingId == "ALL" || !tidingId) {
			selectedMember = {};
			selectedMember.tidingId = definedArtist;
		}
		let foundMember;
		for(var index in foundArtists) {
			if (foundArtists[index].tidingId == tidingId) {
				foundMember = foundArtists[index];
			}
		}
		
		selectedMember = foundMember;
	} else {
		selectedMember = {};
		selectedMember.tidingId = definedArtist;
	}
	
	const contactButtonTextDefault = "Contact Playing Artist";
	const contactButtonTextActive = "Contact Artist :tidingId";
	
	if(userTidingId) {
		let contactButton = document.getElementById("contactCreator");
		if(selectedMember) {
			contactButton.disabled = false;
			contactButton.innerHTML = contactButtonTextActive.replace(":tidingId", selectedMember.tidingId);
			contactButton.title = "Search for an artist to contact";
		} else {
			contactButton.disabled = true;
			contactButton.innerHTML = contactButtonTextDefault;
			contactButton.title = "";
		}
	}
}

function searchForArtists() {
	let approxTidingId = document.getElementById("artist-search").value;
	
	if(approxTidingId.length < 2) {
		return;
	}
	
	$.ajax({
		url:"/sound/search-artists?name="+approxTidingId,
		type: 'GET',
		success: function(members){
			if(members.length) {
				foundArtists = members;
				populateMatchingArtists(foundArtists);
				selectArtistForSearch();
			}
		}
	});
}

function populateArtistOptions() {
	let optionTemplate = "<option>:value</option>";
	let options = "";
	
	options += optionTemplate.replace(":value", "ALL");
	foundArtists.forEach(function (artist) {
		options += optionTemplate.replace(":value", artist.tidingId);
	});
	
	document.getElementById("artist-search").innerHTML = options;
}

function populateMatchingArtists(memberList) {
	let matchingArtistList = document.getElementById("matchingArtists");
	let option = "<option value=\":name\">";
	let options = "";
	memberList.forEach(function (member) {
		options += option.replace(":name", member.tidingId);
	});
	matchingArtistList.innerHTML = options;
}

function openContactCreator() {
	document.getElementById("sendMessageForm").style.display = "block";
	document.getElementById("contactCreator").style.display = "none";
}

function cancelContactCreator() {
	document.getElementById("sendMessageForm").style.display = "none";
	document.getElementById("contactCreator").style.display = "block";
}

function playPauseMusic(pause) {
	if(pause) {
		document.getElementById("audio-player").pause();
	} else {
		document.getElementById("audio-player").play();
	}
}

function stopMusic() {
	document.getElementById("audio-player").pause();
}

function volume(increase) {
	if(increase) {
		document.getElementById("audio-player").volume += 0.1;
	} else {
		document.getElementById("audio-player").volume -= 0.1;
	}
}

function sendMessageToArtist() {
	let socket = new SockJS('/chatsocket');
	let stompClient = Stomp.over(socket);
	
	let saveMessageUrl = "/routing/saveMessage";
	let urlTemplate = "/direct/tidings/:recipient";
	
	let tiding = {
			'type': 'CHAT',
			'content': "",
			'sender': ""
	}
	
	let message = {
			'content': '',
			'creator': '',
			'recipient': ''
	}
	
	stompClient.connect({}, function () {
		let messageContent = document.getElementById("messageContent").value;
		if(!messageContent) {
			return;
		}
		
		let selectedInstrumental = recentInstrumentals.find(function (inst) {
			return inst.tidingId == selectedMember.tidingId;
		});
		
		message.content = messageContent;
		message.creator = userTidingId.id;
		message.recipient = selectedInstrumental.channelId;
		
		tiding.content = messageContent;
		tiding.sender = userTidingId.tidingId;
		
		stompClient.send(urlTemplate.replace(":recipient",selectedMember.tidingId, {}, JSON.stringify(tiding)));
		stompClient.send(saveMessageUrl, {}, JSON.stringify(message));
		
		operationCompleteAlert();
		document.getElementById("messageContent").value = "";
	});
}

const soundRowTemplate = 
	`<tr>
		<td class="text-center">:name <button class="btn btn-success" onclick="downloadSound(:id)">Download</button></td>
	</tr>`;
const selectedSoundRowTemplate = 
	`<tr class="birefy-fixed-view-selected">
		<td class="text-center">:name <button class="btn btn-default" onclick="selectTrackToPlay(:id)">Play</button>
		<button class="btn btn-success" onclick="downloadSound(:id)">Download</button>
		</td>
	</tr>`;

(function () {
	if(!userTidingId) {
		document.getElementById("contactCreator").title = "You must be logged in to contact artists";
	}
})();
