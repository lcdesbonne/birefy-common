var closeInfoPopup = function () {
	let infoBox = document.getElementById('popup-infoBox');
	infoBox.classList.add('hidden');
}

var openInfoPopup = function () {
	let infoBox = document.getElementById('popup-infoBox');
	infoBox.classList.remove('hidden');
}

//Confirm action dialogue box
{
const confirmActionInfoBox =
`
<div class="container overlay text-center" id="popup-infoBox">
	<div class="popup-subset">
		<div class="row text-center">
			<h2>Notice</h2>
		</div>
		<div class="row">
			<p>:messageContent</p>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<input class="form-control btn btn-warning" type="button" value="Yes" onclick="popupConfirmAction()">
			</div>
			<div class="col-sm-6">
				<input class="form-control btn btn-info" type="button" value="No" onclick="popupCancelAction()">
			</div>
		</div>
	</div>
</div>
`;

const confirmActionContent =
`
<div class="popup-subset">
		<div class="row text-center">
			<h2>Notice</h2>
		</div>
		<div class="row">
			<p>:messageContent</p>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<input class="form-control btn btn-warning" type="button" value="Yes" onclick="popupConfirmAction()">
			</div>
			<div class="col-sm-6">
				<input class="form-control btn btn-info" type="button" value="No" onclick="popupCancelAction()">
			</div>
		</div>
	</div>
`;


let callBackFunction;

function showYesNoConfirm(message, callback) {
	let popupContent = confirmActionInfoBox.replace(/:messageContent/g, message);
	let existingPopUpBox = document.getElementById('popup-infoBox');
			
	if (!existingPopUpBox) {
		document.body.innerHTML += popupContent;
	} else {
		existingPopUpBox.innerHTML = confirmActionContent.replace(/:messageContent/g, message);
	}
	
	openInfoPopup();
	
	callBackFunction = callback;

}

function popupConfirmAction() {
	callBackFunction();
	closeInfoPopup();
}

function popupCancelAction() {
	closeInfoPopup();
}
}


//Operation Complete dialogue
{
const operationComplete = 
`
<div class="container overlay text-center" id="popup-infoBox">
	<div class="popup-subset">
		<div class="row text-center">
			<h2>Operation Complete</h2>
		</div>
		<div class="row text-center">
			<input class="form-control btn btn-success" style="width: 30%" type="button" value="OK" onclick="proceed()">
		</div>
	</div>
</div>
`;

const operationCompleteContent =
`
<div class="popup-subset">
	<div class="row text-center">
		<h2>Operation Complete</h2>
	</div>
	<div class="row text-center">
		<input class="form-control btn btn-info" style="width: 30%" type="button" value="OK" onclick="proceed()">
	</div>
</div>
`;

let callBack;

function operationCompleteAlert(callback) {
	let existingPopUpBox = document.getElementById('popup-infoBox');
	
	if (!existingPopUpBox) {
		document.body.innerHTML += operationComplete;
	} else {
		existingPopUpBox.innerHTML = operationCompleteContent;
	}
	
	if(callback) {
		callBack = callback;
	}
	
	openInfoPopup();
}

function proceed() {
	closeInfoPopup();
	if(callBack) {
		callBack();
	}
}
}

//Operation failure alert box
{
const operationFailureDialogue =
`
<div class="container overlay text-center" id="popup-infoBox">
	<div class="row popup-subset">
		<div class="text-center">
			<h2 style="color: red">:message</h2>
			<h6>For additional assistance contact support@birefy.com</h6>
		</div>
		<div class="text-center">
			<input class="form-control btn btn-default" style="width: 30%" type="button" onclick="closeFailureDialogue()" value="Close">
		</div>
	</div>
</div>
`;

const operationFailureDialogueContent =
`
<div class="popup-subset">
	<div class="row text-center">
		<h2 style="color: red">:message</h2>
		<h6>For additional assistance contact support@birefy.com</h6>
	</div>
	<div class="row text-center">
		<input class="form-control btn btn-default" style="width: 30%" type="button" onclick="closeFailureDialogue()" value="Close">
	</div>
</div>
`;

let DEFAULT_FAILURE_MESSAGE = "Operation Failed";

let callBack;

function openFailureDialogue(message, callback) {
	let existingPopUpBox = document.getElementById('popup-infoBox');
	let messageContent = message ? message : DEFAULT_FAILURE_MESSAGE;
	
	if (!existingPopUpBox) {
		document.body.innerHTML += operationFailureDialogue.replace(/:message/g, messageContent);
	} else {
		existingPopUpBox.innerHTML = operationFailureDialogueContent.replace(/:message/g, messageContent);
	}
	
	if(callback) {
		callBack = callback;
	}
	
	openInfoPopup();
}

function closeFailureDialogue() {
	closeInfoPopup();
	
	if(callBack) {
		callBack();
	}
}
}

//General Info
{
const generalInfoDialogue =
`
<div class="container overlay text-center" id="popup-infoBox">
	<div class="popup-subset">
		<div class="row text-center">
			<h2>:message</h2>
		</div>
		<div class="row text-center">
			<input class="form-control btn btn-info" style="width: 30%" type="button" onclick="closeInfoDialogue()" value="OK">
		</div>
	</div>
</div>
`;

const generalInfoDialogueContent =
`
<div class="popup-subset">
	<div class="row text-center">
		<h2>:message</h2>
	</div>
	<div class="row text-center">
		<input class="form-control btn btn-info" style="width: 30%" type="button" onclick="closeInfoDialogue()" value="OK">
	</div>
</div>
`;

function openGeneralInfoDialogue(message) {
	let existingPopUpBox = document.getElementById('popup-infoBox');
	
	if (!existingPopUpBox) {
		document.body.innerHTML += generalInfoDialogue.replace(/:message/g, message);
	} else {
		existingPopUpBox.innerHTML = generalInfoDialogueContent.replace(/:message/g, message);
	}
	
	openInfoPopup();
}

function closeInfoDialogue() {
	closeInfoPopup();
}

}
//Legal action regarding content ownership
{
const contentOwnershipDialogue = 
`
<div class="container overlay text-center" id="popup-infoBox">
	<div class="popup-subset">
		<div class="row text-center">
			<h2>Important</h2>
		</div>
		<div class="row text-center">
			<p>Can you confirm that content you are about to upload is your own original work?</p>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<input class="form-control btn btn-success" type="button" value="Confirm" onclick="proceedOwnershipConfirm()">
			</div>
			<div class="col-sm-6">
				<input class="form-control btn btn-default" type="button" value="Cancel" onclick="ownerShipCancelAction()">
			</div>
		</div>
	</div>
</div>
`;

const contentOwnershipContent = 
`
<div class="popup-subset">
	<div class="row text-center">
		<h2>Important</h2>
	</div>
	<div class="row text-center">
		<p>Can you confirm that content you are about to upload is your own original work?</p>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<input class="form-control btn btn-success" type="button" value="Confirm" onclick="proceedOwnershipConfirm()">
		</div>
		<div class="col-sm-6">
			<input class="form-control btn btn-default" type="button" value="Cancel" onclick="ownershipCancelAction()">
		</div>
	</div>
</div>
`;

let callBackFunction;

function openContentOwnershipConfirmDialogue(confirmationAction) {
	callBackFunction = confirmationAction;
	
	let existingPopUpBox = document.getElementById('popup-infoBox');
	
	if (!existingPopUpBox) {
		document.body.innerHTML += contentOwnershipDialogue;
	} else {
		existingPopUpBox.innerHTML = contentOwnershipContent;
	}
	
	openInfoPopup();
	
}

function proceedOwnershipConfirm() {
	closeInfoPopup();
	
	callBackFunction();
}

function ownerShipCancelAction() {
	closeInfoPopup();
}

}


