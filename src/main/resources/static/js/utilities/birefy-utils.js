/*
* This file contains useful utility functions
* 
*/

/*
* List and collection functions
*/
function uniques(firstList, secondList) {
	var uniquesList = [];
	var nextList = secondList.slice();
	for (var x = 0; x < firstList.length; x++) {
		var isUnique = true;
		var value = firstList[x];
		for(var y = 0; y < nextList.length; y++) {
			if(value == nextList[y]) {
				isUnique = false;
				nextList.splice(y,1);
				--y;
				break;
			}
		}
		
		if (isUnique) {
			//Add to a unique list
			uniquesList.push(value);
		}
	}
	
	return uniquesList.concat(nextList);
}

/*
 * Date utility functions
 */

function formatLocalDateTime(localDateTimeArray) {
	return localDateTimeArray.slice(0, 3).join("-");
}

/*
 * Tabular display functions
 */

/**
 * Provides the content for the fixed view table
 * This function will attempt to populate the template with the values 
 * in the objects contained within listOfContent. The keys should
 * match the name of the placeholder in the template prefixed with 
 * a colon ':'. The row template is optional. If omitted the list 
 * of content will be expected to be a list of strings.
 * 
 * The number of rows returned will always be equal to the number
 * in view value. In the case where there are not enough elements
 * to populate the number in view empty rows will be supplied
 * 
 * @param startIndex, position in the list to begin the list
 * @param numberInView, maximum number of items to display, should be odd
 * @param listOfContent, the content to display
 * @param rowTemplate, optional template
 * @returns String
 */
function birefyFixedViewTable(startIndex, numberInView, listOfContent, rowTemplate, selectedRowTemplate) {
	if(numberInView % 2 == 0) {
		numberInView += 1;
	}
	
	const defaultRowTemplate = 
		`<tr>
			<td>:value</td>
		</tr>`;
	const emptyRow = 
		`<tr>
			<td style="opacity:0; border:none"> None </td>
		</tr>`;
	const selectedRow = 
		`<tr class="info">
			<td>:value</td>
		</td>`;
	
	let median = (numberInView/2).toFixed(0) - 1;
	
	//Relative to the whole list
	let beginIndex = (startIndex + 1) - median > 0 ? startIndex - median : 0;	
	
	//Relative to the whole list
	let endIndex = beginIndex + numberInView < listOfContent.length ? 
			beginIndex + numberInView : listOfContent.length;
	
	if (endIndex - beginIndex < numberInView && beginIndex > 0) {
		let shortfall = numberInView - (endIndex - beginIndex);
		
		beginIndex = beginIndex - shortfall > 0 ? beginIndex - shortfall : 0;
	}
	
	//Relative to the list in view
	let highlightedIndex = startIndex - beginIndex;
	
	let contentToDisplay;
	if(listOfContent.length >= numberInView) {
		contentToDisplay = listOfContent.slice(beginIndex, endIndex);
	} else {
		contentToDisplay = listOfContent.slice();
	}
	
	let tabularResult = "";
	if(!rowTemplate || !selectedRowTemplate) {
		for(let x = 0; x < contentToDisplay.length; x++) {
			let value = contentToDisplay[x];
			if(x == highlightedIndex) {
				tabularResult += selectedRow.replace(':value',value);
			} else {
				tabularResult += defaultRowTemplate.replace(':value', value);
			}
		}
	} else {
		//Identify the key values in the objects in the list
		for(let x = 0; x < contentToDisplay.length; x++) {
			let value = contentToDisplay[x];
			
			let rowToAdd
			if(x == highlightedIndex) {
				rowToAdd = selectedRowTemplate;
			} else {
				rowToAdd = rowTemplate;
			}
			
			let keysArray = Object.keys(value);
			for(let keyIndex in keysArray) {
				rowToAdd = rowToAdd.replace(new RegExp(':'+keysArray[keyIndex],'g'), value[keysArray[keyIndex]]);
			}
			
			tabularResult += rowToAdd;
		}
	}
	
	if(contentToDisplay.length < numberInView) {
		for(let x = 0; x < (numberInView - contentToDisplay.length); x++ ) {
			tabularResult += emptyRow;
		}
	}
	
	return tabularResult;
}

/**
 * Loading page functionality
 */
function ceaseLoadingPage() {
	document.getElementById("loaderBackground").style.display = "none";
}


/**
 * Display functions
 */
//Creating a floating footer
(function() {
	$('footer').css('position', $(document).height() > $(window).height() ? "inherit" : "fixed");
	
	if($(document).height() + 110 > $(window).height()) {
		$('body').css('margin-bottom', '110px');
	}
})();
