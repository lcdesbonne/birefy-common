//Functions for the concept submission page
window.onload = function () {
	ceaseLoadingPage();
}

var listOfTopics = [];
var listOfQuestions = [];
var foundTags = [];
var newTags = []; //For new tags assigned to a new topic
var existingTags = []; //For existing tags assigned to a new topic

var currentlySelectedTopic;
var currentlySelectedQuestion;

function updateTopicData() {
	//Extract the current user input data
	var topicNameText = $("#topName")["0"].value;
	
	if (!topicNameText || topicNameText.length < 3) {
		return;
	}
	
	currentlySelectedTopic = null;
	
	/*
	 * Search to see if what has been entered matches what has already 
	 * been found
	 */
	for (var i = 0; i < listOfTopics.length; i++) {
		if (topicNameText == listOfTopics[i].topicName) {
			//populate the data for the corresponding topic
			currentlySelectedTopic = listOfTopics[i].id;
			displayTopicAndQuestionData();
			return;
		}
	}
	
	//Search for all topics containing the specified text
	$.ajax({
		url: 'concepts/allTopicsByTitle?title='+topicNameText,
		type: 'GET',
		success: function (data) {
			listOfTopics = data.topics;
			listOfQuestions = data.questions;
			
			//Update the options available
			populateTopicAndQuestionOptions();
			
			//Reset the information on display
			clearAllDetail();
			//Clear the question title also
			$("#questName").val("");
		}
	});
}

function updateQuestionData() {
	//Extract the current user question input
	var questionNameText = $("#questName").val();
	
	if (!questionNameText || questionNameText.length < 3) {
		return;
	}
	
	currentlySelectedQuestion = null;
	
	/*
	 * Search to see if what has been searched for matches what
	 * has already been found
	 */
	for (var i = 0; i < listOfQuestions.length; i++) {
		if (questionNameText == listOfQuestions[i].title) {
			//populate the data for the corresponding question
			currentlySelectedQuestion = listOfQuestions[i].id;
			//Also specify the selected topic
			currentlySelectedTopic = listOfQuestions[i].topicId;
			displayTopicAndQuestionData();
			return;
		}
	}
	
	/*
	 * Search for all questions containing the specified text
	 */
	$.ajax({
		url: 'concepts/allQuestionsByTitle?title='+questionNameText,
		type: 'GET',
		success: function (data) {
			listOfQuestions = data.questions;
			listOfTopics = data.topics;
			
			//Update the options available
			populateTopicAndQuestionOptions();
			//Only clear question data
			$("#questDet").val("");
			displayTopicAndQuestionData(true);
		}
	});
}

/*
 * Updates the datalists corresponding to topics
 * and questions with current data.
 */
function populateTopicAndQuestionOptions() {
	var topicList = $("#topicData");
	var questionList = $("#questionData");
	
	if (listOfTopics.length > 0) {
		var options = "";
		//Update the options for the topic list
		for (var i = 0; i < listOfTopics.length; i++) {
			options += "<option value=\""+listOfTopics[i].topicName+"\">";
		}
		topicList.html(options);
	} else {
		topicList.html("");
	}
	
	if (listOfQuestions.length > 0) {
		var options = "";
		//Update the options for the question list
		for (var i = 0; i < listOfQuestions.length; i++) {
			options += "<option value=\""+listOfQuestions[i].title+"\">";
		}
		questionList.html(options);
	} else {
		questionList.html("");
	}
}

/*
 * Populate the data associated with the selected topic
 * and question
 */
function displayTopicAndQuestionData(preserveTopicDetail) {
	var topicDetail = $("#topDet");
	if (currentlySelectedTopic) {
		//Display the data for the topic
		for (var i = 0; i < listOfTopics.length; i++) {
			if (listOfTopics[i].id == currentlySelectedTopic) {
				topicDetail.val(listOfTopics[i].topicDetail);
				
				//Disable the submission button for the topic
				$("#subTopic").attr("disabled", true);
				//Disable the edit of topic detail
				topicDetail.attr("readonly", true);
				
				//Disable the edit and addition of tags
				$("#tgName").attr("disabled", true);
				$("#adTg").attr("disabled", true);
				
				//Search for all the tags for this topic
				$.ajax({
					url: '/admin/tagsForTopic?topicId='+ currentlySelectedTopic,
					type: 'GET',
					success: function(data) {
						existingTags = data;
						//Populate the tags for the topic
						constructTagLists();
					}
				});
				break;
			}
		}
				
	} else {
		if (!preserveTopicDetail) {
			//Clear the detail
			topicDetail.val("");
		}
		
		//Enable the submission for the topic
		$("#subTopic").attr("disabled", false);
		//Enable the edit of the topic detail
		topicDetail.attr("readonly", false);
		
		//Enable the edit and addition of tags
		$("#tgName").attr("disabled", false);
		$("#adTg").attr("disabled", false);
	}
	
	if (currentlySelectedQuestion) {
		//Display the data for the question
		var questionDetail = $("#questDet");
		
		for (var i = 0; i < listOfQuestions.length; i++) {
			if (listOfQuestions[i].id == currentlySelectedQuestion) {
				questionDetail.val(listOfQuestions[i].detail);
				
				//Disable the submission for the question
				$("#subQuest").attr("disabled", true);
				
				//Disable edit of topic detail
				questionDetail.attr("readonly", true)
				break;
			}
		}
	} else {
		var questionDetail = $("#questDet");
		//Clear the question detail
		questionDetail.val("");
		//Enable the submission for the question
		$("#subQuest").attr("disabled", false);
		//Enable edit of text box
		questionDetail.attr("readonly", false);
	}
}

function clearAllDetail() {	
	//Clear the tag lists and reactivate the submission buttons
	$("#tagDisplay").html("");
	
	currentlySelectedQuestion = null;
	currentlySelectedTopic = null;
	
	displayTopicAndQuestionData();
}


/*
 * populate tags for existing topic
 */
function displayTagsForTopic() {
	//Creates a display with no delete options
	var blankColumns = existingTags.length % 3;
	var row = "<div class=\"row\">";
	var endRow = "</div>";
	var column = "<div class=\"col-sm-2\"><label class=\"label-info conceptName full-width text-center\">--i--</label></div>";
	var emptyColumn = "<div class=\"col-sm-2\"></div>";
	var content = row;
	var endOfRow = false;
	
	if (existingTags.length == 0) {
		//Do nothing
		return
	} else {
		for (var i = 0; i < existingTags.length; i++) {
			if (i % 6 == 0 && i != 0) {
				content += column.replace("--i--",existingTags[i].tagName);
				//End the row
				content += endRow;
				endOfRow = true;
				continue;
			}
			if (endOfRow) {
				content += row;
				endOfRow = false;
			}
			content += column.replace("--i--", existingTags[i].tagName);
		}
		
		if (blankColumns > 0) {
			for (var i = 0; i < blankColumns; i++) {
				content += emptyColumn;
			}
			//End the row
			content += endRow;
		}
		
		//Append to any existing content
		var existingContent = document.getElementById("tagDisplay").innerHTML;
		$("#tagDisplay").html(existingContent + content);
	}
	
}

/*
 * Assigns both new and existing tags to a modifiable display
 * of tags for topics
 * 
 * NOTE should always be run before displayTagsForTopic
 */
function populateTagsForNewTopic() {
	//Creates a display with delete options
	var blankColumns = newTags.length % 3;
	var row = "<div class=\"row\">";
	var endRow = "</div>";
	var columnWithDelete = "<div class=\"col-sm-4\"><label>--i--<img src=\"/images/deleteCross.svg\" onclick=\"removeTagFromDisplay('--i--')\"></div>";
	var emptyColumn = "<div class=\"col-sm-4\"></div>";
	var content = row;
	var endOfRow = false;
	
	if (newTags.length == 0) {
		//Empty the tag list
		$("#tagDisplay").html("");
	} else {
		for (var i = 0; i < newTags.length; i++) {
			var tagData = newTags[i].id ? newTags[i].name : newTags[i];
		
			if (i % 3 == 0 && i != 0) {
				content += columnWithDelete.replace(/--i--/g, tagData);
				//End the row
				content += endRow;
				endOfRow = true;
				continue;
			}
			if (endOfRow) {
				content += row;
				endOfRow = false;
			}
			content += columnWithDelete.replace(/--i--/g, tagData);
		}
		
		if (blankColumns > 0) {
			for (var i = 0; i < blankColumns; i++) {
				content += emptyColumn;
			}
			//End the row
			content += endRow;
		}
		$("#tagDisplay").html(content);
	}
}

function searchTag() {
	var tagName = $("#tgName").val();
	//Check to see if this tag exists
	$.ajax({
		url: '/questions/findTags?tagName='+tagName,
		type: 'GET',
		success: function (data) {
			foundTags = data;
			//Populate the tag list options
			populateTagOptions();
		}
	});
}

function populateTagOptions() {
	var options = "";
	for (var x = 0; x < foundTags.length; x++) {
		options += '<option value="'+foundTags[x].tagName+'" />';
	}
	
	$("#tagData").html(options);
}

function selectNewTag() {
	var tName = $("#tgName").val();
	
	for (var i = 0; i < newTags.length; i++) {
		if (newTags[i].id == tName || newTags[i] == tName) {
			return;
		}
	}
	
	//Check to see if this tag is already applied to the topic
	for (var i = 0; i < existingTags.length; i++) {
		if (tName == existingTags[i].tagName) {
			return;
		}
	}
	
	//Check to see if the tag already exists in the database
	for (var i = 0; i < foundTags.length; i++) {
		if (tName == foundTags[i].tagName) {
			var tag = {
					name: tName,
					id: foundTags[i].id
			}
			newTags.push(tag);
			constructTagLists();
			return;
		}
	}
	
	newTags.push(tName);
	constructTagLists();
}


function removeTagFromDisplay(name) {
	//Search the new tags list
	//Apply functionality to remove a tag that was never part of the topic
	for (var x = 0; x < newTags.length; x++) {
		if (newTags[x].name == name || newTags[x] == name) {
			//Remove the unwanted tag
			newTags.splice(x, 1);
			break;
		}
	}
	
	//Reconstruct the tag display lists
	constructTagLists();
}

function constructTagLists() {
	populateTagsForNewTopic();
	displayTagsForTopic();
}

//Also saves tags associated with the new topic
function submitNewTopic() {
	//Can only update topics which were created by the user
	
	var topic = {
			id: 0,
			topicName: "",
			topicDetail: ""
	};
	
	if ($("#topName").val() && $("#topDet").val()) {
		topic.topicName = $("#topName").val();
		topic.topicDetail = $("#topDet").val();
		topic.id = currentlySelectedTopic;
	} else {
		return;
	}
	
	$.ajax({
		url: '/concepts/processTopic',
		contentType: 'application/json',
		method: 'POST',
		data: topic,
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
		}
	}).done(function(topicId) {
		if (topicId == 0) {
			openFailureDialogue("You dont't have permission to update the Topic.");
			location.reload();
		} else {
			//Save any tags for the topic
			addTagsToTopic(topicId);
			
			//TODO perhaps do some review of what had been done
			//Reload the page
			//location.reload();
		}
	});
}

function submitNewQuestion() {
	//TODO implement the update flow for questions created by the user
	var question = {
			id: 0,
			title: "",
			detail: "",
			topicId: 0,
			appUrl: ""
	};
	
	question.title = $("#questName").val();
	question.detail = $("#questDet").val();
	
	//Retrieve the associated topic for the question
	if (currentlySelectedTopic) {
		question.topicId = currentlySelectedTopic;
	}
	
	if (question.topicId == 0) {
		//A new topic has been created and needs to be saved
		var topic = {
				id: 0,
				topicName: "",
				topicDetail: ""
		};
		
		if ($("#topName").val() && $("#topDet").val()) {
			topic.topicName = $("#topName").val();
			topic.topicDetail = $("#topDet").val();
		} else {
			openFailureDialogue("You must specify or create a Topic.")
			return;
		}
		
		$.ajax({
			url: '/concepts/processTopic',
			contentType: 'application/json',
			method: 'POST',
			data: JSON.stringify(topic),
			headers : {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function(topicId) {
			if (topicId == 0) {
				openFailureDialogue("You do not have permission to update this topic.");
				location.reload();
				return;
			}
			//Reload the page
			
			question.topicId = topicId;
			addTagsToTopic(topicId);
			
			$.ajax({
				url: '/concepts/processQuestion',
				contentType: 'application/json',
				method: 'POST',
				data: JSON.stringify(question),
				headers: {
					'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
				}
			}).done(function(questionId) {
				if (questionId == 0) {
					openFailureDialogue("You do not have permission to update this question.");
					location.reload();
				} else {
					operationCompleteAlert();
					//Reload the page
					location.reload();
				}
			});
		});
	} else {
		$.ajax({
			url: '/concepts/processQuestion',
			contentType: 'application/json',
			method: 'POST',
			data: JSON.stringify(question),
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function(questionId) {
			if (questionId == 0) {
				openFailureDialogue("You do not have permission to update this question.");
				location.reload();
			} else {
				operationCompleteAlert();
				location.reload();
			}
		});
	}
}

function addTagsToTopic(topicId) {
	//Generate the update object
	var updateObject  = {
		topicId: 0,
		newTagNames: [],
		existingTagIds: []
	};
	
	//Cycle through the new tags
	for (var i = 0; i < newTags.length; i++) {
		var nTag = newTags[i];
		if (nTag.id) {
			updateObject.existingTagIds.push(nTag.id);
		} else {
			updateObject.newTagNames.push(nTag);
		}
	}
	
	//Retrieve the topic name from the web page and find the corresponding ID
	updateObject.topicId = topicId;
	
	//Submit the request to add the new tags
	$.ajax({
		url: '/concepts/addTagsTopic',
		type: 'POST',
		contentType: 'application/json',
		data: JSON.stringify(updateObject),
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
		}
	}).done(function() {
		//reload the page
		location.reload();
	});
}


