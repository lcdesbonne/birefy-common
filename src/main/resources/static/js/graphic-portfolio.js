window.onload = function() {
	ceaseLoadingPage();
	
	populateGraphicsTableWithAll();
	initializeSelectedImage();
	document.getElementById("imageCategory").innerHTML = populateCategoryOptions();
	displayTagDataForGraphics();
};

var currentlySelectedImage;

var updatedAddedTagMap = {};
var updatedRemovedTagMap = {};
var foundTags = [];
var newUploadTagList = [];

var graphicsUploadOwnershipConfirmAction = function () {
	document.getElementById("uploadDialogueContainer").style.display = "block";
}
function openGraphicsUpload() {
	openContentOwnershipConfirmDialogue(graphicsUploadOwnershipConfirmAction);
}

function openTagEdit(id) {
	currentlySelectedImage = id;
	displayRemovableTagsForImage(currentlySelectedImage);
	document.getElementById("updateTagsContainer").style.display = "block";
}

function cancelUploadDialogue() {
	document.getElementById("imageName").value = "";
	document.getElementById("uploadDialogueContainer").style.display = "none";
}

function closeTagEdit() {
	document.getElementById("updateTagsContainer").style.display = "none";
}

function populateGraphicsTableWithAll() {
	let tableEntries = "";
	
	let selected = false;
	
	if(userAvatars.length) {
		userAvatars.forEach(function(avatar) {
			tableEntries += imageTableRow.replace(/:id/g,avatar.id)
			.replace(/:name/g,avatar.name)
			.replace(":categories", populateCategoryOptions())
			.replace(":category", avatar.category)
			.replace(":price", avatar.price);
		});
	}
	
	if(userScenes.length) {
		userScenes.forEach(function(scene) {
			tableEntries += imageTableRow.replace(/:id/g,scene.id)
			.replace(/:name/g,scene.name)
			.replace(":categories", populateCategoryOptions())
			.replace(":category", scene.category)
			.replace(":price", scene.price);
		});
	}
	
	if(userProps.length) {
		userProps.forEach(function(prop) {
			tableEntries += imageTableRow.replace(/:id/g,prop.id)
			.replace(/:name/g,prop.name)
			.replace(":categories", populateCategoryOptions())
			.replace(":category", prop.category)
			.replace(":price", prop.price);
		});
	}
	
	if(userIcons.length) {
		userIcons.forEach(function(icon) {
			tableEntries += imageTableRow.replace(/:id/g,icon.id)
			.replace(/:name/g,icon.name)
			.replace(":categories", populateCategoryOptions())
			.replace(":category", icon.category)
			.replace(":price", icon.price);
		});
	}
	
	document.getElementById("user-graphics").innerHTML = tableEntries;
}

function initializeSelectedImage() {
	let selected = false;
	if (userAvatars.length) {
		selectImage(userAvatars[0].id);
		selected = true;
	} else if (userScenes.length && !selected) {
		selectImage(userScenes[0].id);
		selected = true;
	} else if (userProps.length && !selected) {
		selectImage(userProps[0].id);
		selected = true;
	} else if (userIcons.length && !selected) {
		selectImage(userIcons[0].id);
		selected = true;
	}	
}

function revertChanges(id) {
	let imageToRevert;
	
	imageToRevert = userAvatars.find(function(avatar) {
		return avatar.id === id;
	});
	
	if(!imageToRevert) {
		imageToRevert = userScenes.find(function (scene) {
			return scene.id === id;
		});
	}
	
	if(!imageToRevert) {
		imageToRevert = userProps.find(function (prop) {
			return prop.id === id;
		});
	}
	
	if(!imageToRevert) {
		imageToRevert = userIcons.find(function (icon) {
			return icon.id === id;
		});
	}
	
	if(imageToRevert) {
		document.getElementById("name-"+id).value = soundToRevert.name;
		document.getElementById("category-"+id).value = soundToRevert.value;
		document.getElementById("price-"+id).value = soundToRevert.value;
	}
}

function applyChangesToImage(id, deleteAction) {
	let imageModificationForm = document.getElementById("modify-"+id);
	let imageForUpdate;
	for(let x = 0; x < userAvatars.length; x++) {
		if(userAvatars[x].id == id) {
			imageForUpdate = userAvatars[x];
			break;
		}
	}
	if(!imageForUpdate) {
		for(let x = 0; x < userScenes.length; x++) {
			if(userScenes[x].id == id) {
				imageForUpdate = userScenes[x];
				break;
			}
		}
	}
	
	if(!imageForUpdate) {
		for(let x = 0; x < userProps.length; x++) {
			if(userProps[x].id == id) {
				imageForUpdate = userProps[x];
				break;
			}
		}
	}
	
	if(!imageForUpdate) {
		for(let x = 0; x < userIcons.length; x++) {
			if(userIcons[x].id == id) {
				imageForUpdate = userIcons[x];
				break;
			}
		}
	}
	
	if(!imageForUpdate) {
		openFailureDialogue("An error has occurred unable to apply changes.");
		return;
	}
	
	if(!deleteAction) {
		//Apply update action
		showYesNoConfirm("Are you sure you want to update "+imageForUpdate.name, function() {
			let newName = document.getElementById("name-"+id).value;
			let newCategory = document.getElementById("category-"+id).value;
			let newPrice = document.getElementById("price-"+id).value;
			
			let imageUpdate = {};
			imageUpdate.paintingId = id;
			imageUpdate.newPaintingName = newName;
			imageUpdate.newCategory = newCategory;
			imageUpdate.addedTags = updatedAddedTagMap[id];
			imageUpdate.removedTags = updatedRemovedTagMap[id];
			imageUpdate.price = newPrice;
			
			$.ajax({
				url: 'graphic-portfolio/update-painting',
				method: 'POST',
				contentType: 'application/json',
				data: JSON.stringify(imageUpdate),
				headers: {
					'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
				}
			}).done(function (result) {
				if (result === true) {
					operationCompleteAlert();
					window.location.reload();
				} else {
					openFailureDialogue();
				}
			}); 
		});
		
	} else if (deleteAction == true) {
		//Apply the delete action
		showYesNoConfirm("Are you sure you want to delete "+ imageForUpdate.name, function (){
			$.ajax({
				url: 'graphic-portfolio/delete-painting',
				method: 'POST',
				contentType: 'application/json',
				data: JSON.stringify(id),
				headers: {
					'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
				}
			}).done(function (result){
				if (result === true) {
					operationCompleteAlert();
					window.location.reload();
				} else {
					openFailureDialogue("Operation failed");
				}
			});
		});
	}
}

function populateCategoryOptions() {
	let options = "";
	for(let optionIndex in categories) {
		options += selectOptionTemplate.replace(/:value/g, categories[optionIndex]);
	}
	return options;
};

function selectImage(id) {
	let selectedImage = findImageByIdFromLoadedImages(id);

	if(currentlySelectedImage) {
		let previouslySelectedImage = document.getElementById("row-"+currentlySelectedImage);
		if(previouslySelectedImage) {
			previouslySelectedImage.classList.remove("birefy-fixed-view-selected");
		}
	}
	
	if(selectedImage) {
		document.getElementById("row-"+id).setAttribute("class", "birefy-fixed-view-selected");
		currentlySelectedImage = id;
		
		//Add the image to the view
		document.getElementById("selected-graphic").setAttribute("src", paintingLocationUrlTemplate.replace(":id", id));
	}
}

function findImageByIdFromLoadedImages(id) {
	let image;
	for(let x = 0; x < userAvatars.length; x++) {
		if(userAvatars[x].id == id) {
			image = userAvatars[x];
			break;
		}
	}
	if(!image) {
		for(let x = 0; x < userScenes.length; x++) {
			if(userScenes[x].id == id) {
				image = userScenes[x];
				break;
			}
		}
	}
	
	if(!image) {
		for(let x = 0; x < userProps.length; x++) {
			if(userProps[x].id == id) {
				image = userProps[x];
				break;
			}
		}
	}
	
	if(!image) {
		for(let x = 0; x < userIcons.length; x++) {
			if(userIcons[x].id == id) {
				image = userIcons[x];
				break;
			}
		}
	}
	return image;
}

function displayNewImageTagList() {
	elementId = "new-upload-tag-list";
	if (!newUploadTagList || !newUploadTagList.length) {
		document.getElementById(elementId).innerHTML = "<p class=\"text-center\"><label>None Selected</label></p>";
		return;
	}
	
	var blankColumns = newUploadTagList.length % 3;
	var row = "<div class=\"row\">";
	var endRow = "</div>";
	var column = "<div class=\"col-sm-4\"><label>--i--</label><button class=\"btn\" onclick=\"removeTagFromNewUpload('--i--')\">X</button></div>";
	var emptyColumn = "<div class=\"col-sm-4\"></div>";
	var content = row;
	var endOfRow = false;
	
	for(let i = 0; i < newUploadTagList.length; i++) {
		if (i % 3 == 0 && i != 0) {
			content += column.replace(/--i--/g, newUploadTagList[i].tag);
			
			//End the row
			content += endRow;
			endOfRow = true;
			continue;
		}
		if (endOfRow) {
			content += row;
			endOfRow = false;
		}
		
		content += column.replace(/--i--/g, newUploadTagList[i].tag);	
	}
	
	if (blankColumns > 0) {
		for (var i = 0; i < blankColumns; i++) {
			content += emptyColumn;
		}
		//End the row
		content += endRow;
	}
	
	document.getElementById(elementId).innerHTML = content;
}

function displayRemovableTagsForImage(imageId) {
	elementId = "selected-tags-for-graphic";
	
	let tagList = getCurrentTagsForPainting(imageId);
	
	if (!tagList || !tagList.length) {
		document.getElementById(elementId).innerHTML = "<p class=\"text-center\"><label>None Selected</label></p>";
		return;
	}
	
	var blankColumns = tagList.length % 3;
	var row = "<div class=\"row\">";
	var endRow = "</div>";
	var column = "<div class=\"col-sm-4\"><label>--i--</label><button class=\"btn\" onclick=\"removeTagFromImage(--id--, '--i--')\">X</button></div>";
	var emptyColumn = "<div class=\"col-sm-4\"></div>";
	var content = row;
	var endOfRow = false;
	
	for(var i = 0; i < tagList.length; i++) {
		if (i % 3 == 0 && i != 0) {
			content += column.replace(/--i--/g, tagList[i].tag).replace(/--id--/g, imageId);
			
			//End the row
			content += endRow;
			endOfRow = true;
			continue;
		}
		if (endOfRow) {
			content += row;
			endOfRow = false;
		}
		
		content += column.replace(/--i--/g, tagList[i].tag).replace(/--id--/g, imageId);	
	}
	
	if (blankColumns > 0) {
		for (var i = 0; i < blankColumns; i++) {
			content += emptyColumn;
		}
		//End the row
		content += endRow;
	}
	
	document.getElementById(elementId).innerHTML = content;
}

function addTagToNewImage() {
	let tagName = document.getElementById("new-image-tag-to-add").value;
	
	if(newUploadTagList.find(function(tag) {
		return tag.tag == tagName;
	})) {
		return;
	}
	
	let foundTag = foundTags.find(function (tag) {
		return tag.tag == tagName;
	});
	
	if(foundTag) {
		newUploadTagList.push(foundTag);
	} else {
		let newTag = {
				id: null,
				tag: tagName
		};
		newUploadTagList.push(newTag);
	}
	
	displayNewImageTagList();
	document.getElementById("new-image-tag-to-add").value = "";
	document.getElementById("tagString").value = JSON.stringify(newUploadTagList);
}

function addTagToImage() {
	let imageId = currentlySelectedImage;
	let existingTagList = tagsForPaintings[imageId];
	let addedTagList = updatedAddedTagMap[imageId];
	let removedTagList = updatedRemovedTagMap[imageId];
	
	let tagName = document.getElementById("image-tag-to-add").value;
	
	if((existingTagList && existingTagList.some(function (tag) {
		return tag.tag == tagName;
	})) || (addedTagList && addedTagList.some(function (tag) {
		return tag.tag == tagName;
	}))) {
		return;
	}
	
	if(!addedTagList) {
		addedTagList = [];
	}
	
	//Add the new tag
	let foundTag = foundTags.find(function (tag) {
		return tag.tag == tagName;
	});
	
	if(foundTag) {
		addedTagList.push(foundTag);
	} else {
		let newTag = {
				id: null,
				tag: tagName
		};
		
		addedTagList.push(newTag);
	}
	
	updatedAddedTagMap[imageId] = addedTagList;
	
	displayRemovableTagsForImage(imageId);
	displayTagDataForGraphics();
	document.getElementById("image-tag-to-add").value = "";
}

function removeTagFromNewUpload(tagName) {
	for (index in newUploadTagList) {
		if(newUploadTagList[index].tag == tagName) {
			newUploadTagList.splice(index, 1);
			break;
		}
	}
	displayNewImageTagList();
}

function removeTagFromImage(imageId, tagName) {
	let existingTagList = tagsForPaintings[imageId];
	let addedTagList = updatedAddedTagMap[imageId];
	let removedTagList = updatedRemovedTagMap[imageId];
	
	if(!removedTagList) {
		removedTagList = [];
	}
	
	//Identify if the tag is in the added tag list
	if(addedTagList) {
		for(let index in addedTagList) {
			if(addedTagList[index].tag == tagName) {
				addedTagList.splice(index, 1);
				break;
			}
		}
	}
	
	if(existingTagList) {
		if(existingTagList.some(function (tag) {
			return tag.tag == tagName;
		})) {
			let tagToRemove = existingTagList.find(function (tag) {
				return tag.tag == tagName;
			});
			
			removedTagList.push(tagToRemove);
		}
	}
	
	updatedAddedTagMap[imageId] = addedTagList;
	removedTagList[imageId] = removedTagList;
	
	//Re-construct the tag selection on the update tag view
	displayRemovableTagsForImage(imageId);
	displayTagDataForGraphics();
}

function searchForTagsForNewImage() {
	let tagName = document.getElementById("new-image-tag-to-add").value;
	
	if(tagName.length < 2) {
		return;
	}
	
	$.ajax({
		url: '/birefy-galleries/search-painting-tags?tag='+tagName,
		method: 'GET',
		success: function (results) {
			foundTags = results;
			populateNewUploadTagSuggestions();
		}
	});
}

function searchForTagsForImage() {
	let tagName = document.getElementById("image-tag-to-add").value;
	
	if(tagName.length < 2) {
		return;
	}
	
	$.ajax({
		url: '/birefy-galleries/search-painting-tags?tag='+tagName,
		method: 'GET',
		success: function (results) {
			foundTags = results;
			populateTagSuggestions();
		}
	});
}

function populateNewUploadTagSuggestions() {
	let option = "<option>:value</option>";
	
	let tagOptions = "";
	foundTags.forEach(function (tag) {
		tagOptions += option.replace(":value",tag.tag);
	});
	
	document.getElementById("new-image-tag-suggestions").innerHTML = tagOptions;
}

function populateTagSuggestions() {
	let option = "<option>:value</option>";
	
	let tagOptions = "";
	foundTags.forEach(function (tag) {
		tagOptions += option.replace(":value", tag.tag);
	});
	
	document.getElementById("image-tag-suggestions").innerHTML = tagOptions;
}

function displayTagDataForGraphics() {
	let currentTagList = getCurrentTagsForPainting(currentlySelectedImage);
	
	let currentTagNames = [];
	currentTagList.forEach(function (tag) {
		currentTagNames.push(tag.tag);
	});
	
	document.getElementById("tagList-"+currentlySelectedImage).innerHTML = currentTagNames.join();
	
}

function getCurrentTagsForPainting(paintingId) {
	let paintingTags = tagsForPaintings[paintingId];
	let addedTags = updatedAddedTagMap[paintingId];
	let removedTags = updatedRemovedTagMap[paintingId];
	
	let currentTagList = [];
	
	if(paintingTags) {
		paintingTags.forEach(function(tag) {
			if(!removedTags || (removedTags && !removedTags.some(function(t) {
				return t.tag == tag.tag;
			}))) {
				currentTagList.push(tag);
			}
		});
	}
	
	if(addedTags) {
		currentTagList = currentTagList.concat(addedTags);
	}
	
	return currentTagList;
}

function verifySoftware() {
	let userSoftwareList = document.getElementById("softwareInput").value;
	
	if (userSoftwareList && userSoftwareList.length > 0) {
		document.getElementById("uploadGraphic").disabled = false;
		document.getElementById("software-error").style.display = "none";
	} else {
		document.getElementById("uploadGraphic").disabled = true;
		document.getElementById("software-error").style.display = "block";
	}
}

const imageTableRow = 
`<tr id="row-:id" onclick="selectImage(:id)" style="cursor:pointer"><td>
	<form id="modify-:id" class="form" action="javascript:;" onsubmit="applyChangesToImage(:id)">
		<div class="form-group">
			<input class="form-control" id="name-:id" value=":name" placeholder="Graphic Title"/>
		</div>
		<div class="form-group form-inline"> 
			<select class="form-control" id="category-:id" selected=":category">:categories</select>
			<label class="form-control" style="border: none; box-shadow: none; background: none">Sell Price (£)</label>
			<input class="form-control" id="price-:id" type="number" value=":price"
				placeholder="0.00" min="0" step="0.01"/>
		</div>
		<div class="form-group form-inline">
			<label class="form-control" style="border: none; box-shadow: none; background: none">Tags: </label>
			<text id="tagList-:id" class="form-control" style="border: none; box-shadow: none; background: none"></text>
		</div>
		<div class="form-inline">
			<div class="form-group">
				<input name="imageAction" class="btn btn-warning" type="submit" value="Update"/>
				<input name="imageAction" class="btn btn-danger" type="button" onclick="applyChangesToImage(:id, true)" value="Delete"/>
				<input class="btn btn-default" type="button" onclick="openTagEdit(:id)" value="Revert"/>
				<input class="btn btn-warning" type="button" value="Edit Tags" onclick="openTagEdit()"/>
			</div>
		</div>
	</form>
</td></tr>`;

const selectOptionTemplate = "<option value=\":value\">:value</option>";
	
const paintingLocationUrlTemplate = applicationDomain+"/birefy-galleries/stream-painting?id=:id";
	