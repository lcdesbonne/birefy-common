function populateMemberSuggestions(isCustomer) {
	let tidingId;
	if(isCustomer) {
		tidingId = document.getElementById("customerTidingId").value;
		document.getElementById("customerId").value = null;
		
		document.getElementById("supplierId").value = user.memberId;
		document.getElementById("supplierTidingId").value = user.tidingId;
	} else {
		tidingId = document.getElementById("supplierTidingId").value;
		document.getElementById("supplierId").value = null;
		
		document.getElementById("customerId").value = user.memberId;
		document.getElementById("customerTidingId").value = user.tidingId;
	}
	
	if(tidingId.length > 1) {
		$.ajax({
			url: "/create-deal/search-members?tidingId="+tidingId,
			type: 'GET',
			success: function (members) {
				let selectedMember = members.find(function (member) {
					return member.tidingId == tidingId;
				});
				
				if(selectedMember) {
					if(isCustomer) {
						document.getElementById("customerId").value = selectedMember.memberId;
						document.getElementById("customerTidingId").value = selectedMember.tidingId;
					} else {
						document.getElementById("supplierId").value = selectedMember.memberId;
						document.getElementById("supplierTidingId").value = selectedMember.tidingId;
					}
				}
				
				populateMemberOptions(isCustomer, members);
			}
		});
	}
}

function populateMemberOptions(isCustomer, memberList) {
	let memberOptions = "";
	
	memberList.forEach(function (member) {
		memberOptions += optionTemplate.replace(":value", member.tidingId);
	});
	
	if(isCustomer) {
		document.getElementById("customer-suggestions").innerHTML = memberOptions;
	} else {
		document.getElementById("supplier-suggestions").innerHTML = memberOptions;
	}
}

const optionTemplate = "<option>:value</option>";

