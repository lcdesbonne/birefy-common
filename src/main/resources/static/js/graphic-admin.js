window.onload = function () {	
	populateCategoryOptions();
	populateImageTable(allRecentImages);
}

var foundPainters = [];
var selectedImageId;
var currentTidingTarget;

function populateImageTable(imageList) {
	let tableEntries = "";
	selectedImageId = null;
	
	imageList.forEach(function (image) {
		tableEntries += paintingTableRowTemplate.replace(/:id/g,image.id)
			.replace(/:category/g, image.category)
			.replace(/:tidingId/g, image.tidingId)
			.replace(/:name/g, image.name);
	});
	
	document.getElementById("graphics-list").innerHTML = tableEntries;
	
	if(imageList.length) {
		//select the first image
		selectImage(imageList[0].id);
	}
}

function deletePainting(id) {
	//Apply the delete action
	showYesNoConfirm("Are you sure you want to delete this image?", function () {
		$.ajax({
			url: 'graphics-admin/delete',
			method: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(id),
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function (result){
			if (result === true) {
				operationCompleteAlert();
				//Populate the page with all paintings
				findAllPaintings();
			} else {
				openFailureDialogue("Issue deleting image.");
			}
		});
	});
}

function approvePainting(id) {
	$.ajax({
		url: 'graphics-admin/approve',
		method: 'POST',
		contentType: 'application/json',
		data: JSON.stringify(id),
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
		}
	}).done(function (result){
		if (result === true) {
			operationCompleteAlert();
			findPaintingsForReview();
		} else {
			openFailureDialogue("Issue approving painting.");
		}
		
	})
}

function openSearchDialogue() {
	document.getElementById("searchDialogue").style.display = "block";
}

function cancelSearchDialogue() {
	document.getElementById("searchDialogue").style.display = "none";
}

function populateCategoryOptions() {
	let optionList = "";
	categories.forEach(function (category) {
		optionList += optionTemplate.replace(":value", category);
	});
	
	optionList += optionTemplate.replace(":value", "ALL");
	document.getElementById("category-select").innerHTML = optionList;
}

function searchForPaintings() {
	let tidId = document.getElementById("tiding-id-search").value;
	let category = document.getElementById("category-select").value;
	let imageTitle = document.getElementById("image-title-search").value;
	
	let painter = foundPainters.find(function (painter) {
		return painter.tidingId == tidId;
	});
	
	let memberId;
	if(painter) {
		memberId = painter.memberId;
	}
	
	category = category == "ALL" ? null : category;
	
	if (!memberId && !imageTitle && category) {
		findImagesByCategory(category);
		
	} else if (memberId && !imageTitle && category) {
		findImagesByPainterAndCategory(tidingId, memberId, category);
		
	} else if (!memberId && imageTitle && category) {
		findImageByTitleAndCategory(imageTitle, category);
		
	} else if (memberId && imageTitle && category) {
		findImagesByTitleCategoryAndPainter(imageTitle, category,tidingId, memberId);
		
	} else if (!memberId && !imageTitle && !category) {
		findAllPaintings();
		
	} else if (memberId && !imageTitle && !category) {
		findImagesByPainter(memberId, tidingId);
		
	} else if (!memberId && imageTitle && !category) {
		findPaintingByTitle(imageTitle);
		
	} else if (memberId && imageTitle && !category) {
		findPaintingsByArtistWithTitle(tidingId, imageTitle);
		
	}
	
	document.getElementById("searchDialogue").style.display = "none";
}

function findAllPaintings() {	
	$.ajax({
		url: "birefy-galleries/all-paintings",
		method: 'GET',
		success: function (paintings) {
			populateImageTable(paintings);
		}
	});
}

function findPaintingsForReview() {
	$.ajax({
		url: "graphics-admin/admin",
		method: 'GET',
		success: function (paintings) {
			populateImageTable(paintings);
		}
	});
}

function findPaintingsByArtistWithTitle(memberId, imageTitle) {	
	$.ajax({
		url: "/birefy-galleries/paintings-by-artist-title?painter="+memberId+"&title="+imageTitle,
		method: 'GET',
		success: function (paintings) {
			populateImageTable(paintings);
		}
	});
}

function findPaintingByTitle(title) {	
	$.ajax({
		url: "/birefy-galleries/paintings-by-title?title="+title,
		method: 'GET',
		success: function (paintings) {
			populateImageTable(paintings);
		}
	});
}

function findImagesByPainter(memberId, tidingId) {	
	$.ajax({
		url: "/birefy-galleries/artwork-by-painter?painter="+memberId,
		method: 'GET',
		success: function (paintings) {
			populateImageTable(paintings);
		}
	});
}

function findImagesByCategory(category) {	
	$.ajax({
		url: "/birefy-galleries/paintings-by-category?category="+category,
		method: 'GET',
		success: function (paintings) {
			populateImageTable(paintings);
		}
	})
}

function findImagesByTitleCategoryAndPainter(title, category, tidingId, memberId) {	
	$.ajax({
		url: "/birefy-galleries/search-by-title-category-and-artist?title="+title+"&category="+category+"&painter="+memberId,
		method: 'GET',
		success: function (paintings) {
			populateImageTable(paintings);
		}
	});
}

function findImagesByPainterAndCategory(tidingId, memberId, category) {	
	$.ajax({
		url: "/birefy-galleries/all-images-by-painter-category?painter="+memberId+"&category="+category,
		method: 'GET',
		success: function (paintings) {
			populateImageTable(paintings);
		}
	})	
}

function findImageByTitleAndCategory(title, category) {
	//Populate the search criteria	
	$.ajax({
		url:"/birefy-galleries/find-image-by-title-and-category?title="+title+"&category="+category,
		method: 'GET',
		success: function (paintings) {
			populateImageTable(paintings);
		}
	});
}

function findPainters() {
	let tidingId = document.getElementById("tiding-id-search").value;
	$.ajax({
		url: '/birefy-galleries/search-painters?name='+tidingId,
		method: 'GET',
		success: function (painters) {
			foundPainters = painters;
			populateArtistSuggestions(painters);
		}
	});
}

function populateArtistSuggestions(painters) {
	let options = "";
	
	painters.forEach(function (painter) {
		options += optionTemplate.replace(":value", painter.tidingId);
	});
	
	document.getElementById("artist-list").innerHTML = options;
}

function openMessageDialogueForPainter(tidingId){
	document.getElementById("contact-header").innerHTML = "Contact " + tidingId;
	document.getElementById("message-dialogue").style.display = "block";
	currentTidingTarget = tidingId;
}

function closeMessageDialogue() {
	document.getElementById("message-dialogue").style.display = "none";
}

function selectImage(id) {
	if(selectedImageId) {
		//Remove the highlighting from the row
		document.getElementById("row-" + selectedImageId).classList.remove("info");
	}
	
	selectedImageId = id;
	let selectedImage = document.getElementById("selected-graphic");
	let selectedImageRow = document.getElementById("row-"+id);
	selectedImage.setAttribute("src", paintingLocationUrlTemplate.replace(":id", id));
	selectedImageRow.setAttribute("class", "info");
}

function sendMessageToArtist() {
	if(currentTidingTarget) {
		let tidingId = currentTidingTarget;
		let socket = new SockJS('/chatsocket');
		let stompClient = Stomp.over(socket);
		
		let saveMessageUrl = "/routing/saveMessage";
		let urlTemplate = "/direct/tidings/:recipient";
		
		let tiding = {
				'type': 'CHAT',
				'content': "",
				'sender': ""
		}
		
		let message = {
				'content': '',
				'creator': '',
				'recipient': ''
		}
		
		stompClient.connect({}, function () {
			let messageContent = document.getElementById("messageContent").innerHTML;
			message.content = messageContent;
			message.creator = userTidingId;
			message.recipient = tidingId;
			
			tiding.content = messageContent;
			tiding.sender = userTidingId;
			
			stompClient.send(urlTemplate.replace(":recipient",tidingId, {}, JSON.stringify(tiding)));
			stompClient.send(saveMessageUrl, {}, JSON.stringify(message));
			
			operationCompleteAlert();
			document.getElementById("messageContent").innerHTML = "";
		});
	}
}

const optionTemplate = "<option>:value</option>";

const paintingTableRowTemplate = 
	`<tr id="row-:id" onclick="selectImage(:id)"><td>
	<form id="modify-:id" class="form" action="javascript:;" onsubmit="deletePainting(:id)">
		<div class="form-group">
			<input class="form-control" id="name-:id" value=":name" placeholder="Graphic Title"/>
			<input class="form-control" id="category-:id" value=":category" />
		</div>
		<div class="form-inline">
			<div class="form-group">
				<input class="btn btn-success" type="button" value="Approve" onclick="approvePainting(:id)">
				<input  class="btn btn-danger" type="submit" value="Delete">
				<input class="btn btn-default" type="button" onclick="openMessageDialogueForPainter(':tidingId')" value="Contact :tidingId">
			</div>
		</div>
	</form>
</td></tr>`;

const paintingLocationUrlTemplate = applicationDomain+"/birefy-galleries/stream-painting?id=:id";

