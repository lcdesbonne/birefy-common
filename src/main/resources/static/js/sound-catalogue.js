window.onload = function () {
	ceaseLoadingPage();
	
	populateSoundTableWithAll();
	document.getElementById("soundCategory").innerHTML = populateCategoryOptions();
}

var updatingSoundId;
var playingSoundId;
//Map of soundIds to tagIds to add 
var addedTagUpdates = {};
//Map of soundIds to tagIds to remove
var removedTagUpdates = {};

var foundTags = [];

var soundUploadLegalActionCallback = function() {
	document.getElementById("uploadDialogueContainer").style.display = "block";
}
function openSoundUpload() {
	openContentOwnershipConfirmDialogue(soundUploadLegalActionCallback);
}

function cancelUploadDialogue() {
	document.getElementById("soundName").value = "";
	document.getElementById("uploadDialogueContainer").style.display = "none";
}

function openEditTagDialogue(soundId, soundName) {
	updatingSoundId = soundId;
	document.getElementById("editTagListContainer").style.display = "block";
	if (soundName) {
		document.getElementById("tagEditHeader").innerHTML = "Editing Tags For '" + soundName + "'";		
	} else {
		document.getElementById("tagEditHeader").innerHTML = "Editing Tag List";
	}
	
	if(tagsForSounds[soundId]) {
		document.getElementById("modifiableTagDisplay").innerHTML = displayModifiableTagList(currentSoundTagList(soundId));		
	} else {
		document.getElementById("modifiableTagDisplay").innerHTML = "No Tags...";
	}
}

function closeEditTagDialogue() {
	document.getElementById("editTagListContainer").style.display = "none";
}

function populateSoundTableWithAll() {
	let tableEntries = "";
	
	if(userInstrumentals.length) {
		userInstrumentals.forEach(function (instrumental) {
			tableEntries += soundForm.replace(/:id/g,instrumental.id)
			.replace(":categories",populateCategoryOptions(instrumental.category))
			.replace(/:name/g, instrumental.name)
			.replace(":tagList", currentlySelectedTagString(instrumental.id))
			.replace(":price", instrumental.price);
		});
	}
	
	if(userEffects.length) {
		userEffects.forEach(function (effect) {
			tableEntries += soundForm.replace(/:id/g,effect.id)
			.replace(":categories",populateCategoryOptions(effect.category))
			.replace(/:name/g, effect.name)
			.replace(":tagList", currentlySelectedTagString(effect.id))
			.replace(":price", effect.price);
		})
	}
	
	document.getElementById("user-sounds").innerHTML = tableEntries;
}

function revertChanges(id) {
	let soundToRevert;
	
	soundToRevert = userInstrumentals.find(function(sound) {
		return sound.id === id;
	});
	
	if(!soundToRevert) {
		soundToRevert = userEffects.find(function (effect) {
			return effect.id === id;
		});
	}
	
	if(soundToRevert) {
		document.getElementById("name-"+id).value = soundToRevert.name;
		document.getElementById("category-"+id).value = soundToRevert.value;
		document.getElementById("price-"+id).value = soundToRevert.value;
		
		if(addedTagUpdates[soundToRevert]) {
			addedTagUpdates[soundToRevert] = [];
		}
		
		if(removedTagUpdates[soundToRevert]) {
			removedTagUpdates[soundToRevert] = [];
		}
		
		displayCurrentlySelectedTagsForSound(soundToRevert);
	}
}

function currentlySelectedTagString(soundId) {
let currentTags = currentSoundTagList(soundId);
	
	let selectedTags = "";
	if(currentTags.length) {
		currentTags.forEach(function (tag) {
			selectedTags += tag.tag + ",";
		});
	}
	
	if(!selectedTags) {
		selectedTags = "No tags.";
	}
	return selectedTags;
}
function displayCurrentlySelectedTagsForSound(soundId) {
	document.getElementById("taglist-"+soundId).innerHTML = currentlySelectedTagString(soundId);
}

function applyChangesToSound(id, deleteSound) {
	let soundModificationForm = document.getElementById("modify-"+id);
	
	let newName = document.getElementById("name-"+id).value;
	let newCategory = document.getElementById("category-"+id).value;
	let newPrice = document.getElementById("price-"+id).value;
	
	let soundForUpdate;
	for(let x = 0; x < userInstrumentals.length; x++) {
		if(userInstrumentals[x].id == id) {
			soundForUpdate = userInstrumentals[x];
			break;
		}
	}
	if(!soundForUpdate) {
		for(let x = 0; x < userEffects.length; x++) {
			if(userEffects[x].id == id) {
				soundForUpdate = userEffects[x];
				break;
			}
		}
	}
	
	if(!soundForUpdate) {
		openFailureDialogue("An error has occurred unable to apply changes.");
		return;
	}
	
	if(!deleteSound) {
		//Apply update action
		showYesNoConfirm("Are you sure you want to update "+soundForUpdate.name, function() {
			
			let soundUpdate = {};
			soundUpdate.soundId = id;
			soundUpdate.newSoundName = newName;
			soundUpdate.newCategory = newCategory;
			soundUpdate.addedTags = addedTagUpdates[id];
			soundUpdate.removedTags = removedTagUpdates[id];
			soundUpdate.price = newPrice;
			
			$.ajax({
				url: 'sound-catalogue/update-sound',
				method: 'POST',
				contentType: 'application/json',
				data: JSON.stringify(soundUpdate),
				headers: {
					'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
				}
			}).done(function (result) {
				if (result === true) {
					operationCompleteAlert(window.location.reload());
					window.location.reload();
				} else {
					openFailureDialogue("Operation Failed.");
				}
			}); 
		});
		
	} else if (deleteSound) {
		//Apply the delete action
		showYesNoConfirm("Are you sure you want to delete "+ soundForUpdate.name, function () {
			$.ajax({
				url: 'sound-catalogue/delete-sound',
				method: 'POST',
				contentType: 'application/json',
				data: JSON.stringify(id),
				headers: {
					'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
				}
			}).done(function (result){
				if (result === true) {
					operationCompleteAlert(window.location.reload());
					
				} else {
					openFailureDialogue("Operation failed");
				}
			});
		});
	}
}

function displayModifiableTagList(tagList, upload) {
	let openRow = `<div class="row">`;
	let tagTemplate = upload ? `<div class="col-md-3">
			<label>:tagName<button class="btn" style="color: black" onclick="removeTagFromNewUpload(':tagName')">X</button>
			</div>` : 
		`<div class="col-md-3">
	<label>:tagName<button class="btn" style="color: black" onclick="removeTagFromSound(:soundId, ':tagName')">X</button>
	</div>`;
	let emptyTagTemplate = `<div class="col-md-3"></div>`;
	let endRow = `</div>`;
	
	let emptyCols = tagList.length < 4 ? 4 - tagList.length : tagList.length % 4;
	
	let modifiableTagList = "";
	let columnCounter = 0;
	tagList.forEach(function (tag) {
		if(columnCounter  == 0) {
			modifiableTagList += openRow;
		}
		
		if(columnCounter == 4) {
			modifiableTagList += endRow;
		}
		
		modifiableTagList += tagTemplate.replace(/:tagName/g, tag.tag).replace(":soundId",updatingSoundId);
		columnCounter++
	});
	
	if(emptyCols) {
		let emptyCount = 0;
		
		while(emptyCount < emptyCols) {
			modifiableTagList += emptyTagTemplate;
			emptyCount += 1;
		}
		
		modifiableTagList += endRow;
	}
	return modifiableTagList;
}

function searchForTagsByName(tagUpdateList) {
	let tagName = tagUpdateList ? document.getElementById("findTagInput").value : document.getElementById("uploadTagInput").value;
	
	if(!tagName || tagName.length < 2) {
		return;
	}
	
	$.ajax({
		url: 'sound/search-tags-by-name?tagName='+tagName,
		method: 'GET',
		success: function (tags) {
			foundTags = tags ? tags : [];
			populateTagSuggestions(tagUpdateList);
		}
	});
}

function addTagToNewUpload() {
	let tagName = document.getElementById("uploadTagInput").value;
	let existingTag = foundTags.find(function (exTag) {
		return exTag.tag == tagName;
	});
	
	if (existingTag && !tagsForUpload.find(function (t) {
		return t.tag == existingTag.tag
	})) {
		tagsForUpload.push(existingTag);	
		
	} else if(!tagsForUpload.find(function (t) {
		return t.tag == tagName;
	})){
		let tagToAdd = {
				"tag": tagName
		};
		
		tagsForUpload.push(tagToAdd);
	}
	
	document.getElementById("formTagUploadList").value = JSON.stringify(tagsForUpload);
	document.getElementById("uploadEditTagDisplay").innerHTML = displayModifiableTagList(tagsForUpload, true);
	document.getElementById("uploadTagInput").value = "";
}

function removeTagFromNewUpload(tagName) {
	for(let index in tagsForUpload) {
		if(tagsForUpload[index].tag == tagName) {
			tagsForUpload.splice(index, 1);
			break;
		}
	}
	
	document.getElementById("formTagUploadList").value = JSON.stringify(tagsForUpload);
	document.getElementById("uploadEditTagDisplay").innerHTML = displayModifiableTagList(tagsForUpload);
}

function currentSoundTagList(soundId) {
	let currentSelectedTags = [];
	let existingTagsForSound = tagsForSounds[soundId];
	let addedTags = addedTagUpdates[soundId];
	let removedTags = removedTagUpdates[soundId];
	
	if(addedTags) {
		addedTags.forEach(function (tag) {
			currentSelectedTags.push(tag);
		});
	}
	
	if(existingTagsForSound) {
		existingTagsForSound.forEach(function (tag) {
			if(removedTags && !removedTags.some(function (remTag) {
				return remTag.tag == tag.tag;
			})) {
				currentSelectedTags.push(tag);
			} else if (!removedTags) {
				currentSelectedTags.push(tag);
			}
		});
	}
	
	return currentSelectedTags;
}

function addTagToSound() {
	let soundId = updatingSoundId;
	let tagName = document.getElementById("findTagInput").value;
	let removedTagList = removedTagUpdates[soundId];
	let addedTagList = addedTagUpdates[soundId];
	let existingTags = tagsForSounds[soundId];
	
	if(removedTagList && removedTagList.length &&
			removedTagList.find(function (tag) {
				return tag.tag == tagName;
			})) {
		for(let index in removedTagList) {
			if(removedTagList[index].tag == tagName) {
				removedTagList.splice(index, 1);
				break;
			}
		}
	}
	
	if(existingTags && existingTags.find(function (tag) {
		return tag.tag == tagName;
	})) {
		return;
	}
	
	if(addedTagList && !addedTagList.find(function (tag) {
		return tag.tag == tagName;
	})) {
		let tagToAdd = foundTags.find(function (tag) {
			return tag.tag == tagName;
		});
		
		if(!addedTagList) {
			addedTagList = [];
		}
		if(tagToAdd) {
			addedTagList.push(tagToAdd);
		} else {
			addedTagList.push({id:null, tag:tagName})
		}
	} else if(!addedTagList) {
		addedTagList = [];
		addedTagList.push({id:null, tag:tagName});
	}
	
	removedTagUpdates[soundId] = removedTagList;
	addedTagUpdates[soundId] = addedTagList;
	
	document.getElementById("modifiableTagDisplay").innerHTML = 
		displayModifiableTagList(currentSoundTagList(soundId));
	displayCurrentlySelectedTagsForSound(soundId);
	document.getElementById("findTagInput").value = "";
}

function removeTagFromSound(soundId, tagName) {
	let removedTagList = removedTagUpdates[soundId];
	let addedTagList = addedTagUpdates[soundId];
	let existingTags = tagsForSounds[soundId];
	
	if(removedTagList && removedTagList.length &&
			removedTagList.some(function(tag) {
				return tag.tag == tagName
			})) {
		return;
	}
	
	if(addedTagList && addedTagList.find(function (tag) {
		return tag.tag == tagName;
	})) {
		for(let index in addedTagList) {
			if(addedTagList[index].tag == tagName) {
				addedTagList.splice(index, 1);
				break;
			}
		}
	}
	
	if(addedTagList && addedTagList.length &&
			!addedTagList.some(function (tag) {
		return tag.tag == tagName;
	}) && existingTags && existingTags.some(function (tag) {
		return tag.tag == tagName;
	})) {
		//Check if the tag exists
		let tagToRemove = existingTags.find(function (tag) {
			tag.tag == tagName;
		});
		
		if(tagToRemove) {
			if(!removedTagList) {
				removedTagList = [];
			}
			removedTagList.push(tagToRemove);
		}
	}
	
	removedTagUpdates[soundId] = removedTagList;
	addedTagUpdates[soundId] = addedTagList;
	
	document.getElementById("modifiableTagDisplay").innerHTML = 
		displayModifiableTagList(currentSoundTagList(soundId));
	displayCurrentlySelectedTagsForSound(soundId);
}

function populateTagSuggestions(tagUpdateSuggestions) {
	let listId = tagUpdateSuggestions ? "tagUpdateSuggestions" : "uploadTagSuggestions";
	let optionTemplate = "<option>:value</option>";
	let options = "";
	foundTags.forEach(function (tag) {
		options += optionTemplate.replace(":value", tag.tag);
	});
	
	document.getElementById(listId).innerHTML = options;
}

function populateCategoryOptions(selectedCategory) {
	let options = "";
	
	for(let optionIndex in soundCategories) {
		if(selectedCategory && selectedCategory == soundCategories[optionIndex]) {
			options += currentlySelectedOptionTemplate.replace(/:value/g, soundCategories[optionIndex]);
			
		} else {
			options += selectOptionTemplate.replace(/:value/g, soundCategories[optionIndex]);
		}
	}
	
	return options;
}

function playTrack(id) {
	let audioPlayer = document.getElementById("audio-player-"+id);
	let srcUrl = soundUrlTemplate.replace(":id",id);
	
	if(playingSoundId && playingSoundId != id) {
		stopTrack(playingSoundId);
	}
	
	if(audioPlayer.src != soundUrlTemplate.replace(":id",id)) {
		audioPlayer.src = soundUrlTemplate.replace(":id",id);
		audioPlayer.load();
	}
	
	document.getElementById("audio-player-"+id).play();
	playingSoundId = id;
}

function stopTrack(id) {
	document.getElementById("audio-player-"+id).pause();
}

function verifySoftware() {
	let userSoftwareList = document.getElementById("softwareInput").value;
	
	if (userSoftwareList && userSoftwareList.length > 0) {
		document.getElementById("uploadSound").disabled = false;
		document.getElementById("software-error").style.display = "none";
	} else {
		document.getElementById("uploadSound").disabled = true;
		document.getElementById("software-error").style.display = "block";
	}
}

const soundUrlTemplate = applicationDomain+"/sound/stream-sound?id=:id";

const selectOptionTemplate = "<option value=\":value\">:value</option>";

const currentlySelectedOptionTemplate = "<option value=\":value\" selected=\"selected\">:value</option>";

const soundForm = 
`<tr><td>
<form id="modify-:id" class="form" action="javascript:;" onsubmit="applyChangesToSound(:id)">
	<div class="form-group">
		<input class="form-control" id="name-:id" value=":name" placeholder="Song Title"/>
	</div>
	<div class="form-group form-inline">
		<select class="form-control" id="category-:id">:categories</select>
		<label for="price-:id">Sell Price (£)</label>
		<input class="form-control" id="price-:id" value=":price" type="number"
			placeholder="0.00" min="0" step="0.01"/>
	</div>
	<div class="form-group form-inline">
		<label for="taglist-:id" class="form-control" style="border: none; box-shadow: none">Tags:</label>
		<p id="taglist-:id" class="form-control" style="border: none; box-shadow: none">:tagList</p>
	</div>
	<div class="form-inline">
		<div class="form-group">
			<input class="btn btn-warning" type="button" value="Update" onclick="applyChangesToSound(:id, false)"/>
			<input class="btn btn-danger" type="button" value="Delete" onclick="applyChangesToSound(:id, true)"/>
			<input class="btn btn-warning" type="button" onclick="openEditTagDialogue(:id, ':name')" value="Modify Tags"/>
			<input class="btn btn-default" type="button" onclick="revertChanges(:id)" value="Revert"/>
		</div>
	</div>
</form>
</td>
<td>
<audio id="audio-player-:id">
	<source src="#" type="audio/wav">
</audio>
<button id="play-:id" class="btn btn-info" onclick="playTrack(:id)">Play</button>
<button id="stop-:id" class="btn btn-warning" onclick="stopTrack(:id)">Pause</button>
</td>
</tr>`;

