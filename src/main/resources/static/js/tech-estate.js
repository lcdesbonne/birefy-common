var questionsForUser = topicsAndQuestions.questions;
var topicsForUser = topicsAndQuestions.topics;

var questionSubset = questionsForUser.slice(0);
var selectedQuestion = questionSubset[0];

var updatedQuestion;
var updatedQuestions = [];

/*
 * Below is the format for an updatedTag object within 
 * the updatedQuestionTags list
 * var updatedTag = {
 *			questionId: selectedQuestion.id,
 *			tagNames: [],
 *			removedTags: []
 *	}
 */
var updatedQuestionTags = [];

window.onload = function () {
	ceaseLoadingPage();
	
	setAuthorityDisplay();
	populateTagSearchList();
	populateQuestionSearchList();
	populateTopicSearchList();
	
	//Construct the question and topic tables
	populateQuestionTable();
	populateTopicTable();
	
	//Show all the known technical tags on the system
	populateTechTagSearchList();
	populatePotentialMembersToAdd();
}

var reloadPage = function () {
	location.reload();
}

function populateQuestionTable() {
	var qTable = document.getElementById("questionTable");
	var rows = "";
	//Put the highlighted question at the top
	for(var i = 0; i < questionSubset.length; i++) {
		if (questionSubset[i].id == selectedQuestion.id) {
			//Check to see if this is an updated question
			var questionUpdated = false;
			if (updatedQuestions.length) {
				for(var j = 0; j < updatedQuestions.length; j++) {
					if(updatedQuestions[j].id == questionSubset[i].id && updatedQuestions[j].title) {
						questionUpdated = true;
						var row = "<tr class=\"birefy-fixed-view-selected\"><td><span>"+updatedQuestions[j].title+" <input class=\"btn button-info pos-right\"  type=\"button\" onclick=\"createTitleEditSpace()\" value=\"Edit\"/></span></td></tr> \n";
						rows += row;
						break;
					}
				}
			} 
			
			if (!questionUpdated) {
				var row = "<tr class=\"birefy-fixed-view-selected\"><td><span>"+questionSubset[i].title+" <input class=\"btn button-info pos-right\"  type=\"button\" onclick=\"createTitleEditSpace()\" value=\"Edit\"/></span></td></tr> \n";
				rows += row;
			}
			break;
		}
	}
	
	for (var i = 0; i < questionSubset.length; i++) {
		if (questionSubset[i].id == selectedQuestion.id) {
			continue;
		} else {
			var questionUpdated = false;
			if (updatedQuestions.length) {
				for(var j = 0; j < updatedQuestions.length; j++) {
					if(updatedQuestions[j].id == questionSubset[i].id && updatedQuestions[j].title) {
						questionUpdated = true;
						var row = "<tr><td onclick=\"selectQuestion("+questionSubset[i].id+")\">"+updatedQuestions[j].title+"</td></tr> \n";
						rows += row;
						break;
					}
				}
			}
			
			if (!questionUpdated) {
				var row = "<tr><td onclick=\"selectQuestion("+questionSubset[i].id+")\">"+questionSubset[i].title+"</td></tr> \n";
				rows += row;
			}	
		}
	}
	
	qTable.innerHTML = rows == "" ? "<tr><td>You Have No Questions</td></tr>" : rows;
	
	//Add the question detail to the table
	displayQuestionDetailAndUrl()
}

function displayQuestionDetailAndUrl() {
	generateApplicationAdvice();
	//Check to see if the question is in the updated question list
	for (var i = 0; i < updatedQuestions.length; i++) {
		if (updatedQuestions[i].id == selectedQuestion.id) {
			if (updatedQuestions[i].detail && updatedQuestions[i].detail.length) {
				$("#questionDetail").html(updatedQuestions[i].detail);
			} else {
				if (selectedQuestion.detail && selectedQuestion.detail.length) {
					$("#questionDetail").html(selectedQuestion.detail);
				} else {
					$("#questionDetail").html("No details");
				}
			}
			
			if (updatedQuestions[i].appUrl && updatedQuestions[i].appUrl.length) {
				$("#questionUrl").html(updatedQuestions[i].appUrl);
			} else {
				if (selectedQuestion.appUrl && selectedQuestion.appUrl.length) {
					$("#questionUrl").html(selectedQuestion.appUrl);
				} else {
					$("#questionUrl").html("No details");
				}
			}
			
			if (updatedQuestions[i].source && updatedQuestions[i].source.length) {
				$("#sourceDetail").html(updatedQuestions[i].source);
			} else {
				if (selectedQuestion.source && selectedQuestion.source.length) {
					$("#sourceDetail").html(selectedQuestion.source);
				} else {
					$("#sourceDetail").html("None Set");
				}
			}
			
			if (updatedQuestions[i].port) {
				$("#portDetail").html(updatedQuestions[i].port);
			} else {
				if (selectedQuestion.port && selectedQuestion.port.length) {
					$("#portDetail").html(selectedQuestion.port);
				} else {
					$("#portDetail").html("8080");
				}
			}
			return;
		}
	}
	
	if (selectedQuestion.detail && selectedQuestion.detail.length) {
		$("#questionDetail").html(selectedQuestion.detail);
	} else {
		$("#questionDetail").html("No details");
	}
	
	if (selectedQuestion.appUrl && selectedQuestion.appUrl.length) {
		$("#questionUrl").html(selectedQuestion.appUrl);
	} else {
		$("#questionUrl").html("No details");
	}
	
	if (selectedQuestion.source && selectedQuestion.source.length) {
		$("#sourceDetail").html(selectedQuestion.source);
	} else {
		$("#sourceDetail").html("None Set");
	}
	
	if (selectedQuestion.port) {
		$("#portDetail").html(selectedQuestion.port);
	} else {
		$("#portDetail").html("8080");
	}
	
	displayTagsForQuestion();
	//Populate the question tag removal options
	//techTagForQuestionSearchList();
}

function selectQuestion(id) {
	for(var i = 0; i < questionSubset.length; i++) {
		if (questionSubset[i].id == id) {
			selectedQuestion = questionSubset[i];
			break;
		}
	}
	
	//Construct the question table with the 
	//selected question highlighted
	populateQuestionTable();
	populateTopicTable();
	
	//Apply active functionality
	applyActiveFunctionality();
	
	//Populate the removal list for technical tags
		
}

function populateTopicTable() {
	var topicIdInView = selectedQuestion.topicId;

	for (var i = 0; i < topicsForUser.length; i++) {
		if (topicsForUser[i].id == topicIdInView) {
			$("#topicTitle").html(topicsForUser[i].topicName);
			$("#topicDesc").html(topicsForUser[i].topicDetail);
			break;
		}
	}
	
	//Populate the tags section
	populateTagList(topicIdInView);
}

function populateTagList(topicId) {
	var tagList = [];
	tagList = tagsForTopics[String(topicId)];

	//Display the tags on the page
	displaySelectedTags(tagList, "tagsForTopic");
}

function displaySelectedTags(tagList, elementId, modifiable) {
	if (!tagList || !tagList.length) {
		document.getElementById(elementId).innerHTML = "<p>No Tags</p>";
		return;
	}
	
	var blankColumns = tagList.length % 3;
	var row = "<div class=\"row\">";
	var endRow = "</div>";
	var column = modifiable ? "<div class=\"col-sm-4\"><label>--i--</label><button class=\"btn\" onclick=\"removeTagFromQuestion('--i--')\">X</button>" :
		"<div class=\"col-sm-4\"><label>--i--</label></div>";
	var emptyColumn = "<div class=\"col-sm-4\"></div>";
	var content = row;
	var endOfRow = false;
	
	for(var i = 0; i < tagList.length; i++) {
		if (i % 3 == 0 && i != 0) {
			if(tagList[i].tagName) {
				content += column.replace(/--i--/g,tagList[i].tagName);
			} else {
				content += column.replace(/--i--/g,tagList[i]);
			}
			
			//End the row
			content += endRow;
			endOfRow = true;
			continue;
		}
		if (endOfRow) {
			content += row;
			endOfRow = false;
		}
		if (tagList[i].tagName) {
			content += column.replace(/--i--/g, tagList[i].tagName);
		} else {
			content += column.replace(/--i--/g, tagList[i]);
		}	
	}
	
	if (blankColumns > 0) {
		for (var i = 0; i < blankColumns; i++) {
			content += emptyColumn;
		}
		//End the row
		content += endRow;
	}
	
	document.getElementById(elementId).innerHTML = content;
}

function getTagListForSelectedQuestion() {
	var tagListForQuestion = [];
	var removedTags = [];
	if (updatedQuestionTags.length) {
		for (var x = 0; x < updatedQuestionTags.length; x++) {
			if (updatedQuestionTags[x].questionId == selectedQuestion.id) {
				//Add any tags to the tag list
				tagListForQuestion = updatedQuestionTags[x].tagNames.slice();
				removedTags = updatedQuestionTags[x].removedTags;
				break;
			}
		}
	}
	//Add any existing tags to the question list
	for (var x = 0; x < tagsForQuestions.length; x++) {
		if (tagsForQuestions[x].questionId == selectedQuestion.id) {
			//Add all the tags in the list to be displayed
			var compositeArray = tagListForQuestion.concat(uniques(tagsForQuestions[x].tagNames, 
					removedTags));
			tagListForQuestion.length = 0;
			tagListForQuestion = compositeArray;
		}
	}
	return tagListForQuestion;
}

function displayTagsForQuestion() {
	let tagListForQuestion = getTagListForSelectedQuestion();
	displaySelectedTags(tagListForQuestion, "tagsForQuestion");
}

function displayModifiableTagsForQuestion() {
	let tagListForQuestion = getTagListForSelectedQuestion();
	displaySelectedTags(tagListForQuestion, "question-tag-list", true);
}

function revertTagChangesForQuestion() {
	let questionTagChanges = updatedQuestionTags.find(function (update) {
		return update.questionId = selectedQuestion.id;
	});
	
	if(questionTagChanges) {
		let index = updatedQuestionTags.indexOf(questionTagChanges);
		updatedQuestionTags.splice(index, 1);
	}
	
	displayTagsForQuestion();
	displayModifiableTagsForQuestion();
}

function displayModifiableTagsForTopic() {
	
}

function openUpdateTagsForQuestion() {
	displayModifiableTagsForQuestion();
	populateContributorsForQuestion(selectedQuestion.id);
	document.getElementById("questionTagUpdate").style.display = "block";
	//Add the title of the question to the view.
	document.getElementById("question-contributor-title").innerHTML = selectedQuestion.title;
}

function closeUpdateTagsForQuestion() {
	document.getElementById("questionTagUpdate").style.display = "none";
	contributorsToAdd.length = 0;
	contributorsToRemove.length = 0;
	contributorsToUpdate.length = 0;
	document.getElementById("question-contributor-title").innerHTML = "";
}

function openUpdateTagsForTopic() {
	document.getElementById("topicTagUpdate").style.display = "block";
}

function closeUpdateTagsForTopic() {
	document.getElementById("topicTagUpdate").style.display = "none";
}

function addNewTagToQuestion() {
	//Retrieve the value from the tag input
	var tagToAdd = document.getElementById("techTagToAdd").value;
	
	for(var x = 0; x < updatedQuestionTags.length; x++) {
		if (updatedQuestionTags[x].questionId == selectedQuestion.id) {
			if(updatedQuestionTags[x].tagNames.length) {
				var tagNames = updatedQuestionTags[x].tagNames;
				for(var i = 0; i < tagNames.length; i++) {
					if(tagNames[i] == tagToAdd) {
						return;
					}
				}
			}
			if(updatedQuestionTags[x].removedTags.length) {
				var tagNames = updatedQuestionTags[x].removedTags;
				for(var i = 0; i < tagNames.length; i++) {
					if(tagNames[i] == tagToAdd) {
						//Remove this from the deleted tags list
						updatedQuestionTags[x].removedTags.splice(i,1);
						displayModifiableTagsForQuestion();
						displayTagsForQuestion();
						//techTagForQuestionSearchList();
						break;
					}
				}
			}
		}
	}
	
	//Check if the question already has the tag
	for(var x = 0; x < tagsForQuestions.length; x++) {
		if (tagsForQuestions[x].questionId == selectedQuestion.id) {
			if(tagsForQuestions[x].tagNames.length) {
				var tagNames = tagsForQuestions[x].tagNames;
				for(var i = 0; i < tagNames.length; i++) {
					if(tagNames[i] == tagToAdd) {
						return;
					}
				}
			}
		}
	}
	
	//Add the tag to the updated tag list
	for(var j = 0; j < updatedQuestionTags.length; j++) {
		if(updatedQuestionTags[j].questionId == selectedQuestion.id) {
			updatedQuestionTags[j].tagNames.push(tagToAdd);
			//Reload the tag for question display.
			displayModifiableTagsForQuestion();
			displayTagsForQuestion();
			//techTagForQuestionSearchList();
			return;
		}
	}
	
	let newTagList = [];
	newTagList.push(tagToAdd);
	let removedTagList = [];
	//Add a new entry to the updated tag list
	let updatedTag = {
			'questionId': selectedQuestion.id,
			'tagNames': newTagList,
			'removedTags': removedTagList
	}
	
	updatedQuestionTags.push(updatedTag);
	//techTagForQuestionSearchList();
	displayModifiableTagsForQuestion();
	displayTagsForQuestion();
	
	//Clear the search box
	document.getElementById("techTagToAdd").value = null;
}

function removeTagFromQuestion(tagName) {
	//Retrieve the tag from the input
	var tagToRemove = tagName;
		
	//Check the updated tag list
	for(var x = 0; x < updatedQuestionTags.length; x++) {
		if (updatedQuestionTags[x].questionId == selectedQuestion.id) {
			if(updatedQuestionTags[x].tagNames.length) {
				var tagNames = updatedQuestionTags[x].tagNames;
				for(var i = 0; i < tagNames.length; i++) {
					if(tagNames[i] == tagToRemove) {
						//Remove the tag from here
						tagNames.splice(i,1);
						displayTagsForQuestion();
						displayModifiableTagsForQuestion();
						//techTagForQuestionSearchList();
						return;
					}
				}
			}
			if(updatedQuestionTags[x].removedTags.length) {
				var tagNames = updatedQuestionTags[x].removedTags;
				for(var i = 0; i < tagNames.length; i++) {
					if(tagNames[i] == tagToRemove) {
						return;
					}
				}
			}
			//Add the tag to the removed list
			updatedQuestionTags[x].removedTags.push(tagToRemove);
			displayTagsForQuestion();
			displayModifiableTagsForQuestion();
			//techTagForQuestionSearchList();
			return;
		}
	}
	
	//Generate a new entry in the updatedQuestionTags list
	var newTagList = [];
	var removedTagList = [];
	removedTagList.push(tagToRemove);
	//Add a new entry to the updated tag list
	var updatedTag = {
			'questionId': selectedQuestion.id,
			'tagNames': newTagList,
			'removedTags': removedTagList
	}
	
	updatedQuestionTags.push(updatedTag);
	displayTagsForQuestion();
	displayModifiableTagsForQuestion();
	//techTagForQuestionSearchList();
	
	//Clear the search box
	document.getElementById("techTagToRemove").value = null;
}

function populateTagSearchList() {
	var datalist = document.getElementById("tagNames");
	var options = "";
	
	var keys = Object.keys(tagsForTopics);
	var tagNames = new Set();
	
	if(keys && keys.length) {
		for(var x = 0; x < keys.length; x++) {
			var tags = tagsForTopics[String(keys[x])];
			for(var i = 0; i < tags.length; i++) {
				//Extract the tag names
				tagNames.add(tags[i].tagName);
			}
		}
	}
	
	tagNames = Array.from(tagNames);
	for(var x = 0; x < tagNames.length; x++) {
		var tag = tagNames[x];
		options += "<option value=\""+tag+"\"/>";
	}
	
	datalist.innerHTML = options;
}

function populateQuestionSearchList() {
	var datalist = document.getElementById("questionTitles");
	var options = "";
	for(var x = 0; x < questionsForUser.length; x++) {
		var question = questionsForUser[x];
		options += "<option value=\""+question.title+"\"/>";
	}
	
	datalist.innerHTML = options;
}

function populateTopicSearchList() {
	var datalist = document.getElementById("topicTitles");
	var options = "";
	for(var x = 0; x < topicsForUser.length; x++) {
		var topic = topicsForUser[x];
		options += "<option value=\""+topic.topicName+"\"/>";
	}
	
	datalist.innerHTML = options;
}

//Displays all the technical tags currently known
function populateTechTagSearchList() {
	var tagList = document.getElementById("availableTechTags");
	var options = "";
	for(var x = 0; x < existingTechTags.length; x++) {
		var tag = existingTechTags[x];
		options += "<option value=\""+tag.name+"\"/>";
	}
	tagList.innerHTML = options;
}

function searchForTechTags() {	
	let searchString = document.getElementById("techTagToAdd").value;
	
	if(searchString.length < 3) {
		return;
	}
	
	$.ajax({
		url: '/tech-estate/search-tech-tags?tagName='+searchString,
		method: 'GET',
		success: function (result) {
			existingTechTags = result;
			populateTechTagSearchList();
		}
	});
}

function searchForTopicTags() {
	let searchString = document.getElementById("searchTopicTags").value;
	
	if (searchString.length < 3) {
		return;
	}
	
	$.ajax({
		url: '/tech-estate/search-topic-tags?tagName='+searchString,
		method: 'GET',
		success: function (result) {
			existingTopicTags = result;
			populateTopicTagSuggestions();
		}
	});
}

//Displays all the tags currently assigned to the question.
function techTagForQuestionSearchList() {
	var tagList = document.getElementById("currentTechTags");
	//The tags to be displayed here should be the tags for questions
	var tagsForQuestion = getTagListForSelectedQuestion();
	var options = "";
	for(var x = 0; x < tagsForQuestion.length; x++) {
		var tag = tagsForQuestion[x];
		options += "<option value=\""+tag+"\"/>";
	}
	tagList.innerHTML = options;
}

function filterByTopic(name) {
	var topId;
	//Identify the corresponding topic id
	for (var i = 0; i < topicsForUser.length; i++) {
		if (topicsForUser[i].topicName.indexOf(name) !== -1) {
			topId = topicsForUser[i].id;
			break;
		}
	}
	
	if (topId) {
		var filteredQuestions = [];
		
		for (var x = 0; x < questionsForUser.length; x++){
			if (questionsForUser[x].topicId == topId) {
				filteredQuestions.push(questionsForUser[x]);
			}
		}
		
		if (filteredQuestions.length) {
			questionSubset = filteredQuestions;
			//Assign the selected question
			selectedQuestion = questionSubset[0];
			
			//Construct the question table
			populateQuestionTable();
			
			//Construct the tag table
			populateTopicTable();
		}
	}
	
}

function filterByQuestion(name) {
	var filteredQuestions = [];
	//Create a subset of questions that contain the text
	for(var x = 0; x < questionsForUser.length; x++) {
		if(questionsForUser[x].title.indexOf(name) !== -1) {
			filteredQuestions.push(questionsForUser[x]);
		}
	}
	
	if(filteredQuestions.length) {
		questionSubset = filteredQuestions;
		//Assign the selected question
		selectedQuestion = questionSubset[0];
		
		//Construct the question table
		populateQuestionTable();
		
		//Construct the tag table
		populateTopicTable();
	}
}

function filterByTags(name) {
	var qualifyingTopics = [];
	var filteredQuestions = [];
	//Identify all the topics with the said tag
	//We have a map combining topic ids to a list of tags
	for (var topicId in tagsForTopics) {
		//Loop through the list of tags
		for(var key in tagsForTopics[topicId]) {
			if (tagsForTopics[topicId][key].tagName == name) {
				//Add the topicId to qualifying topics
				qualifyingTopics.push(topicId);
				break;
			}
		}
	}
	
	//Identify all the questions which belong to said topics
	if(qualifyingTopics.length) {
		for(var qKey in questionsForUser) {
			for(var topicId in qualifyingTopics) {
				if (qualifyingTopics[topicId] == questionsForUser[qKey].topicId) {
					filteredQuestions.push(questionsForUser[qKey]);
					break;
				}
			}
		}
	}
	
	//Run the methods to display the data
	if(filteredQuestions.length) {
		questionSubset = filteredQuestions;
		//Assign the selected question
		selectedQuestion = questionSubset[0];
		
		//Construct the question table
		populateQuestionTable();
		
		//Construct the tag table
		populateTopicTable();
	}
}

function handleFilters(filter) {
	var topicFilter = document.getElementById("topicSearch");
	var questionFilter = document.getElementById("questionSearch");
	var tagFilter = document.getElementById("tagSearch");
	
	if (filter == 'topic') {
		if(topicFilter.value && topicFilter.value.length) {
			filterByTopic(topicFilter.value);
		} else {
			clearFilters();
		}
	} else if (filter == 'tag') {
		if (tagFilter.value && tagFilter.value.length) {
			filterByTags(tagFilter.value);
		} else {
			clearFilters();
		}
	} else if (filter == 'question') {
		if (questionFilter.value && questionFilter.value.length) {
			filterByQuestion(questionFilter.value);
		} else {
			clearFilters();
		}
	}
}

function clearFilters() {
	var topicFilter = document.getElementById("topicSearch");
	var questionFilter = document.getElementById("questionSearch");
	var tagFilter = document.getElementById("tagSearch");
	
	topicFilter.value = "";
	questionFilter.value = "";
	tagFilter.value = "";
	
	tagFilter.placeholder = "Search Tags";
	questionFilter.placeholder = "Search Questions";
	topicFilter.placeholder = "Search Topics";
	
	questionSubset = questionsForUser;
	selectedQuestion = questionSubset[0];
	
	populateQuestionTable();
	populateTopicTable();
}

/**
 * Should send the question to pending status if not
 * Royal status, else will activate the question
 * @returns
 */
function sendQuestionForApproval() {
	$.ajax({
		url: '/tech-estate/approval',
		contentType: 'application/json',
		method: 'POST',
		data: JSON.stringify(selectedQuestion.id),
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
		}
	}).done(function (result) {
		if (result === true) {
			operationCompleteAlert(reloadPage);
		} else {
			openFailureDialogue("Failed to activate question.");
		}
	});
}

function deactivateQuestion() {
	$.ajax({
		url: '/tech-estate/deactivate',
		contentType: 'application/json',
		method: 'POST',
		data: JSON.stringify(selectedQuestion.id),
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
		}
	}).done(function (result) {
		if (result == true) {
			operationCompleteAlert(reloadPage);
		} else {
			openFailureDialogue("Failed to deactivate question.");
		}
	})
}

function updateQuestion() {
	if ((!updatedQuestions || !updatedQuestions.length) && !updatedQuestionTags.length) {
		openFailureDialogue("No updates to appply");
		return;
	}
	
	if (updatedQuestions.length) {
		$.ajax({
			url: '/tech-estate/update',
			contentType: 'application/json',
			method: 'POST',
			data: JSON.stringify(updatedQuestions),
			dataType: 'json',
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")	
			}
		}).done(function (result) {
			if (result == true) {
				if(updatedQuestionTags.length) {
					//Apply the update of question tags
					$.ajax({
						url: '/tech-estate/update-question-tags',
						contentType: 'application/json',
						method: 'POST',
						data: JSON.stringify(updatedQuestionTags),
						dataType: 'text',
						headers: {
							'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
						}
					}).done(function() {
						operationCompleteAlert(reloadPage);
					});
				} else {
					operationCompleteAlert(reloadPage);
				}
			} else {
				openFailureDialogue("Failed to apply updates.");
			}
		});
	}
	
	
	if((!updatedQuestions || !updatedQuestions.length) && updatedQuestionTags.length) {
		//Just apply tag updates
		//Apply the update of question tags
		$.ajax({
			url: '/tech-estate/update-question-tags',
			contentType: 'application/json',
			method: 'POST',
			data: JSON.stringify(updatedQuestionTags),
			dataType: 'text',
			headers: {
				'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
			}
		}).done(function() {
			operationCompleteAlert(reloadPage);
		});
		return;
	}
}

//Edit title functionality
function editQuestionTitle() {
	//Check to see if the question for edit exists
	if(!updatedQuestion || !updatedQuestion.id) {
		updatedQuestion = {};
		updatedQuestion.id = selectedQuestion.id;
		updatedQuestion.topicId = selectedQuestion.topicId;
	}
	
	var alreadyUpdated = false;
	if(updatedQuestions.length) {
		//Search to see if the question has been updated already
		for(var x = 0; x < updatedQuestions.length; x++) {
			if (updatedQuestions[x].id == updatedQuestion.id) {
				updatedQuestions[x].title = $("#editTitle").html();
				alreadyUpdated = true;
				break;
			}
		}
	}
	
	if (!alreadyUpdated) {
		//Add the new text to the question title
		updatedQuestion.title = $("#editTitle").html();
		
		//Add to the list of updated questions
		updatedQuestions.push(updatedQuestion);
	}
	
	//Reload the question table
	populateQuestionTable();
}

function createTitleEditSpace() {
	var title = selectedQuestion.title;
	
	if (updatedQuestions.length) {
		for(var x = 0; x < updatedQuestions.length; x++) {
			if (updatedQuestions[x].id == selectedQuestion.id) {
				title = updatedQuestions[x].title ? updatedQuestions[x].title : title;
				break;
			}
		}
	}
	
	var editTitle = "<textarea class=\"form-control\" rows=\"2\" id=\"editTitle\">"+ title +"</textarea>";
	var revertButton = "<button type=\"button\" class=\"btn btn-warning pos-right\" onclick=\"revertQuestion()\">Revert</button>";
	var doneButton =   "<button type=\"button\" class=\"btn btn-success\" onclick=\"editQuestionTitle()\">Done</button>";
	
	var editSection = "<div class=\"p-3 container\" style=\"width: inherit; padding: 3%\"><div class=\"row\">" +
					  editTitle + 
					  "</div>" + 
					  "<div class=\"row\" style=\"padding: 3%\">" + 
					  "<div class=\"col-sm-6\">" + 
					  revertButton + 
					  "</div>" + 
					  "<div class=\"col-sm-6\">" + 
					  doneButton +
					  "</div>" +
					  "</div></div>";
	
	//Modify the table content
	$("#questionTable").html(editSection);
}

function revertQuestion() {
	var id = selectedQuestion.id;
	
	//Remove the updated question from the list
	for (var i = 0; i < updatedQuestions.length; i++) {
		if(updatedQuestions[i].id == id) {
			updatedQuestions.splice(i, 1);
			break;
		}
	}
	
	populateQuestionTable();
}

function editQuestionUrl() {
	//Check to see if the question for edit exists
	if(!updatedQuestion || !updatedQuestion.id) {
		updatedQuestion = {};
		updatedQuestion.id = selectedQuestion.id;
		updatedQuestion.topicId = selectedQuestion.topicId;
	}
	
	var alreadyUpdated = false;
	if(updatedQuestions.length) {
		//Search to see if the question has been updated already
		for(var x = 0; x < updatedQuestions.length; x++) {
			if (updatedQuestions[x].id == updatedQuestion.id) {
				updatedQuestions[x].appUrl = $("#editUrl").html();
				alreadyUpdated = true;
				break;
			}
		}
	}
	
	if (!alreadyUpdated) {
		//Add the new text to the question title
		updatedQuestion.appUrl = $("#editUrl").html();
		
		//Add to the list of updated questions
		updatedQuestions.push(updatedQuestion);
	}
	
	replaceUrlParagraph();
	
	//Display the new url
	displayQuestionDetailAndUrl();
}

//Detail edit space
function createDetailEditSpace() {
	var detail = selectedQuestion.detail;
	
	if (updatedQuestions.length) {
		for(var x = 0; x < updatedQuestions.length; x++) {
			if (updatedQuestions[x].id == selectedQuestion.id) {
				detail = updatedQuestions[x].detail ? updatedQuestions[x].detail : detail;
				break;
			}
		}
	}
	var editTitle = "<textarea class=\"form-control\" rows=\"2\" id=\"editDetail\">"+ detail +"</textarea>";
	var revertButton = "<button type=\"button\" class=\"btn btn-warning pos-right\" onclick=\"revertDetail()\">Revert</button>";
	var doneButton =   "<button type=\"button\" class=\"btn btn-success\" onclick=\"editDetail()\">Done</button>";
	
	var editSection = "<div class=\"p-3 container\" style=\"width: inherit; padding: 3%\"><div class=\"row\">" +
					  editTitle + 
					  "</div>" + 
					  "<div class=\"row\" style=\"padding: 3%\">" + 
					  "<div class=\"col-sm-6\">" + 
					  revertButton + 
					  "</div>" + 
					  "<div class=\"col-sm-6\">" + 
					  doneButton +
					  "</div>" +
					  "</div></div>";
	
	//Modify the table content
	$("#detailContainer").html(editSection);
	
}

function editDetail() {
	//Check to see if the question for edit exists
	if(!updatedQuestion || !updatedQuestion.id) {
		updatedQuestion = {};
		updatedQuestion.id = selectedQuestion.id;
		updatedQuestion.topicId = selectedQuestion.topicId;
	}
	
	var alreadyUpdated = false;
	if(updatedQuestions.length) {
		//Search to see if the question has been updated already
		for(var x = 0; x < updatedQuestions.length; x++) {
			if (updatedQuestions[x].id == updatedQuestion.id) {
				updatedQuestions[x].detail = $("#editDetail").html();
				alreadyUpdated = true;
				break;
			}
		}
	}
	
	if (!alreadyUpdated) {
		//Add the new text to the question title
		updatedQuestion.detail = $("#editDetail").html();
		
		//Add to the list of updated questions
		updatedQuestions.push(updatedQuestion);
	}
	
	replaceDetailParagraph();
	
	//Display the new url
	displayQuestionDetailAndUrl();
}

function createSourceEditSpace() {
var source = selectedQuestion.source;
	
	if (updatedQuestions.length) {
		for(var x = 0; x < updatedQuestions.length; x++) {
			if (updatedQuestions[x].id == selectedQuestion.id) {
				source = updatedQuestions[x].source ? updatedQuestions[x].source : source;
				break;
			}
		}
	}
	var editSource = "<textarea class=\"form-control\" rows=\"2\" id=\"editSource\">"+ source +"</textarea>";
	var revertButton = "<button type=\"button\" class=\"btn btn-warning pos-right\" onclick=\"revertSource()\">Revert</button>";
	var doneButton =   "<button type=\"button\" class=\"btn btn-success\" onclick=\"editSource()\">Done</button>";
	
	var editSection = "<div class=\"p-3 container\" style=\"width: inherit; padding: 3%\"><div class=\"row\">" +
					  editSource + 
					  "</div>" + 
					  "<div class=\"row\" style=\"padding: 3%\">" + 
					  "<div class=\"col-sm-6\">" + 
					  revertButton + 
					  "</div>" + 
					  "<div class=\"col-sm-6\">" + 
					  doneButton +
					  "</div>" +
					  "</div></div>";
	
	//Modify the table content
	$("#sourceContainer").html(editSection);
}

function editSource() {
	//Check to see if the question for edit exists
	if(!updatedQuestion || !updatedQuestion.id) {
		updatedQuestion = {};
		updatedQuestion.id = selectedQuestion.id;
		updatedQuestion.topicId = selectedQuestion.topicId;
	}
	
	var alreadyUpdated = false;
	if(updatedQuestions.length) {
		//Search to see if the question has been updated already
		for(var x = 0; x < updatedQuestions.length; x++) {
			if (updatedQuestions[x].id == updatedQuestion.id) {
				updatedQuestions[x].source = $("#editSource").val();
				alreadyUpdated = true;
				break;
			}
		}
	}
	
	if (!alreadyUpdated) {
		//Add the new text to the question title
		updatedQuestion.source = $("#editSource").val();
		
		//Add to the list of updated questions
		updatedQuestions.push(updatedQuestion);
	}
	
	replaceSourceLink();
	
	//Display the new url
	displayQuestionDetailAndUrl();
}

function createPortEditSpace() {
	var port = selectedQuestion.port;
	
	if (updatedQuestions.length) {
		for(var x = 0; x < updatedQuestions.length; x++) {
			if (updatedQuestions[x].id == selectedQuestion.id) {
				port = updatedQuestions[x].port ? updatedQuestions[x].port : port;
				break;
			}
		}
	}
	var editTitle = "<textarea class=\"form-control\" rows=\"2\" id=\"editPort\">"+ port +"</textarea>";
	var revertButton = "<button type=\"button\" class=\"btn btn-warning pos-right\" onclick=\"revertPort()\">Revert</button>";
	var doneButton =   "<button type=\"button\" class=\"btn btn-success\" onclick=\"editPort()\">Done</button>";
	
	var editSection = "<div class=\"p-3 container\" style=\"width: inherit; padding: 3%\"><div class=\"row\">" +
					  editTitle + 
					  "</div>" + 
					  "<div class=\"row\" style=\"padding: 3%\">" + 
					  "<div class=\"col-sm-6\">" + 
					  revertButton + 
					  "</div>" + 
					  "<div class=\"col-sm-6\">" + 
					  doneButton +
					  "</div>" +
					  "</div></div>";
	
	//Modify the table content
	$("#portContainer").html(editSection);
	
}

function editPort() {
	//Check to see if the question for edit exists
	if(!updatedQuestion || !updatedQuestion.id) {
		updatedQuestion = {};
		updatedQuestion.id = selectedQuestion.id;
		updatedQuestion.topicId = selectedQuestion.topicId;
	}
	
	var alreadyUpdated = false;
	if(updatedQuestions.length) {
		//Search to see if the question has been updated already
		for(var x = 0; x < updatedQuestions.length; x++) {
			if (updatedQuestions[x].id == updatedQuestion.id) {
				updatedQuestions[x].port = $("#editPort").html();
				alreadyUpdated = true;
				break;
			}
		}
	}
	
	if (!alreadyUpdated) {
		//Add the new text to the question title
		updatedQuestion.port = $("#editPort").html();
		
		//Add to the list of updated questions
		updatedQuestions.push(updatedQuestion);
	}
	
	replacePortParagraph();
	
	//Display the new url
	displayQuestionDetailAndUrl();
}

function revertUrl() {
	replaceUrlParagraph();
	revertQuestion();
}

function revertDetail() {
	replaceDetailParagraph();
	revertQuestion();
}

function revertSource() {
	replaceSourceLink();
	revertQuestion();
}

function revertPort() {
	replacePortParagraph();
	revertQuestion();
}

function replaceUrlParagraph() {
	var parentNode = document.getElementById("urlContainer");
	var oldChildNode = parentNode.childNodes[0];
	var urlDetails = document.createElement('p');
	urlDetails.id = 'questionUrl';
	parentNode.replaceChild(urlDetails, oldChildNode);
}

function replaceDetailParagraph() {
	var parentNode = document.getElementById("detailContainer");
	var oldChildNode = parentNode.childNodes[0];
	var questionDetails = document.createElement('p');
	questionDetails.id = 'questionDetail';
	questionDetails.className = 'text-center';
	parentNode.replaceChild(questionDetails, oldChildNode);
}

function replaceSourceLink() {
	var parentNode = document.getElementById("sourceContainer");
	var oldChildNode = parentNode.childNodes[0];
	var sourceLink = document.createElement('a');
	sourceLink.id = "sourceDetail";
	parentNode.replaceChild(sourceLink, oldChildNode);
}

function replacePortParagraph() {
	var parentNode = document.getElementById("portContainer");
	var oldChildNode = parentNode.childNodes[0];
	var portDetails = document.createElement('p');
	portDetails.id = 'portDetail';
	parentNode.replaceChild(portDetails, oldChildNode);
}

function applyActiveFunctionality() {
	if(selectedQuestion.active) {

		document.getElementById("DeactivateQuestion").disabled = false;

		document.getElementById("ActivateQuestion").disabled = true;

	} else if (selectedQuestion.active === false){
		document.getElementById("ActivateQuestion").disabled = false;
		
		document.getElementById("DeactivateQuestion").disabled = true;
	}
}

(function() {
	if(selectedQuestion.active) {
		document.getElementById("DeactivateQuestion").disabled = false;

		document.getElementById("ActivateQuestion").disabled = true;

	} else if (selectedQuestion.active === false){
		document.getElementById("ActivateQuestion").disabled = false;

		document.getElementById("DeactivateQuestion").disabled = true;
	}
})();

function setAuthorityDisplay() {
	if(lordUser === true) {
		//hide the royal display
		$("#royalUser").hide();
		
		//Resize the other columns
		$("#royalUser").parent().children().removeClass().addClass('col-md-6');
	} 
};

function questionHasTechTags() {
	for(var x = 0; x < tagsForQuestions.length; x++) {
		if (tagsForQuestions[x].questionId == selectedQuestion.id) {
			if(tagsForQuestions[x].tagNames.length) {
				return true;
			}
		}
	}
	for(var x = 0; x < updatedQuestionTags.length; x++) {
		if (updatedQuestionTags[x].questionId == selectedQuestion.id) {
			if(updatedQuestionTags[x].tagNames.length) {
				return true
			}
		}
	}
	return false;
}

const applicationBaseUrlAdvice = "The urls for this application <b>MUST</b> begin with <b>/innovation/view-question/q-:id</b>";
const rootUrlBehaviourAdvice = "All applications <b>MUST</b> serve a 200 response on <b>/</b>";
const securityBehaviourAdvice = "Applications <b>MUST</b> be configured to receive <b>SSL</b> connections (you can use self signed certificates)";

function generateApplicationAdvice() {
	$("#baseUrlAdvice").html(applicationBaseUrlAdvice.replace(":id", selectedQuestion.id));
	$("#rootBehaviourAdvice").html(rootUrlBehaviourAdvice);
	$("#securityBehaviourAdvice").html(securityBehaviourAdvice);
}
