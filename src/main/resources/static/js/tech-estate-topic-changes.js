/*
 * Below is the format of an updated topic tag list
 * var updatedTopic = {
 * 	topicId: 0,
 * 	detail: "",
 * 	addedTags: [],
 * 	removedTags: [],
 * 	active: true
 * }
 */
var updatedTopics = [];
//var topicsList = [];
var currentlyEditingTopic;
/*
 * The format of a new contributor is
 *  var topicContributor = {
 *  	memberId: 0,
 *  	tidingId: 'individual',
 *  	topicId: 0
 *  }	
 */
var topicContributorsToAdd = [];
var topicContributorsToRemove = [];

//Should share the functionality for user search...

function openTopicUpdate() {
	document.getElementById("topicTagUpdate").style.display = "block";
	populateEditTopicTable();
}

function closeTopicUpdate() {
	document.getElementById("topicTagUpdate").style.display = "none";
	topicContributorsToAdd.length = 0;
	topicContributorsToRemove.length = 0;
	updatedTopics.length = 0;
}

function resetTopicChanges() {
	topicContributorsToAdd.length = 0;
	topicContributorsToRemove.length = 0;
	updatedTopics.length = 0;
	populateEditTopicTable();
}

function populateEditTopicTable() {
	let topicTableContent = "";
	
	topicsByUser.forEach(function (topic) {
		let existsInUpdates = updatedTopics.find(function (top) {
			return top.topicId == topic.id;
		});
		
		if(!existsInUpdates) {
			if(!currentlyEditingTopic) {
				currentlyEditingTopic = topic.id;
			}
			topicTableContent += topicTableRowTemplate.replace(":title", topic.title)
				.replace(":detail", topic.detail)
				.replace(":active", topic.active)
				.replace(/:topicId/g, topic.id);
		}
	});
	
	updatedTopics.forEach(function (topic) {
		if(!currentlyEditingTopic) {
			currentlyEditingTopic = topic.id;
		}
		
		topicTableContent += topicTableRowTemplate.replace(":title", topic.title)
			.replace(":detail", topic.detail)
			.replace(":active", topic.active)
			.replace(/:topicId/g, topic.topicId);
	});
	
	let userTopicTable = document.getElementById("user-topics-list");
	userTopicTable.innerHTML = topicTableContent ? topicTableContent : noTopicsRowTemplate;
	setSelectedTopicToUpdate(currentlyEditingTopic);
}

function toggleTopicStatus(topicId) {
	let existsInUpdates = updatedTopics.find(function (top) {
		return top.topicId == topicId;
	});
	
	if(existsInUpdates) {
		existsInUpdates.active = existsInUpdates.active === true ? false : true;	
	} else {
		let topicToUpdate = topicsByUser.find(function (topic) {
			return topic.id == topicId;
		});
		
		if(topicToUpdate) {
			//Add the new tag to the updates list
			let updatedTopic = {
					topicId: topicId,
					title: topicToUpdate.title,
					detail: topicToUpdate.detail,
					addedTags: [],
					removedTags: [],
					active: !topicToUpdate.active
			};
			
			updatedTopics.push(updatedTopic);
		}
	}
	
	populateEditTopicTable();
}

function setSelectedTopicToUpdate(topicId) {
	//Populate the tags list
	populateTagsForEditTopic(topicId);
	
	//Populate the contributors list
	populateContributorsForEditTopic(topicId)
	
	//Remove the previously highlighted row
	let previousTargetTopic = document.getElementById("topic-edit-"+currentlyEditingTopic);
	if(previousTargetTopic.classList.contains("info")) {
		previousTargetTopic.classList.remove("info");
	}
	currentlyEditingTopic = topicId;
	
	//Highlight the editing topic
	document.getElementById("topic-edit-"+currentlyEditingTopic).classList.add("info");
}

function populateContributorsForEditTopic(topicId) {
	let tContributors = topicContributors[topicId];
	
	let contributorsForDisplay = [];
	tContributors.forEach(function (contributor) {
		if(!topicContributorsToRemove.find(function (ctbr) {
			return ctbr.memberId == contributor.memberId && ctbr.topicId == topicId;
		}) && !topicContributorsToAdd.find(function (ctbr) {
			return ctbr.memberId == contributor.memberId && ctbr.topicId == topicId;
		})) {
			contributorsForDisplay.push(contributor);
		}
	});
	
	topicContributorsToAdd.forEach(function (contrbtr) {
		if(contrbtr.topicId == topicId) {
			contributorsForDisplay.push(contrbtr);
		}
	});
	
	let tableContent = "";
	contributorsForDisplay.forEach(function (contributor) {
		tableContent += topicContributorTemplate.replace(/:tidingId/g, contributor.tidingId);
	});
	
	document.getElementById("topic-contributors-list").innerHTML = tableContent;
}

function addNewOwnerToTopic() {
	let tidingId = document.getElementById("topicContributor").value;
	document.getElementById("topicContributor").value = "";
	
	let newOwner = availableMembers.find(function (member) {
		return member.tidingId == tidingId;
	});
	
	//Check if in removed list
	let removedContributor = topicContributorsToRemove.find(function (cont) {
		return cont.topicId == currentlyEditingTopic &&
			cont.tidingId == tidingId;
	});
	
	if(removedContributor) {
		let index = topicContributorsToRemove.indexOf(removedContributor);
		topicContributorsToRemove.splice(index, 1);
	}
	
	if(!newOwner || topicContributors[currentlyEditingTopic].find(function (cont) {
		return cont.tidingId == tidingId;
	}) || topicContributorsToAdd.find(function (newContributor) {
		return newContributor.topicId == currentlyEditingTopic 
		&& newContributor.tidingId == tidingId;
	})) {
		return;
	}
	
	let newContributor = {
			topicId: currentlyEditingTopic,
			memberId: newOwner.memberId,
			tidingId: newOwner.tidingId
	};
	
	topicContributorsToAdd.push(newContributor);
	
	populateContributorsForEditTopic(currentlyEditingTopic);
}

function removeOwnerFromTopic(tidingId) {	
	let recentlyAddedOwner = topicContributorsToAdd.find(function (contributor) {
		return contributor.topicId = currentlyEditingTopic && 
			contributor.tidingId == tidingId;
	});
	
	if(recentlyAddedOwner) {
		let index = topicContributorsToAdd.indexOf(recentlyAddedOwner);
		topicContributorsToAdd.splice(index, 1);
	}
	
	let existingOwner = topicContributors[currentlyEditingTopic].find(function (contributor) {
		return contributor.tidingId == tidingId;
	});
	
	if(existingOwner && remainingTopicContributors(topicId).length == 1) {
		openFailureDialogue("A Topic must have at least one contributor.");
	} else if(existingOwner) {
		//Add to the removed list
		let removedOwner = {
				topicId: currentlyEditingTopic,
				memberId: existingOwner.memberId,
				tidingId: existingOwner.tidingId
		};
		topicContributorsToRemove.push(existingOwner);
	}
	populateContributorsForEditTopic(currentlyEditingTopic);
}

function populateTagsForEditTopic(topicId) {
	let tagsForCurrentTopicDisplay = [];
	
	let existingTopicTags = userTopicTags.filter(function (topTags) {
		return topTags.topicId == topicId;
	});
	
	let existingTagNames = [];
	
	if(existingTopicTags) {
		existingTopicTags.forEach(function (ttag) {
			existingTagNames.push(ttag.tagName);
		});
	}
	
	let removedTags = [];
	let addedTags = [];
	
	let topicUpdated = updatedTopics.find(function (top) {
		return top.topicId == topicId;
	});
	
	if(topicUpdated && topicUpdated.removedTags.length) {
		removedTags = topicUpdated.removedTags;
	}
	if(topicUpdated && topicUpdated.addedTags.length) {
		addedTags = topicUpdated.addedTags;
	}
	
	if(addedTags.length) {
		addedTags.forEach(function (tag) {
			tagsForCurrentTopicDisplay.push(tag);
		});
	}
	if(existingTagNames.length) {
		existingTagNames.forEach(function (tag) {
			let isDeleted = false;
			if(removedTags.length) {
				isDeleted = removedTags.find(function (delTag) {
					return delTag == tag;
				});
			}
			
			if(!isDeleted) {
				tagsForCurrentTopicDisplay.push(tag);
			}
		});
	}
	
	displaySelectedTagsForTopic(tagsForCurrentTopicDisplay, "topic-tag-list", true);
}

function addTagToTopic() {
	let tagName = document.getElementById("searchTopicTags").value;
	document.getElementById("searchTopicTags").value = "";
	let removedTopicTags;
	let updatedTopic = updatedTopics.find(function (uTopic) {
		return uTopic.topicId == currentlyEditingTopic;
	});
	if(updatedTopic) {
		removedTopicTags = updatedTopic.removedTags;
	}
	
	if(updatedTopic && removedTopicTags.find(function (uTag) {
		return uTag == tagName
	})) {
		let index = removedTopicTags.indexOf(tagName);
		removedTopicTags.splice(index, 1);
	}
	
	let existingTopicTag = userTopicTags.find(function (tag) {
		return tag.topicId == currentlyEditingTopic &&
			tag.tagName == tagName;
	});
	
	if (!existingTopicTag) {
		if(updatedTopic && 
				!updatedTopic.addedTags.find(function(tag) {
					return tag == tagName;
				})) {
			updatedTopic.addedTags.push(tagName);
		} else if(!updatedTopic) {
			//Add a new updated topic
			let upTopic = {
					topicId: currentlyEditingTopic,
					detail: null,
					addedTags: [tagName],
					removedTags: [],
					active: topicsByUser.find(function (top) {return top.id == currentlyEditingTopic;}).active
			}	
			updatedTopics.push(upTopic);
		}
	}
	
	populateTagsForEditTopic(currentlyEditingTopic);
}

function removeTagFromTopic(tagName) {
	let updatedTopicTags;
	
	let updatedTopic = updatedTopics.find(function (uTopic) {
		return uTopic.topicId == currentlyEditingTopic;
	});
	if(updatedTopic) {
		updatedTopicTags = updatedTopic.addedTags;
	}
	
	if(updatedTopic && updatedTopicTags.find(function (uTag) {
		return uTag == tagName
	})) {
		let index = updatedTopicTags.indexOf(tagName);
		updatedTopicTags.splice(index, 1);
	}
	
	let existingTopicTag = userTopicTags.find(function (tag) {
		return tag.topicId == currentlyEditingTopic &&
			tag.tagName == tagName;
	});
	
	if (existingTopicTag) {
		if(updatedTopic) {
			updatedTopic.removedTags.push(existingTopicTag.tagName);
		} else {
			//Add new update topic entry
			let uTopic = {
					topicId: currentlyEditingTopic,
					detail: null,
					addedTags: [],
					removedTags: [existingTopicTag.tagName],
					active: topicsByUser.find(function (top) {return top.id == currentlyEditingTopic;}).active
			}
			updatedTopics.push(uTopic);
		}
	}
	
	populateTagsForEditTopic(currentlyEditingTopic);
}

function displaySelectedTagsForTopic(tagList, elementId) {
	if (!tagList || !tagList.length) {
		document.getElementById(elementId).innerHTML = "<p class=\"text-center\"><label>No Tags</label></p>";
		return;
	}
	
	var blankColumns = tagList.length % 3;
	var row = "<div class=\"row\">";
	var endRow = "</div>";
	var column = "<div class=\"col-sm-4\"><label style=\"padding-left: 1%; padding-right: 1%\">--i--</label><button class=\"btn\" onclick=\"removeTagFromTopic('--i--')\">X</button></div>";
	var emptyColumn = "<div class=\"col-sm-4\"></div>";
	var content = row;
	var endOfRow = false;
	
	for(var i = 0; i < tagList.length; i++) {
		if (i % 3 == 0 && i != 0) {
			//End the row
			content += endRow;
			endOfRow = true;
		}
		
		if (endOfRow) {
			content += row;
			endOfRow = false;
		}
		if (tagList[i].tagName) {
			content += column.replace(/--i--/g, tagList[i].tagName);
		} else {
			content += column.replace(/--i--/g, tagList[i]);
		}	
	}
	
	if (blankColumns > 0) {
		for (var i = 0; i < blankColumns; i++) {
			content += emptyColumn;
		}
		//End the row
		content += endRow;
	}
	
	document.getElementById(elementId).innerHTML = content;
}

function remainingTopicContributors(topicId) {
	let existingContributors = topicContributors[topicId];
	
	let addedContributors = topicContributorsToAdd.filter(function (cont) {
		return cont.topicId == topicId;
	});
	
	let removedContributors = topicContributorsToRemove.filter(function (cont) {
		return cont.topicId == topicId;
	});
	
	let totalContributors;
	if(addedContributors) {
		totalContributors = existingContributors.concat(addedContributors);
	} else {
		totalContributors = existingContributors;
	}
	
	let remainingContributors = totalContributors.filter(function (contributor) {
		if(removedContributors.find(function (cont) {
			return cont.tidingId == contributor.tidingId;
		})) {
			return false;
		} else {
			return true;
		}
	});
	
	return remainingContributors;
}

function populateTopicTagSuggestions() {
	let topicTagSuggestions = document.getElementById("topicTagSuggestions");
	
	let options = "";
	
	existingTopicTags.forEach(function (foundTag) {
		options += "<option value=\""+foundTag.tagName+"\"/>";
	});
	
	topicTagSuggestions.innerHTML = options;
}

function saveChanges() {
	if(updatedTopics.length) {
		updateChangesToTopic(updatedTopics);
	}
	
	if(topicContributorsToAdd.length) {
		addNewContributorsToTopics(topicContributorsToAdd);
	}
	
	if(topicContributorsToRemove.length) {
		removeContributors(topicContributorsToRemove);
	}
	
	window.location.reload();
}

function updateChangesToTopic(updatedTopics) {
	$.ajax({
		url: '/tech-estate/update-topic-details',
		contentType: 'application/json',
		method: 'POST',
		data: JSON.stringify(updatedTopics),
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
		}
	}).done(function () {
		//TODO close the dialogue perhaps...
	});
}

function addNewContributorsToTopics(newTopicContributors) {
	$.ajax({
		url: '/tech-estate/add-topic-contributors',
		contentType: 'application/json',
		method: 'POST',
		data: JSON.stringify(newTopicContributors),
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
		}
	}).done(function () {
		//TODO close the dialogue perhaps...
	});
}

function removeContributors(removedContributors) {
	$.ajax({
		url: '/tech-estate/remove-topic-contributors',
		contentType: 'application/json',
		method: 'POST',
		data: JSON.stringify(removedContributors),
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
		}
	}).done(function () {
		//TODO close the dialogue perhaps...
	});
}

const topicContributorTemplate = 
`
<tr>
	<td>:tidingId</td>
	<td class="text-left"><button class="btn btn-warning" onclick="removeOwnerFromTopic(':tidingId')">Remove</button></td>
</tr>
`;

const topicTableRowTemplate = 
`<tr id="topic-edit-:topicId" onclick="setSelectedTopicToUpdate(:topicId)" style="cursor:pointer">
	<td><input class="form-control" type="text" value=":title"></td>
	<td><input class="form-control" type="text" value=":detail"></td>
	<td style="cursor: pointer" onclick="toggleTopicStatus(:topicId)">:active</td>
</tr>`;

const noTopicsRowTemplate =
`<tr>
	<td cols="3">You Have Not Created Any Topics</td>
</tr>`;



