window.onload = function() {
	ceaseLoadingPage();
	
	/*document.getElementById("totalAmountPayableMessage").innerHTML = 
		totalAmountPayableMessageTemplate.replace(":value", revenueBreakdown.totalPayablePence / 100);*/
	
	document.getElementById("totalPayable").value = revenueBreakdown.totalPayablePence / 100;
		
	populateBreakdownTable();
}

//var userAccountNumber;
//var userSortCode;
//var userAccountName;

//function confirmRetrievePayout() {
//	userAccountNumber = document.getElementById("userAccountNumber").value;
//	userSortCode = document.getElementById("userSortCode").value;
//	userAccountName = document.getElementById("userAccountName").value;
//	
//	let messageContent = "<pre>Are you sure the details you have entered are correct ?\n" +
//		"Account Holder Name: " + userAccountName + " \n" +
//		"Account Number: " + userAccountNumber + " \nSort Code: " + userSortCode + "</pre>";
//	
//	showYesNoConfirm(messageContent, submitPayoutRequest);
//}

//function openAccountDetailsDialogue() {
//	document.getElementById("accountDetailsDialogue").style.display = "block";
//	
//}
//
//function cancelPayoutDetails() {
//	document.getElementById("accountDetailsDialogue").style.display = "none";
//}

//let submitPayoutRequest = function () {
//	let memberPaymentDetails = {
//			"bankAccountHolderName": userAccountName,
//			"name": userAccountName,
//			"country": "GB",
//			"bankAccountCountry": "GB",
//			"accountNumber": userAccountNumber,
//			"sortCode": userSortCode,
//	};
//	
//	$.ajax({
//		url: '/revenue-payable/collect-payment',
//		method: 'POST',
//		data: JSON.stringify(memberPaymentDetails),
//		contentType: 'application/json',
//		headers: {
//			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
//		}
//	});
//}

function populateTotalPayable() {
	let totalPayable = revenueBreakdown.totalPayable;
	
}

function populateBreakdownTable() {
	//Populate the table with values from the sold item list
	let tableContent = "";
	let tableBody = document.getElementById("soldInventory");
	
	let soldItems = revenueBreakdown.breakdownOfSoldInventory;
	
	if (soldItems.length) {
		soldItems.forEach(function (item) {
			tableContent += salesItemTableEntry
				.replace(":category", item.category)
				.replace(":title", item.title)
				.replace(":revenue", item.pricePence / 100)
				.replace(":sales", item.sales);
		});
		
	} else {
		tableContent = noSalesTableEntry;
	}
	
	tableBody.innerHTML = tableContent;
}

const totalAmountPayableMessageTemplate = "Amount payable to you £:value";

const salesItemTableEntry =
`
<tr>
<td>:category</td>
<td>:title</td>
<td>:revenue</td>
<td>:sales</td>
</tr>
`;

const noSalesTableEntry = 
`
<tr>
<td>N/A</td>
<td>N/A</td>
<td>0</td>
<td>0</td>
</tr>
`;