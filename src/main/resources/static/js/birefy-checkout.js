//var stripe = Stripe('pk_test_oq4ZKZSCY6hFucGf6FSjyhT6');
var stripe = Stripe('pk_live_C0qfD7uUCCbrrMTjjnipIafF');

var elements = stripe.elements();

var cardStyle = {
		base: {
			fontSize: '20px',
			color: "black",
		}
};

var card = elements.create('card', {style: cardStyle});

window.onload = function () {
	ceaseLoadingPage();
	
	populatePurchaseDetails();
	card.mount("#card-element");
}

function populatePurchaseDetails() {
	document.getElementById("purchase-details").innerHTML = 
		detailsTemplate.replace(":productName", productName)
		.replace(":productPrice", (productPrice / 100));
}

function makePurchase() {
	//openFailureDialogue("Purchases are currently unsupported. Feature support coming soon.");
	
	var cardholderName = document.getElementById('cardholder-name').value;
	
	stripe.handleCardPayment(
			secret, card, {
				payment_method_data: {
					billing_details: {name: cardholderName}
				}
			}).then(function(result) {
				if (result.error) {
					openFailureDialogue("Processing the payment has failed. Please try again");
				} else {
					postProcessPaymentIntent(result.paymentIntent);
				}
			});
}

function postProcessPaymentIntent(paymentIntent) {
	$.ajax({
		url: '/purchase/post-process-payment',
		contentType: 'application/json',
		method: 'POST',
		data: JSON.stringify(paymentIntent),
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
		}
	}).done(function (result) {
		switch (result) {
		case ('CLEARED'): {
			operationCompleteAlert(proceedToDownloadPage);
			break;
		}
		case ('PENDING'): {
			openFailureDialogue("Your payment is still being processed. Access to your purchase will be made available once payment processing has completed.", null);
			break;
		}
		default: {
			openFailureDialogue();
		}
		}
	})
}

var proceedToDownloadPage = function () {
	let purchase = {'id': itemId, 'category': category};
	
	$.ajax({
		url: '/purchase/action',
		method: 'POST',
		contentType: 'application/json',
		mimeType: 'application/json',
		dataType: 'html',
		data: JSON.stringify(purchase),
		headers: {
			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
		},
		error: function () {
			openFailureDialogue("Unable to process card details.");
		},
		success: function (result) {
			window.location.href = result;
		}
	});
}

//function postProcessPayment() {
//	//Send details to the server
//	let purchase = {};
//	let chargeDetails = {};
//	
//	chargeDetails.customerMemberId = customerId;
//	chargeDetails.supplierMemberId = supplierId;
//	
//	if (category == "DEAL") {
//		chargeDetails.dealId = itemId;
//	} else {
//		chargeDetails.mediaId = itemId;
//	}
//	chargeDetails.source = result.token;
//	chargeDetails.value = productPrice;
//	
//	chargeDetails.mediaCategory = category;
//	
//	purchase.chargeDescription = productName;
//	purchase.chargeDetails = chargeDetails;
//	
//	//Send the information to the server
//	$.ajax({
//		url: '/purchase/action',
//		method: 'POST',
//		contentType: 'application/json',
//		mimeType: 'application/json',
//		dataType: 'html',
//		data: JSON.stringify(purchase),
//		headers: {
//			'X-CSRF-TOKEN': $("meta[name='_csrf']").attr("content")
//		},
//		error: function () {
//			openFailureDialogue("Unable to process card details.");
//		},
//		success: function (result) {
//			window.location.href = result;
//		}
//	});
//}

function cancelPurchase() {
	let redirectStatement;

	switch (category) {
	case "DEAL": redirectStatement = dealsPage; break;
	case "SOUND": redirectStatement = soundPage; break;
	case "PAINTING": redirectStatement = paintingPage; break;
	}
	
	window.location = redirectStatement;
}


const detailsTemplate = 
`
<tr>
	<td>:productName</td>
	<td>:productPrice</td>
</tr>
`;

const soundPage = "/sound";
const paintingPage = "/birefy-galleries";
const dealsPage = "/deals";
const homePage = "/home";

