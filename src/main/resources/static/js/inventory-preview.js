window.onload = function () {
	ceaseLoadingPage();
	
	//Set the content title
	document.getElementById("content-title").innerHTML = contentTitle;
	
	if(isSoundPreview) {
		document.getElementById("soundPreview").style.display = "block";
		document.getElementById("currently-playing").src = contentUrl;
		
		let audioPlayer = document.getElementById("audio-player");
		audioPlayer.load();
	} else {
		document.getElementById("imagePreview").style.display = "block";
		document.getElementById("displaying-image").src = contentUrl;
	}
}

var playing = false;

function playResumeMusic() {
	if (playing) {
		document.getElementById("audio-player").pause();
		playing = false;
	} else {
		document.getElementById("audio-player").play();
		playing = true;
	}
}

function volumeUp() {
	document.getElementById("audio-player").volume += 0.1;
}

function volumeDown() {
	document.getElementById("audio-player").volume -= 0.1;
}

