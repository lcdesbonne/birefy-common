//Global variable listOfTopics
//Global variable listOfQuestions

/*
 * Different page scrolling functionality will be created based
 * what is being scroll through 
 */

//Topic id that marks the top of the topic table
var currentTopicId;
var topicIndexInList; // The index in the complete results list
var topicIndexInView; // The index of the topic in view
var availableQuestionsCount; // The available questions for this topic
var availableQuestionsForTopic;
// Question id that marks the top of the question table
var currentQuestionId;
var questionIndexInList; // The index index in the
								// availableQuestionsForTopic list
var questionIndexInView; // The index of the question in view

// Full topic objects reflecting what is currently displayed
var topicsInView;
// Full question objects reflecting what is currently displayed
var questionsInView;

// An array of topic names
var topicViewList;
// An array of question names
var questionViewList;
// The detail for the current question
var questionDetail = "";

// The median of the result set to display
var resultSetMedian;
var resultsPerView = maxEntriesPerView;
var maxEntriesPerView = 9;

// A boolean flag indicating that the topic cycle should 
// Move to the next with questions available for viewing
var topicWithQuestions = false;

var initialLoad = true;

//Tags retrieved from tag search
var knownTags;
//Tags selected by the user
var selectedTags = [];

/*
 * Initialize the lists
 */
function initializeLists() {
	ceaseLoadingPage();
	
	topicIndexInList = 0;
	topicIndexInView = 0;
	availableQuestionsForTopic = [];
	questionIndexInList = 0;
	questionIndexInView = 0;
	topicsInView = [];
	questionsInView = [];
	topicViewList = [];
	questionViewList = [];
	// Identify the initial topic and question ids
	currentTopicId = listOfTopics.length ? listOfTopics[0].id : false;
	
	if(!currentTopicId) {
		setNoSearchResults();
	} else {
		// calculate the results per view for topics
		calculateResultsPerView(true);
		
		// Build the first set of viewable topics
		for (var i = 0; i < resultsPerView; i++) {
			topicsInView.push(listOfTopics[i]);
			topicViewList.push(listOfTopics[i].topicName);
		}
		// Construct the topic table
		generateTopicTable(0);
		
		// Calculate the results per view for questions
		displayQuestionsForTopic();
		
		addQuestionDetail();
		
		if (initialLoad) {
			//Hide the topic form
			$(".topSearch").hide();
		}
		initialLoad = false;
	}
}

function loadEndOfData() {
	calculateResultsPerView(true);
	topicsInView = [];
	topicViewList = [];
	topicIndexInList = listOfTopics.length - 1;
	topicIndexInView = resultsPerView - 1;
	for (var i = listOfTopics.length - resultsPerView; i < listOfTopics.length; i++) {
		topicsInView.push(listOfTopics[i]);
		topicViewList.push(listOfTopics[i].topicName);
	}
	generateTopicTable(topicIndexInView);
	calculateResultsPerView(false);
	displayQuestionsForTopic();
	addQuestionDetail();
}

function calculateResultSetMedianIndex() {
	return (resultsPerView - 1)/2 < 0 ? 0 : (resultsPerView - 1)/2;
}

function displayQuestionsForTopic(questionIndex) {
	if(!questionIndex) {
		questionIndex = 0;
	}
	// Calculate the results per view for questions
	calculateResultsPerView(false);
	questionsInView = [];
	questionViewList = [];
	questionDetail = "No Questions Found With Current Search Criteria";
	for (var i = 0; i < resultsPerView; i++) {
		questionsInView.push(availableQuestionsForTopic[i]);
		questionViewList.push(availableQuestionsForTopic[i].title);
	}
	if (resultsPerView >= 0) {
		currentQuestionId = questionsInView[questionIndex].id;
		questionDetail = questionsInView[questionIndex].detail;
	}
	// Construct the question table
	generateQuestionTable(questionIndex);
}

function setNoSearchResults() {
	document.getElementById("questionTable").innerHTML = "No Results Found.";
	document.getElementById("topicTable").innerHTML = "No Results Found.";
}

/*
 * Accepts a boolean indicating whether this should be done for topic or
 * question
 */
function calculateResultsPerView(topic) {
	// Reset to default values
	resultsPerView = maxEntriesPerView;
	if (topic) {
		if (listOfTopics.length < maxEntriesPerView) {
			// Modify the resultsPerView
			var listSize = listOfTopics.length;
			// Assess whether the length of the list is even
			if (listSize % 2 == 0) {
				resultsPerView = listSize - 1;
			} else {
				resultsPerView = listSize;
			}
		}
	} else {
		// Assess the number of available questions for the
		// Selected topic
		var currentTopicId = listOfTopics.length ? listOfTopics[topicIndexInList].id : false;
		if(currentTopicId) {
			availableQuestionsCount = 0;
			availableQuestionsForTopic = [];
			for (var i = 0; i < listOfQuestions.length; i++) {
				var question = listOfQuestions[i];
				if (question.topicId == currentTopicId) {
					availableQuestionsCount++;
					availableQuestionsForTopic.push(question);
				}
			}
			if (availableQuestionsCount < maxEntriesPerView) {
				var listSize = availableQuestionsCount;
				if (listSize % 2 == 0) {
					resultsPerView = listSize - 1;
				} else {
					resultsPerView = listSize;
				}
			}
		}
	}
}

function getIndexInTopicQuestions(questionTitle) {
	// cycle through the list of questions in view
	// return the matching index
	for (var i = 0; i < questionsInView.length; i++) {
		if (questionsInView[i].title == questionTitle) {
			return i;
		}
	}
}

/*
 * Accepts the index of the question in view
 */
function generateQuestionsInViewSet() {
	var preceedingIndex = questionIndexInList - resultSetMedian;
	var proceedingIndex = questionIndexInList + resultSetMedian;
	// Clear the list of questions In view
	questionViewList = [];
	questionsInView = [];
	for (var i = preceedingIndex; i <= proceedingIndex; i++) {
		questionViewList.push(availableQuestionsForTopic[i].title);
		questionsInView.push(availableQuestionsForTopic[i]);
	}
}

function generateTopicInViewSet() {
	var preceedingIndex = topicIndexInList - resultSetMedian;
	var proceedingIndex = topicIndexInList + resultSetMedian;
	if (topicIndexInList == 0) {
		preceedingIndex = 0;
		proceedingIndex = 2*resultSetMedian;
	}
	// Clear the list of questions In view
	topicViewList = [];
	for (var i = preceedingIndex; i <= proceedingIndex; i++) {
		topicViewList.push(listOfTopics[i].topicName);
	}
}

function isNearEndOfResultSet(dataList) {
	var dataLength = dataList.length;
	if (dataList % 2 == 0) {
		// Even data list
		dataLength -= 1;
	}
	return (dataLength - questionIndexInList - 1) <= resultSetMedian;
}

function isNearEndOfTopicData(topicTitle) {
	var dataLength = listOfTopics.length;
	//Retrieve the index of the topic in the data
	var topicIndex = -1;
	for (var i = 0; i < dataLength; i++) {
		if (listOfTopics[i].topicName == topicTitle) {
			topicIndex = i;
			break;
		}
	}
	topicIndexInList = topicIndex;
	return (dataLength - topicIndex - 1) <= resultSetMedian;
}

/*
 * Generates topic, question, and detail for use in the page based on a scroll
 * action on the question list
 */
function cycleQuestionList(increase) {
	if(currentTopicId == false) {
		return;
	}
	// Question scrolling is constrained by the topic in view
	calculateResultsPerView(false);
	var questionTitle = questionViewList[questionIndexInView];
	var viewIndex = getIndexInTopicQuestions(questionTitle);
	
	// If the index is less than the median highlight
	// Do not modify the data, instead just increase or decrease the
	// indexToHighlight
	resultSetMedian = calculateResultSetMedianIndex();
	
	if (resultSetMedian == 0 && listOfQuestions.length != 0) {
		if (increase) {
			//Load the next topic with questions
			questionIndexInList = 0;
			questionIndexInView = 0;
			topicWithQuestions = true;
			cycleTopicList(true);
			return;
		} else {
			questionIndexInList = 0;
			questionIndexInView = 0;
			topicWithQuestions = true;
			cycleTopicList(false);
			return;
		}
	}
	
		if(increase) {
			// If we are at the median point check to see if a cycle can be done
			// Maintaining the median point
			if (viewIndex == resultSetMedian && !isNearEndOfResultSet(availableQuestionsForTopic)) {
				// Increment the question in view by one
				questionIndexInList += 1;
				// Generate a new set of questions
				generateQuestionsInViewSet();
				generateQuestionTable(resultSetMedian);
			} else if (questionIndexInList != (availableQuestionsForTopic.length - 1)){
				// Need to increment the index in the list
				questionIndexInList += 1;
				// Need to increment the index in view
				questionIndexInView = viewIndex + 1;
				generateQuestionTable(questionIndexInView)
			} else {
				//Load the next topic
				questionIndexInList = 0;
				questionIndexInView = 0;
				questionsInView = [];
				topicWithQuestions = true;
				cycleTopicList(true);
			}
		} else {
			// Decreasing action taken
			if (viewIndex == resultSetMedian && questionIndexInList > resultSetMedian) {
				// Decrement the question view by one
				questionIndexInList -= 1;
				// Generate a new set of questions
				generateQuestionsInViewSet();
				generateQuestionTable(resultSetMedian);
			} else if (viewIndex != 0) {
				questionIndexInList -= 1;
				questionIndexInView = viewIndex - 1;
				generateQuestionTable(questionIndexInView)
			} else {
				//Load the previous topic
				questionIndexInList = 0;
				questionIndexInView = 0;
				questionsInView = [];
				topicWithQuestions = true;
				cycleTopicList(false);
			}
		}
		questionDetail = availableQuestionsForTopic[questionIndexInList].detail;
	 
	addQuestionDetail();
}

/*
 * Generates topic, question, and detail for use in the page based on a scroll
 * action on the topic list
 */
function cycleTopicList(increase) {	
	if (currentTopicId == false) {
		return;
	}
	calculateResultsPerView(true);
	resultSetMedian = calculateResultSetMedianIndex();
	
	if(increase) {
		//If at the median point check to see if a cycle can be
		//done maintaining the point
		if (topicIndexInView == resultSetMedian && !isNearEndOfTopicData(topicViewList[topicIndexInView])) {
			//Increment the question in view by 1
			topicIndexInList += 1;
			//Generate a new set of topics
			generateTopicInViewSet();
			generateTopicTable(resultSetMedian);
		} else if (topicIndexInList != (listOfTopics.length - 1)) {
			//Increment the index in the list
			topicIndexInList += 1;
			topicIndexInView += 1;
			generateTopicTable(topicIndexInView);
		} else {
			//Reset to the beginning of the topics
			topicIndexInList = 0;
			topicIndexInView = 0;
			questionIndexInList = 0;
			questionIndexInView = 0;
			generateTopicInViewSet();
			generateTopicTable(0);
		}
	} else {
		//Decreasing action taken
		if (topicIndexInView == resultSetMedian && topicIndexInList > resultSetMedian) {
			//Decrement values
			topicIndexInList -= 1;
			generateTopicInViewSet();
			generateTopicTable(resultSetMedian);
		} else if (topicIndexInList != 0) {
			topicIndexInView -= 1;
			topicIndexInList -= 1;
			generateTopicTable(topicIndexInView);
		} else {
			//No more to load
			if (!topicWithQuestions) {
				loadEndOfData();
				return;
			} else {
				topicIndexInList = listOfTopics.length - 1;
				topicIndexInView = resultsPerView - 1;
			}
		}
	}
	
	displayQuestionsForTopic();
	addQuestionDetail();
	if (questionsInView.length == 0 && topicWithQuestions) {
		cycleTopicList(increase);
	} else if (questionsInView.length != 0 && topicWithQuestions) {
		topicWithQuestions = false;
	}
}

/*
 * Modify the topic and question lists based on a filter by topic title
 */
function filterListByTopicTitle() {
	var title = document.getElementById("topValue").value;
	
	if(selectedTags.length != 0) {
		var selectedTagIds = getSelectedTagIds();
		//Perform a search with tags
		$.ajax({
			url: '/questions/filterByTopicNameAndTags?name='+title+'&tags='+selectedTagIds,
			type: 'GET',
			success: function (data) {
				//Assign the new topic and question lists
				listOfTopics = data.topics;
				listOfQuestions = data.questions;
				initializeLists();
			}
		});
	} else {
		$.ajax({
			url: '/questions/filterByTopicTitle?title='+title,
			type: 'GET',
			success: function (data) {
				// Assign the new topic and question lists
				listOfTopics = data.topics;
				listOfQuestions = data.questions;
				initializeLists();
			}
		});
	}
}

/*
 * Modify the topic and question lists based on a filter by question title
 */
function filterListByQuestionTitle() {
	var title = document.getElementById("txtValue").value;
	
	if (selectedTags.length != 0) {
		var selectedTagIds = getSelectedTagIds();
		$.ajax({
			url: '/questions/filterByQuestionNameAndTags?title='+title+'&tags='+selectedTagIds,
			type: 'GET',
			success: function (data) {
				// Assign the new topic and question lists
				listOfTopics = data.topics;
				listOfQuestions = data.questions;
				initializeLists();
			}
		})
	} else {
		$.ajax({
			url: '/questions/filterQuestionByTitle?title='+title,
			type: 'GET',
			success: function (data) {
				// Assign new topic and question lists
				listOfTopics = data.topics;
				listOfQuestions = data.questions;
				initializeLists();
			}
		});
	}
}

(function() {
	$('input[type="radio"]').click(function(){
		var inputValue = $(this).attr("value");
		var targetForm = $("." + inputValue);
		$(".textQuery").not(targetForm).hide();
		$(targetForm).show();
	});
})();


/*
 * Retrieves a list of tags registered
 */
function searchAvailableTags() {
	let tagName = document.getElementById("selectedTagName").value;
	
	if(!tagName || tagName.length < 2) {
		return;
	}
	
	$.ajax({
		url: '/questions/findTags?tagName='+tagName,
		type: 'GET',
		success: function (data) {
			knownTags = data;
			populateTagSearchList();
		}
	});
}

function getSelectedTagIds() {
	var selectedTagIds = [];
	for(var i = 0; i < selectedTags.length; i++) {
		selectedTagIds.push(selectedTags[i].id);
	}
	return selectedTagIds;
}

function populateTagSearchList() {
	var datalist = document.getElementById("taglist");
	var options = "";
	for(var x = 0; x < knownTags.length; x++) {
		var tag = knownTags[x];
		options += "<option value=\""+tag.tagName+"\">";
	}
	
	datalist.innerHTML = options;
}

function selectTag() {
	var tagName = document.getElementById("selectedTagName").value;
	//Find the tag details in the known tags
	for(var i = 0; i < knownTags.length; i++) {
		if (knownTags[i].tagName == tagName && !containsTagName(tagName)) {
			selectedTags.push(knownTags[i]);
			break;
		}
	}
		
	//Display the selected tags in the page
	displaySelectedTags();
	filterListByTopicTitle();
	document.getElementById("selectedTagName").value = "";
}

function containsTagName(tagName) {
	for(var i = 0; i < selectedTags.length; i++) {
		if(selectedTags[i].tagName == tagName) {
			return true;
		}
	}
	return false;
}

function unselectTag(tagName) {
	//Find the details in the selected tags for removal
	for(var i = 0; i < selectedTags.length; i++) {
		if (selectedTags[i].tagName == tagName) {
			selectedTags.splice(i,1);
			break;
		}
	}
	
	//Display the selected tags in the page
	displaySelectedTags();
	filterListByTopicTitle();
}

function displaySelectedTags() {
	var blankColumns = selectedTags.length % 3;
	var row = "<div class=\"row\">";
	var endRow = "</div>";
	var column = "<div class=\"col-sm-4\"><label>--i--<img src=\"/images/deleteCross.svg\" onclick=\"unselectTag('--i--')\"></div>";
	var emptyColumn = "<div class=\"col-sm-4\"></div>";
	var content = row;
	var endOfRow = false;
	
	if (selectedTags.length == 0) {
		document.getElementById("tlist").innerHTML = "<p>All Tags</p>";
		return;
	}
	for(var i = 0; i < selectedTags.length; i++) {
		if (i % 3 == 0 && i != 0) {
			content += column.replace(/--i--/g,selectedTags[i].tagName);
			//End the row
			content += endRow;
			endOfRow = true;
			continue;
		}
		if (endOfRow) {
			content += row;
			endOfRow = false;
		}
		content += column.replace(/--i--/g, selectedTags[i].tagName);
	}
	
	if (blankColumns > 0) {
		for (var i = 0; i < blankColumns; i++) {
			content += emptyColumn;
		}
		//End the row
		content += endRow;
	}
	
	document.getElementById("tlist").innerHTML = content;
}

// table row format <tr><td></td></tr> add class info to tr when necessary

/*
 * Generates the html for the topic table, accepts an integer which will
 * indicate which row will be highlighted Note: currently will be using a
 * default bootstrap class info
 */
function generateTopicTable(indexToHighlight) {
	if(currentTopicId == false) {
		return;
	}
	var tTable = document.getElementById("topicTable");
	var rows = "";
	for(var i = 0; i < topicViewList.length; i++) {
		var detail = findTopicData(topicViewList[i]);
		if (indexToHighlight == i) {
			var row = "<tr style=\"background-image: url('images/conceptDarkTranslucent.png')\"><td>"+topicViewList[i]+"</td></tr> \n";
			rows += row;
			//Add topic detail to the page
			document.getElementById("topicDetail").innerHTML = findTopicData(topicViewList[i]);
		} else {
			var row = "<tr><td>"+topicViewList[i]+"</td></tr> \n";
			rows += row;
		}
	}
	if(listOfTopics.length < maxEntriesPerView) {
		//Add some blank rows
		for (var i = 0; i < (maxEntriesPerView - listOfTopics.length); i++) {
			var row = "<tr><td style=\"opacity:0; border:none\"> - </td></tr>";
			rows += row;
		}
	}
	tTable.innerHTML = rows;
} 

function findTopicData(topicName) {
	for(var i = 0; i < listOfTopics.length; i++) {
		if (listOfTopics[i].topicName == topicName) {
			return listOfTopics[i].topicDetail;
		}
	}
}

/*
 * Generates the html for the question table, accepts an integer indicating the
 * row to be highlighted Note: currently will be using a default bootstrap class
 * info
 */
function generateQuestionTable(indexToHighlight) {
	var qTable = document.getElementById("questionTable");
	var rows = "";
	for(var i = 0; i < questionViewList.length; i++ ) {
		if (indexToHighlight == i) {
			var row = "<tr style=\"background-image: url('images/conceptDarkTranslucent.png')\"><td>"+questionViewList[i]+"</td></tr> \n";
			rows += row;
		} else {
			var row = "<tr><td>"+questionViewList[i]+"</td></tr> \n";
			rows += row;
		}
	}
	qTable.innerHTML = rows;
}

/*
 * Populate the currently highlighted question detail to the detail table
 */
function addQuestionDetail() {
	var qDetail = document.getElementById("questionDetails");
	var row = "<tr><td>"+questionDetail+"</td></tr>";
	qDetail.innerHTML = row;
}

function openQuestion() {
	if(currentTopicId == false) {
		return;
	}
	
	if (currentQuestionId) {
		var questionId = currentQuestionId;
		window.location = "/innovation?id="+questionId;
	} else {
		openFailureDialogue("Unable to identify question. Cannot proceed");
	}
	
}


