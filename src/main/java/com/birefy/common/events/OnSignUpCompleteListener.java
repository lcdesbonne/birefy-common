package com.birefy.common.events;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import com.birefy.common.dto.CommonUser;
import com.birefy.common.entity.MemberVerification;
import com.birefy.common.messages.MessageProperty;
import com.birefy.common.repo.MemberVerificationRepository;

@Component
public class OnSignUpCompleteListener implements ApplicationListener<OnSignUpCompleteEvent> {
	private static final Logger LOGGER = LoggerFactory.getLogger(OnSignUpCompleteListener.class);
	
	@Value("${application-url.domain}")
	private String applicationDomain;
	
	@Value("${application-url.port}")
	private String applicationPort;
	
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	@Qualifier("verificationMessage")
	private MessageSource messageSource;
	@Autowired
	private MemberVerificationRepository memberVerificationRepository;
	
	@Override
	public void onApplicationEvent(OnSignUpCompleteEvent event){
		CommonUser user = (CommonUser) event.getSource();
		String token = UUID.randomUUID().toString();
		
		//Create a member verification and save to the database
		MemberVerification memberVerification = new MemberVerification(user.getId(), token);
		try {
			memberVerificationRepository.addNewRecord(memberVerification);
			
			//Send confirmation email
			String email = user.getUsername();
			String subject = "Birefy Registration Confirmation";
			
			StringBuilder webAddressBuilder = new StringBuilder();
			webAddressBuilder.append(applicationDomain);
			
			if(StringUtils.isNotEmpty(applicationPort)) {
				webAddressBuilder.append(":" + applicationPort);
			}
			MessageProperty[] messapeProperties = new MessageProperty[] {
					new MessageProperty("webAddress", webAddressBuilder.toString())
					, new MessageProperty("token", token)
			};
			
			String message = messageSource.getMessage("verify-account", messapeProperties, event.getLocale());
			
			SimpleMailMessage electronicMessage = new SimpleMailMessage();
			electronicMessage.setTo(email);
			electronicMessage.setSubject(subject);
			electronicMessage.setText(message);
			
			mailSender.send(electronicMessage);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			LOGGER.error("Error setting validation details for user: "+user.getUsername());
		}
		
	}
}
