package com.birefy.common.events;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import com.birefy.common.dto.tiding.TidingIdDTO;
import com.birefy.common.tidings.Tiding;

@Component
public class TidingsEventListener {
	private static final Logger LOGGER = LoggerFactory.getLogger(TidingsEventListener.class);
	
	@Autowired
	private SimpMessageSendingOperations messagingTemplate;
	
	@EventListener
	public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
		StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
		
		String username = (String) headerAccessor.getSessionAttributes().get("username");
		
		if(Optional.ofNullable(username).isPresent()){
			LOGGER.info("User Disconnected : " + username);
			
			Tiding tiding = new Tiding();
			tiding.setType(Tiding.MessageType.LEAVE);
			tiding.setSender(new TidingIdDTO(username));
			
			messagingTemplate.convertAndSend("tidings", tiding);
		}
	}
}
