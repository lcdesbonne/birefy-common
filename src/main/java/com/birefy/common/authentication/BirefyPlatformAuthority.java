package com.birefy.common.authentication;

import org.springframework.security.core.GrantedAuthority;

public enum BirefyPlatformAuthority implements GrantedAuthority {
	//Administration privileges
	ROYAL(2),
	//Approved members of the Birefy community
	LORD(1), 
	//Allowed to view and purchase content, these are members of the public
	CITIZEN(0);
	
	private int value;
	
	private BirefyPlatformAuthority(int value) {
		this.value = value;
	}
	
	public int value() {
		return value;
	}
	
	@Override
	public String getAuthority() {
		return this.name();
	}
	
}
