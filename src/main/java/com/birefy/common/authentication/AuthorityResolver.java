package com.birefy.common.authentication;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import com.birefy.common.authentication.credentials.CommonUser;

public class AuthorityResolver {
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthorityResolver.class);
	
	public static void accessLevel(BirefyPlatformAuthority authority, String url) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(!Optional.ofNullable(user).isPresent()) {
			switch ((BirefyPlatformAuthority) user.getAuthorities().stream().findFirst().get()) {
			case ROYAL: return;
			case LORD : {
				if (authority == BirefyPlatformAuthority.LORD) {
					return;
				} else {
					generateException(user, url);
				}
			};
			case CITIZEN : {
				if (authority == BirefyPlatformAuthority.CITIZEN) {
					return;
				} else {
					generateException(user, url);
				}
			};
			default: generateException(user, url);
			}
		}
	}
	
	public static Optional<BirefyPlatformAuthority> getBirefyPlatformAuthority(List<GrantedAuthority> authorities) {
		BirefyPlatformAuthority[] platformAuthorities = BirefyPlatformAuthority.values();
		
		for (GrantedAuthority auth : authorities) {
			for (BirefyPlatformAuthority platformAuth : platformAuthorities) {
				if (platformAuth == auth) {
					return Optional.of(platformAuth);
				}
			}
		}
		
		return Optional.empty();
	}
	
	private static void generateException(CommonUser user, String url) {
		String userId = Optional.ofNullable(user).isPresent() ? String.valueOf(user.getId()) : "unknown";
		LOGGER.error("Permission denied to " + url + " for user " + userId);
		throw new AccessDeniedException("Permission denied for " + url + " " + userId);
	}
	
}
