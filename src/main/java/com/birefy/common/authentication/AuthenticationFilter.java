package com.birefy.common.authentication;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.birefy.common.authentication.credentials.CommonUser;

public class AuthenticationFilter extends BasicAuthenticationFilter {
	
	public AuthenticationFilter(final AuthenticationManager authManager) {
		super(authManager);
	}
	
	@Override
	protected void onSuccessfulAuthentication(HttpServletRequest request, 
			HttpServletResponse response, Authentication authResult) throws IOException {
		
//		CommonUser userCred = (CommonUser)authResult.getPrincipal();
//
//		response.setHeader("token", userCred.getAuthToken());
	}

}
