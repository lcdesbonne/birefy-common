package com.birefy.common.authentication;

import java.util.Arrays;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Service;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.member.MemberDTO;
import com.birefy.common.entity.Member;
import com.birefy.common.exception.UserVerificationException;
import com.birefy.common.service.MemberService;

@Service("birefyPlatformAuthenticationProvider")												
public class BirefyPlatformAuthenticationProvider implements OAuth2UserService<OidcUserRequest, OidcUser> {
	private static final String VERIFICATION_ADVICE_MESSAGE = "Please contact support at support@birefy.com";
	private static final String EMAIL_REQUIRES_VERIFICATION_MESSAGE = "Please verify your email";

	@Autowired
	private MemberService memberService;
	
	@Override
	public OidcUser loadUser(OidcUserRequest userRequest) throws OAuth2AuthenticationException {
		boolean emailVerified = userRequest.getIdToken().getEmailVerified();
		CommonUser userDetails = new CommonUser(userRequest.getAdditionalParameters(), userRequest.getIdToken());
		
		String email = userDetails.getEmail();
		
		Optional<Member> existingMember = memberService.getMemberByEmail(email);
				
		if (existingMember.isPresent()) {
			Member member = existingMember.get();
			
			boolean permitted = emailVerified & member.isActive() && member.isEnabled();
			
			if (!permitted) {
				throw new OAuth2AuthenticationException(new UserVerificationException(), VERIFICATION_ADVICE_MESSAGE);
			}
			
			BirefyPlatformAuthority authority = getMemberAuthority(member);
			
			userDetails.setAuthorities(Arrays.asList(authority));
			userDetails.setId(member.getId());
			
		} else {
			
			if (!emailVerified) {
				throw new OAuth2AuthenticationException(new UserVerificationException(), EMAIL_REQUIRES_VERIFICATION_MESSAGE);
			}
			//Create a new member
			MemberDTO newMember = mapToNewMemberDTO(userDetails);
			
			Member member = memberService.addNewMember(newMember);
			
			BirefyPlatformAuthority authority = getMemberAuthority(member);
			
			userDetails.setAuthorities(Arrays.asList(authority));
			userDetails.setId(member.getId());
		}
		
		return userDetails;
	}

	
	
	private MemberDTO mapToNewMemberDTO(CommonUser commonUser) {		
		MemberDTO memberDTO = new MemberDTO();
		memberDTO.setFirstName(commonUser.getFirstName());
		memberDTO.setLastName(commonUser.getLastName());
		memberDTO.setEmail(commonUser.getEmail());
		
		return memberDTO;
	}
	
	
	private BirefyPlatformAuthority getMemberAuthority(Member member) {
		BirefyPlatformAuthority memberAuthority = null;
		
		switch (member.getAuthority()) {
		case 0: {
			memberAuthority = BirefyPlatformAuthority.CITIZEN;
			break;
		}
		case 1: {
			memberAuthority = BirefyPlatformAuthority.LORD;
			break;
		}
		case 2: {
			memberAuthority = BirefyPlatformAuthority.ROYAL;
			break;
		}
		}
		
		return memberAuthority;
	}

}
