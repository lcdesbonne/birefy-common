package com.birefy.common.authentication.credentials;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

public class BirefyPlatformAuthentication implements Authentication {
	private static final long serialVersionUID = 1L;
	
	private String name;
	private CommonUser principal;
	private boolean authenticated;
	private Object details;
	
	public BirefyPlatformAuthentication(CommonUser commonUser, boolean authenticated, String name) {
		this.principal = commonUser;
		this.name = name;
		this.authenticated = authenticated;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return principal.getAuthorities();
	}

	@Override
	public Object getCredentials() {
		return this;
	}

	@Override
	public Object getDetails() {
		return details;
	}

	@Override
	public Object getPrincipal() {
		return principal;
	}

	@Override
	public boolean isAuthenticated() {
		return authenticated;
	}

	@Override
	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
		this.authenticated = isAuthenticated;
	}

}
