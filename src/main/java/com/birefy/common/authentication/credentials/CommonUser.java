package com.birefy.common.authentication.credentials;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

import com.birefy.common.authentication.BirefyPlatformAuthority;

public class CommonUser implements OidcUser {
	
	private Long id;
	private String firstName;
	private String lastName;
	private String email;
	private List<BirefyPlatformAuthority> authority;
	private OidcIdToken authToken;
	private Map<String, Object> attributes;
	private OidcUserInfo oidcUserInfo;
	private Map<String, Object> claims;
	
	public CommonUser(Map<String, Object> attributes, OidcIdToken authToken) {
		this.firstName = authToken.getGivenName();
		this.lastName = authToken.getFamilyName();
		this.email = authToken.getEmail();
		this.authToken = authToken;
		this.claims = authToken.getClaims();
	}
	
	public CommonUser(long id, List<BirefyPlatformAuthority> authority, Map<String, Object> attributes, OidcIdToken authToken) {
		this.id = id;
		this.firstName = authToken.getGivenName();
		this.lastName = authToken.getFamilyName();
		this.email = authToken.getEmail();
		this.authority = authority;
		this.attributes = attributes;
		this.authToken = authToken;
		this.claims = authToken.getClaims();
	}
	
	public Long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authority;
	}
	
	public void setAuthorities(List<BirefyPlatformAuthority> authority) {
		this.authority = authority;
	}

	@Override
	public Map<String, Object> getAttributes() {
		return attributes;
	}

	@Override
	public String getName() {
		return firstName  + " " + lastName;
	}

	@Override
	public Map<String, Object> getClaims() {
		return claims;
	}
	
	public void setClaims(Map<String, Object> claims) {
		this.claims = claims;
	}

	@Override
	public OidcUserInfo getUserInfo() {
		return oidcUserInfo;
	}
	
	public void setOidcUserInfo(OidcUserInfo oidcUserInfo) {
		this.oidcUserInfo = oidcUserInfo;
	}

	@Override
	public OidcIdToken getIdToken() {
		return authToken;
	}
	
}
