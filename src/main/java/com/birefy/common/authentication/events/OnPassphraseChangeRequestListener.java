package com.birefy.common.authentication.events;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.Properties;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.entity.MemberVerification;
import com.birefy.common.messages.MessageProperty;
import com.birefy.common.repo.MemberVerificationRepository;
import com.birefy.common.utils.BirefyApplicationUtils;
import com.google.api.client.util.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.Gmail.Users.Messages.Send;
import com.google.api.services.gmail.model.Message;

@Component
public class OnPassphraseChangeRequestListener implements ApplicationListener<OnPasswordChangeRequestEvent> {
	private static final Logger LOGGER = LoggerFactory.getLogger(OnPassphraseChangeRequestListener.class);
	
	@Value("${application-url.domain}")
	private String applicationDomain;
	
	@Value("${application-url.port}")
	private String applicationPort;
	
	@Value("${birefy.email.address}")
	private String birefyEmailAddress;
	
//	@Qualifier("gmailClient")
//	@Autowired
//	private Gmail gmailService;
	@Autowired
	@Qualifier("verificationMessage")
	private MessageSource messageSource;
	@Autowired
	private MemberVerificationRepository memberVerificationRepository;
	@Autowired
	private BirefyApplicationUtils birefyApplicationUtils;
	
	@Override
	public void onApplicationEvent(OnPasswordChangeRequestEvent event){
		//Temporarily disabled due to technical reasons.
		
//		CommonUser user = (CommonUser) event.getSource();
//		String token = UUID.randomUUID().toString();
//		
//		Optional<MemberVerification> existingVerificationDetails = memberVerificationRepository.getVerificationDetails(user.getId());
//		
//		try {
//			if (existingVerificationDetails.isEmpty()) {
//				MemberVerification memberVerification = new MemberVerification(user.getId(), token);
//				memberVerificationRepository.addNewRecord(memberVerification);
//			} else {
//				MemberVerification memberVerification = existingVerificationDetails.get();
//				memberVerification.setChangePassword(false);
//				memberVerification.setToken(token);
//				memberVerification.setCreationDate(LocalDate.now());
//				
//				memberVerificationRepository.update(memberVerification);
//			}
//			
//			//Send confirmation email
//			String email = user.getEmail();
//			String subject = "Birefy Passphrase Update Request";
//			
//			String applicationAddress = birefyApplicationUtils.getApplicationDomain();
//			
//			MessageProperty[] messapeProperties = new MessageProperty[] {
//					new MessageProperty("webAddress", applicationAddress)
//					, new MessageProperty("token", token), new MessageProperty("memberId", user.getId().toString())
//			};
//			
//			String message = messageSource.getMessage("passphrase-update", messapeProperties, event.getLocale());
//			
//			MimeMessage electronicMessage = new MimeMessage(Session.getDefaultInstance(new Properties()));
//			electronicMessage.setFrom(new InternetAddress("noreply@birefy.com", "noreply (Birefy Platform)"));
//			electronicMessage.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(email));
//			electronicMessage.setSubject(subject);
//			electronicMessage.setText(message);
//			
//			Send preparedMessage = gmailService.users().messages().send("platform", createGmailMessage(electronicMessage));
//			
//			preparedMessage.execute();
//			
//			LOGGER.info("Fulfilled passphrase change request for user: " + user.getId());
//			
//		} catch (NoSuchFieldException | IllegalAccessException | MessagingException | IOException  e) {
//			LOGGER.error("Error setting validation details for user: "+user.getEmail());
//			LOGGER.error(e.getMessage());
//			
//			throw new RuntimeException("Failure to complete passphrase change request to " + user.getEmail());
//		}
	}
	
	private Message createGmailMessage(MimeMessage messageToSend) throws MessagingException, IOException {
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		messageToSend.writeTo(buffer);
		byte[] bytes = buffer.toByteArray();
		
		String encodedMailMessage = Base64.encodeBase64URLSafeString(bytes);
		
		Message message = new Message();
		message.setRaw(encodedMailMessage);
		return message;
	}
}
