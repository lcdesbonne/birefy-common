package com.birefy.common.authentication.events;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.birefy.common.controller.SignUpController;
import com.birefy.common.dto.member.MemberDTO;
import com.birefy.common.entity.Member;
import com.birefy.common.service.MemberService;

@Component
public class AuthenticationCompletePostProcessing implements AuthenticationSuccessHandler {
	@Autowired
	private MemberService memberService;
	@Autowired
	private SignUpController signUpController;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		
		OAuth2User userPrincipal = (OAuth2User) authentication.getPrincipal();
		
		Map<String, Object> userAttributes = userPrincipal.getAttributes();
		
		String email = (String) userAttributes.get("email");
		
		Optional<Member> existingMember = memberService.getMemberByEmail(email);
		
		if (existingMember.isEmpty()) {
			//Redirect the logged in user to Sign up page
			MemberDTO newMember = new MemberDTO();
			newMember.setEmail(email);
			newMember.setFirstName((String) userAttributes.get("first_name"));
			newMember.setLastName((String) userAttributes.get("family_name"));
			
			signUpController.signUp(newMember);
		}
	}

}
