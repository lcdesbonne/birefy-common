package com.birefy.common.authentication.events;

import java.util.List;
import java.util.Locale;

import org.springframework.context.ApplicationEvent;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.messages.MessageProperty;

public class OnPasswordChangeRequestEvent extends ApplicationEvent {
	private static final long serialVersionUID = 1L;
	private CommonUser user;
	private Locale locale;
	private List<MessageProperty> messageProperties;
	
	public OnPasswordChangeRequestEvent(CommonUser user, List<MessageProperty> messageProperties) {
		super(user);
		this.user = user;
		this.locale = Locale.ENGLISH;
	}
	
	public OnPasswordChangeRequestEvent(CommonUser user) {
		super(user);
	}

	public CommonUser getUser() {
		return user;
	}

	public void setUser(CommonUser user) {
		this.user = user;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public List<MessageProperty> getMessageProperties() {
		return messageProperties;
	}

	public void setMessageProperties(List<MessageProperty> messageProperties) {
		this.messageProperties = messageProperties;
	}
	
}
