package com.birefy.common.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.codec.binary.Base64;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.birefy.common.authentication.credentials.CommonUser;

@Component
public class FileUtilityActions {
	private final Logger LOGGER = LoggerFactory.getLogger(FileUtilityActions.class);
	
	@Value("${inventory.filesystem.root}")
	private String INVENTORY_FILE_LOCATION_ROOT;
	
	@Value("${inventory.filesystem.path.separator}")
	private String INVENTORY_PATH_SEPARATOR;
	
	@Value("${sound.filesystem.root}")
	private String SOUND_FILE_LOCATION_ROOT;
	
	@Value("${painting.filesystem.root}")
	private String PAINTING_FILE_LOCATION_ROOT;
	
	@Value("${sound.filesystem.path.separator}")
	private String SOUND_PATH_SEPARATOR;
	
	@Value("${painting.filesystem.path.separator}")
	private String PAINTING_PATH_SEPARATOR;
	
	@Value("${spring.servlet.multipart.max-file-size}")
	private String maxFileSize;
	
	private int DEFAULT_FILE_CHUNK_SIZE_BYTES = 1000;
	
	public StreamingResponseBody streamSoundFileFromSystem(String urlSuffix, int offSet) throws FileNotFoundException {
		
		return streamFileFromSystem(urlSuffix, offSet, SOUND_FILE_LOCATION_ROOT,  SOUND_PATH_SEPARATOR);
	}
	
	public StreamingResponseBody streamPaintingFileFromSystem(String urlSuffix, int offSet) {
		return streamFileFromSystem(urlSuffix, offSet, PAINTING_FILE_LOCATION_ROOT, PAINTING_PATH_SEPARATOR);
	}
	
	public StreamingResponseBody streamInventoryFileFromSystem(String urlSuffix, int offSet) {
		return streamFileFromSystem(urlSuffix, offSet, INVENTORY_FILE_LOCATION_ROOT, INVENTORY_PATH_SEPARATOR);
	}
	
	public boolean downloadSoundFile(String urlSuffix, HttpServletResponse response) {
		File file = new File(SOUND_FILE_LOCATION_ROOT + SOUND_PATH_SEPARATOR + urlSuffix);
		
		boolean success = true;
		try (InputStream inputStream = new FileInputStream(file); 
				OutputStream outputStream = response.getOutputStream()) {
			IOUtils.copy(inputStream, outputStream);
		} catch (IOException e) {
			CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime now = LocalDateTime.now();
			LOGGER.error("Issue downloading file, " + urlSuffix + ". Requested by user: " + user.getId() + ". At: " + dtf.format(now));
			success = false;
		}
		
		return success;
	}
	
	public boolean downloadInventoryFile(String urlSuffix, HttpServletResponse response) {
		File file = new File(INVENTORY_FILE_LOCATION_ROOT + INVENTORY_PATH_SEPARATOR + urlSuffix);
		
		boolean success = true;
		try(InputStream inputStream = new FileInputStream(file);
				OutputStream outputStream  = response.getOutputStream()) {
			IOUtils.copy(inputStream, outputStream);
		} catch (IOException e) {
			CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime now = LocalDateTime.now();
			LOGGER.error("Issue downloading inventory file, " + urlSuffix + ". Requested by user: " + user.getId() + ". At: " + dtf.format(now));
			success = false;
		}
		return success;
	}
	
	public boolean downloadImageFile(String urlSuffix, HttpServletResponse response) {
		File file = new File(PAINTING_FILE_LOCATION_ROOT + PAINTING_PATH_SEPARATOR + urlSuffix);
		
		boolean success = true;
		try(InputStream inputStream = new FileInputStream(file);
				OutputStream outputStream = response.getOutputStream()) {
			IOUtils.copy(inputStream, outputStream);
		} catch (IOException e) {
			CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime now = LocalDateTime.now();
			LOGGER.error("Issue downloading file, " + urlSuffix + ". Requested by user: " + user.getId() + ". At: " + dtf.format(now));
			success = false;
		}
		return success;
	}
	
	private StreamingResponseBody streamFileFromSystem(String urlSuffix, int offSet, String fileLocationRoot, String pathSeparator) {
		StreamingResponseBody responseBody = new StreamingResponseBody() {
			@Override
			public void writeTo(OutputStream out) throws IOException {
				File file  = new File(fileLocationRoot + pathSeparator + formatForWindowsPath(urlSuffix));
				byte[] fileContent = Files.readAllBytes(file.toPath());
				
				int arrayPosition = offSet;
				while(arrayPosition < fileContent.length) {
					int lengthToWrite = arrayPosition + DEFAULT_FILE_CHUNK_SIZE_BYTES > fileContent.length ? 
							fileContent.length - arrayPosition : DEFAULT_FILE_CHUNK_SIZE_BYTES;
					
					out.write(fileContent, arrayPosition, lengthToWrite);
					arrayPosition += DEFAULT_FILE_CHUNK_SIZE_BYTES;
				}
			}
		};
		
		return responseBody;
	}
	
	/**
	 * Writes a file to the system and returns the location of the saved file
	 * @param MultipartFile multipartFile
	 * @return String
	 * @throws IOException
	 */	
	public String writeSoundFileToSystem(MultipartFile file) throws IOException {
		String userSoundDirectory = userSoundDirectory();
		File fileDirectory = new File(SOUND_FILE_LOCATION_ROOT + userSoundDirectory);
		
		if(!fileDirectory.exists()) {
			fileDirectory.mkdirs();
		}
		String internalSoundTitle = UUID.randomUUID().toString() + getFileExtension(file.getOriginalFilename());
		String fileLocationFull = fileDirectory.getPath() + SOUND_PATH_SEPARATOR + internalSoundTitle;
		String fileLocationShort = userSoundDirectory + internalSoundTitle;
		try(InputStream inputStream = file.getInputStream();
				OutputStream out = new FileOutputStream(fileLocationFull)) {
			IOUtils.copy(inputStream, out);
		}
		
		return fileLocationShort;
	}
	
	/**
	 * Writes a file to the system and returns the location od the saved file
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public String writePaintingFileToSystem(MultipartFile file) throws IOException {
		String userPaintingDirectory = userPaintingDirectory();
		File fileDirectory = new File(PAINTING_FILE_LOCATION_ROOT + userPaintingDirectory);
		
		if(!fileDirectory.exists()) {
			fileDirectory.mkdirs();
		}
		String internalPaintingTitle = UUID.randomUUID().toString() + getFileExtension(file.getOriginalFilename());
		String fileLocationFull = fileDirectory.getPath() + PAINTING_PATH_SEPARATOR + internalPaintingTitle;
		String fileLocationShort = userPaintingDirectory + internalPaintingTitle;
		try (InputStream inputStream = file.getInputStream();
				OutputStream out = new FileOutputStream(fileLocationFull)) {
			IOUtils.copy(inputStream, out);
		}
		return fileLocationShort;
	}
	
	public String writeInventoryFileToSystem(MultipartFile file) throws IOException {
		String userInventoryDirectory = userInventoryDirectory();
		File fileDirectory = new File(INVENTORY_FILE_LOCATION_ROOT + userInventoryDirectory);
		
		if(!fileDirectory.exists()) {
			fileDirectory.mkdirs();
		}
		String internalInventoryTitle = UUID.randomUUID().toString() + getFileExtension(file.getOriginalFilename());
		String fileLocationFull = fileDirectory.getPath() + INVENTORY_PATH_SEPARATOR + internalInventoryTitle;
		String fileLocationShort = userInventoryDirectory + internalInventoryTitle;
		try (InputStream inputStream = file.getInputStream();
				OutputStream out = new FileOutputStream(fileLocationFull)) {
			IOUtils.copy(inputStream, out);
		}
		return fileLocationShort;
	}
	
	public boolean deleteSoundFileFromSystem(String url) {
		return deleteFileFromSystem(url, SOUND_FILE_LOCATION_ROOT, SOUND_PATH_SEPARATOR);
	}
	
	public boolean deletePaintingFileFromSystem(String url) {
		return deleteFileFromSystem(url, PAINTING_FILE_LOCATION_ROOT, PAINTING_PATH_SEPARATOR);
	}
	
	public boolean deleteInventoryFileFromSystem(String url) {
		return deleteFileFromSystem(url, INVENTORY_FILE_LOCATION_ROOT, INVENTORY_PATH_SEPARATOR);
	}
	
	public String userSoundDirectory() {
		return userDirectory(SOUND_PATH_SEPARATOR);
	}
	
	public String userInventoryDirectory() {
		return userDirectory(INVENTORY_PATH_SEPARATOR);
	}
	
	public String userPaintingDirectory() {
		return userDirectory(PAINTING_PATH_SEPARATOR);
	}
	
	public String getMaxFileSize() {
		return "File size must not exceed " + this.maxFileSize;
	}
	
	private String userDirectory(String pathSeparator) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		StringBuilder sb = new StringBuilder();
		sb.append(pathSeparator);
		sb.append(Base64.encodeBase64URLSafeString(user.getEmail().getBytes()));
		sb.append(pathSeparator);
		return sb.toString();
	}
	
	private boolean deleteFileFromSystem(String url, String fileLocationRoot, String pathSeparator) {
		boolean deleted = false;
		String fileLocation = fileLocationRoot + pathSeparator + url;
		File file = new File(fileLocation);
		if (file.exists()) {
			deleted = file.delete();
		} else {
			LOGGER.error("File path provided: "+url + ", not found");
		}
		return deleted;
	}
	
	private String formatForWindowsPath(String url) {
		return url.replaceAll(" ", " ");
	}
	
	public String getFileExtension(String fileName) {
		String[] nameParts = fileName.split("\\.");
		
		String extension = "." + nameParts[nameParts.length - 1];
		return extension.toLowerCase();
	}
}
