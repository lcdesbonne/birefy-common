package com.birefy.common.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.question.QQuestionShort;
import com.birefy.common.dto.question.QQuestionTitleAndUrl;
import com.birefy.common.dto.question.QuestionContributor;
import com.birefy.common.dto.question.SkillsAndDetails;
import com.birefy.common.dto.topic.QTopicShort;
import com.birefy.common.dto.topic.QTopicsAndQQuestions;
import com.birefy.common.dto.topic.TopicContributor;
import com.birefy.common.entity.QMemberQuestion;
import com.birefy.common.entity.QMemberTopic;
import com.birefy.common.entity.QQuestion;
import com.birefy.common.entity.QTopic;
import com.birefy.common.entity.tiding.MemberChannel;
import com.birefy.common.entity.utils.mapper.QQuestionToShortMapper;
import com.birefy.common.entity.utils.mapper.QTopicToShortMapper;
import com.birefy.common.repo.QMemberQuestionRepository;
import com.birefy.common.repo.QMemberTopicRepository;
import com.birefy.common.repo.QQuestionRepository;
import com.birefy.common.repo.QTopicRepository;
import com.birefy.common.repo.TechnicalTagRepository;

@Service
public class QQuestionService {
	private static final Logger LOGGER = LoggerFactory.getLogger(QQuestionService.class);
	
	@Autowired
	private QQuestionRepository questionRepository;
	@Autowired
	private QTopicRepository topicRepository;
	@Autowired
	private TidingsService tidingsService;
	@Autowired
	private QMemberQuestionRepository memberQuestionRepository;
	@Autowired
	private TechnicalTagRepository technicalTagRepository;
	@Autowired
	private QMemberTopicRepository memberTopicRepository;
	
	public QTopicsAndQQuestions retrieveLiveQuestionData() {
		//Retrieve live questions
		List<QQuestion> liveQuestions = questionRepository.searchQuestions(true);
		//Retrieve live topics
		List<QTopic> liveTopics = topicRepository.findAll(true);
		
		//Map QQuestion to QQuestionShort
		List<QQuestionShort> shortQs = liveQuestions.stream().map(q -> {
			return QQuestionToShortMapper.map(q);
		}).collect(Collectors.toList());
		
		//Map QTopic to QTopicShort
		List<QTopicShort> shortTs = liveTopics.stream().map(t -> {
			return QTopicToShortMapper.map(t);
		}).collect(Collectors.toList());
		
		QTopicsAndQQuestions topicQuestions = new QTopicsAndQQuestions(shortTs, shortQs);
		return topicQuestions;
	}
	
	public QTopicsAndQQuestions topicsByTitle(String title, boolean active) {
		List<QTopic> topics = new ArrayList<>();
		//Retrieve topics by title
		if (title == null || title.isEmpty()) {
			topics = topicRepository.findAll(active);
		} else {
			topics = topicRepository.findByTitle(active, title);
		}
		
		//Gather topic ids
		List<Long> topicIds = topics.stream().map(top -> {
			return top.getId();
		}).collect(Collectors.toList());
		
		if (topicIds.isEmpty()) {
			return new QTopicsAndQQuestions();
		}
		
		List<QQuestion> questions = questionRepository.searchQuestionsByTopic(topicIds, active);
		List<QQuestionShort> shortQs = questions.stream().map(q -> {
			return QQuestionToShortMapper.map(q);
		}).collect(Collectors.toList());
		List<QTopicShort> shortTs = topics.stream().map(t -> {
			return QTopicToShortMapper.map(t);
		}).collect(Collectors.toList());
		
		QTopicsAndQQuestions topicQuestions = new QTopicsAndQQuestions(shortTs, shortQs);
		return topicQuestions;
	}
	
	public QTopicsAndQQuestions questionsByTitle(String title, boolean active) {
		List<QQuestion> questions = new ArrayList<>();
		//Retrieve questions
		if (title == null || title.isEmpty()) {
			questions = questionRepository.searchQuestions(active);
		} else {
			questions = questionRepository.searchQuestionsByTitle(title, active);
		}
		
		if (questions.isEmpty()) {
			return new QTopicsAndQQuestions();
		}
		//Gather topic ids
		List<Long> topicIds = questions.stream().map(q -> {
			return q.getTopicId();
		}).collect(Collectors.toList());
		//Search for topics
		List<QTopic> topics = topicRepository.findById(topicIds);
		
		//Map to shorts
		List<QQuestionShort> shortQs = questions.stream().map(q -> {
			return QQuestionToShortMapper.map(q);
		}).collect(Collectors.toList());
		
		List<QTopicShort> shortTs = topics.stream().map(t -> {
			return QTopicToShortMapper.map(t);
		}).collect(Collectors.toList());
		
		QTopicsAndQQuestions topicQuestions = new QTopicsAndQQuestions(shortTs, shortQs);
		return topicQuestions;
	}
	
	public QTopicsAndQQuestions topicsByTags(List<Long> tagIds, boolean active) {
		//Valid topic
		List<QTopic> topics = topicRepository.findByTags(active, tagIds);
		
		List<Long> topicIds = topics.stream().map(topic -> {
			return topic.getId();
		}).collect(Collectors.toList());
		
		List<QQuestion> questions = questionRepository.searchQuestionsByTopic(topicIds, true);
		
		List<QTopicShort> shortTs = topics.stream().map(topic -> {
			return QTopicToShortMapper.map(topic);
		}).collect(Collectors.toList());
		List<QQuestionShort> shortQs = questions.stream().map(question -> {
			return QQuestionToShortMapper.map(question);
		}).collect(Collectors.toList());
		
		QTopicsAndQQuestions data = new QTopicsAndQQuestions();
		data.setQuestions(shortQs);
		data.setTopics(shortTs);
		return data;
	}
	
	public QTopicsAndQQuestions questionsByTopicNameAndTags(String name, List<Long> tags, boolean active) {
		List<QTopic> topics;
		if(name == null || name.isEmpty()) {
			topics = topicRepository.findByTags(active, tags);
		} else {
			topics = topicRepository.findByTitleAndTags(active, name, tags);
		}
		
		List<Long> topicIds = topics.stream().map(topic -> {
			return topic.getId();
		}).collect(Collectors.toList());
		
		List<QQuestion> questions;
		if(!CollectionUtils.isEmpty(topicIds)) {
			questions = questionRepository.searchQuestionsByTopic(topicIds, true);
		} else {
			questions = new ArrayList<>();
		}
		
		List<QTopicShort> shortTs = topics.stream().map(topic -> {
			return QTopicToShortMapper.map(topic);
		}).collect(Collectors.toList());
		
		List<QQuestionShort> shortQs = questions.stream().map(question -> {
			return QQuestionToShortMapper.map(question);
		}).collect(Collectors.toList());
		
		QTopicsAndQQuestions data = new QTopicsAndQQuestions();
		data.setQuestions(shortQs);
		data.setTopics(shortTs);
		return data;
	}
	
	public QTopicsAndQQuestions questionsByQuestionNameAndTags(String questionTitle, List<Long> tags, boolean active) {
		List<QTopic> topics = topicRepository.findByTags(active, tags);
		
		List<Long> topicIds = topics.stream().map(topic -> {
			return topic.getId();
		}).collect(Collectors.toList());
		
		List<QQuestion> questions = questionRepository.searchQuestionsByTopicAndTitle(topicIds, questionTitle, active);
		
		List<QTopicShort> shortTs = topics.stream().map(topic -> {
			return QTopicToShortMapper.map(topic);
		}).collect(Collectors.toList());
		
		List<QQuestionShort> shortQs = questions.stream().map(question -> {
			return QQuestionToShortMapper.map(question);
		}).collect(Collectors.toList());
		
		QTopicsAndQQuestions data = new QTopicsAndQQuestions();
		data.setQuestions(shortQs);
		data.setTopics(shortTs);
		return data;
	}
	
	public QQuestionTitleAndUrl getTitleAndUrlForQuestion(long questionId) {
		return questionRepository.getTitleAndUrlForQuestion(questionId);
	}
	
	public Map<String, SkillsAndDetails> retrieveTidingIdsAndSkillsForContributorsToQuestion(long questionId) {
		//Retrieve tiding ids and member ids for the question
		List<QMemberQuestion> contributorDetails = memberQuestionRepository.findCreatorsForQuestions(Arrays.asList(questionId));
		List<MemberChannel> details = tidingsService.channelDetailsForMembers(contributorDetails.stream().map(QMemberQuestion::getMemberId)
				.collect(Collectors.toList()));
		
		List<MemberChannel> activeMembers = details.stream().filter(member -> member.isActive()).collect(Collectors.toList());
		List<Long> activeMemberIds = activeMembers.stream().map(member -> member.getMemberId()).collect(Collectors.toList());
		
		Map<String, SkillsAndDetails> specificSkillsForContributors = new HashMap<>();
		
		//Map tiding id to the list of skills
		for(long memberId : activeMemberIds) {
			List<String> techTags = technicalTagRepository.specificTechnicalTagsForMember(memberId);
			String tidingId = activeMembers.stream().filter(member -> member.getMemberId() == memberId)
					.map(m -> m.getTidingId()).findFirst().get();
			
			String contributionDetails = contributorDetails.stream().filter(d -> d.getMemberId().equals(memberId))
					.findFirst().get().getDetails();
			
			specificSkillsForContributors.put(tidingId, new SkillsAndDetails(techTags, contributionDetails));
		}
		
		return specificSkillsForContributors;
	}
	
	public Map<Long, List<QuestionContributor>> findQuestionContributors(List<Long> questionIds) {
		Map<Long, List<QuestionContributor>> questionContributorMap = new HashMap<>();
		List<QMemberQuestion> contributors = memberQuestionRepository.findCreatorsForQuestions(questionIds);
		
		for(QMemberQuestion contributor : contributors) {
			if(!questionContributorMap.containsKey(contributor.getQuestionId())) {
				questionContributorMap.put(contributor.getQuestionId(), new ArrayList<QuestionContributor>());
			}
			//Retrieve the tiding id for the member
			String tidingId = tidingsService.getTidingIdForMember(contributor.getMemberId());
			
			questionContributorMap.get(contributor.getQuestionId())
			.add(new QuestionContributor(contributor.getMemberId(), contributor.getDetails(), tidingId));
		}
		
		return questionContributorMap;
	}
	
	public Map<Long, List<TopicContributor>> findTopicContributors(List<Long> topicIds) {
		Map<Long, List<TopicContributor>> topicContributorMap = new HashMap<>();
		List<QMemberTopic> contributors = memberTopicRepository.findCreatorsForTopic(topicIds);
		
		for(QMemberTopic contributor : contributors) {
			if(!topicContributorMap.containsKey(contributor.getTopicId())) {
				topicContributorMap.put(contributor.getTopicId(), new ArrayList<TopicContributor>());
			}
			
			String tidingId = tidingsService.getTidingIdForMember(contributor.getMemberId());
			
			topicContributorMap.get(contributor.getTopicId())
				.add(new TopicContributor(contributor.getMemberId(), tidingId));
		}
		
		return topicContributorMap;
	}
	
	public void deleteContributorsFromQuestion(List<QMemberQuestion> contributors) {
		Set<Long> affectedQuestionIds = contributors.stream().map(c -> c.getQuestionId()).collect(Collectors.toSet());
		
		CommonUser user = (CommonUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		affectedQuestionIds.forEach(qId -> {
			if(memberQuestionRepository.isContributorToQuestion(qId, user.getId())) {
				List<Long> existingMembers = memberQuestionRepository.findCreatorsOfQuestion(qId);
				
				List<Long> removedMembers = contributors.stream().filter(c -> c.getQuestionId().equals(qId))
						.map(QMemberQuestion::getMemberId).collect(Collectors.toList());
				
				existingMembers.removeAll(removedMembers);
				if(existingMembers.size() >= 1) {
					memberQuestionRepository.removeContributorsFromQuestion(qId, removedMembers);
				} else {
					LOGGER.error("There must be at least 1 contributor for a question");
					throw new RuntimeException("There must be at least 1 contributor for a question.");
				}
			}
		});
	}
	
	public void addContributorsToQuestion(List<QMemberQuestion> contributors) {
		Set<Long> affectedQuestionIds = contributors.stream().map(c -> c.getQuestionId()).collect(Collectors.toSet());
		
		CommonUser user = (CommonUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		affectedQuestionIds.forEach(qId -> {
			if(memberQuestionRepository.isContributorToQuestion(qId, user.getId())) {
				Set<QMemberQuestion> addedMembers = contributors.stream().filter(c -> c.getQuestionId().equals(qId))
						.collect(Collectors.toSet());
				
				addedMembers.forEach(newContributor -> {
					try {
						memberQuestionRepository.addNewRecord(newContributor);
					} catch (NoSuchFieldException | IllegalAccessException e) {
						LOGGER.error("Error adding a new contributor to question", e);
						throw new RuntimeException("Unable to add contributors to question.");
					} 
				});
				
			}
		});
	}
	
	public void updateContributorDetails(List<QMemberQuestion> contributors) {
		Set<Long> affectedQuestionIds = contributors.stream().map(c -> c.getQuestionId()).collect(Collectors.toSet());
		
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		affectedQuestionIds.forEach(qId -> {
			if(memberQuestionRepository.isContributorToQuestion(qId, user.getId())) {
				Set<QMemberQuestion> detailsToUpdate = contributors.stream().filter(c -> c.getQuestionId().equals(qId))
						.collect(Collectors.toSet());
				
				detailsToUpdate.forEach(details -> {
					memberQuestionRepository.updateContributorDetails(details);
				});
			}
		});
	}
}
