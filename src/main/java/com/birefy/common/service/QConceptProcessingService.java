package com.birefy.common.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.birefy.common.authentication.BirefyPlatformAuthority;
import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.question.QQuestionShort;
import com.birefy.common.dto.tiding.TagUpdateDTO;
import com.birefy.common.dto.topic.QTopicShort;
import com.birefy.common.dto.topic.QTopicsAndQQuestions;
import com.birefy.common.entity.QMemberQuestion;
import com.birefy.common.entity.QMemberTopic;
import com.birefy.common.entity.QQuestion;
import com.birefy.common.entity.QTag;
import com.birefy.common.entity.QTopic;
import com.birefy.common.entity.QTopicTag;
import com.birefy.common.entity.utils.mapper.QQuestionToShortMapper;
import com.birefy.common.entity.utils.mapper.QTopicToShortMapper;
import com.birefy.common.repo.QMemberQuestionRepository;
import com.birefy.common.repo.QMemberTopicRepository;
import com.birefy.common.repo.QQuestionRepository;
import com.birefy.common.repo.QTagRepository;
import com.birefy.common.repo.QTopicRepository;
import com.birefy.common.repo.QTopicTagRepository;

@Service
public class QConceptProcessingService {
	private static final Logger LOGGER = LoggerFactory.getLogger(QConceptProcessingService.class);
	private static final String REFLECTION_ERROR = "Issue with reflection in the persistence class CommonRepository";
	
	@Autowired
	private QTopicRepository topicRepository;
	@Autowired
	private QQuestionRepository questionRepository;
	@Autowired
	private QTagRepository tagRepository;
	@Autowired
	private QTopicTagRepository topicTagRepository;
	@Autowired
	private QMemberTopicRepository memberTopicRepository;
	@Autowired
	private QMemberQuestionRepository memberQuestionRepository;
	
	@Transactional
	public long processTopicData(QTopicShort topic) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		long topicId = 0;
		if (topic.getId() == 0) {
			//A new topic has been defined
			QTopic newTopic = new QTopic();
			newTopic.setActive(false);
			newTopic.setDetail(topic.getTopicDetail());
			newTopic.setTitle(topic.getTopicName());
			
			try {
				topicRepository.addNewRecord(newTopic);
				
				//Associate the member with the topic
				List<QTopic> topics = topicRepository.findByTitle(false, topic.getTopicName());
				topicId =  topics.stream().findFirst().get().getId();
				
				memberTopicRepository.addNewRecord(new QMemberTopic(user.getId(), topicId));
			} catch (NoSuchFieldException | IllegalAccessException e) {
				LOGGER.error(REFLECTION_ERROR, e);
			}
		} else {
			//update the topic if the user is Royal or created the topic
			//Identify the creator of the topic
			List<Long> creatorIds = memberTopicRepository.findCreatorsForTopic(Arrays.asList(topic.getId()))
					.stream().map(QMemberTopic::getMemberId).collect(Collectors.toList());
			if (user.getAuthorities().contains(BirefyPlatformAuthority.ROYAL) || creatorIds.contains(user.getId())) {
				try {
					QTopic updatedTopic = new QTopic();
					updatedTopic.setId(topic.getId());
					updatedTopic.setTitle(topic.getTopicName());
					updatedTopic.setDetail(topic.getTopicDetail());
					topicRepository.update(updatedTopic);
				} catch (NoSuchFieldException | IllegalAccessException e) {
					LOGGER.error(REFLECTION_ERROR, e);
				}
			}
			topicId =  topic.getId();
		}
		
		return topicId;
	}
	
	@Transactional
	public long processQuestionData(QQuestionShort question) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		long questionId = 0;
		if (question.getId() == 0) {
			//A new question has been specified
			QQuestion newQuestion = new QQuestion();
			newQuestion.setTitle(question.getTitle());
			newQuestion.setDetail(question.getDetail());
			newQuestion.setTopicId(question.getTopicId());
			newQuestion.setInception(LocalDate.now());
			
			try {
				questionRepository.addNewRecord(newQuestion);
				
				List<QQuestion> questions = questionRepository
						.searchQuestionsByTitle(question.getTitle(), false);
				//Associate the user with the question
				memberQuestionRepository.addNewRecord(new QMemberQuestion(user.getId(), 
						questions.stream().findFirst().get().getId()));
				
				questionId = questions.stream().findFirst().get().getId();
			} catch (NoSuchFieldException | IllegalAccessException e) {
				LOGGER.error(REFLECTION_ERROR, e);
			}
		} else {
			//Update the question if the user is Royal or a creator
			List<Long> creators = memberQuestionRepository.findCreatorsOfQuestion(question.getId());
			
			if (user.getAuthorities().contains(BirefyPlatformAuthority.ROYAL) || creators.contains(user.getId())) {
				QQuestion updatedQuestion = new QQuestion();
				updatedQuestion.setId(question.getId());
				updatedQuestion.setTitle(question.getTitle());
				updatedQuestion.setDetail(question.getDetail());
				updatedQuestion.setTopicId(question.getTopicId());
				
				try {
					questionRepository.update(updatedQuestion);
				} catch (NoSuchFieldException | IllegalAccessException e) {
					LOGGER.error(REFLECTION_ERROR, e);
				}
			}
			
			questionId =  question.getId();
		}
		
		return questionId;
	}
	
	@Transactional
	public void saveNewTopicTags(TagUpdateDTO tagUpdateDetails) {
		List<Long> brandNewTagIds = new ArrayList<Long>();
		
		//Save all new tags and retrieve their ids
		for (String tagName : tagUpdateDetails.getNewTagNames()) {
			try {
				tagRepository.addNewRecord(new QTag(tagName));
			} catch (IllegalAccessException | NoSuchFieldException e) {
				LOGGER.error(REFLECTION_ERROR, e);
				return;
			}
			
			brandNewTagIds.add(tagRepository.searchByTitle(tagName)
					.stream().findFirst().get().getId());
		}
		
		brandNewTagIds.addAll(tagUpdateDetails.getExistingTagIds());
		
		Set<Long> tagsForTopic = brandNewTagIds.stream().collect(Collectors.toSet());
		
		long topicId = tagUpdateDetails.getTopicId();
		tagsForTopic.forEach(tagId -> {
			try {
				topicTagRepository.addNewRecord(new QTopicTag(topicId, tagId));
			} catch (IllegalAccessException | NoSuchFieldException e) {
				LOGGER.error(REFLECTION_ERROR, e);
			}
			
		});
	}
	
	public QTopicsAndQQuestions findAllTopicsByTitle(String title) {
		if (title == null || title.isEmpty()) {
			//Perhaps should throw an exception instead
			return new QTopicsAndQQuestions();
		}
		
		List<QTopic> topics = topicRepository.findAllByTitle(title);
		
		List<Long> topicIds = topics.stream().map(top -> {
			return top.getId();
		}).collect(Collectors.toList());
		
		if (topicIds.isEmpty()) {
			return new QTopicsAndQQuestions();
		}
		
		List<QQuestion> questions = questionRepository.searchAllQuestionsByTopic(topicIds);
		
		List<QQuestionShort> shortQs = questions.stream().map(q -> {
			return QQuestionToShortMapper.map(q);
		}).collect(Collectors.toList());
		List<QTopicShort> shortTs = topics.stream().map(t -> {
			return QTopicToShortMapper.map(t);
		}).collect(Collectors.toList());
		
		QTopicsAndQQuestions topicQuestions = new QTopicsAndQQuestions(shortTs, shortQs);
		return topicQuestions;
	}
	
	public QTopicsAndQQuestions findAllQuestionsByTitle(String title) {
		if (title == null || title.isEmpty()) {
			return new QTopicsAndQQuestions();
		}
		
		List<QQuestion> questions = questionRepository.searchAllQuestionsByTitle(title);
		
		if (questions.isEmpty()) {
			return new QTopicsAndQQuestions();
		}
		
		List<Long> topicIds = questions.stream().map(q -> {
			return q.getTopicId();
		}).collect(Collectors.toList());
		
		List<QTopic> topics = topicRepository.findById(topicIds);
		
		List<QQuestionShort> shortQs = questions.stream().map(q -> {
			return QQuestionToShortMapper.map(q);
		}).collect(Collectors.toList());
		
		List<QTopicShort> shortTs = topics.stream().map(t -> {
			return QTopicToShortMapper.map(t);
		}).collect(Collectors.toList());
		
		QTopicsAndQQuestions topicQuestions = new QTopicsAndQQuestions(shortTs, shortQs);
		
		return topicQuestions;
	}

}
