package com.birefy.common.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.birefy.common.dto.capital.ChargeDetails.MediaCategory;
import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.capital.MemberPaymentDetails;
import com.birefy.common.dto.capital.PayoutSummary;
import com.birefy.common.dto.capital.RevenueSummary;
import com.birefy.common.dto.capital.SoldItem;
import com.birefy.common.entity.MemberBeneficiary;
import com.birefy.common.entity.Revenue;
import com.birefy.common.repo.MemberBeneficiaryRepository;
import com.birefy.common.repo.MemberRepository;
import com.birefy.common.repo.RevenueRepository;
import com.currencycloud.client.CurrencyCloudClient;
import com.currencycloud.client.model.Balance;
import com.currencycloud.client.model.Beneficiary;
import com.currencycloud.client.model.Currency;
import com.currencycloud.client.model.Payment;


//@Service
@Deprecated
public class PayoutService {
	private static Logger LOGGER = LoggerFactory.getLogger(PayoutService.class);
	
	@Value("${birefy.charges.transaction}")
	private Integer applicationFee;
	
	@Autowired
	private RevenueRepository revenueRepository;
	@Autowired
	private MemberRepository memberRepository;
	@Autowired
	private CurrencyCloudClient currencyCloudClient;
	@Autowired
	private MemberBeneficiaryRepository memberBeneficiaryRepository;
	
	public PayoutSummary calculateRevenueByMember(Long memberId) {		
		List<RevenueSummary> paymentsForMember = revenueRepository.salesOverThePastYear(memberId);
		
		LocalDateTime lastPaidDate = memberRepository.memberLastPaid(memberId);
		
		List<SoldItem> items = new ArrayList<>();
		
		for (RevenueSummary rev : paymentsForMember) {
			Long inventoryId = rev.getProductId();
			MediaCategory category = MediaCategory.valueOf(rev.getCategory());
			
			boolean isMapped = items.stream().anyMatch(item -> item.getInventoryId().equals(inventoryId) && 
					item.getCategory() == category);
			
			if (!isMapped) {
				List<RevenueSummary> revenue = paymentsForMember.stream().filter(pm -> MediaCategory.valueOf(pm.getCategory()) == category 
						&& pm.getProductId().equals(inventoryId)).collect(Collectors.toList());
				
				String description = rev.getDescription();
				Integer sales = revenue.size();
				
				Integer revenuePence = revenue.stream().map(RevenueSummary::getInventoryRequestPrice).reduce((prev, next) -> prev + next).get();
				
				items.add(new SoldItem(category, description, sales, revenuePence));
			}
		}
		
		Integer totalPayable = items.stream().map(SoldItem::getPricePence).reduce((prev, next) -> prev + next).orElse(0);
		
		return new PayoutSummary(items, totalPayable, lastPaidDate);
	}
	
	public boolean updateRevenue(Revenue addedRevenue) {
		boolean success;
		try {
			revenueRepository.addNewRecord(addedRevenue);
			success = true;
		} catch (NoSuchFieldException | IllegalAccessException e) {			
			LOGGER.error("Error adding revenue record:" + addedRevenue.toString(), e);
			success = false;
		}
		
		return success;
	}
	
	public boolean payBirefyBeneficiary(PayoutSummary payoutSummary, MemberPaymentDetails paymentDetails, Long memberId) {
		Integer amountPayable = payoutSummary.getTotalPayablePence();
		
		BigDecimal amountPayablePounds = BigDecimal.valueOf((double) amountPayable / 100);
		
		Balance currentBalance = currencyCloudClient.retrieveBalance("GBP");
		
		boolean success = false;
		
		try {
			if (currentBalance.getAmount().compareTo(amountPayablePounds) > 0) {
				
				Optional<String> existingBeneficiaryUsed = findBeneficiaryDetailsToUse(paymentDetails, memberId);
				
				String beneficiaryId;
				
				if (existingBeneficiaryUsed.isPresent()) {
					beneficiaryId = existingBeneficiaryUsed.get();
				} else {
					beneficiaryId = createNeBeneficiary(paymentDetails);
					memberBeneficiaryRepository.addNewRecord(new MemberBeneficiary(memberId, beneficiaryId));
				}
				
				Payment newPayment = Payment.create("GBP", beneficiaryId, amountPayablePounds, "Birefy sales payable", "Birefy", null, "regular", null, null);
				
				newPayment = currencyCloudClient.createPayment(newPayment, null);
				
				//Update the revenue table for this user
				revenueRepository.updatePaymentDetailsForMember(memberId, newPayment.getId(), true);
				success = true;
			}
		} catch (Exception e) {
			LOGGER.error("Error creating payment to member " + memberId, e);
			success = false;
		}
		
		return success;
	}
	
	public boolean collectPayment(MemberPaymentDetails paymentDetails) {
		CommonUser user = (CommonUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		PayoutSummary payoutSummary = calculateRevenueByMember(user.getId());
		
		return payBirefyBeneficiary(payoutSummary, paymentDetails, user.getId());
	}
	
	private Beneficiary buildBeneficiaryDetails(Beneficiary beneficiary, List<Map<String, String>> requiredFields, MemberPaymentDetails paymentDetails) {
		String beneficiaryEntityTypeKey = "beneficiary_entity_type";
		
		//Details required for the UK
		String sortCodeRegexKey = "sort_code"; //^\d{6}$
		String accountNumberRegexKey = "acct_number"; //^\d{8}$
		String addressRegexKey = "beneficiary_address";
		String cityRegexKey = "beneficiary_city";
		String countryRegexKey = "beneficiary_country"; //^[A-z]{2}$
		String paymentTypeKey = "payment_type";
		String individualPaymentType = "regular";
		String entityTypeKey = "beneficiary_entity_type";
		String entityTypeValue = "individual";
		//Note: payment type may need to be specified as regular
		
		for (Map<String, String> required : requiredFields) {
			//Initially process the payments for individuals
			//Handle sort code
			if (required.get(beneficiaryEntityTypeKey).equals("individual") 
					&& required.get(paymentTypeKey).equalsIgnoreCase(individualPaymentType)
					&& required.get(entityTypeKey).equalsIgnoreCase(entityTypeValue)) {
				
				beneficiary.setPaymentTypes(Arrays.asList(individualPaymentType));
				
				if (required.containsKey(sortCodeRegexKey)) {
					Pattern sortCodePattern = Pattern.compile(required.get(sortCodeRegexKey));
					
					Matcher matcher = sortCodePattern.matcher(paymentDetails.getSortCode());
					
					if (matcher.matches()) {
						beneficiary.setRoutingCodeType1("sort_code");
						beneficiary.setRoutingCodeValue1(paymentDetails.getSortCode());
					} else {
						throw new RuntimeException("Invalid Sort Code.");
					}
				}
				
				//Handle account number
				String accountNumberPattern = required.get(accountNumberRegexKey);
				
				Pattern aNumPattern = Pattern.compile(accountNumberPattern);
				Matcher matcher = aNumPattern.matcher(paymentDetails.getAccountNumber());
				
				if (matcher.matches()) {
					beneficiary.setAccountNumber(paymentDetails.getAccountNumber());
				} else {
					throw new RuntimeException("Invalid Account Number");
				}
				
				break;
			} else {
				continue;
			}
		}
		
		return beneficiary;
	}
	
	private Optional<String> findBeneficiaryDetailsToUse(MemberPaymentDetails paymentDetails, Long memberId) {
		List<String> existingCardDetails = memberBeneficiaryRepository.findBeneficiaryIdsUsedByMember(memberId);
		
		if (!existingCardDetails.isEmpty()) {
			String usedSortCode = paymentDetails.getSortCode();
			String usedAccountNumber = paymentDetails.getAccountNumber();
			
			for (String beneficiaryId : existingCardDetails) {
				Beneficiary existingBeneficiary = currencyCloudClient.retrieveBeneficiary(beneficiaryId);
				
				if (
					existingBeneficiary.getAccountNumber().equals(usedAccountNumber) &&
					existingBeneficiary.getRoutingCodeType1().equals("sort_code") &&
					existingBeneficiary.getRoutingCodeValue1().equals(usedSortCode)) {
					
					return Optional.of(beneficiaryId);
				}
			}
		}
		
		return Optional.empty();
	}
	
	public boolean verifyMemberPassphrase(String passPhrase) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication();
		
		String storedPassPhrase = memberRepository.getMemberPassPhrase(user.getId());
		
		return passPhrase != null 
				&& storedPassPhrase != null
				&& storedPassPhrase.equals(passPhrase);
	}
	
	private String createNeBeneficiary(MemberPaymentDetails paymentDetails) {
		//Identify beneficiary details for Britain
		List<Map<String, String>> detailsRequired = currencyCloudClient.beneficiaryRequiredDetails("GBP", 
				paymentDetails.getCountry().toString(), paymentDetails.getBankAccountCountry().toString());
		
		Beneficiary beneficiary = Beneficiary.create(paymentDetails.getBankAccountHolderName(), 
				paymentDetails.getBankAccountCountry().toString(), "GBP", paymentDetails.getName());
		
		beneficiary = buildBeneficiaryDetails(beneficiary, detailsRequired, paymentDetails);
		
		//Create a beneficiary
		beneficiary = currencyCloudClient.createBeneficiary(beneficiary);
		
		return beneficiary.getId();
	}

}
