package com.birefy.common.service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.birefy.common.dto.topic.TopicUpdate;
import com.birefy.common.dto.topic.UpdatedTopicContributor;
import com.birefy.common.entity.QMemberTopic;
import com.birefy.common.entity.QTag;
import com.birefy.common.entity.QTopic;
import com.birefy.common.entity.QTopicTag;
import com.birefy.common.repo.QMemberTopicRepository;
import com.birefy.common.repo.QTagRepository;
import com.birefy.common.repo.QTopicRepository;
import com.birefy.common.repo.QTopicTagRepository;

@Service
public class TopicService {
	private static final Logger LOGGER = LoggerFactory.getLogger(TopicService.class);
	
	@Autowired
	private QTopicRepository topicRepository;
	@Autowired
	private QTagRepository tagRepository;
	@Autowired
	private QTopicTagRepository topicTagRepository;
	@Autowired
	private QMemberTopicRepository memberTopicRepository;
	
	public void updateTopicAndTopicTags(List<TopicUpdate> topicUpdate) {
		topicUpdate.forEach(update -> {
			QTopic topicToUpdate = new QTopic(
					update.getTopicId(),
					update.getTitle(),
					update.getDetail(),
					update.getActive()
					);
			try {
				topicRepository.update(topicToUpdate);
			} catch (Exception e) {
				throw new RuntimeException("Failed to update topic with id: " + update.getTopicId(), e);
			}
			
			List<String> tagsToAdd = update.getAddedTags();
			if(!tagsToAdd.isEmpty()) {
				addNewTagsToTopic(update.getTopicId(), tagsToAdd);
			}
			
			List<String> tagsToRemove = update.getRemovedTags();
			if(!tagsToRemove.isEmpty()) {
				removeTagsFromTopic(update.getTopicId(), tagsToRemove);
			}	
		});
	}
	
	public List<QTag> searchTopicTagsByName(String tagName) {
		return tagRepository.searchByTitle(tagName);
	}
	
	public void addNewContributorsToTopc(List<UpdatedTopicContributor> newContributors) {
		List<QMemberTopic> newMembers = newContributors.stream().map(cont -> {
			return new QMemberTopic(cont.getMemberId(), cont.getTopicId());
		}).collect(Collectors.toList());
		
		newMembers.forEach(newContributor -> {
			try {
				memberTopicRepository.addNewRecord(newContributor);
			} catch (NoSuchFieldException e) {
				LOGGER.error("Failed to link member " 
						+ newContributor.getMemberId() + " to topic " + newContributor.getTopicId(), e);
			} catch (IllegalAccessException e) {
				LOGGER.error("Failed to link member " 
						+ newContributor.getMemberId() + " to topic " + newContributor.getTopicId(), e);
			}
		});
	}
	
	public void removeContributorsFromTopic(List<UpdatedTopicContributor> removedContributors) {
		Set<Long> topicIds = removedContributors.stream().map(UpdatedTopicContributor::getTopicId).collect(Collectors.toSet());
		
		topicIds.forEach(topic -> {
			List<Long> removedMembers = removedContributors.stream().filter(remCont -> remCont.getTopicId().equals(topic))
					.map(UpdatedTopicContributor::getMemberId).collect(Collectors.toList());
			
			memberTopicRepository.deleteContributorsFromTopic(topic, removedMembers);
		});
	}
	
	private void addNewTagsToTopic(Long topicId, List<String> tagNames) {
		List<QTag> existingTagsToLink = tagRepository.findExistingTags(tagNames);
		
		List<String> unknownNewTags;
		if(!CollectionUtils.isEmpty(existingTagsToLink)) {
			unknownNewTags = tagNames.stream().filter(tName -> {
				return existingTagsToLink.stream().noneMatch(qt -> qt.getTagName().equals(tName));
			}).collect(Collectors.toList());
		} else {
			unknownNewTags = tagNames;
		}
		
		if(!CollectionUtils.isEmpty(unknownNewTags)) {
			unknownNewTags.forEach(tag -> {
				try {
					tagRepository.addNewRecord(new QTag(tag));
				} catch (Exception e) {
					throw new RuntimeException("Unable to add tag '" + tag +"'", e);
				}
			});
			
			List<QTag> newTagsToLink = tagRepository.findExistingTags(unknownNewTags);
			existingTagsToLink.addAll(newTagsToLink);
		}
		
		existingTagsToLink.forEach(existingTag -> {
			try {
				topicTagRepository.addNewRecord(new QTopicTag(topicId, existingTag.getId()));
			} catch (Exception e) {
				throw new RuntimeException("Unable to link tag " + existingTag.getId() + 
						", with topic " + topicId, e);
			}
		});
	}
	
	private void removeTagsFromTopic(Long topicId, List<String> tagNames) {
		List<QTag> existingTags = tagRepository.findExistingTags(tagNames);
		List<Long> tagIds = existingTags.stream().map(QTag::getId).collect(Collectors.toList());
		topicTagRepository.deleteTagsFromTopic(tagIds, topicId);
	}

}
