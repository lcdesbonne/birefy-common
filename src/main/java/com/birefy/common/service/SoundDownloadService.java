package com.birefy.common.service;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.sound.SoundNameAndLocation;
import com.birefy.common.repo.SoundPurchasesRepository;
import com.birefy.common.repo.SoundRepository;

@Service
public class SoundDownloadService {
	@Autowired
	private SoundPurchasesRepository soundPurchasesRepository;
	@Autowired
	private FileUtilityActions fileUtilityActions;
	@Autowired
	private SoundRepository soundRepository;
	
	public boolean userCanDownloadFile(Long soundId) {
//		CommonUser user = (CommonUser)SecurityContextHolder.getContext()
//				.getAuthentication().getPrincipal();
//		
//		boolean hasPurchased = soundPurchasesRepository.userHasPurchasedSound(user.getId(), soundId);
//		return hasPurchased;
		
		//Any visitor can now download sounds.
		
		return true;
	}
	
	public boolean downloadFile(Long soundId, HttpServletResponse response) {
		boolean success = userCanDownloadFile(soundId);
		if(success) {
			SoundNameAndLocation soundDetails = soundRepository.getSoundNameAndLocation(soundId);
			String soundName = soundDetails.getName() + fileUtilityActions.getFileExtension(soundDetails.getLocation());
			response.addHeader("Content-Disposition", "attachment; fileName=" + soundName);
			String url = soundRepository.getSoundLocation(soundId);
			success = fileUtilityActions.downloadSoundFile(url, response);
		}
		
		return success;
	}
}
