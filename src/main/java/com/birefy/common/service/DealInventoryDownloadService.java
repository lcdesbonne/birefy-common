package com.birefy.common.service;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.deal.DealInventoryNameAndLocation;
import com.birefy.common.repo.DealInventoryRepository;

@Service
public class DealInventoryDownloadService {
	@Autowired
	private DealInventoryRepository dealInventoryRepository;
	@Autowired
	private FileUtilityActions fileUtilityActions;
	
	public boolean userCanDownloadContent(Long inventoryId) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		return dealInventoryRepository.canDownloadInventory(inventoryId, user.getId());
	}
	
	public boolean downloadFile(Long dealInventoryId, HttpServletResponse response) {
		boolean success = userCanDownloadContent(dealInventoryId);
		
		if(success) {
			DealInventoryNameAndLocation inventoryDetails = dealInventoryRepository.getDealInventoryNameAndLocation(dealInventoryId);
			
			String inventoryFileName = inventoryDetails.getName() + fileUtilityActions.getFileExtension(inventoryDetails.getLocation());
			response.addHeader("Content-Disposition", "attachment); filename=" + inventoryFileName);
			success = fileUtilityActions.downloadInventoryFile(inventoryDetails.getLocation(), response);
		}
		
		return success;
	}
}
