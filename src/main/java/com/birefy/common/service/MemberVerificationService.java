package com.birefy.common.service;

import java.time.LocalDate;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.authentication.events.OnPasswordChangeRequestEvent;
import com.birefy.common.entity.Member;
import com.birefy.common.entity.MemberVerification;
import com.birefy.common.repo.MemberRepository;
import com.birefy.common.repo.MemberVerificationRepository;

@Service
public class MemberVerificationService {
	private static final Logger LOGGER = LoggerFactory.getLogger(MemberVerificationService.class);
	
	private static final String VERIFICATION_DETAILS_NOT_FOUND_FOR_MEMBER = "Passphrase request denied. Unable to find member verification details for member: %d";
	private static final String TOKEN_CREDENTIALS_ERROR = "The token provided for passphrase update is invalid. requested by member: %d";
	private static final Long EXPIRY_DAYS = 1L;
	
	@Autowired
	private MemberVerificationRepository memberVerificationRepository;
	@Autowired
	private MemberRepository memberRepository;
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	public void requestPasswordChange() {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		eventPublisher.publishEvent(new OnPasswordChangeRequestEvent(user));
	}
	
	public void setPassphraseUpdateable(String token, Long memberId) {
		Optional<MemberVerification> memberVerification = memberVerificationRepository.getVerificationDetails(memberId);
		
		if (memberVerification.isEmpty()) {
			LOGGER.error(String.format(VERIFICATION_DETAILS_NOT_FOUND_FOR_MEMBER, memberId));
			throw new RuntimeException("Passphrase request denied.");
		}
		
		MemberVerification verificationDetails = memberVerification.get();
		
		boolean canChangePassword = verificationDetails.getToken().equals(token) && verificationDetails.getMemberId().equals(memberId)
				&& verificationDetails.getCreationDate().isBefore(LocalDate.now().plusDays(EXPIRY_DAYS));
		
		if (!canChangePassword) {
			LOGGER.error(String.format(TOKEN_CREDENTIALS_ERROR, memberId));
			throw new RuntimeException("Token validation failed.");
		} else {
			verificationDetails.setChangePassword(true);
		
			memberVerificationRepository.update(verificationDetails);
		}
	}
	
	public void updatePassPhrase(String passphrase) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		Optional<MemberVerification> verificationDetails = memberVerificationRepository.getVerificationDetails(user.getId());
		
		Member memberDetails = memberRepository.selectFullMemberDetailsById(user.getId());
		
		if (verificationDetails.isEmpty() || !verificationDetails.get().getChangePassword()) {
			LOGGER.error(String.format(VERIFICATION_DETAILS_NOT_FOUND_FOR_MEMBER, user.getId()));
			throw new RuntimeException("Passphrase change request denied.");
		}
		
		MemberVerification details = verificationDetails.get();
		details.setChangePassword(false);
		
		memberDetails.setKey(passphrase);
		
		try {
			memberRepository.update(memberDetails);
			
			memberVerificationRepository.update(details);
		} catch (IllegalAccessException | NoSuchFieldException e) {
			LOGGER.error("Error updating password for member " + user.getId() + "/n" + e.getMessage());
			throw new RuntimeException(e);
		}
	}
	
	public boolean hasConfiguredVerificationDetails() {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		Optional<MemberVerification> verificationDetails = memberVerificationRepository.getVerificationDetails(user.getId());
		String passPhrase = memberRepository.getMemberPassPhrase(user.getId());
		
		boolean isConfigured = verificationDetails.isPresent() && StringUtils.isNotEmpty(passPhrase);
		
		return isConfigured;
	}
	
	public boolean verifyPassphrase(long memberId, String passphrase) {		
		String storedPassphrase = memberRepository.getMemberPassPhrase(memberId);
		
		//TODO add some encryption
		
		boolean valid = storedPassphrase != null && storedPassphrase.equals(passphrase);
		
		return valid;
	}
	
}
