package com.birefy.common.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.http.fileupload.FileItemIterator;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.birefy.common.authentication.BirefyPlatformAuthority;
import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.sound.SoundShort;
import com.birefy.common.dto.sound.SoundTagDTO;
import com.birefy.common.dto.sound.SoundUpdate;
import com.birefy.common.dto.sound.SoundUpload;
import com.birefy.common.dto.sound.SoundWithTidingId;
import com.birefy.common.dto.tiding.TidingIdDTO;
import com.birefy.common.entity.Sound;
import com.birefy.common.entity.Sound.Category;
import com.birefy.common.entity.SoundDescriptiveTag;
import com.birefy.common.entity.SoundTag;
import com.birefy.common.entity.tiding.MemberChannel;
import com.birefy.common.repo.SoundDescriptiveTagRepository;
import com.birefy.common.repo.SoundRepository;
import com.birefy.common.repo.SoundTagRepository;
import com.birefy.common.repo.TidingMessageRepository;

@Service
public class SoundService {
	private final Logger LOGGER = LoggerFactory.getLogger(SoundService.class);
	
	@Autowired
	private SoundRepository soundRepository;
	@Autowired
	private FileUtilityActions fileUtilityActions;
	@Autowired
	private TidingMessageRepository tidingMessageRepository;
	@Autowired
	private SoundDescriptiveTagRepository soundDescriptiveTagRepository;
	@Autowired
	private SoundTagRepository soundTagRepository;
	
	
	public boolean saveSoundFile(SoundUpload soundUpload) {
		boolean success = false;
		String fileLocation;
		try {
			MultipartFile soundFile = soundUpload.getSoundFile();
			fileLocation = fileUtilityActions.writeSoundFileToSystem(soundFile);
		} catch (IOException io){
			LOGGER.error("Unable to upload file", io);
			return success;
		}
		
		success = saveFileDataToDatabase(fileLocation, soundUpload.getTitle(), soundUpload.getCategory(), soundUpload.getPrice(), soundUpload.getSoftware());
		List<SoundTagDTO> tagsForUpload;
		try {
			tagsForUpload = soundUpload.getTags();
		} catch (Exception e) {
			tagsForUpload = new ArrayList<>();
		}
		if(success && !CollectionUtils.isEmpty(tagsForUpload)) {
			List<SoundTagDTO> tagsToAdd = saveUnknownTags(tagsForUpload);
			//Identify the newly saved sound by the unique location
			int indexOfLastSlash = fileLocation.lastIndexOf('\\') + 1;
			CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			
			Long soundId = soundRepository.findSoundByLocation(fileLocation.substring(indexOfLastSlash), user.getId());
			
			List<SoundDescriptiveTag> soundTagLinks = tagsToAdd.stream().map(tag -> new SoundDescriptiveTag(soundId, tag.getId()))
					.collect(Collectors.toList());
			
			for(SoundDescriptiveTag tagLink : soundTagLinks) {
				try {
					soundDescriptiveTagRepository.addNewRecord(tagLink);
				} catch (NoSuchFieldException | IllegalAccessException e) {
					LOGGER.error("Error saving tags for the new sound upload.", e);
					success = false;
				}
			}
		}
		return success;
	}
		
	public StreamingResponseBody streamSoundFile(Long soundId, int byteOffSet) throws FileNotFoundException {
		SoundShort sound = soundRepository.getSoundById(soundId);
		String soundUrl = sound.getLocation();
		StreamingResponseBody response = fileUtilityActions.streamSoundFileFromSystem(soundUrl, byteOffSet);
        return response;
	}
	
	public boolean deleteSoundFile(long soundId) {
		boolean success = false;
		//Check that the user created the file
		SoundShort sound = soundRepository.getSoundById(soundId);
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(!sound.getMemberId().equals(user.getId()) && !user.getAuthorities().stream().anyMatch(auth -> auth == BirefyPlatformAuthority.ROYAL)) {
			LOGGER.error("User "+user.getId()+ " does not have permission to delete sound " + sound.getId());
			return success;
		}
		
		//Delete the file first
		success = fileUtilityActions.deleteSoundFileFromSystem(sound.getLocation());
		if(!success) {
			LOGGER.error("Unable to delete file at "+sound.getLocation());
			return success;
		}
		
		//Delete the entry from the database for the file
		success = soundRepository.deleteSoundById(sound.getId());
		return success;
	}
	
	public List<SoundWithTidingId> allRecentSounds() {
		List<SoundShort> soundShorts = soundRepository.allRecentSounds();
		return convertSoundShortToSoundWithTiding(soundShorts);
	}
	
	public List<SoundWithTidingId> allSoundsForReview() {
		List<SoundShort> soundShorts = soundRepository.allRecentSoundsForReview();
		return convertSoundShortToSoundWithTiding(soundShorts);
	}
	
	public boolean approveSound(Long soundId) {
		return soundRepository.approveSound(soundId);
	}
	
	public List<SoundWithTidingId> recentSounds(Sound.Category category) {
		List<SoundShort> sounds;
		switch (category) {
		case EFFECT: sounds = soundRepository.recentSoundEffects(); break;
		case INSTRUMENTAL: sounds = soundRepository.recentSoundInstrumentals(); break;
		default : sounds = new ArrayList<>();
		}
		
		return convertSoundShortToSoundWithTiding(sounds);
	}
	
	public List<SoundWithTidingId> recentSoundsTags(Sound.Category category, List<Long> tagIds) {
		List<SoundShort> sounds;
		switch (category) {
		case EFFECT: sounds = soundRepository.recentSoundEffectsTags(tagIds); break;
		case INSTRUMENTAL: sounds = soundRepository.recentSoundInstrumentalsTags(tagIds); break;
		default: sounds = new ArrayList<>();
		}
		return convertSoundShortToSoundWithTiding(sounds);
	}
	
	public List<SoundWithTidingId> searchSoundsByTitleAndCategory(String title, Sound.Category category) {
		List<SoundShort> sounds = soundRepository.soundsByNameAndCategory(title, category);
		return convertSoundShortToSoundWithTiding(sounds);
	}
	
	public List<SoundWithTidingId> searchSoundsByTitleAndCategoryTags(String title, Sound.Category category, List<Long> tagIds) {
		List<SoundShort> sounds = soundRepository.soundsByNameAndCategoryTags(title, category, tagIds);
		return convertSoundShortToSoundWithTiding(sounds);
	}
	
	public List<SoundWithTidingId> searchSoundsByTitleCategoryAndArtist(String title, Sound.Category category, Long memberId) {
		//Identify all valid memberIds matching the tidingId
		List<MemberChannel> tidingMembers = tidingMessageRepository.findChannelForMembers(Arrays.asList(memberId));
		
		List<SoundShort> sounds = soundRepository.findSoundByNameAndMemberCategory(Arrays.asList(memberId), title, category);
		List<SoundWithTidingId> soundsWithTidingIds = new ArrayList<>();
		
		for(SoundShort sound : sounds) {
			SoundWithTidingId s = new SoundWithTidingId(sound);
			String tId = tidingMembers.stream().filter(member -> member.getMemberId() == sound.getMemberId())
					.findFirst().get().getTidingId();
			s.setTidingId(tId);
			soundsWithTidingIds.add(s);
		}
		return soundsWithTidingIds;
	}
	
	public List<SoundWithTidingId> searchSoundsByTitleCategoryAndArtistTags(String title, Sound.Category category, 
			Long memberId, List<Long> tagIds) {
		//Identify all valid memberIds matching the tidingId
		List<MemberChannel> tidingMembers = tidingMessageRepository.findChannelForMembers(Arrays.asList(memberId));
		
		List<SoundShort> sounds = soundRepository.findSoundByNameAndMemberCategoryTags(Arrays.asList(memberId), title, category, tagIds);
		List<SoundWithTidingId> soundsWithTidingIds = new ArrayList<>();
		
		for(SoundShort sound : sounds) {
			SoundWithTidingId s = new SoundWithTidingId(sound);
			String tId = tidingMembers.stream().filter(member -> member.getMemberId() == sound.getMemberId())
					.findFirst().get().getTidingId();
			s.setTidingId(tId);
			soundsWithTidingIds.add(s);
		}
		return soundsWithTidingIds;
	}
	
	public List<SoundWithTidingId> allSoundsByArtistAndCategory(Long memberId, Sound.Category category) {
		List<SoundShort> sounds = soundRepository.soundsByMemberCategory(memberId, category);
		
		return convertSoundShortToSoundWithTiding(sounds);
	}
	
	public List<SoundWithTidingId> allSoundsByArtistAndCategoryTags(Long memberId, Sound.Category category, List<Long> tagIds) {
		List<SoundShort> sounds = soundRepository.soundsByMemberCategoryTags(memberId, category, tagIds);
		
		return convertSoundShortToSoundWithTiding(sounds);
	}
	
	public List<SoundWithTidingId> searchSoundsByTitle(String title) {
		List<SoundShort> sounds = soundRepository.findSoundsByTitle(title);
		
		return convertSoundShortToSoundWithTiding(sounds);
	}
	
	public List<SoundWithTidingId> searchSoundsByTitleTags(String title, List<Long> tagIds) {
		List<SoundShort> sounds = soundRepository.findSoundsByTitleTags(title, tagIds);
		
		return convertSoundShortToSoundWithTiding(sounds);
	}
	
	public List<SoundWithTidingId> searchSoundsByArtistAndTitle(Long memberId, String title) {
		List<SoundShort> sounds = soundRepository.findSoundsByMemberAndTitle(memberId, title);
		
		return convertSoundShortToSoundWithTiding(sounds);
	}
	
	public List<SoundWithTidingId> searchSoundsByArtistAndTitleTags(Long memberId, String title, List<Long> tagIds) {
		List<SoundShort> sounds = soundRepository.findSoundsByMemberAndTitleTags(memberId, title, tagIds);
		
		return convertSoundShortToSoundWithTiding(sounds);
	}
	
	public List<SoundWithTidingId> searchSoundsByMember(Long memberId) {
		List<SoundShort> sounds = soundRepository.soundsByMember(memberId);
		
		return convertSoundShortToSoundWithTiding(sounds);
	}
	
	public List<SoundWithTidingId> searchSoundsByMemberTags(Long memberId, List<Long> tagIds) {
		List<SoundShort> sounds = soundRepository.soundsByMemberTags(memberId, tagIds);
		
		return convertSoundShortToSoundWithTiding(sounds);
	}
	
	public SoundWithTidingId getSoundById(Long soundId) {
		SoundShort sound = soundRepository.getSoundById(soundId);
		
		List<SoundWithTidingId> convertedSound = convertSoundShortToSoundWithTiding(Arrays.asList(sound));
		
		return convertedSound.get(0);
	}
	
	public List<SoundWithTidingId> getAllSoundsByUserWithPricing(long userId) {
		List<SoundShort> sounds = soundRepository.soundsByMemberWithPrice(userId);
		
		List<SoundWithTidingId> convertedSounds = convertSoundShortToSoundWithTiding(sounds);
		
		return convertedSounds;
	}
	
	public Map<Long, List<SoundTagDTO>> tagsForSounds(List<Long> soundIds) {
		Map<Long, List<SoundTagDTO>> tagsForSounds;
		if(CollectionUtils.isEmpty(soundIds)) {
			tagsForSounds = new HashMap<>();
		} else {
			tagsForSounds = soundDescriptiveTagRepository.findTagsForSounds(soundIds);
		}
		return tagsForSounds;
	}
	
	public List<SoundTagDTO> searchForSoundTagsByName(String tagName) {
		List<SoundTagDTO> tags = soundTagRepository.searchForTags(tagName);
		return tags;
	}
	
	public List<MemberChannel> searchArtists(String tidingId) {
		List<TidingIdDTO> tidingMembers;
		try {
			CommonUser userDetails = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			List<MemberChannel> channelDetails = tidingMessageRepository.findChannelForMembers(Arrays.asList(userDetails.getId()));	
			tidingMembers = tidingMessageRepository.findTidingIds(tidingId, channelDetails.get(0).getId());
		} catch (Exception e) {
			LOGGER.debug("Unable to obtain users log in details", e);
			tidingMembers = tidingMessageRepository.findTidingIdsLikeTiding(tidingId);
		}
		List<MemberChannel> tidingChannelDetails = tidingMessageRepository.findChannelsByChannelIds(
				tidingMembers.stream().map(tch -> tch.getChannelId()).collect(Collectors.toList())
				);
		List<Long> memberIds = tidingChannelDetails.stream().map(t -> t.getMemberId()).collect(Collectors.toList());
		
		List<Long> musicalMembers = soundRepository.membersWithMusic(memberIds);
		
		return tidingChannelDetails.stream().filter(tm -> musicalMembers.contains(tm.getMemberId())).collect(Collectors.toList());
	}
	
	public boolean updateSound(SoundUpdate soundUpdate) {
		boolean success = false;
		SoundShort existingSound = soundRepository.getSoundById(soundUpdate.getSoundId());
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if(existingSound.getMemberId() != user.getId()) {
			return success;
		}
		
		if(Optional.ofNullable(soundUpdate.getNewCategory()).isPresent() && 
				Optional.ofNullable(soundUpdate.getNewSoundName()).isPresent()
				&& !Optional.ofNullable(soundUpdate.getPrice()).isPresent()) {
			success = soundRepository.updateSoundNameAndCategory(soundUpdate.getSoundId(), 
					soundUpdate.getNewSoundName(), soundUpdate.getNewCategory());
		} else if (Optional.ofNullable(soundUpdate.getNewCategory()).isPresent() &&
				!Optional.ofNullable(soundUpdate.getNewSoundName()).isPresent()
				&& !Optional.ofNullable(soundUpdate.getPrice()).isPresent()) {
			success = soundRepository.updateSoundCategory(soundUpdate.getSoundId(), soundUpdate.getNewCategory());
		} else if (!Optional.ofNullable(soundUpdate.getNewCategory()).isPresent() && 
				Optional.ofNullable(soundUpdate.getNewSoundName()).isPresent()
				&& !Optional.ofNullable(soundUpdate.getPrice()).isPresent()) {
			success = soundRepository.updateSoundName(soundUpdate.getSoundId(), soundUpdate.getNewSoundName());
		} else if(Optional.ofNullable(soundUpdate.getNewCategory()).isPresent() && 
				Optional.ofNullable(soundUpdate.getNewSoundName()).isPresent()
				&& Optional.ofNullable(soundUpdate.getPrice()).isPresent()) {
			success = soundRepository.updateSoundNameAndCategoryAndPrice(soundUpdate.getSoundId(), 
					soundUpdate.getNewSoundName(), soundUpdate.getNewCategory(), soundUpdate.getPrice());
		} else if (Optional.ofNullable(soundUpdate.getNewCategory()).isPresent() &&
				!Optional.ofNullable(soundUpdate.getNewSoundName()).isPresent()
				&& Optional.ofNullable(soundUpdate.getPrice()).isPresent()) {
			success = soundRepository.updateSoundCategoryAndPrice(soundUpdate.getSoundId(), 
					soundUpdate.getNewCategory(), soundUpdate.getPrice());
		} else if (!Optional.ofNullable(soundUpdate.getNewCategory()).isPresent() && 
				Optional.ofNullable(soundUpdate.getNewSoundName()).isPresent()
				&& Optional.ofNullable(soundUpdate.getPrice()).isPresent()) {
			success = soundRepository.updateSoundNameAndPrice(soundUpdate.getSoundId(), 
					soundUpdate.getNewSoundName(), soundUpdate.getPrice());
		}
		
		
		if(!CollectionUtils.isEmpty(soundUpdate.getRemovedTags())) {
			List<Long> removedTags = soundUpdate.getRemovedTags().stream()
					.map(t -> t.getId()).collect(Collectors.toList());
			soundDescriptiveTagRepository.deleteTagsFromSound(soundUpdate.getSoundId(), removedTags);
		}
		
		if(!CollectionUtils.isEmpty(soundUpdate.getAddedTags())) {
			List<SoundTagDTO> tagsToAdd = saveUnknownTags(soundUpdate.getAddedTags());
			
			List<SoundDescriptiveTag> addedTags = tagsToAdd.stream()
					.map(t -> new SoundDescriptiveTag(soundUpdate.getSoundId(), t.getId()))
					.collect(Collectors.toList());
			
			for(SoundDescriptiveTag t: addedTags){
				try {
					soundDescriptiveTagRepository.addNewRecord(t);
				} catch (NoSuchFieldException | IllegalAccessException e) {
					LOGGER.error("Unable to add new tags to sound with id: " + soundUpdate.getSoundId());
					success = false;
				}
			}
		}
		return success;
	}
	
	private List<SoundWithTidingId> convertSoundShortToSoundWithTiding(List<SoundShort> sounds) {
		List<Long> memberIds = new ArrayList<>();
		memberIds.addAll(sounds.stream().map(sound -> sound.getMemberId()).collect(Collectors.toSet()));
		List<MemberChannel> channelDetails;
		if(!memberIds.isEmpty()) {
			channelDetails = tidingMessageRepository.findChannelForMembers(memberIds);
		} else {
			channelDetails = new ArrayList<>();
		}
		List<SoundWithTidingId> soundsWithTidingId = new ArrayList<>();
		for(SoundShort s : sounds) {
			SoundWithTidingId swtid = new SoundWithTidingId(s);
			MemberChannel targetChannel = channelDetails.stream().filter(cd -> cd.getMemberId() == s.getMemberId())
					.collect(Collectors.toList()).get(0);
			String tidingId = targetChannel.getTidingId();
			Long channelId = targetChannel.getId();
			swtid.setTidingId(tidingId);
			swtid.setChannelId(channelId);
			soundsWithTidingId.add(swtid);
		}
		
		return soundsWithTidingId;
	}
	
	private boolean saveFileDataToDatabase(String location, String fileName, Category category, Float price, String software) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		boolean success = false;
		try {
			Sound sound = new Sound();
			sound.setMemberId(user.getId());
			sound.setLocation(location);
			sound.setName(fileName);
			sound.setCategory(category);
			sound.setPrice(price);
			sound.setSoftware(software);
			sound.setReview(true);
			
			soundRepository.addNewRecord(sound);
			
			success = true;
		} catch (Exception e) {
			LOGGER.error("Failed to save sound details to database", e);
		}
		
		return success;		
	}
	
	private List<SoundTagDTO> saveUnknownTags(List<SoundTagDTO> tagList) {
		List<SoundTagDTO> unknownTags = tagList.stream().filter(t -> !Optional.ofNullable(t.getId())
				.isPresent()).collect(Collectors.toList());
		if(CollectionUtils.isEmpty(unknownTags)) {
			return tagList;
		} else {
			List<SoundTagDTO> knownTags = tagList.stream().filter(t -> Optional.ofNullable(t.getId()).isPresent())
					.collect(Collectors.toList());
			
			unknownTags.forEach(tag -> {
				if(!soundTagRepository.tagExists(tag.getTag())) {
					SoundTag newTag = new SoundTag();
					newTag.setTag(tag.getTag());
					try {
						soundTagRepository.addNewRecord(newTag);
					} catch (Exception e) {
						LOGGER.error("Failed to create new tag '" + tag.getTag(), e);
					}
				}
				SoundTagDTO tagToAdd = soundTagRepository.getTagByName(tag.getTag());
				if(Optional.ofNullable(tagToAdd).isPresent()) {
					knownTags.add(tagToAdd);
				}
			});
			
			return knownTags;
		}
 	}
}
