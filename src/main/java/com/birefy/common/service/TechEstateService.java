package com.birefy.common.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.birefy.common.dto.question.QQuestionDTO;
import com.birefy.common.dto.question.QuestionContributor;
import com.birefy.common.dto.question.QuestionTechTags;
import com.birefy.common.dto.question.TechTagAndQuestion;
import com.birefy.common.dto.tiding.TidingAndMemberID;
import com.birefy.common.dto.topic.QTagWithTopic;
import com.birefy.common.dto.topic.QTopicAndQQuestionsFull;
import com.birefy.common.dto.topic.QTopicDTO;
import com.birefy.common.entity.QMemberQuestion;
import com.birefy.common.entity.QQuestion;
import com.birefy.common.entity.QTag;
import com.birefy.common.entity.QTopic;
import com.birefy.common.entity.TechnicalTag;
import com.birefy.common.entity.utils.mapper.QQuestionToDTOMapper;
import com.birefy.common.entity.utils.mapper.QTopicToDTOMapper;
import com.birefy.common.entity.utils.mapper.QuestionDTOToQuestionMapper;
import com.birefy.common.repo.QMemberQuestionRepository;
import com.birefy.common.repo.QQuestionRepository;
import com.birefy.common.repo.QTagRepository;
import com.birefy.common.repo.QTopicRepository;
import com.birefy.common.repo.TechnicalTagRepository;
import com.birefy.common.repo.TidingMessageRepository;

@Service
public class TechEstateService {
	private static final Logger LOGGER = LoggerFactory.getLogger(TechEstateService.class);
	@Autowired
	private QQuestionRepository questionRepository;
	@Autowired
	private QTopicRepository topicRepository;
	@Autowired
	private QTagRepository tagRepository;
	@Autowired
	private TechnicalTagRepository technicalTagRepository;
	@Autowired
	private QMemberQuestionRepository memberQuestionRepository;
	@Autowired
	private TidingMessageRepository tidingMessageRepository;
	
	public Map<QTopicAndQQuestionsFull, Map<Long, List<QTag>>> retrieveTechByUser(long memberId) {
		List<QQuestion> questions = questionRepository.allQuestionsByMember(memberId);
		
		Set<Long> topicIds = questions.stream().map(q -> q.getTopicId()).collect(Collectors.toSet());
		
		List<QTopic> topics = topicRepository.findById(topicIds.stream().collect(Collectors.toList()));
		
		List<QQuestionDTO> qquestionsDTO = questions.stream().map(q -> QQuestionToDTOMapper.map(q)).collect(Collectors.toList());
		List<QTopicDTO> qtopicsDTO = topics.stream().map(t -> QTopicToDTOMapper.map(t)).collect(Collectors.toList());
		
		QTopicAndQQuestionsFull topicsAndQuestions = new QTopicAndQQuestionsFull(qtopicsDTO, qquestionsDTO);
		
		Map<Long, List<QTag>> topicsAndTags = getAllTagsForTopics(topicIds);
		
		Map<QTopicAndQQuestionsFull, Map<Long, List<QTag>>> userData = new HashMap<>();
		userData.put(topicsAndQuestions, topicsAndTags);
		
		return userData;
	}
	
	public Map<Long, List<QTag>> getAllTagsForTopics(Set<Long> topicIds) {
		if(CollectionUtils.isEmpty(topicIds)) {
			return new HashMap<>();
		}
		
		List<QTagWithTopic> topicsAndTags = tagRepository.getTagsForTopics(topicIds);
		
		BiPredicate<QTagWithTopic, QTagWithTopic> tagsForTopic = (topTag, tt) -> topTag.getTopicId() == tt.getTopicId();
		Map<Long, List<QTag>> structuredTopicsAndTags = new HashMap<>();
		
		topicsAndTags.forEach(tt -> {
			List<QTag> tags = topicsAndTags.stream().filter(toptag -> tagsForTopic.test(toptag, tt)).map(tota -> new QTag(tota.getTagId(), tota.getTagName())).collect(Collectors.toList());
			structuredTopicsAndTags.put(tt.getTopicId(), tags);
		});
				
		return structuredTopicsAndTags;
		}
	
	public Boolean applyQuestionUpdates(List<QQuestionDTO> questions) {
		for (QQuestionDTO question : questions) {
			try {
				questionRepository.update(QuestionDTOToQuestionMapper.map(question));
			} catch (NoSuchFieldException | IllegalAccessException e) {
				LOGGER.error(String.format("Update of question with id %d", question.getId()), e);
				return false;
			}
		}
		return true;
	}
	
	public boolean updateQuestionForReview(Long questionId, boolean isReview) {
		try {
			questionRepository.alterReviewState(questionId, isReview);
			return true;
		} catch (Exception e) {
			LOGGER.error("Unable to alter review state", e);
			return false;
		}
	}
	
	public List<QuestionTechTags> techTagsForQuestions(List<Long> questionIds) {
		List<TechTagAndQuestion> tagAndQuestions = technicalTagRepository.getTechnicalTagsForQuestion(questionIds);
		
		Set<Long> qIds = tagAndQuestions.stream().map(tq -> tq.getQuestionId()).collect(Collectors.toSet());
		
		List<QuestionTechTags> questionsAndTags = qIds.stream().map(id -> {
			List<String> tagNames = tagAndQuestions.stream().filter(tagAndQ -> tagAndQ.getQuestionId().equals(id))
					.map(ftq -> ftq.getTagName()).collect(Collectors.toList());
			return new QuestionTechTags(id, tagNames);
		}).collect(Collectors.toList());
		
		return questionsAndTags;
	}
	
	public List<String> findAvailableTechTags() {
		List<String> technicalTags = technicalTagRepository.findAllTechTags();
		return technicalTags;
	}
	
	public List<TechnicalTag> findTechTagsByName(String searchCharacters) {
		List<TechnicalTag> results;
		
		if (searchCharacters.length() < 2) {
			results = new ArrayList<>();
			
		} else {
			results = technicalTagRepository.searchTagsByName(searchCharacters);
		}
		
		return results;
	}
	
	public List<QTopic> topicsByMember(Long memberId) {
		return topicRepository.getTopicsByMember(memberId);
	}
	
	public List<QTagWithTopic> findTagsForTopics(List<Long> topicIds) {
		return tagRepository.getTagsForTopics(topicIds.stream().collect(Collectors.toSet()));
	}
	
	public List<TidingAndMemberID> searcMembersByTidingId(String tidingId) {
		return tidingMessageRepository.findActiveMembersByTidingId(tidingId);
	}

}
