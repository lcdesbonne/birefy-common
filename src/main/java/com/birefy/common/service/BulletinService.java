package com.birefy.common.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.birefy.common.authentication.BirefyPlatformAuthority;
import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.bulletin.BulletinShort;
import com.birefy.common.dto.bulletin.BulletinsWithTechTags;
import com.birefy.common.entity.Bulletin;
import com.birefy.common.entity.BulletinTechnicalTag;
import com.birefy.common.entity.MemberBulletin;
import com.birefy.common.entity.TechnicalTag;
import com.birefy.common.repo.BulletinRepository;
import com.birefy.common.repo.BulletinTechnicalTagRepository;
import com.birefy.common.repo.MemberBulletinRepository;
import com.birefy.common.repo.TechnicalTagRepository;

@Service
public class BulletinService {
	public static Logger LOGGER = LoggerFactory.getLogger(BulletinService.class);
	
	@Autowired
	private BulletinRepository bulletinRepository;
	@Autowired
	private MemberBulletinRepository memberBulletinRepository;
	@Autowired
	private TechnicalTagRepository technicalTagRepository;
	@Autowired
	private BulletinTechnicalTagRepository bulletinTechnicalTagRepository;
	
	public List<BulletinShort> allOpenBulletins() throws AccessDeniedException {
		List<BulletinShort> bulletins = new ArrayList<>();
		
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		BirefyPlatformAuthority authority = (BirefyPlatformAuthority) user.getAuthorities().stream().findFirst().get();
		
		if (authority.equals(BirefyPlatformAuthority.ROYAL)) {
			bulletins = bulletinRepository.allOpenBulletins();
		} else {
			throw new AccessDeniedException("Only Royals can access all open bulletins.");
		}
		return bulletins;
	}
	
	public List<BulletinShort> allAvailableBulletinsForMember() {
		List<BulletinShort> bulletins = new ArrayList<>();
		
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		bulletins = bulletinRepository.allAvailableBulletinsForMember(user.getId());
		
		return bulletins;
	}
	
	public List<BulletinShort> allTargetedBulletinsForMember() {
		List<BulletinShort> bulletins = new ArrayList<>();
		
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		bulletins = bulletinRepository.allTargetedBulletins(user.getId());
		
		return bulletins;
	}
	
	@Transactional
	public boolean createNewBulletin(Bulletin newBulletin, List<TechnicalTag> technicalTags) throws Exception {
		boolean success = true;
		
		//Save the new bulletin
		bulletinRepository.addNewRecord(newBulletin);
		long bulletinId = bulletinRepository.findBulletinIdByDescription(newBulletin.getDescription());
		newBulletin.setId(bulletinId);
		
		//Link member to the bulletin
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		MemberBulletin memberBulletin = new MemberBulletin(newBulletin.getId(), user.getId());
		memberBulletinRepository.addNewRecord(memberBulletin);
		
		//Identify all new tags
		List<TechnicalTag> newTags = technicalTags.stream().filter(tag -> !Optional.ofNullable(tag.getId()).isPresent())
				.collect(Collectors.toList());
		List<TechnicalTag> existingTags = technicalTags.stream().filter(tag -> Optional.ofNullable(tag.getId()).isPresent())
				.collect(Collectors.toList());
		
		if (!newTags.isEmpty()) {
			//Add the new tags to the database
			for (TechnicalTag tag : newTags) {
				technicalTagRepository.addNewRecord(tag);
				
				long tagId = technicalTagRepository.findTagByName(tag.getName());
				tag.setId(tagId);
				
				existingTags.add(tag);	
			}
		}
		
		if (!existingTags.isEmpty()) {
			//Link the tags to the bulletin
			for (TechnicalTag tag : existingTags) {
				BulletinTechnicalTag bulletinTechTag = new BulletinTechnicalTag(newBulletin.getId(), tag.getId());
				bulletinTechnicalTagRepository.addNewRecord(bulletinTechTag);
			}
		}
			
		return success;
	}
	
	public Bulletin updateBulletin(Bulletin bulletin) {
		try {
			bulletinRepository.update(bulletin);
			return bulletin;
		} catch (NoSuchFieldException | IllegalAccessException e) {
			LOGGER.error("Failed to update bulletin", e);
			return null;
		}
		
	}
	
	public BulletinsWithTechTags getBulletinDataForMember() {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Bulletin> bulletins = bulletinRepository.getBulletinsByMember(user.getId());
		
		List<Long> bulletinIds = bulletins.stream().map(b -> b.getId()).collect(Collectors.toList());
		
		Map<Long, List<String>> techTagsForBulletins = bulletinsAndTechTags(bulletinIds);
		
		BulletinsWithTechTags bulletinsWithTechTags = new BulletinsWithTechTags(bulletins, techTagsForBulletins);
		
		return bulletinsWithTechTags;
	}
	
	public Map<Long, List<String>> bulletinsAndTechTags(List<Long> bulletinIds) {
		Map<Long, List<String>> bulletinsAndTechTags = bulletinIds.isEmpty() ? new HashMap<>() :
		bulletinTechnicalTagRepository.tagsForBulletins(bulletinIds);
		
		return bulletinsAndTechTags;
	}
	
	public List<TechnicalTag> allAvailableTechnicalTags() {
		return technicalTagRepository.findAllTechTagsFull();
	}
}
