package com.birefy.common.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.birefy.common.cluster.ClusterUtilities;
import com.birefy.common.cluster.DeploymentRequest;
import com.birefy.common.dto.question.QuestionIdAndSource;
import com.birefy.common.entity.QQuestion;
import com.birefy.common.repo.QQuestionRepository;

import freemarker.template.TemplateException;

@Service
public class TechContainerManagementService {
	private static Logger LOGGER = LoggerFactory.getLogger(TechContainerManagementService.class);
	
	@Value("${cluster.container.launch-programme}")
	private String LAUNCH_PROGRAMME;
	
	@Value("${cluster.container.shutdown-programme}")
	private String SHUTDOWN_PROGRAMME;
	
	@Autowired
	private QQuestionRepository questionRepository;
	
	@Autowired
	private ClusterUtilities clusterUtilities;
	
	@Scheduled(cron = "0 0 0 * * ?")
	public void launchContainersForQuestions() {
		LOGGER.info("Executing container launch job. Date: " + LocalDateTime.now().toString());
		
		List<DeploymentRequest> questionsToLaunch = questionRepository.retrieveDeploymentRequests();
		
		clusterUtilities.clearDeploymentFolder();
		
		for(DeploymentRequest deploymentRequest : questionsToLaunch) {
			try {
				
				String deploymentFileName = clusterUtilities.generateDeploymentContent(deploymentRequest);
				
				//TODO: figure out a way to fully automate the deployment of projects
				/*
				 * String command = String.format(LAUNCH_PROGRAMME, deploymentFileName);
				 * 
				 * String[] commandParameters = command.split("\\|");
				 * 
				 * LOGGER.info("Preparing to execute the following command: " +
				 * commandParameters[0].strip()); LOGGER.info("With parameter: " +
				 * commandParameters[1].strip()); LOGGER.info("With parameter: " +
				 * commandParameters[2].strip()); LOGGER.info("With parameter: " +
				 * commandParameters[3].strip());
				 * 
				 * Process process = new ProcessBuilder(commandParameters[0].strip(),
				 * commandParameters[1].strip(), commandParameters[2].strip(),
				 * commandParameters[3].strip()).start();
				 * 
				 * CommandConsumer commandConsumer = new
				 * CommandConsumer(process.getInputStream(),
				 * postProcessContainerLaunchFunction(deploymentRequest.getQuestionId()));
				 * 
				 * Executors.newSingleThreadExecutor().submit(commandConsumer);
				 * 
				 * int exitCode = process.waitFor(); if (exitCode != 0) {
				 * LOGGER.error("Issue during launch of project with id: " +
				 * deploymentRequest.getQuestionId() + ". At time " + LocalDateTime.now()
				 * +" please refer to kubernetes cluster logs.." ); }
				 */
			} catch (IOException e) {
				LOGGER.error("Issue during launch of project with id: "+deploymentRequest.getQuestionId(), e);
			} catch (TemplateException t) {
				LOGGER.error("Unable to generate deployment file from template for question with id: " + deploymentRequest.getQuestionId());
			}
		}
		
	}
	
	public void shutdownContainersForQuestions(List<Long> questionIds) {
		questionIds.forEach(qId -> {
			try {
				String deletionProcessFileName = clusterUtilities.deleteProjectContent(qId);

				Process process = Runtime.getRuntime().exec(String.format(SHUTDOWN_PROGRAMME, deletionProcessFileName));
				
				CommandConsumer commandConsumer = new CommandConsumer(process.getInputStream(), 
						postProcessContainerShutdownFunction(qId));
				
				Executors.newSingleThreadExecutor().submit(commandConsumer);
				
				
				int exitCode = process.waitFor();
				if (exitCode != 0) {
					LOGGER.error("Issue during shutdown of project with id: " + qId + "At time "
				+ LocalDateTime.now() +" please refer to kubernetes cluster logs.." );
				}
			} catch (IOException | InterruptedException e) {
				LOGGER.error("Issue during shutdown of project with id: "+qId, e);
			} catch (TemplateException t) {
				LOGGER.error("Issue with tenmplate, unable to shutdown question with id: " + qId, t);
			}
		});
	}
	
	private Consumer<Object> postProcessContainerLaunchFunction(Long questionId) {
		Consumer<Object> postProcessFunction = (appUrl) -> {
			if(StringUtils.isNotEmpty((String)appUrl)) {
				QQuestion activatedQuestion = new QQuestion();
				activatedQuestion.setId(questionId);
				activatedQuestion.setAppUrl((String)appUrl);
				activatedQuestion.setActive(true);
				
				try {
					questionRepository.update(activatedQuestion);
				} catch (IllegalAccessException | NoSuchFieldException e) {
					LOGGER.error("Error updating project with id: " + questionId + ", launch likely successful. App url: " + appUrl, e);
					e.printStackTrace();
				}
			} else {
				LOGGER.error("Unale to attain a valid app url for project with id: " + questionId);
			}
		};
		
		return postProcessFunction;
	}
	
	private Consumer<Object> postProcessContainerShutdownFunction(Long questionId) {
		Consumer<Object> postProcessContainerShutdownFunction = (successCode) -> {
			if ((Integer)successCode == 0) {
				//TODO verify what the success code should be..
				QQuestion deactivatedQuestion = new QQuestion();
				deactivatedQuestion.setId(questionId);
				deactivatedQuestion.setActive(false);
				deactivatedQuestion.setDeleteRequested(false);
				
				try {
					questionRepository.update(deactivatedQuestion);
				} catch (IllegalAccessException | NoSuchFieldException e) {
					LOGGER.error("Error updating project with id: " + questionId + ", deactivation likely successfull.", e);
				}
			} else {
				LOGGER.error("Unale to shutdown container for project with id: " + questionId);
			}
				
		};
		
		return postProcessContainerShutdownFunction;
	}
	
	
	private static class CommandConsumer implements Runnable {
		private InputStream inputStream;
		private Consumer<Object> consumer;
		
		public CommandConsumer(InputStream inputStream, Consumer<Object> consumer) {
			this.inputStream = inputStream;
			this.consumer = consumer;
		}
		
		@Override
		public void run() {
			new BufferedReader(new InputStreamReader(inputStream)).lines()
				.forEach(consumer);
		}
		
	}
}
