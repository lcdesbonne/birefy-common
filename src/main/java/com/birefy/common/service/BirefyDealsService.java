package com.birefy.common.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.deal.DealDTO;
import com.birefy.common.dto.deal.DealInventoryDTO;
import com.birefy.common.dto.deal.DealInventoryNameAndLocation;
import com.birefy.common.dto.deal.DealUpdateDTO;
import com.birefy.common.dto.deal.InventoryPreviewMetadata;
import com.birefy.common.dto.deal.InventoryUpdateDTO;
import com.birefy.common.dto.deal.NewDeal;
import com.birefy.common.dto.deal.NewInventory;
import com.birefy.common.dto.tiding.TidingAndMemberID;
import com.birefy.common.entity.Deal;
import com.birefy.common.entity.DealInventory;
import com.birefy.common.repo.DealInventoryRepository;
import com.birefy.common.repo.DealRepository;
import com.birefy.common.repo.InventoryTypeRepository;
import com.birefy.common.repo.TidingMessageRepository;
import com.birefy.common.utils.BirefyApplicationUtils;

@Service
public class BirefyDealsService {
	private static final Logger LOGGER = LoggerFactory.getLogger(BirefyDealsService.class);
	
	@Autowired
	private DealRepository dealRepository;
	@Autowired
	private DealInventoryRepository dealInventoryRepository;
	@Autowired
	private FileUtilityActions fileUtilityActions;
	@Autowired
	private TidingMessageRepository tidingMessageRepository;
	@Autowired
	private InventoryTypeRepository inventoryTypeRepository;
	@Autowired
	private BirefyApplicationUtils birefyApplicationUtils;
	
	private final List<String> supportedImageFormats = List.of(".jpeg", ".jpg", ".gif", ".png");
	private final List<String> supportedSoundFormats = List.of(".wav", ".mp3", ".flac");
	
	public List<DealDTO> loadDealsWithMember() {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		
		return mapDealsToDealDTO(dealRepository.dealsWithMember(user.getId()));
	}
	
	public List<DealInventoryDTO> inventoryForDeals(List<Long> dealIds) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		
		return dealInventoryRepository.inventoryForDeals(dealIds, user.getId());
	}
	
	public boolean saveNewDeal(NewDeal newDeal) {
		Deal createdDeal = new Deal();
		createdDeal.setCreationDate(LocalDateTime.now(ZoneOffset.UTC));
		createdDeal.setActive(true);
		createdDeal.setCustomerConfirm(false);
		createdDeal.setSupplierConfirm(false);
		createdDeal.setCustomerDetails(newDeal.getCustomerDetails());
		createdDeal.setSupplierDetails(newDeal.getSupplierDetails());
		createdDeal.setPrice(newDeal.getPrice());
		createdDeal.setCustomerId(newDeal.getCustomerId());
		createdDeal.setSupplierId(newDeal.getSupplierId());
		
		boolean success;
		try {
			dealRepository.addNewRecord(createdDeal);
			success = true;
		} catch (Exception e) {
			success = false;
			LOGGER.error("Error creating deal involving customer " + newDeal.getCustomerId() + 
					", and supplier " + newDeal.getSupplierId() + " at " + LocalDateTime.now(ZoneOffset.UTC).toString(),
					e);
		}
		
		return success;
	}
	
	public boolean updateDeal(DealUpdateDTO update) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		boolean canUpdateDeal = dealRepository.canUpdateDeal(update.getDealId(), user.getId());
		
		boolean success = false;
		if(canUpdateDeal) {
			Deal dealToUpdate = update.mapToDeal();
			try {
				dealRepository.update(dealToUpdate);
				success = true;
			} catch (Exception e) {
				LOGGER.error("Failed to update deal with id: " + update.getDealId() + " by user " + user.getId(),
						e);
			}
		}
		
		return success;
	}
	
	public boolean rejectDeal(Long dealId) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return dealRepository.rejectDeal(dealId, user.getId());
	}
	
	public void prepareUploadOfInventory(Long dealId, Model model) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		boolean canAddInventory = dealRepository.canAddInventory(user.getId(), dealId);
		
		if(canAddInventory) {
			NewInventory inventoryForm = new NewInventory();
			inventoryForm.setDealId(dealId);
			
			model.addAttribute("newInventory", inventoryForm);
			model.addAttribute("types", inventoryTypeRepository.allTypes());
			model.addAttribute("maxFileSize", fileUtilityActions.getMaxFileSize());
			
		} else {
			throw new RuntimeException("Permission denied to " + user.getId() + "to add inventory to deal " + dealId);
		}
	}
	
	public boolean saveNewInventory(NewInventory newInventory) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		boolean canUpdateDeal = dealRepository.canUpdateDeal(newInventory.getDealId(), user.getId());
		if(!canUpdateDeal) {
			throw new RuntimeException("Permission denied to " + user.getId() + "to add inventory to deal " +
					newInventory.getDealId());
		}
		boolean success = false;
		String fileLocation;
		try {
			MultipartFile inventoryFile = newInventory.getInventoryFile();
			fileLocation = fileUtilityActions.writeInventoryFileToSystem(inventoryFile);
		} catch (IOException io) {
			LOGGER.error("Unable to upload inventory file", io);
			return success;
		}
		
		DealInventory inventory = new DealInventory();
		inventory.setDealId(newInventory.getDealId());
		inventory.setTitle(newInventory.getTitle());
		inventory.setAuthorId(user.getId());
		inventory.setInventoryTypeId(newInventory.getInventoryTypeId());
		inventory.setLocation(fileLocation);
		inventory.setCanPreview(canPreviewInventory(fileLocation));
		inventory.setRunCommands(newInventory.getRunCommands());
		inventory.setActive(true);
		
		try {
			dealInventoryRepository.addNewRecord(inventory);
			success = true;
		} catch (Exception e) {
			success = false;
			fileUtilityActions.deleteInventoryFileFromSystem(fileLocation);
			LOGGER.error("Error saving inventory file for user " + user.getId() + " in deal " + newInventory.getDealId());
		}
		
		return success;
	}
	
	public boolean updateInventory(InventoryUpdateDTO inventoryUpdate) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		boolean isAuthor = dealInventoryRepository.isAuthor(user.getId(), inventoryUpdate.getInventoryId());
		
		if(!isAuthor) {
			throw new RuntimeException("Permission denied for user " + user.getId() + 
					" for the update of inventory "+ inventoryUpdate.getInventoryId());
		}
		
		DealInventory inventory = new DealInventory();
		inventory.setId(inventoryUpdate.getInventoryId());
		inventory.setCanPreview(inventoryUpdate.isCanPreview());
		inventory.setTitle(inventoryUpdate.getTitle());
		
		boolean success = false;
		try {
			dealInventoryRepository.update(inventory);
			success = true;
		} catch (Exception e) {
			LOGGER.error("Error updating inventory " + inventory.getId() + " by user " + user.getId(), e);
		}
		
		return success;
	}
	
	public boolean deleteInventory(Long inventoryId) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		return dealInventoryRepository.deleteInventoryByAuthor(inventoryId, user.getId());
	}
	
	public boolean supplierConfirmDeal(Long dealId) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return dealRepository.supplierConfirmDeal(dealId, user.getId());
	}
	
	public boolean customerConfirmDeal(Long dealId) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return dealRepository.customerConfirmDeal(dealId, user.getId());
	}
	
	public boolean supplierUnconfirmDeal(Long dealId) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return dealRepository.supplierUnconfirm(dealId, user.getId());
	}
	
	public boolean customerUnconfirmDeal(Long dealId) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return dealRepository.customerUnconfirm(dealId, user.getId());
	}
	
	public StreamingResponseBody previewInventoryContent(Long inventoryId) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		boolean canViewInventory = dealInventoryRepository.canDownloadInventory(inventoryId, user.getId());
		
		if(!canViewInventory) {
			throw new RuntimeException("Permission denied to view content");
		}
		
		try {
			return streamInventory(inventoryId, 0);
		} catch (FileNotFoundException e) {
			throw new RuntimeException("File Not Found");
		}	
	}
	
	public InventoryPreviewMetadata inventoryPreviewMetadata(Long inventoryId) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		InventoryPreviewMetadata metadata = null;
		if(dealInventoryRepository.canDownloadInventory(inventoryId, user.getId())) {
			DealInventoryNameAndLocation nameAndLocation = dealInventoryRepository.getDealInventoryNameAndLocation(inventoryId);
			
			
			if (canPreviewInventory(nameAndLocation.getLocation())) {
				boolean isSoundFile = supportedSoundFormats.contains(fileUtilityActions.getFileExtension(nameAndLocation.getLocation()));
				String contentUrl = birefyApplicationUtils.getApplicationDomain() + "/deals/stream-inventory?id=" + inventoryId;
				
				metadata = new InventoryPreviewMetadata(isSoundFile, nameAndLocation.getName(), inventoryId, contentUrl);
			}
		}
		
		return metadata;
	}
	
	
	private StreamingResponseBody streamInventory(Long inventoryId, int byteOffset) throws FileNotFoundException {
		DealInventoryNameAndLocation nameAndLocation = dealInventoryRepository.getDealInventoryNameAndLocation(inventoryId);
		String inventoryFileLocation = nameAndLocation.getLocation();
		StreamingResponseBody response = fileUtilityActions.streamInventoryFileFromSystem(inventoryFileLocation, byteOffset);
		
		return response;
	}
	
	
	private List<DealDTO> mapDealsToDealDTO(List<Deal> deals) {
		Set<Long> memberIds = new HashSet<>();
		
		memberIds.addAll(deals.stream().map(Deal::getCustomerId).collect(Collectors.toList()));
		memberIds.addAll(deals.stream().map(Deal::getSupplierId).collect(Collectors.toList()));
		
		List<TidingAndMemberID> tidingIds = tidingMessageRepository.getAllActiveMemberAndTidingIds(memberIds);
		Map<Long, String> memberIdToTidingMap = tidingIds.stream().collect(
				Collectors.toMap(TidingAndMemberID::getMemberId, TidingAndMemberID::getTidingId));
		
		
		List<DealDTO> mappedDeals = deals.stream()
				.map(d -> new DealDTO(
						d.getId(),
						new TidingAndMemberID(d.getCustomerId(), memberIdToTidingMap.get(d.getCustomerId())),
						new TidingAndMemberID(d.getSupplierId(), memberIdToTidingMap.get(d.getSupplierId())),
						d.isSupplierConfirm(),
						d.isCustomerConfirm(),
						d.getSupplierDetails(),
						d.getCustomerDetails(),
						d.getCreationDate().toString(),
						d.getActive(),
						d.getPaid(),
						d.getPrice()
						)).collect(Collectors.toList());
		
		return mappedDeals;
	}
	
	private boolean canPreviewInventory(String filename) {
		String fileExtension = fileUtilityActions.getFileExtension(filename);
		return supportedImageFormats.contains(fileExtension) || supportedSoundFormats.contains(fileExtension);
	}
}
