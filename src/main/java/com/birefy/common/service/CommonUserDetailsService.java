package com.birefy.common.service;

import java.util.Arrays;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.birefy.common.dto.CommonUser;
import com.birefy.common.entity.Member;
import com.birefy.common.repo.MemberRepository;
import com.birefy.common.security.Authority;

@Service("userDetailsService")
@Transactional
public class CommonUserDetailsService implements UserDetailsService {
	@Autowired
	private MemberRepository memberRepository;
	
	PasswordEncoder passwordEncoder =
				PasswordEncoderFactories.createDelegatingPasswordEncoder();

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Member> memberFound = memberRepository.identifyMemberByEmail(username);
		if (memberFound.isEmpty()) {
			throw new UsernameNotFoundException("User unknown");
		}
		
		Member member = memberFound.get();
		CommonUser details = new CommonUser();
		details.setId(member.getId());
		details.setActive(member.isActive());
		details.setFirstName(member.getFirstName());
		details.setLastName(member.getFamilyName());
		details.setPassword(passwordEncoder.encode(member.getKey()));
		details.setUserName(member.getEmail());
		details.setIsEnabled(member.isEnabled());
		
		switch (member.getAuthority()) {
		case 0: {
			details.setAuthorities(Arrays.asList(Authority.CITIZEN));
			break;
		}
		case 1: {
			details.setAuthorities(Arrays.asList(Authority.LORD));
			break;
		}
		case 2: {
			details.setAuthorities(Arrays.asList(Authority.ROYAL));
			break;
		}
		}
		
		return details;
	}
	
	public Member getUserDetailsById(Long memberId) {
		return memberRepository.selectFullMemberDetailsById(memberId);
	}
}
