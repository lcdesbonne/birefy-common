package com.birefy.common.service;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.painting.PaintingNameAndLocation;
import com.birefy.common.repo.ImagePurchasesRepository;
import com.birefy.common.repo.PaintingRepository;

@Service
public class PaintingDownloadService {
	@Autowired
	private ImagePurchasesRepository imagePurchasesRepository;
	@Autowired
	private FileUtilityActions fileUtilityActions;
	@Autowired
	private PaintingRepository paintingRepository;
	
	public boolean userCanDownloadFile(Long paintingId) {
//		CommonUser user = (CommonUser)SecurityContextHolder.getContext()
//				.getAuthentication().getPrincipal();
//		
//		boolean hasPurchased = imagePurchasesRepository.userHasPurchasedImage(user.getId(), paintingId);
		
		//Any user now has the permission to download paintings
		
		return true;
	}
	
	public boolean downloadFile(Long paintingId, HttpServletResponse response) {
		boolean success = userCanDownloadFile(paintingId);
		if(success) {
			PaintingNameAndLocation paintingDetails = paintingRepository.getPaintingNameAndLocation(paintingId);
			String paintingName = paintingDetails.getName() + fileUtilityActions.getFileExtension(paintingDetails.getLocation());
			response.addHeader("Content-Disposition", "attachment; filename=" + paintingName);
			String url = paintingRepository.getPaintingLocation(paintingId);
			success = fileUtilityActions.downloadImageFile(url, response);
		}
		
		return success;
	}
}
