package com.birefy.common.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.tiding.Conversation;
import com.birefy.common.dto.tiding.NewGroupResponse;
import com.birefy.common.dto.tiding.TidingIdDTO;
import com.birefy.common.dto.tiding.TidingIsGroup;
import com.birefy.common.entity.tiding.BlockedList;
import com.birefy.common.entity.tiding.MemberChannel;
import com.birefy.common.entity.tiding.Message;
import com.birefy.common.repo.MemberBulletinRepository;
import com.birefy.common.repo.TidingMessageRepository;
import com.birefy.common.tidings.Tiding;
import com.birefy.common.tidings.Tiding.MessageType;

@Service
public class TidingsService {
	private final Logger LOGGER = LoggerFactory.getLogger(TidingsService.class);
	@Autowired
	private TidingMessageRepository tidingMessageRepository;
	@Autowired
	private MemberBulletinRepository memberBulletinRepository;
	
	public boolean tidingIsGroup(String tidingId) {
		return tidingMessageRepository.isGroup(tidingId);
	}
	
	public TidingIdDTO currentUserTidingId() {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Long memberId = user.getId();
		
		Optional<MemberChannel> tidingDetails = Optional.ofNullable(channelDetailsForMember(memberId));
		
		String tidingId = tidingDetails.orElse(new MemberChannel()).getTidingId();
		Long channelId = tidingDetails.orElse(new MemberChannel()).getId();
		
		return new TidingIdDTO(tidingId, channelId);
	}
	
	public MemberChannel channelDetailsForMember(Long memberId) {
		List<MemberChannel> details = tidingMessageRepository.findChannelForMembers(Arrays.asList(memberId));
		
		MemberChannel userTidingDetails; 
		
		if(!details.isEmpty()) {
			userTidingDetails = details.stream().findFirst().get();
		} else {
			userTidingDetails = null;
		}
		
		return userTidingDetails;
	}
	
	public List<MemberChannel> channelDetailsForMembers(List<Long> memberIds) {
		List<MemberChannel> details = tidingMessageRepository.findChannelForMembers(memberIds);
		return details;
	}
	
	public List<Conversation> recentConversationsForUser() {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Long memberId = user.getId();
		
		MemberChannel tidingDetails = channelDetailsForMember(memberId);
		
		if (tidingDetails == null) {
			return new ArrayList<>();
		}
		
		Long tidingId = tidingDetails.getId();
		
		Set<Message> messages = tidingMessageRepository.retrieveMessagesForUser(tidingId, LocalDateTime.now().minusDays(15), 
				LocalDateTime.now());
		
		Set<Long> channelIds = messages.stream().map(msg -> msg.getCreator()).collect(Collectors.toSet());
		channelIds.addAll(messages.stream().map(msg -> msg.getRecipient()).collect(Collectors.toSet()));
		
		List<TidingIdDTO> tidDTOs = channelIds.isEmpty() ? new ArrayList<>() : tidingMessageRepository.findTidingIdsByChannelId(channelIds.stream().collect(Collectors.toList()));
		
		Map<Long, String> channelIdToTidingIdMap = tidDTOs.stream().collect(Collectors.toMap(TidingIdDTO::getChannelId, TidingIdDTO::getTidingId));
		
		//Create a list of all one to one conversations
		Set<Long> correspondanceIds = messages.stream().filter(mes -> !mes.getCreator().equals(tidingId))
				.map(m -> m.getCreator()).collect(Collectors.toSet());
		
		correspondanceIds.addAll(messages.stream().filter(mes -> !mes.getRecipient().equals(tidingId))
				.map(m -> m.getRecipient()).collect(Collectors.toSet()));
		
		List<Conversation> conversations = new ArrayList<>();
		
		if(!correspondanceIds.isEmpty()) {
			//Identify all group messages first
			List<TidingIsGroup> isTidingGroup = tidingMessageRepository.isGroup(correspondanceIds);
			
			List<Long> groupTidings = isTidingGroup.stream().filter(itg -> itg.isGroup()).map(itideGr -> itideGr.getTidingId())
					.collect(Collectors.toList());
			
			for (Long groupTiding : groupTidings) {
				//Retrieve all the conversations for the group
				Set<Message> groupMessages = tidingMessageRepository.retrieveMessagesForUser(groupTiding, LocalDateTime.now().minusDays(15), 
						LocalDateTime.now());
				
				//Remove all group messages from the user message list
				messages.removeAll(groupMessages);
				
				//Convert messages to Tidings
				List<Tiding> tidings = groupMessages.stream().map(mes -> new Tiding(MessageType.CHAT, 
						mes.getContent(), new TidingIdDTO(channelIdToTidingIdMap.get(mes.getCreator()), mes.getCreator()))).collect(Collectors.toList());
				
				conversations.add(new Conversation(new TidingIdDTO(channelIdToTidingIdMap.get(groupTiding), groupTiding), tidings, true));
			}
			
			//Handle all non group correspondence
			List<Long> standardTidings = isTidingGroup.stream().filter(itg -> !itg.isGroup()).map(itideGr -> itideGr.getTidingId())
					.collect(Collectors.toList());
			
			for (Long standardTidingId : standardTidings) {
				List<Message> conversationMessages = messages.stream().filter(msg -> msg.getCreator().equals(standardTidingId) || msg.getRecipient().equals(standardTidingId))
						.collect(Collectors.toList());
				
				List<Tiding> tidings = conversationMessages.stream().map(mes -> new Tiding(MessageType.CHAT,
						mes.getContent(), new TidingIdDTO(channelIdToTidingIdMap.get(mes.getCreator()), mes.getCreator()))).collect(Collectors.toList());
				
				conversations.add(new Conversation(new TidingIdDTO(channelIdToTidingIdMap.get(standardTidingId), standardTidingId), tidings));
			}	
		}
		
		return conversations;
	}
	
	public Conversation openConversation(Long userTidingId, Long receiverTidingId) {
		List<Tiding> tidings = tidingMessageRepository.findConversation(userTidingId, receiverTidingId);
		List<TidingIdDTO> tidingIds = tidingMessageRepository.findTidingIdsByChannelId(Arrays.asList(receiverTidingId));
		return new Conversation(tidingIds.get(0), tidings);
	}
	
	public List<TidingIdDTO> contactChannelsForBulletin(Long bulletinId) {
		List<Long> memberIds = memberBulletinRepository.creatorsOfBulletin(bulletinId);
		
		List<MemberChannel> channelDetails = tidingMessageRepository.findChannelForMembers(memberIds);
		
		List<TidingIdDTO> tidingIds = channelDetails.stream().map(mc -> 
			 new TidingIdDTO(mc.getTidingId(), mc.getId())).collect(Collectors.toList());
		
		return tidingIds;
	}
	
	public List<TidingIdDTO> searchTidingUsersByTidingId(String tidingId) {
		CommonUser user  = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		//Retrieve the users tidingId
		MemberChannel channelDetailsForMember = channelDetailsForMember(user.getId());
		
		List<TidingIdDTO> tidingIds = tidingMessageRepository.findTidingIds(tidingId, channelDetailsForMember.getId());
		
		return tidingIds;
	}
	
	public void saveMessage(Message message) {
		message.setCreationDate(LocalDateTime.now());
		tidingMessageRepository.addNewMessage(message);
	}
	
	public NewGroupResponse createGroup(String groupTidingId) {
		//Add the group to member channel
		MemberChannel groupChannel = new MemberChannel();
		groupChannel.setActive(true);
		groupChannel.setTidingId(groupTidingId);
		
		CommonUser user = (CommonUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		MemberChannel channelDetails = tidingMessageRepository.findChannelForMembers(Arrays.asList(user.getId())).stream().findFirst().get();
		
		NewGroupResponse response;
		try {
			tidingMessageRepository.addGroupToTidings(groupChannel);
			TidingIdDTO tidingId = tidingMessageRepository.findTidingId(groupTidingId, channelDetails.getId());
			response = new NewGroupResponse(true, tidingId);
		} catch (Exception e) {
			response = new NewGroupResponse();
			response.setSuccess(false);
			LOGGER.error("Unable to create group with name: "+groupTidingId, e);
		}
		
		return response;
	}
	
	public Map<String, Boolean> checkTidingIdExists(List<String> tidingIds) {
		Map<String, Boolean> tidingIdExistMap = tidingMessageRepository.tidingIdsExist(tidingIds);
		return tidingIdExistMap;
	}
	
	public boolean leaveGroup(Long groupTidingId) {
		boolean success = true;
		//Modify the blocked list for this user with the group
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		MemberChannel userTidingDetails = channelDetailsForMember(user.getId());
		BlockedList blockedEntry = new BlockedList(userTidingDetails.getId(), groupTidingId);
		
		try {
			tidingMessageRepository.addToBlocked(blockedEntry);
		} catch(Exception e) {
			LOGGER.error("Unable to block "+groupTidingId+", for user "+userTidingDetails.getTidingId(), e);
			success = false;
		}
		
		return success;
	}
	
	public String getTidingIdForMember(Long memberId) {
		return tidingMessageRepository.getTidingIdForMember(memberId);
	}

}
