package com.birefy.common.service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.birefy.common.authentication.BirefyPlatformAuthority;
import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.authentication.events.OnPasswordChangeRequestEvent;
import com.birefy.common.dto.member.MemberDTO;
import com.birefy.common.dto.tiding.TidingAndMemberID;
import com.birefy.common.entity.Member;
import com.birefy.common.entity.MemberVerification;
import com.birefy.common.entity.QMemberQuestion;
import com.birefy.common.entity.tiding.MemberChannel;
import com.birefy.common.exception.UserVerificationException;
import com.birefy.common.repo.MemberRepository;
import com.birefy.common.repo.MemberVerificationRepository;
import com.birefy.common.repo.QMemberQuestionRepository;
import com.birefy.common.repo.TidingMessageRepository;
import com.birefy.common.validation.NewTidingIdValidator;

@Service
public class MemberService {
	private static final Logger LOGGER = LoggerFactory.getLogger(MemberService.class);
	@Autowired
	private MemberRepository memberRepository;
	@Autowired
	private TidingMessageRepository tidingMessageRepository;
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	@Autowired
	private NewTidingIdValidator newTidingIdValidator;
	
	public static final String verificationConfirmationPage = "verifyconfirm";
	public static final String loginSuccessPage = "loginSuccess";
	public static final String signUpPage = "signup";
	
	/**
	 * Retrieves a list of members based on requested page number,
	 * page numbering begins at 0 for the first page.
	 * @param pageNumber
	 * @return
	 */
	@Transactional(readOnly = true)
	public List<Member> listMembers() {
		return memberRepository.selectAllMembers();
	}
	
	@Transactional
	public Member addNewMember(MemberDTO newMember) {		
		Member member = new Member();
		member.setFirstName(newMember.getFirstName());
		member.setFamilyName(newMember.getLastName());
		member.setEmail(newMember.getEmail());
		//By default all new members are citizens until they
		//contribute to a project
		member.setAuthority(BirefyPlatformAuthority.CITIZEN.value());
		//member.setKey(newMember.getPassword());
		member.setActive(true);
		member.setEnabled(true);
		
		Member savedMember = null;
		
		//save the new member to the database
		try {
			memberRepository.addNewRecord(member);
			
			//Add the tiding Id information to the database
			Optional<Member> result = memberRepository.identifyMemberByEmail(member.getEmail());
			if (result.isEmpty()) {
				LOGGER.error("Issue creating new member with email:" + member.getEmail());
				throw new RuntimeException("Issue creating new member with email:" + member.getEmail());
			}
			savedMember = result.get();
			long newMemberId = savedMember.getId();
			
			MemberChannel memberChannel = new MemberChannel();
			memberChannel.setMemberId(newMemberId);
			memberChannel.setTidingId(generateTidingIdForMember(savedMember));
			memberChannel.setActive(true);
			
			tidingMessageRepository.addMemberToTidings(memberChannel);
			
		} catch(Exception e) {
			LOGGER.error("Issue creating new member", e);
		}
		
		return savedMember;
	}
	
	public Optional<Member> getMemberByEmail(String email) {
		return memberRepository.identifyMemberByEmail(email);
	}
	
	@Transactional
	public boolean applyDetailUpdates(MemberDTO memberDetails) {
		boolean result = false;
		Member memberUpdate = new Member();
		
		//Retrieve the id from the security context
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if(Optional.ofNullable(memberDetails.getContactNumber()).isEmpty()) {
			memberUpdate.setPhone(StringUtils.EMPTY);
		}
		
		memberUpdate.setId(user.getId());
		memberUpdate.setEnabled(true);
		try {
			memberRepository.update(memberUpdate);
			LOGGER.trace("Updated member with id: " + user.getId());
			result = true;
		} catch (IllegalAccessException | NoSuchFieldException e) {
			LOGGER.error("Unable to update member with id: " + user.getId(), e);
		}
		
		
		if(Optional.ofNullable(memberDetails.getTidingId()).isPresent()
				&& !memberDetails.getTidingId().isEmpty()) {
			//Update the tiding id
			tidingMessageRepository.updateTidingIdForMember(memberDetails.getTidingId(), user.getId());
			result = true;
		}
		
		return result;
	}
	
	
	public String postProcessSignInAndRedirect(String memberEmail) {
		Member memberDetails = memberRepository.findMemberByEmail(memberEmail);
		
		if (memberDetails == null) {
			return signUpPage;
		} else {
			return loginSuccessPage;
		}
	}
	
	
	private boolean isTokenExpired(MemberVerification memberVerification) {
		LocalDate today = LocalDate.now();
		boolean isExpired = memberVerification.getCreationDate().isAfter(today);
		
		if (isExpired) {
			CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			//Need to issue a new token and allow the user to verify again
			eventPublisher.publishEvent(new OnPasswordChangeRequestEvent(user));
		}
		
		return isExpired;
	}
	
	private String generateTidingIdForMember(Member newMember) {
		long numericalPart = newMember.getId();
		String proposedTidingId = StringUtils.lowerCase(newMember.getFirstName() + "." + newMember.getFamilyName() + numericalPart);
		
		while(!newTidingIdValidator.isValid(proposedTidingId, null)) {
			++numericalPart;
			proposedTidingId = StringUtils.lowerCase(newMember.getFirstName() + "." + newMember.getFamilyName() + numericalPart);
		}
		
		return proposedTidingId;
	}
}
