package com.birefy.common.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.painting.PaintingShort;
import com.birefy.common.dto.painting.PaintingShortWithTidingId;
import com.birefy.common.dto.painting.PaintingTagDTO;
import com.birefy.common.dto.painting.PaintingUpdate;
import com.birefy.common.dto.painting.PaintingUpload;
import com.birefy.common.dto.sound.SoundTagDTO;
import com.birefy.common.dto.tiding.TidingIdDTO;
import com.birefy.common.entity.Painting;
import com.birefy.common.entity.PaintingDescriptiveTag;
import com.birefy.common.entity.PaintingTag;
import com.birefy.common.entity.SoundTag;
import com.birefy.common.entity.tiding.MemberChannel;
import com.birefy.common.repo.PaintingDescriptiveTagRepository;
import com.birefy.common.repo.PaintingRepository;
import com.birefy.common.repo.PaintingTagRepository;
import com.birefy.common.repo.TidingMessageRepository;

@Service
public class PaintingService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PaintingService.class);
	
	@Autowired
	private PaintingRepository paintingRepository;
	@Autowired
	private FileUtilityActions fileUtilityActions;
	@Autowired
	private TidingMessageRepository tidingMessageRepository;
	@Autowired
	private PaintingDescriptiveTagRepository paintingDescriptiveTagRepository;
	@Autowired
	private PaintingTagRepository paintingTagRepository;
	
	
	public boolean savePaintingFile(PaintingUpload paintingUpload) {
		boolean success = false;
		String fileLocation;
		try {
			MultipartFile paintingFile = paintingUpload.getPaintingFile();
			fileLocation = fileUtilityActions.writePaintingFileToSystem(paintingFile);
		} catch (IOException io) {
			LOGGER.error("Unable to upload file", io);
			return success;
		}
		
		success = saveFileToDatabase(fileLocation, paintingUpload.getTitle(), paintingUpload.getCategory(), paintingUpload.getPrice(), paintingUpload.getSoftware());
		
		List<PaintingTagDTO> tags;
		try {
			tags = paintingUpload.getTags();
		} catch (IOException e) {
			tags = new ArrayList<>();
		}
		
		if(success && !CollectionUtils.isEmpty(tags)) {
			List<PaintingTagDTO> tagsToAdd = saveUnknownTags(tags);
			
			int indexOfLastSlash = fileLocation.lastIndexOf('\\') + 1;
			CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			
			Long paintingId = paintingRepository.findPaintingByLocationAndMember(fileLocation.substring(indexOfLastSlash), user.getId());
			
			List<PaintingDescriptiveTag> paintingTagLinks = tagsToAdd.stream()
					.map(t -> new PaintingDescriptiveTag(paintingId, t.getId())).collect(Collectors.toList());
			
			for(PaintingDescriptiveTag tagLink : paintingTagLinks) {
				try {
					paintingDescriptiveTagRepository.addNewRecord(tagLink);
				} catch (NoSuchFieldException | IllegalAccessException e) {
					LOGGER.error("Error saving tags for the new sound upload.", e);
					success = false;
				}
			}
		}
		return success;
	}
	
	public StreamingResponseBody streamPaintingFile(Long paintingId, int byteOffset) throws FileNotFoundException {
		PaintingShort painting = paintingRepository.getPaintingById(paintingId);
		String paintingUrl = painting.getLocation();
		StreamingResponseBody response = fileUtilityActions.streamPaintingFileFromSystem(paintingUrl, byteOffset);
		return response;
	}
	
	public boolean deletePainting(Long paintingId) {
		boolean success = false;
		PaintingShort painting = paintingRepository.getPaintingById(paintingId);
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(!painting.getMemberId().equals(user.getId())) {
			LOGGER.error("User "+user.getId()+ " does not have permission to delete painting "+painting.getId());
			return success;
		}
		
		//Delete the file first
		success = fileUtilityActions.deletePaintingFileFromSystem(painting.getLocation());
		if(!success) {
			LOGGER.error("Unable to delete file at "+painting.getLocation());
			return success;
		}
		
		success = paintingRepository.deletePainting(paintingId);
		return success;
	}
	
	public PaintingShortWithTidingId getPaintingById(Long paintingId) {
		PaintingShort painting = paintingRepository.getPaintingById(paintingId);
		List<PaintingShortWithTidingId> mappedPaintings = convertPaintingShortToPaintingWithTidingId(Arrays.asList(painting));
		return mappedPaintings.get(0);
	}
	
	public List<PaintingShortWithTidingId> allActivePaintings() {
		List<PaintingShort> paintings = paintingRepository.findAllPaintings();
		return convertPaintingShortToPaintingWithTidingId(paintings);
	}
	
	public List<PaintingShortWithTidingId> allPaintingsForReview() {
		List<PaintingShort> paintings = paintingRepository.findAllPaintingsForReview();
		return convertPaintingShortToPaintingWithTidingId(paintings);
	}
	
	public List<PaintingShortWithTidingId> allActivePaintingsWithTags(List<Long> tagIds) {
		List<PaintingShort> paintings = paintingRepository.findAllPaintingsWithTags(tagIds);
		return convertPaintingShortToPaintingWithTidingId(paintings);
	}
	
	public List<PaintingShortWithTidingId> allPaintingsByCategory(Painting.Category category) {
		List<PaintingShort> paintings = paintingRepository.recentPaintingsByCategory(category);
		return convertPaintingShortToPaintingWithTidingId(paintings);
	}
	
	public List<PaintingShortWithTidingId> allPaintingsByCategoryTags(Painting.Category category, List<Long> tagIds) {
		List<PaintingShort> paintings = paintingRepository.recentPaintingsByCategoryTags(category, tagIds);
		return convertPaintingShortToPaintingWithTidingId(paintings);
	}
	
	public List<PaintingShortWithTidingId> searchPaintingByTitleAndCategory(String title, Painting.Category category) {
		List<PaintingShort> paintings = paintingRepository.findPaintingByNameAndCategory(title, category);
		return convertPaintingShortToPaintingWithTidingId(paintings);
	}
	
	public List<PaintingShortWithTidingId> searchPaintingByTitleAndCategoryTags(String title, Painting.Category category, List<Long> tagIds) {
		List<PaintingShort> paintings = paintingRepository.findPaintingByNameAndCategoryTags(title, category, tagIds);
		return convertPaintingShortToPaintingWithTidingId(paintings);
	}
	
	public List<PaintingShortWithTidingId> searchPaintingsByTitleCategoryAndArtist(String title, Painting.Category category, Long memberId) {		
		List<PaintingShort> paintings = paintingRepository.paintingByNameMemberAndCategory(memberId, title, category);
		return convertPaintingShortToPaintingWithTidingId(paintings);
	}
	
	public List<PaintingShortWithTidingId> searchPaintingsByTitleCategoryAndArtistTags(String title, Painting.Category category, Long memberId, List<Long> tagIds) {
		List<PaintingShort> paintings = paintingRepository.paintingByNameMemberAndCategoryTag(memberId, title, category, tagIds);
		return convertPaintingShortToPaintingWithTidingId(paintings);
	}
	
	public List<PaintingShortWithTidingId> allPaintingsByPainterAndCategory(Long memberId, Painting.Category category) {
		List<PaintingShort> paintings = paintingRepository.paintingsByMemberAndCategory(memberId, category);
		return convertPaintingShortToPaintingWithTidingId(paintings);
	}
	
	public List<PaintingShortWithTidingId> allPaintingsByPainterAndCategoryTags(Long memberId, Painting.Category category, List<Long> tagIds) {
		List<PaintingShort> paintings = paintingRepository.paintingsByMemberAndCategoryTags(memberId, category, tagIds);
		return convertPaintingShortToPaintingWithTidingId(paintings);
	}
	
	public List<PaintingShortWithTidingId> paintingsByArtist(Long memberId) {
		List<PaintingShort> paintings = paintingRepository.paintingsByMember(memberId);
		return convertPaintingShortToPaintingWithTidingId(paintings);
	}
	
	public List<PaintingShortWithTidingId> paintingsByArtistTags(Long memberId, List<Long> tagIds) {
		List<PaintingShort> paintings = paintingRepository.paintingsByMemberTags(memberId, tagIds);
		return convertPaintingShortToPaintingWithTidingId(paintings);
	}
	
	public List<PaintingShortWithTidingId> paintingsByTitle(String title) {
		List<PaintingShort> paintings = paintingRepository.findPaintingByName(title);
		return convertPaintingShortToPaintingWithTidingId(paintings);
	}
	
	public List<PaintingShortWithTidingId> paintingsByTitleTags(String title, List<Long> tagIds) {
		List<PaintingShort> paintings = paintingRepository.findPaintingByNameTags(title, tagIds);
		return convertPaintingShortToPaintingWithTidingId(paintings);
	}
	
	public List<PaintingShortWithTidingId> paintingsByArtistWithTitle(Long memberId, String title) {
		List<PaintingShort> paintings = paintingRepository.findPaintingByArtistAndTitle(memberId, title);
		return convertPaintingShortToPaintingWithTidingId(paintings);
	}
	
	public List<PaintingShortWithTidingId> paintingsByArtistWithTitleTags(Long memberId, String title, List<Long> tagIds) {
		List<PaintingShort> paintings = paintingRepository.findPaintingByArtistAndTitleTags(memberId, title, tagIds);
		return convertPaintingShortToPaintingWithTidingId(paintings);
	}
	
	public boolean approvePainting(Long imageId) {
		return paintingRepository.approvePainting(imageId);
	}
	

	@SuppressWarnings("finally")
	public List<MemberChannel> searchPainters(String tidingId) {
		List<TidingIdDTO> tidingMembers = new ArrayList<>();
		
		try {
			CommonUser userDetails = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			List<MemberChannel> channelDetails = tidingMessageRepository.findChannelForMembers(Arrays.asList(userDetails.getId()));
			tidingMembers = tidingMessageRepository.findTidingIds(tidingId, channelDetails.get(0).getId());
		} catch (Exception e) {
			LOGGER.debug("Unable to obtain users log in credentials, assuming user is not logged in.", e);
			
			tidingMembers = tidingMessageRepository.findTidingIdsLikeTiding(tidingId);
		} finally {
			List<MemberChannel> tidingMemberDetails = tidingMessageRepository.findChannelsByChannelIds(
					tidingMembers.stream().map(ch -> ch.getChannelId()).collect(Collectors.toList())
					);
			List<Long> memberIds = tidingMemberDetails.stream().map(MemberChannel::getMemberId).collect(Collectors.toList());
			
			List<Long> paintingMembers = paintingRepository.findPainters(memberIds);
			
			return tidingMemberDetails.stream().filter(tm -> paintingMembers.contains(tm.getMemberId())).collect(Collectors.toList());
		}
	}
	
	public List<PaintingTagDTO> findTagsForPainting(String tagName) {
		return paintingTagRepository.searchForTags(tagName);
	}
	
	public Map<Long, List<PaintingTagDTO>> tagsForPaintings(List<Long> paintingIds) {
		return paintingDescriptiveTagRepository.findTagsForPaintings(paintingIds);
	}
	
	public boolean updatePainting(PaintingUpdate paintingUpdate) {
		boolean success = false;
		PaintingShort existingPainting = paintingRepository.getPaintingById(paintingUpdate.getPaintingId());
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if(!existingPainting.getMemberId().equals(user.getId())) {
			return success;
		}
		
		if(Optional.ofNullable(paintingUpdate.getNewCategory()).isPresent() &&
				Optional.ofNullable(paintingUpdate.getNewPaintingTitle()).isPresent()
				&& !Optional.ofNullable(paintingUpdate.getPrice()).isPresent()) {
			success = paintingRepository.updatePaintingNameAndCategory(paintingUpdate.getPaintingId(), 
					paintingUpdate.getNewPaintingTitle(), paintingUpdate.getNewCategory());
			
		} else if (Optional.ofNullable(paintingUpdate.getNewCategory()).isPresent() && 
				!Optional.ofNullable(paintingUpdate.getNewPaintingTitle()).isPresent()
				&& !Optional.ofNullable(paintingUpdate.getPrice()).isPresent()) {
			success = paintingRepository.updatePaintingCategory(paintingUpdate.getPaintingId(), paintingUpdate.getNewCategory());
			
		} else if (!Optional.ofNullable(paintingUpdate.getNewCategory()).isPresent() &&
				Optional.ofNullable(paintingUpdate.getNewPaintingTitle()).isPresent()
				&& !Optional.ofNullable(paintingUpdate.getPrice()).isPresent()) {
			success = paintingRepository.updatePaintingName(paintingUpdate.getPaintingId(), paintingUpdate.getNewPaintingTitle());
			
		} else if(Optional.ofNullable(paintingUpdate.getNewCategory()).isPresent() &&
				Optional.ofNullable(paintingUpdate.getNewPaintingTitle()).isPresent()
				&& Optional.ofNullable(paintingUpdate.getPrice()).isPresent()) {
			success = paintingRepository.updatePaintingNameAndCategoryAndPrice(paintingUpdate.getPaintingId(), 
					paintingUpdate.getNewPaintingTitle(), paintingUpdate.getNewCategory(), paintingUpdate.getPrice());
			
		} else if (Optional.ofNullable(paintingUpdate.getNewCategory()).isPresent() && 
				!Optional.ofNullable(paintingUpdate.getNewPaintingTitle()).isPresent()
				&& Optional.ofNullable(paintingUpdate.getPrice()).isPresent()) {
			success = paintingRepository.updatePaintingCategoryAndPrice(paintingUpdate.getPaintingId(), paintingUpdate.getNewCategory(),
					paintingUpdate.getPrice());
			
		} else if (!Optional.ofNullable(paintingUpdate.getNewCategory()).isPresent() &&
				Optional.ofNullable(paintingUpdate.getNewPaintingTitle()).isPresent()
				&& Optional.ofNullable(paintingUpdate.getPrice()).isPresent()) {
			success = paintingRepository.updatePaintingNameAndPrice(paintingUpdate.getPaintingId(), paintingUpdate.getNewPaintingTitle()
					, paintingUpdate.getPrice());
		}
		
		if(!CollectionUtils.isEmpty(paintingUpdate.getRemovedTags())) {
			List<Long> removedTagId = paintingUpdate.getRemovedTags().stream().map(PaintingTagDTO::getId).collect(Collectors.toList());
			try {
				paintingDescriptiveTagRepository.deleteTagsFromPainting(paintingUpdate.getPaintingId(), removedTagId);
			} catch(Exception e) {
				LOGGER.error("Failed to add tags to painting with ID: "+paintingUpdate.getPaintingId(), e);
				success = false;
			}
		}
		
		if(!CollectionUtils.isEmpty(paintingUpdate.getAddedTags())) {
			List<PaintingTagDTO> addedTags = saveUnknownTags(paintingUpdate.getAddedTags());
			List<PaintingDescriptiveTag> tagLinks = addedTags.stream()
					.map(adTag -> new PaintingDescriptiveTag(paintingUpdate.getPaintingId(),adTag.getId())).collect(Collectors.toList());
			
			for(PaintingDescriptiveTag tag : tagLinks) {
				try {
					paintingDescriptiveTagRepository.addNewRecord(tag);
				} catch (NoSuchFieldException | IllegalAccessException e) {
					LOGGER.error("Failed to add tags to painting with ID: "+paintingUpdate.getPaintingId(), e);
					success = false;
				}
			};
		}
		
		return success;
	}
	
	private boolean saveFileToDatabase(String location, String fileName, Painting.Category category, Float price, String software) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		boolean success = false;
		try {
			Painting painting = new Painting();
			painting.setMemberId(user.getId());
			painting.setLocation(location);
			painting.setName(fileName);
			painting.setCategory(category);
			painting.setPrice(price);
			painting.setSoftware(software);
			painting.setReview(true);
			
			paintingRepository.addNewRecord(painting);
			
			success = true;
		} catch (Exception e) {
			LOGGER.error("Failed to save painting details to database", e);
		}
		return success;
	}
	
	private List<PaintingShortWithTidingId> convertPaintingShortToPaintingWithTidingId(List<PaintingShort> paintings) {
		List<Long> memberIds = new ArrayList<>();
		memberIds.addAll(paintings.stream().map(PaintingShort::getMemberId).collect(Collectors.toList()));
		List<MemberChannel> channelDetails;
		if(!memberIds.isEmpty()) {
			channelDetails = tidingMessageRepository.findChannelForMembers(memberIds);
		} else {
			channelDetails = new ArrayList<>();
		}
		List<PaintingShortWithTidingId> paintingsWithTidingId = new ArrayList<>();
		for(PaintingShort p : paintings) {
			PaintingShortWithTidingId pwtid = new PaintingShortWithTidingId(p);
			MemberChannel targetChannel = channelDetails.stream().filter(cd -> p.getMemberId().equals(cd.getMemberId())).findAny().get();
			String tidingId = targetChannel.getTidingId();
			Long channelId = targetChannel.getId();
			pwtid.setTidingId(tidingId);
			pwtid.setChannelId(channelId);
			paintingsWithTidingId.add(pwtid);
		}
		return paintingsWithTidingId;
	}
	
	private List<PaintingTagDTO> saveUnknownTags(List<PaintingTagDTO> tagList) {
		List<PaintingTagDTO> unknownTags = tagList.stream().filter(t -> !Optional.ofNullable(t.getId())
				.isPresent()).collect(Collectors.toList());
		if(CollectionUtils.isEmpty(unknownTags)) {
			return tagList;
		} else {
			List<PaintingTagDTO> knownTags = tagList.stream().filter(t -> Optional.ofNullable(t.getId()).isPresent())
					.collect(Collectors.toList());
			
			unknownTags.forEach(tag -> {
				if(!paintingTagRepository.tagExists(tag.getTag())) {
					PaintingTag newTag = new PaintingTag();
					newTag.setTag(tag.getTag());
					try {
						paintingTagRepository.addNewRecord(newTag);
					} catch (Exception e) {
						LOGGER.error("Failed to create new tag '" + tag.getTag(), e);
					}
				}
				PaintingTagDTO tagToAdd = paintingTagRepository.getTagByName(tag.getTag());
				if(Optional.ofNullable(tagToAdd).isPresent()) {
					knownTags.add(tagToAdd);
				}
			});
			
			return knownTags;
		}
	}
}
