package com.birefy.common.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.birefy.common.constants.QuestionState;
import com.birefy.common.dto.question.QQuestionDTO;
import com.birefy.common.dto.question.QQuestionShort;
import com.birefy.common.dto.question.QQuestionShortExtended;
import com.birefy.common.dto.question.QuestionTagUpdate;
import com.birefy.common.dto.question.QuestionTechTags;
import com.birefy.common.dto.question.QuestionUrlUpdate;
import com.birefy.common.dto.question.TechTagAndQuestion;
import com.birefy.common.dto.topic.QTopicAndQQuestionsFull;
import com.birefy.common.dto.topic.QTopicDTO;
import com.birefy.common.dto.topic.QTopicShort;
import com.birefy.common.dto.topic.QTopicsAndQQuestions;
import com.birefy.common.dto.topic.QTopicsAndQQuestionsExtended;
import com.birefy.common.dto.topic.TopicAndTags;
import com.birefy.common.entity.QQuestion;
import com.birefy.common.entity.QQuestionTechnicalTag;
import com.birefy.common.entity.QTag;
import com.birefy.common.entity.QTopic;
import com.birefy.common.entity.QTopicTag;
import com.birefy.common.entity.TechnicalTag;
import com.birefy.common.entity.utils.mapper.QQuestionToDTOMapper;
import com.birefy.common.entity.utils.mapper.QQuestionToExtendedMapper;
import com.birefy.common.entity.utils.mapper.QQuestionToShortMapper;
import com.birefy.common.entity.utils.mapper.QTopicToDTOMapper;
import com.birefy.common.entity.utils.mapper.QTopicToShortMapper;
import com.birefy.common.repo.QQuestionRepository;
import com.birefy.common.repo.QQuestionTechnicalTagRepository;
import com.birefy.common.repo.QTagRepository;
import com.birefy.common.repo.QTopicRepository;
import com.birefy.common.repo.QTopicTagRepository;
import com.birefy.common.repo.TechnicalTagRepository;

@Service
public class AdministrationService {
	private static final Logger LOGGER = LoggerFactory.getLogger(AdministrationService.class);
	private static String ADD_NEW_RECORD_ERROR = "Unable to add new record";

	@Autowired
	private QTagRepository tagRepository;
	@Autowired
	private QTopicTagRepository topicTagRepository;
	@Autowired
	private QTopicRepository topicRepository;
	@Autowired
	private QQuestionRepository questionRepository;
	@Autowired
	private TechnicalTagRepository technicalTagRepository;
	@Autowired
	private QQuestionTechnicalTagRepository questionTechnicalTagRepository;

	public void addTagsToTopic(TopicAndTags newTags) {
		List<Long> tagIds = newTags.getTagIds();
		Long topicId = newTags.getTopicId();

		for (Long tagId : tagIds) {
			QTopicTag link = new QTopicTag(topicId, tagId);
			try {
				topicTagRepository.addNewRecord(link);
			} catch (Exception e) {
				LOGGER.error("Unable to add new tag to topic with ID:" + topicId, e);
			}
			LOGGER.info("Addition of new tags to topic " + topicId + " process complete.");
		}
	}

	public void deleteTagsFromTopic(TopicAndTags deleteTags) {
		List<Long> tagIds = deleteTags.getTagIds();
		Long topicId = deleteTags.getTopicId();
		topicTagRepository.deleteTagsFromTopic(tagIds, topicId);
	}

	public void alterActiveStateTopic(Long topicId, boolean active) {
		topicRepository.modifyActiveState(topicId, active);
		if (!active) {
			questionRepository.deactivateQuestionsForTopic(topicId);
		}
	}

	public void alterActiveStateQuestion(Long questionId, boolean active) {
		// Questions cannot be active if the topic is not active
		if (active) {
			// Get the topic for the question
			long topicId = questionRepository.topicforQuestion(questionId);
			if (!topicRepository.isActive(topicId)) {
				// TODO throw an application exception here
				throw new IllegalArgumentException("Topic is not active, so question cannot be active.");
			}
			// Remove the review status of the question
			questionRepository.alterReviewState(questionId, true);
		} else {
			questionRepository.alterReviewState(questionId, false);
		}
		questionRepository.alterActiveState(questionId, active);
	}

	public void createNewTag(String tagName) {
		QTag tag = new QTag();
		tag.setTagName(tagName);
		try {
			tagRepository.addNewRecord(tag);
		} catch (Exception e) {
			LOGGER.error("Unable to create tag with name: " + tagName, e);
		}
	}

	public List<QTag> getTagsForTopic(long tagId) {
		return tagRepository.getTagsForTopic(tagId);
	}

	public QTopicAndQQuestionsFull findPendingQuestionsByTitle(String title) {
		List<QQuestion> questions = questionRepository.searchQuestionByTitleAndIsActiveExtended(title, false);

		if (questions.isEmpty()) {
			return new QTopicAndQQuestionsFull();
		}

		// Collect the topicIds
		List<Long> topicIds = questions.stream().map(q -> q.getTopicId()).collect(Collectors.toList());

		// Search for corresponding topics
		List<QTopic> topics = topicRepository.findById(topicIds);

		// Map to shorts
		List<QQuestionDTO> shortQs = questions.stream().map(q -> {
			return QQuestionToDTOMapper.map(q);
		}).collect(Collectors.toList());

		List<QTopicDTO> shortTs = topics.stream().map(t -> {
			return QTopicToDTOMapper.map(t);
		}).collect(Collectors.toList());

		QTopicAndQQuestionsFull topicQuestions = new QTopicAndQQuestionsFull(shortTs, shortQs);
		return topicQuestions;
	}

	public QTopicAndQQuestionsFull findReviewQuestionsByTitle(String title) {
		List<QQuestion> questions = questionRepository.searchQuestionsByTitleWithReviewStatus(title, true);

		if (questions.isEmpty()) {
			return new QTopicAndQQuestionsFull();
		}

		// Collect the topicIds
		List<Long> topicIds = questions.stream().map(q -> q.getTopicId()).collect(Collectors.toList());

		// Search for corresponding topics
		List<QTopic> topics = topicRepository.findById(topicIds);

		// Map to shorts
		List<QQuestionDTO> shortQs = questions.stream().map(q -> {
			return QQuestionToDTOMapper.map(q);
		}).collect(Collectors.toList());

		List<QTopicDTO> shortTs = topics.stream().map(t -> {
			return QTopicToDTOMapper.map(t);
		}).collect(Collectors.toList());

		QTopicAndQQuestionsFull topicQuestions = new QTopicAndQQuestionsFull(shortTs, shortQs);
		return topicQuestions;
	}

	public QTopicAndQQuestionsFull findPendingTopicsByTitle(String title) {
		List<QTopic> topics = topicRepository.findByTitle(false, title);

		if (topics.isEmpty()) {
			return new QTopicAndQQuestionsFull();
		}

		List<Long> topicIds = topics.stream().map(t -> t.getId()).collect(Collectors.toList());

		List<QQuestion> questions = questionRepository.searchQuestionsByTopic(topicIds, false);

		// Map to short versions
		List<QTopicDTO> shortT = topics.stream().map(t -> {
			return QTopicToDTOMapper.map(t);
		}).collect(Collectors.toList());

		List<QQuestionDTO> shortQ = questions.stream().map(q -> {
			return QQuestionToDTOMapper.map(q);
		}).collect(Collectors.toList());

		QTopicAndQQuestionsFull topicsAndQuestions = new QTopicAndQQuestionsFull(shortT, shortQ);
		return topicsAndQuestions;
	}

	@Transactional
	public void updateQuestionTags(List<QuestionTagUpdate> questionTagUpdates) {
		questionTagUpdates.forEach(updateableQuestion -> {
			// Identify the tags to add
			Optional<List<String>> tagsToAdd = Optional.ofNullable(updateableQuestion.getTagNames());
			if(tagsToAdd.isPresent()) {
				tagsToAdd.get().forEach(tag -> {
					if(!StringUtils.isEmpty(tag)) {
						// Check if the tag exists in the db
						Optional<Long> tagFound = Optional.ofNullable(technicalTagRepository.findTagByName(tag));

						if (tagFound.isPresent()) {
							// Add the link between the question and the tag
							QQuestionTechnicalTag qTechTag = new QQuestionTechnicalTag(updateableQuestion.getQuestionId(),
									tagFound.get());
							try {
								questionTechnicalTagRepository.addNewRecord(qTechTag);
							} catch (NoSuchFieldException | IllegalAccessException e) {
								LOGGER.error(ADD_NEW_RECORD_ERROR, e);
							}
						} else {
							// Create a new tag
							TechnicalTag newTag = new TechnicalTag();
							newTag.setName(tag);
							try {
								technicalTagRepository.addNewRecord(newTag);
								// Retrieve the new id
								long tagId = technicalTagRepository.findTagByName(tag);
								// Add the link between the question and the tag
								questionTechnicalTagRepository
										.addNewRecord(new QQuestionTechnicalTag(updateableQuestion.getQuestionId(), tagId));
							} catch (NoSuchFieldException | IllegalAccessException e) {
								LOGGER.error(ADD_NEW_RECORD_ERROR, e);
							}
						}
					}
					
				});
			}
			
			// Identify the tags to remove
			// They should all already exist in the system
			List<String> tagsToRemove = updateableQuestion.getRemovedTags();
			tagsToRemove.forEach(tag -> {
				long tagId = technicalTagRepository.findTagByName(tag);
				questionTechnicalTagRepository.deleteRecord(updateableQuestion.getQuestionId(), tagId);
			});
		});
	}

	public List<QuestionTechTags> findTechnicalTagsForQuestionByName(String questionName, QuestionState status) {
		if (questionName == null || questionName.isEmpty()) {
			// Retrieve data for all questions with the given state
			switch (status) {
			case pending: {
				return restructureQuestionTagResults(technicalTagRepository.techTagsForQuestionByActive(false));
			}
			case active: {
				return restructureQuestionTagResults(technicalTagRepository.techTagsForQuestionByActive(true));
			}
			case review: {
				return restructureQuestionTagResults(technicalTagRepository.techTagsForQuestionsForReview());
			}
			default: {
				return new ArrayList<>();
			}
			}
		} else {
			switch (status) {
			case pending: {
				return restructureQuestionTagResults(
						technicalTagRepository.techTagsForQuestionByActiveAndName(false, questionName));
			}
			case active: {
				return restructureQuestionTagResults(
						technicalTagRepository.techTagsForQuestionByActiveAndName(true, questionName));
			}
			case review: {
				return restructureQuestionTagResults(
						technicalTagRepository.techTagsForQuestionForReviewByTitle(questionName));
			}
			default: {
				return new ArrayList<>();
			}
			}
		}
	}
	
	public Boolean updateQuestionUrl(QuestionUrlUpdate questionUrlUpdate) {
		QQuestion question = new QQuestion();
		
		question.setId(questionUrlUpdate.getQuestionId());
		question.setAppUrl(questionUrlUpdate.getUrl());
		
		try {
			questionRepository.update(question);
		} catch (IllegalAccessException | NoSuchFieldException e) {
			LOGGER.error("Error updating question with id" + questionUrlUpdate.getQuestionId(), e);
			return false;
		}
		return true;
	}
	
	public Boolean updateSourceUrl(QuestionUrlUpdate questionSourceUpdate) {
		QQuestion question = new QQuestion();
		
		question.setId(questionSourceUpdate.getQuestionId());
		question.setSource(questionSourceUpdate.getUrl());
		
		try {
			questionRepository.update(question);
		} catch (IllegalAccessException | NoSuchFieldException e) {
			LOGGER.error("Error updating question with id" + questionSourceUpdate.getQuestionId(), e);
			return false;
		}
		return false;
	}

	private List<QuestionTechTags> restructureQuestionTagResults(List<TechTagAndQuestion> tagAndQuestions) {
		Set<Long> qIds = tagAndQuestions.stream().map(tq -> tq.getQuestionId()).collect(Collectors.toSet());

		List<QuestionTechTags> questionsAndTags = qIds.stream().map(id -> {
			List<String> tagNames = tagAndQuestions.stream().filter(tagAndQ -> tagAndQ.getQuestionId().equals(id))
					.map(ftq -> ftq.getTagName()).collect(Collectors.toList());
			return new QuestionTechTags(id, tagNames);
		}).collect(Collectors.toList());

		return questionsAndTags;
	}
}
