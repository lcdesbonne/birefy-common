package com.birefy.common.tidings;

import com.birefy.common.dto.tiding.TidingIdDTO;

public class Tiding {
	private MessageType type;
	private String content;
	private TidingIdDTO sender;
	
	public Tiding() {
	}
	
	public Tiding(MessageType type, String content, TidingIdDTO sender) {
		this.type = type;
		this.content = content;
		this.sender = sender;
	}

	public MessageType getType() {
		return type;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public TidingIdDTO getSender() {
		return sender;
	}

	public void setSender(TidingIdDTO sender) {
		this.sender = sender;
	}
	
	public enum MessageType {
		CHAT, JOIN, LEAVE
	}
}
