package com.birefy.common.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.birefy.common.authentication.credentials.CommonUser;

@Component
public class BirefyApplicationUtils {
	@Value("${application-url.domain}")
	private String applicationDomain;
	
	@Value("${application-url.port}")
	private String applicationPort;
	
	@Value("${gateway.application-url.domain}")
	private String gatewayApplicationDomain;
	
	@Value("${gateway.application-url.port}")
	private String gatewayApplicationPort;
	
	public String getApplicationUrl() {
		return "https://" + applicationDomain + ":" + applicationPort + "/";
	}
	
	public String getApplicationDomain() {
		return "https://" + applicationDomain;
	}
	
	public String getApplicationDomainNoHttps() {
		return "http://" + applicationDomain;
	}
	
	public String gatewayApplicationUrl() {
		return "http://" + gatewayApplicationDomain + ":" + gatewayApplicationPort + "/";
	}
	
	public CommonUser getCurrentUserDetails() {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		return user;
	}
}
