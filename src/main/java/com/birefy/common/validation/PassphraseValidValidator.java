package com.birefy.common.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.birefy.common.service.PayoutService;

//@Component
@Deprecated
public class PassphraseValidValidator implements ConstraintValidator<PassphraseValidCheck, String> {
	@Autowired
	private PayoutService payoutService;
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return payoutService.verifyMemberPassphrase(value);
	}

}
