package com.birefy.common.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = PassphraseValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface PassphraseValidCheck {
	String message() default "The passphrase entered is incorrect";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
