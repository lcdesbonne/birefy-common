package com.birefy.common.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = PasswordValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface PasswordCheck {
	String message() default "Passwords must be at least 6 characters long";
	Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
