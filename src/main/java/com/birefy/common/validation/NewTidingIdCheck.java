package com.birefy.common.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = NewTidingIdValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface NewTidingIdCheck {
	String message() default "Invalid tiding id, or the tiding ID already exists.\nIt must be at least 3 characters long."
			+ "\nIt must contain at least one alphabetical character.";
	Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
