package com.birefy.common.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = FileValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface FileCheck {
	String message() default "A file must be provided.";
	Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
