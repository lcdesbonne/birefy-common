package com.birefy.common.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

public class PassphraseValidator implements ConstraintValidator<PassphraseCheck, String>{

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		boolean isValid = StringUtils.isNotEmpty(value) && value.length() > 5;
		return isValid;
	}

}
