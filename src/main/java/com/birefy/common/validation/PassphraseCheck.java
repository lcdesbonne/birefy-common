package com.birefy.common.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = PassphraseValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface PassphraseCheck {
	String message() default "Invalid passphrase. It must be at least 6 characters long and cannot be empty";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
