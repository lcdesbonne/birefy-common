package com.birefy.common.validation;

import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

public class PhoneNumberValidator implements ConstraintValidator<PhoneNumberCheck, String>{

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		boolean isValid;
		Optional<String> number = Optional.ofNullable(value);
		if(!number.isPresent() || StringUtils.isEmpty(number.get())) {
			isValid = true;
		} else {
			String phoneNumber = number.get();
			isValid = phoneNumber.matches("[0-9]+") &&
					(phoneNumber.length() > 8) && (phoneNumber.length() < 14);
		}
		return isValid;
	}

}
