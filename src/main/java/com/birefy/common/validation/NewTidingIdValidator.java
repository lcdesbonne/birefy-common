package com.birefy.common.validation;

import java.util.Arrays;
import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.birefy.common.repo.TidingMessageRepository;

@Component
public class NewTidingIdValidator implements ConstraintValidator<NewTidingIdCheck, String>{
	@Autowired
	private TidingMessageRepository tidingMessageRepository;
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		boolean isValid;
		
		Optional<String> tidingId = Optional.ofNullable(value);
		if(tidingId.isPresent() && tidingIdContentCompliance(tidingId.get())){
			isValid = !tidingMessageRepository.tidingIdsExist(Arrays.asList(tidingId.get())).get(tidingId.get());
		} else {
			isValid = false;
		}
		return isValid;
	}
	
	private boolean tidingIdContentCompliance(String tidingId) {
		boolean isCompliant = tidingId.length() > 2 
				&& tidingId.matches(".*[a-zA-z]+.*");
		return isCompliant;
	}

}
