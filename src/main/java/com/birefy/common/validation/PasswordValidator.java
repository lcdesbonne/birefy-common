package com.birefy.common.validation;

import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

public class PasswordValidator implements ConstraintValidator<PasswordCheck, String>{

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		boolean isValid;
		Optional<String> password = Optional.ofNullable(value);
		
		if(!password.isPresent()) {
			isValid = false;
		} else {
			String pwd = password.get();
			isValid = !StringUtils.isBlank(pwd) && pwd.length() >= 6;
		}
		return isValid;
	}

}
