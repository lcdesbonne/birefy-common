package com.birefy.common.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

public class DealDescriptionValidator implements ConstraintValidator<DealDescriptionCheck, String>{

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		boolean isValid = !StringUtils.isAllEmpty(value);
		return isValid;
	}
}
