package com.birefy.common.repo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.birefy.common.entity.QMemberQuestion;
import com.birefy.common.entity.QQuestion;
import com.birefy.common.entity.utils.database.MemberQuestionRowMapper;

@Repository
public class QMemberQuestionRepository extends CommonRepository<QMemberQuestion> {
	private static final String FIND_CREATORS_OF_QUESTION = "SELECT " + QMemberQuestion.MEMBER_ID_COLUMN +
			" FROM " + QMemberQuestion.TABLE_NAME + " WHERE " + QMemberQuestion.QUESTION_ID_COLUMN + 
			" = :questionId";
	
	private static final String FIND_CREATORS_OF_QUESTIONS = "SELECT * FROM " + QMemberQuestion.TABLE_NAME +
			" WHERE " + QMemberQuestion.QUESTION_ID_COLUMN + " IN (:questionIds)";
	
	private static final String UPDATE_CONTRIBUTOR_DETAILS = "UPDATE " + QMemberQuestion.TABLE_NAME +
			" SET " + QMemberQuestion.CONTRIBUTION_DETAILS_COLUMN + " = :details" + 
			" WHERE " + QMemberQuestion.QUESTION_ID_COLUMN + " = :questionId" + 
			" AND " + QMemberQuestion.MEMBER_ID_COLUMN + " = :memberId";
	
	private static final String REMOVE_CONTRIBUTORS_FROM_QUESTION = "DELETE FROM " + QMemberQuestion.TABLE_NAME +
			" WHERE " + QMemberQuestion.QUESTION_ID_COLUMN + " = :questionId" + 
			" AND " + QMemberQuestion.MEMBER_ID_COLUMN + " IN (:memberIds)";
	
	private static final String IS_CONTRIBUTOR_TO_QUESTION = "SELECT COUNT("+QMemberQuestion.MEMBER_ID_COLUMN+") > 0 " +
			" FROM " + QMemberQuestion.TABLE_NAME + " WHERE " + QMemberQuestion.MEMBER_ID_COLUMN + " = :memberId" +
			" AND " + QMemberQuestion.QUESTION_ID_COLUMN + " = :questionId";
	
	private static final String NUMBER_OF_SUBSCRIPTION_QUESTIONS = "SELECT COUNT("+QMemberQuestion.QUESTION_ID_COLUMN+")" +
			" FROM " + QMemberQuestion.TABLE_NAME + 
			" INNER JOIN " + QQuestion.TABLE_NAME + " ON " + QMemberQuestion.QUESTION_ID_COLUMN + " = " + QQuestion.ID_COLUMN +
			" WHERE " + QQuestion.SUBSCRIPTION_COLUMN + " AND " + QQuestion.ACTIVE_COLUMN +
			" AND " + QMemberQuestion.MEMBER_ID_COLUMN + " = :memberId";
	
	public List<Long> findCreatorsOfQuestion(Long questionId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("questionId", questionId);
		
		List<Long> creatorIds = namedParameterJdbcTemplate.queryForList(FIND_CREATORS_OF_QUESTION, 
				parameters, Long.class);
		
		return creatorIds;
	}
	
	public boolean isContributorToQuestion(Long questionId, Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("questionId", questionId);
		
		boolean isContributor = namedParameterJdbcTemplate.queryForObject(IS_CONTRIBUTOR_TO_QUESTION, parameters, Boolean.class);
		
		return isContributor;
	}
	
	public boolean updateContributorDetails(QMemberQuestion detailsToUpdate) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", detailsToUpdate.getMemberId());
		parameters.addValue("questionId", detailsToUpdate.getQuestionId());
		parameters.addValue("details", detailsToUpdate.getDetails());
		
		int updates = namedParameterJdbcTemplate.update(UPDATE_CONTRIBUTOR_DETAILS, parameters);
		
		return updates == 1;
	}
	
	public boolean removeContributorsFromQuestion(Long questionId, List<Long> memberIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("questionId", questionId);
		parameters.addValue("memberIds", memberIds);
		
		int updates = namedParameterJdbcTemplate.update(REMOVE_CONTRIBUTORS_FROM_QUESTION, parameters);
		
		return updates > 0;
	}
	
	public List<QMemberQuestion> findCreatorsForQuestions(List<Long> questionIds) {
		List<QMemberQuestion> results;
		if(CollectionUtils.isEmpty(questionIds)) {
			results = new ArrayList<>();
		} else {
			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("questionIds", questionIds);
			
			results = namedParameterJdbcTemplate.query(FIND_CREATORS_OF_QUESTIONS, parameters, new MemberQuestionRowMapper());
		}
		
		return results;
	}
	
	public Integer subscriptionQuestionsForMember(Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		return namedParameterJdbcTemplate.queryForObject(NUMBER_OF_SUBSCRIPTION_QUESTIONS, parameters, Integer.class);
	}
}
