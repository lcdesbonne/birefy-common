package com.birefy.common.repo;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.dto.sound.SoundNameAndLocation;
import com.birefy.common.dto.sound.SoundShort;
import com.birefy.common.entity.Sound;
import com.birefy.common.entity.SoundDescriptiveTag;
import com.birefy.common.entity.utils.database.SoundNameAndLocationMapper;
import com.birefy.common.entity.utils.database.SoundRowMapper;

@Repository
public class SoundRepository extends CommonRepository<Sound> {
	private static final String KEY_COLUMNS = Sound.ID_COLUMN + "," + Sound.MEMBER_COLUMN + ","
			+ Sound.LOCATION_COLUMN + "," + Sound.CATEGORY_COLUMN + "," + Sound.NAME_COLUMN;
	
	private static final String INNER_JOIN_WITH_DESCRIPTIVE_TAGS = " INNER JOIN " + SoundDescriptiveTag.TABLE_NAME + " ON " + Sound.TABLE_NAME +"." + Sound.ID_COLUMN + " = " + SoundDescriptiveTag.SOUND_COLUMN;
	
	private static final String ALL_RECENT_SOUNDS = "SELECT " + KEY_COLUMNS + " FROM " + Sound.TABLE_NAME + 
			" ORDER BY " + Sound.CREATION_DATE_COLUMN + " DESC";
	
	private static final String ALL_SOUNDS_FOR_REVIEW = "SELECT " + KEY_COLUMNS + " FROM " + Sound.TABLE_NAME + 
			" WHERE " + Sound.REVIEW_COLUMN + " ORDER BY " + Sound.CREATION_DATE_COLUMN + " DESC";
	
	private static final String SOUNDS_BY_MEMBER_CATEGORY = "SELECT " + KEY_COLUMNS + " FROM " + Sound.TABLE_NAME +
			" WHERE " + Sound.MEMBER_COLUMN + " = :memberId AND " + Sound.CATEGORY_COLUMN + " = :category";
	
	private static final String SOUNDS_BY_MEMBER_CATEGORY_TAGS = "SELECT " + KEY_COLUMNS + " FROM " + Sound.TABLE_NAME +
			INNER_JOIN_WITH_DESCRIPTIVE_TAGS +
			" WHERE " + Sound.MEMBER_COLUMN + " = :memberId AND " + Sound.CATEGORY_COLUMN + " = :category" +
			" AND " + SoundDescriptiveTag.TAG_COLUMN + " IN (:tagIds)";
	
	private static final String RECENT_SOUND_INSTRUMENTALS = "SELECT " + KEY_COLUMNS + " FROM " + Sound.TABLE_NAME +
			" WHERE " + Sound.CATEGORY_COLUMN + " = '" + Sound.Category.INSTRUMENTAL + "' ORDER BY " + Sound.CREATION_DATE_COLUMN +
			" DESC";
	
	private static final String RECENT_SOUND_INSTRUMENTALS_TAGS = "SELECT " + KEY_COLUMNS + " FROM " + Sound.TABLE_NAME +
			INNER_JOIN_WITH_DESCRIPTIVE_TAGS + 
			" WHERE " + Sound.CATEGORY_COLUMN + " = '" + Sound.Category.INSTRUMENTAL + "' AND " + SoundDescriptiveTag.TAG_COLUMN + " IN (:tagIds)" +
			" ORDER BY " + Sound.CREATION_DATE_COLUMN + " DESC";
			
	
	private static final String RECENT_SOUND_EFFECTS = "SELECT " + KEY_COLUMNS + " FROM " + Sound.TABLE_NAME + 
			" WHERE " + Sound.CATEGORY_COLUMN + " = '" + Sound.Category.EFFECT + "' ORDER BY " + Sound.CREATION_DATE_COLUMN + 
			" DESC";
	
	private static final String RECENT_SOUND_EFFECTS_TAGS = "SELECT " + KEY_COLUMNS + " FROM " + Sound.TABLE_NAME + 
			INNER_JOIN_WITH_DESCRIPTIVE_TAGS +
			" WHERE " + Sound.CATEGORY_COLUMN + " = '" + Sound.Category.EFFECT + " AND " + SoundDescriptiveTag.TAG_COLUMN + " IN (:tagIds) " +
			" ORDER BY " + Sound.CREATION_DATE_COLUMN + " DESC";
	
	private static final String FIND_SOUND_BY_NAME_MEMBER_CATEGORY = "SELECT " + KEY_COLUMNS + " FROM " +Sound.TABLE_NAME +
			" WHERE " + Sound.MEMBER_COLUMN + " in (:memberIds) AND lower(" + Sound.NAME_COLUMN + ") LIKE lower(:name) AND "+Sound.CATEGORY_COLUMN +
			" = :category";
	
	private static final String FIND_SOUND_BY_NAME_MEMBER_CATEGORY_TAGS = "SELECT " + KEY_COLUMNS + " FROM " +Sound.TABLE_NAME +
			INNER_JOIN_WITH_DESCRIPTIVE_TAGS + 
			" WHERE " + Sound.MEMBER_COLUMN + " in (:memberIds) AND lower(" + Sound.NAME_COLUMN + ") LIKE lower(:name) AND "+Sound.CATEGORY_COLUMN + " = :category" +
			" AND " + SoundDescriptiveTag.TAG_COLUMN + " IN (:tagIds)";
	
	private static final String GET_SOUND_BY_ID = "SELECT " + KEY_COLUMNS + "," + Sound.PRICE_COLUMN + " FROM " + Sound.TABLE_NAME + 
			" WHERE " + Sound.ID_COLUMN + " = :soundId";
	
	private static final String GET_SOUND_LOCATION = "SELECT " + Sound.LOCATION_COLUMN + " FROM " + Sound.TABLE_NAME + 
			" WHERE " + Sound.ID_COLUMN + " = :id";
	
	private static final String GET_SOUND_NAME_AND_LOCATION = "SELECT " + Sound.NAME_COLUMN + ", " + Sound.LOCATION_COLUMN +
			" FROM " + Sound.TABLE_NAME + 
			" WHERE " + Sound.ID_COLUMN + " = :id";
	
	private static final String FIND_SOUND_BY_NAME_AND_CATEGORY = "SELECT " + KEY_COLUMNS + " FROM " + Sound.TABLE_NAME +
			" WHERE " + Sound.CATEGORY_COLUMN + " = :category AND " + Sound.NAME_COLUMN + " LIKE :name LIMIT 30";
	
	private static final String FIND_SOUND_BY_NAME_AND_CATEGORY_TAGS = "SELECT " + KEY_COLUMNS + " FROM " + Sound.TABLE_NAME +
			INNER_JOIN_WITH_DESCRIPTIVE_TAGS + 
			" WHERE " + Sound.CATEGORY_COLUMN + " = :category AND lower(" + Sound.NAME_COLUMN + ") LIKE lower(:name)" +
			" AND " + SoundDescriptiveTag.TAG_COLUMN + " IN (:tagIds) LIMIT 30";
	
	private static final String FIND_ARTISTS = "SELECT "+Sound.MEMBER_COLUMN + " FROM " + Sound.TABLE_NAME +
			" WHERE " + Sound.MEMBER_COLUMN + " IN (:memberIds)";
	
	private static final String FIND_SOUNDS_BY_TITLE = "SELECT "+ KEY_COLUMNS + " FROM " + Sound.TABLE_NAME + 
			" WHERE lower(" + Sound.NAME_COLUMN + ") LIKE lower(:title) ORDER BY " + Sound.NAME_COLUMN +
			" DESC";
	
	private static final String FIND_SOUNDS_BY_TITLE_TAGS = "SELECT " + KEY_COLUMNS + " FROM " + Sound.TABLE_NAME +
			INNER_JOIN_WITH_DESCRIPTIVE_TAGS +
			" WHERE lower(" + Sound.NAME_COLUMN + ") LIKE lower(:title) AND " + SoundDescriptiveTag.TAG_COLUMN + " IN (:tagIds)" +
			" ORDER BY " + Sound.NAME_COLUMN +
			" DESC";
	
	private static final String FIND_SOUNDS_BY_MEMBER_TITLE = "SELECT " + KEY_COLUMNS + " FROM " + Sound.TABLE_NAME + 
			" WHERE " + Sound.MEMBER_COLUMN + " = :memberId AND lower(" + Sound.NAME_COLUMN + ") LIKE lower(:name)" +
			" ORDER BY " + Sound.NAME_COLUMN;
	
	private static final String FIND_SOUNDS_BY_MEMBER_TITLE_TAGS = "SELECT " + KEY_COLUMNS + " FROM " + Sound.TABLE_NAME +
			INNER_JOIN_WITH_DESCRIPTIVE_TAGS +
			" WHERE " + Sound.MEMBER_COLUMN + " = :memberId AND lower(" + Sound.NAME_COLUMN + ") LIKE lower(:name)" +
			" AND " + SoundDescriptiveTag.TAG_COLUMN + " IN (:tagIds)" +
			" ORDER BY " + Sound.NAME_COLUMN;
	
	private static final String FIND_SOUND_BY_ARTIST = "SELECT " + KEY_COLUMNS + " FROM " + Sound.TABLE_NAME + 
			" WHERE " + Sound.MEMBER_COLUMN + " = :memberId ORDER BY " + Sound.NAME_COLUMN;
	
	private static final String FIND_SOUND_ARTIST_WITH_PRICE = "SELECT " + KEY_COLUMNS + ", " + Sound.PRICE_COLUMN + 
			" FROM " + Sound.TABLE_NAME + " WHERE " + Sound.MEMBER_COLUMN + " = :memberId ORDER BY " + Sound.NAME_COLUMN;
	
	private static final String FIND_SOUND_BY_ARTIST_TAGS = "SELECT " + KEY_COLUMNS + " FROM " + Sound.TABLE_NAME +
			" INNER JOIN " + SoundDescriptiveTag.TABLE_NAME + " ON " + Sound.TABLE_NAME + " = " + SoundDescriptiveTag.SOUND_COLUMN +
			" WHERE " + Sound.MEMBER_COLUMN + " = :memberId AND " + SoundDescriptiveTag.TAG_COLUMN + " IN (:tagIds) " +
			" ORDER BY " + Sound.NAME_COLUMN;
	
	private static final String DELETE_SOUND_BY_ID = "DELETE FROM "+Sound.TABLE_NAME+" WHERE "+Sound.ID_COLUMN+" = :id";
	
	private static final String UPDATE_SOUND_NAME_AND_CATEGORY = "UPDATE "+Sound.TABLE_NAME+" SET "+Sound.NAME_COLUMN + " = :name"+
			", " + Sound.CATEGORY_COLUMN + " = :category WHERE " + Sound.ID_COLUMN + " = :id";
	
	private static final String UPDATE_SOUND_NAME_AND_CATEGORY_PRICE = "UPDATE "+Sound.TABLE_NAME+" SET "+Sound.NAME_COLUMN + " = :name"+
			", " + Sound.CATEGORY_COLUMN + " = :category,"+ Sound.PRICE_COLUMN+" = :price WHERE " + Sound.ID_COLUMN + " = :id";
	
	private static final String UPDATE_SOUND_NAME = "UPDATE "+Sound.TABLE_NAME+" SET " + Sound.NAME_COLUMN + " = :name" + 
			" WHERE " + Sound.ID_COLUMN + " = :id";
	
	private static final String UPDATE_SOUND_NAME_PRICE = "UPDATE "+Sound.TABLE_NAME+" SET " + Sound.NAME_COLUMN + " = :name," 
			+ Sound.PRICE_COLUMN+" = :price WHERE " + Sound.ID_COLUMN + " = :id";
	
	private static final String UPDATE_SOUND_CATEGORY = "UPDATE "+Sound.TABLE_NAME+" SET " + Sound.CATEGORY_COLUMN + " = :category"+
			" WHERE "+Sound.ID_COLUMN+" = :id";
	
	private static final String UPDATE_SOUND_CATEGORY_PRICE = "UPDATE "+Sound.TABLE_NAME+" SET " + Sound.CATEGORY_COLUMN + " = :category"
			+ Sound.PRICE_COLUMN+" = :price WHERE "+Sound.ID_COLUMN+" = :id";
	
	private static final String FIND_SOUND_ID_BY_LOCATION = "SELECT " + Sound.ID_COLUMN + " FROM " + Sound.TABLE_NAME + " WHERE " +
	Sound.LOCATION_COLUMN + " LIKE :fileName AND " + Sound.MEMBER_COLUMN + " = :memberId";
	
	private static final String SOUND_OWNER_ID = "SELECT " + Sound.MEMBER_COLUMN + " FROM " + Sound.TABLE_NAME + " WHERE " +
			Sound.ID_COLUMN + " = :id";
	
	private static final String APPROVE_SOUND = "UPDATE " + Sound.TABLE_NAME + " SET " + Sound.REVIEW_COLUMN + " = FALSE " + 
			" WHERE " + Sound.ID_COLUMN + " = :id";
	
	
	public List<SoundShort> allRecentSounds() {
		List<SoundShort> results = jdbcTemplate.query(ALL_RECENT_SOUNDS, new SoundRowMapper());
		return results;
	}
	
	public List<SoundShort> allRecentSoundsForReview() {
		List<SoundShort> results = jdbcTemplate.query(ALL_SOUNDS_FOR_REVIEW, new SoundRowMapper());
		return results;
	}
	
	public boolean approveSound(Long soundId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", soundId);
		
		int updates = namedParameterJdbcTemplate.update(APPROVE_SOUND, parameters);
		
		return updates > 0;
	}
	
	public String getSoundLocation(long soundId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", soundId);
		String url = namedParameterJdbcTemplate.queryForObject(GET_SOUND_LOCATION, parameters, String.class);
		return url;
	}
	
	public SoundNameAndLocation getSoundNameAndLocation(Long soundId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", soundId);
		SoundNameAndLocation result = namedParameterJdbcTemplate.queryForObject(GET_SOUND_NAME_AND_LOCATION, 
				parameters, new SoundNameAndLocationMapper());
		
		return result;
	}
	
	public List<SoundShort> soundsByMember(Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		
		List<SoundShort> results = namedParameterJdbcTemplate.query(FIND_SOUND_BY_ARTIST, parameters, new SoundRowMapper());
		return results;
	}
	
	public List<SoundShort> soundsByMemberWithPrice(Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		
		List<SoundShort> results = namedParameterJdbcTemplate.query(FIND_SOUND_ARTIST_WITH_PRICE, parameters, new SoundRowMapper());
		return results;
	}
	
	public List<SoundShort> soundsByMemberTags(Long memberId, List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("tagIds", tagIds);
		
		List<SoundShort> results = namedParameterJdbcTemplate.query(FIND_SOUND_BY_ARTIST_TAGS, parameters, new SoundRowMapper());
		return results;
	}
	
	public List<SoundShort> soundsByNameAndCategory(String name, Sound.Category category) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("category", category.toString());
		parameters.addValue("name", "%"+name+"%");
		List<SoundShort> results = namedParameterJdbcTemplate.query(FIND_SOUND_BY_NAME_AND_CATEGORY, parameters, new SoundRowMapper());
		return results;
	}
	
	public List<SoundShort> soundsByNameAndCategoryTags(String name, Sound.Category category, List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("category", category.toString());
		parameters.addValue("name", "%"+name+"%");
		parameters.addValue("tagIds", tagIds);
		List<SoundShort> results = namedParameterJdbcTemplate.query(FIND_SOUND_BY_NAME_AND_CATEGORY_TAGS, parameters, new SoundRowMapper());
		return results;
	}
	
	public List<SoundShort> soundsByMemberCategory(Long memberId, Sound.Category category) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("category", category.toString());
		List<SoundShort> results = namedParameterJdbcTemplate.query(SOUNDS_BY_MEMBER_CATEGORY,parameters, new SoundRowMapper());
		return results;
	}
	
	public List<SoundShort> soundsByMemberCategoryTags(Long memberId, Sound.Category category, List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("category", category.toString());
		parameters.addValue("tagIds", tagIds);
		List<SoundShort> results = namedParameterJdbcTemplate.query(SOUNDS_BY_MEMBER_CATEGORY_TAGS,parameters, new SoundRowMapper());
		return results;
	}
	
	public List<Long> membersWithMusic(List<Long> memberIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberIds", memberIds);
		List<Long> ids = namedParameterJdbcTemplate.queryForList(FIND_ARTISTS, parameters, Long.class);
		return ids;
	}
	
	public List<SoundShort> recentSoundInstrumentals() {
		List<SoundShort> results = jdbcTemplate.query(RECENT_SOUND_INSTRUMENTALS, new SoundRowMapper());
		return results;
	}
	
	public List<SoundShort> recentSoundInstrumentalsTags(List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tagIds", tagIds);
		List<SoundShort> results = namedParameterJdbcTemplate.query(RECENT_SOUND_INSTRUMENTALS_TAGS, parameters, new SoundRowMapper());
		return results;
	}
	
	public List<SoundShort> recentSoundEffects() {
		List<SoundShort> results = jdbcTemplate.query(RECENT_SOUND_EFFECTS, new SoundRowMapper());
		return results;
	}
	
	public List<SoundShort> recentSoundEffectsTags(List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tagIds", tagIds);
		
		List<SoundShort> results = namedParameterJdbcTemplate.query(RECENT_SOUND_EFFECTS_TAGS, parameters, new SoundRowMapper());
		return results;
	}
	
	public Long soundOwnerId(Long soundId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", soundId);
		Long ownerId = namedParameterJdbcTemplate.queryForObject(SOUND_OWNER_ID, parameters, Long.class);
		return ownerId;
	}
	
	public List<SoundShort> findSoundByNameAndMemberCategory(List<Long> memberId, String soundName, Sound.Category category) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberIds", memberId);
		parameters.addValue("name", "%"+soundName+"%");
		parameters.addValue("category", category.toString());
		List<SoundShort> results = namedParameterJdbcTemplate.query(FIND_SOUND_BY_NAME_MEMBER_CATEGORY, parameters, new SoundRowMapper());
		return results;
	}
	
	public List<SoundShort> findSoundByNameAndMemberCategoryTags(List<Long> memberId, String soundName, Sound.Category category, List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberIds", memberId);
		parameters.addValue("name", "%"+soundName+"%");
		parameters.addValue("category", category.toString());
		parameters.addValue("tagIds", tagIds);
		List<SoundShort> results = namedParameterJdbcTemplate.query(FIND_SOUND_BY_NAME_MEMBER_CATEGORY_TAGS, parameters, new SoundRowMapper());
		return results;
	}
	
	public List<SoundShort> findSoundsByTitle(String title) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue(":title", "%"+title+"%");
		List<SoundShort> results = namedParameterJdbcTemplate.query(FIND_SOUNDS_BY_TITLE, parameters, new SoundRowMapper());
		return results;
	}
	
	public List<SoundShort> findSoundsByTitleTags(String title, List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue(":title", "%"+title+"%");
		parameters.addValue("tagIds", tagIds);
		List<SoundShort> results = namedParameterJdbcTemplate.query(FIND_SOUNDS_BY_TITLE_TAGS, parameters, new SoundRowMapper());
		return results;
	}
	
	public List<SoundShort> findSoundsByMemberAndTitle(Long memberId, String title) {
		MapSqlParameterSource parameter = new MapSqlParameterSource();
		parameter.addValue("memberId", memberId);
		parameter.addValue(":title", title);
		List<SoundShort> results = namedParameterJdbcTemplate.query(FIND_SOUNDS_BY_MEMBER_TITLE, parameter, new SoundRowMapper());
		return results;
	}
	
	public List<SoundShort> findSoundsByMemberAndTitleTags(Long memberId, String title, List<Long> tagIds) {
		MapSqlParameterSource parameter = new MapSqlParameterSource();
		parameter.addValue("memberId", memberId);
		parameter.addValue(":title", title);
		parameter.addValue("tagIds", tagIds);
		List<SoundShort> results = namedParameterJdbcTemplate.query(FIND_SOUNDS_BY_MEMBER_TITLE_TAGS, parameter, new SoundRowMapper());
		return results;
	}
	
	public SoundShort getSoundById(Long soundId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("soundId", soundId);
		SoundShort sound = namedParameterJdbcTemplate.queryForObject(GET_SOUND_BY_ID, parameters, new SoundRowMapper());
		return sound;
	}
	
	public boolean deleteSoundById(long soundId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", soundId);
		int deletedRows = namedParameterJdbcTemplate.update(DELETE_SOUND_BY_ID, parameters);
		
		return deletedRows > 0;
	}
	
	public boolean updateSoundNameAndCategory(long soundId, String name, Sound.Category category) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", soundId);
		parameters.addValue("name", name);
		parameters.addValue("category", category.toString());
		int updatedRows = namedParameterJdbcTemplate.update(UPDATE_SOUND_NAME_AND_CATEGORY, parameters);
		return updatedRows > 0;
	}
	
	public boolean updateSoundNameAndCategoryAndPrice(long soundId, String name, Sound.Category category, Float price) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", soundId);
		parameters.addValue("name", name);
		parameters.addValue("category", category.toString());
		parameters.addValue("price", price);
		
		int updatedRows = namedParameterJdbcTemplate.update(UPDATE_SOUND_NAME_AND_CATEGORY_PRICE, parameters);
		return updatedRows > 0;
	}
	
	public boolean updateSoundName(long soundId, String name) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", soundId);
		parameters.addValue("name", name);
		int updatedRows = namedParameterJdbcTemplate.update(UPDATE_SOUND_NAME, parameters);
		return updatedRows > 0;
	}
	
	public boolean updateSoundNameAndPrice(long soundId, String name, Float price) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", soundId);
		parameters.addValue("name", name);
		parameters.addValue("price", price);
		int updatedRows = namedParameterJdbcTemplate.update(UPDATE_SOUND_NAME_PRICE, parameters);
		return updatedRows > 0;
	}
	
	public boolean updateSoundCategory(long soundId, Sound.Category category) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", soundId);
		parameters.addValue("category", category.toString());
		int updatedRows = namedParameterJdbcTemplate.update(UPDATE_SOUND_CATEGORY, parameters);
		return updatedRows > 0;
	}
	
	public boolean updateSoundCategoryAndPrice(long soundId, Sound.Category category, Float price) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", soundId);
		parameters.addValue("category", category.toString());
		parameters.addValue("price", price);
		int updatedRows = namedParameterJdbcTemplate.update(UPDATE_SOUND_CATEGORY_PRICE, parameters);
		return updatedRows > 0;
	}
	
	public Long findSoundByLocation(String fileName, Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("fileName", "%" + fileName);
		parameters.addValue("memberId", memberId);
		Long soundId = namedParameterJdbcTemplate.queryForObject(FIND_SOUND_ID_BY_LOCATION, parameters, Long.class);
		
		return soundId;
	}
}
