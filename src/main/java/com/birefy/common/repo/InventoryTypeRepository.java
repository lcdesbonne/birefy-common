package com.birefy.common.repo;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.birefy.common.entity.InventoryType;
import com.birefy.common.entity.utils.database.InventoryTypeRowMapper;

@Repository
public class InventoryTypeRepository extends CommonRepository<InventoryType>{
	private static final String ALL_TYPES = "SELECT * FROM " + InventoryType.TABLE_NAME;
	
	public List<InventoryType> allTypes() {
		List<InventoryType> allTypes = jdbcTemplate.query(ALL_TYPES, new InventoryTypeRowMapper());
		return allTypes;
	}
}
