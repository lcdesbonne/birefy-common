package com.birefy.common.repo;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.dto.bulletin.BulletinShort;
import com.birefy.common.entity.Bulletin;
import com.birefy.common.entity.BulletinTechnicalTag;
import com.birefy.common.entity.MemberBulletin;
import com.birefy.common.entity.QMemberQuestion;
import com.birefy.common.entity.QQuestionTechnicalTag;
import com.birefy.common.entity.utils.database.BulletinRowMapper;
import com.birefy.common.entity.utils.database.BulletinShortRowMapper;

@Repository
public class BulletinRepository extends CommonRepository<Bulletin> {
	private static final String KEY_COLUMNS = Bulletin.TABLE_NAME + "." +Bulletin.ID_COLUMN + " , " + 
			Bulletin.TABLE_NAME + "." + Bulletin.DESCRIPTION_COLUMN + " , " + Bulletin.TABLE_NAME + "." + Bulletin.DATE_COLUMN;
	
	private static final String ALL_OPEN_BULLETINS = "SELECT " + KEY_COLUMNS + " FROM " + Bulletin.TABLE_NAME
			+ " WHERE " + Bulletin.OPEN_COLUMN + " ORDER BY " + Bulletin.DATE_COLUMN + " DESC";
	
	private static final String TARGETED_OPEN_BULLETINS = "SELECT " + KEY_COLUMNS + " FROM " + Bulletin.TABLE_NAME +
			" INNER JOIN " + BulletinTechnicalTag.TABLE_NAME + " ON " + BulletinTechnicalTag.TABLE_NAME + "." + BulletinTechnicalTag.BULLETIN_COLUMN + " = " + Bulletin.TABLE_NAME + "." + Bulletin.ID_COLUMN +
			" WHERE " + BulletinTechnicalTag.TABLE_NAME + "." + BulletinTechnicalTag.TECH_COLUMN + " IN " +
			" (SELECT "+ QQuestionTechnicalTag.TABLE_NAME +"." + QQuestionTechnicalTag.TAG_ID_COLUMN + " FROM " + QQuestionTechnicalTag.TABLE_NAME +
			" INNER JOIN " + QMemberQuestion.TABLE_NAME + " ON "+ QMemberQuestion.TABLE_NAME + "." + QMemberQuestion.QUESTION_ID_COLUMN + " = " + QQuestionTechnicalTag.TABLE_NAME + "." + QQuestionTechnicalTag.QUESTION_ID_COLUMN +
			" WHERE " + QMemberQuestion.TABLE_NAME + "." + QMemberQuestion.MEMBER_ID_COLUMN + " = :memberId )" +
			" AND " + Bulletin.TABLE_NAME + "." + Bulletin.OPEN_COLUMN +
			" ORDER BY " + Bulletin.DATE_COLUMN + " DESC LIMIT 30";
	
	private static final String ALL_AVAILABLE_TO_MEMBER = "SELECT " + KEY_COLUMNS + " FROM " + Bulletin.TABLE_NAME +
			" INNER JOIN " + BulletinTechnicalTag.TABLE_NAME + " ON " + BulletinTechnicalTag.TABLE_NAME + "." + BulletinTechnicalTag.BULLETIN_COLUMN + " = " + Bulletin.TABLE_NAME + "." + Bulletin.ID_COLUMN +
			" WHERE " + BulletinTechnicalTag.TABLE_NAME + "." + BulletinTechnicalTag.TECH_COLUMN + " IN " +
			" (SELECT "+ QQuestionTechnicalTag.TABLE_NAME +"." + QQuestionTechnicalTag.TAG_ID_COLUMN + " FROM " + QQuestionTechnicalTag.TABLE_NAME +
			" INNER JOIN " + QMemberQuestion.TABLE_NAME + " ON "+ QMemberQuestion.TABLE_NAME + "." + QMemberQuestion.QUESTION_ID_COLUMN + " = " + QQuestionTechnicalTag.TABLE_NAME + "." + QQuestionTechnicalTag.QUESTION_ID_COLUMN +
			" WHERE " + QMemberQuestion.TABLE_NAME + "." + QMemberQuestion.MEMBER_ID_COLUMN + " = :memberId )" +
			" OR NOT " + Bulletin.TABLE_NAME + "." + Bulletin.TARGETED_COLUMN +
			" AND " + Bulletin.TABLE_NAME + "." + Bulletin.OPEN_COLUMN +
			" ORDER BY " + Bulletin.DATE_COLUMN + " DESC LIMIT 30";
	
	private static final String BULLETIN_ID_BY_DESCRIPTION = "SELECT " + Bulletin.ID_COLUMN + " FROM " + Bulletin.TABLE_NAME + " WHERE " + Bulletin.DESCRIPTION_COLUMN +
			" = :description";
	
	private static final String BULLETINS_BY_MEMBER = "SELECT * FROM " + Bulletin.TABLE_NAME + " INNER JOIN " + MemberBulletin.TABLE_NAME + 
			" ON " + MemberBulletin.TABLE_NAME + "." + MemberBulletin.BULLETIN_ID_COLUMN + " = " + Bulletin.TABLE_NAME + "." + Bulletin.ID_COLUMN + 
			" WHERE " + MemberBulletin.TABLE_NAME + "." + MemberBulletin.MEMBER_ID_COLUMN + " = :memberId";
	
	public List<BulletinShort> allOpenBulletins() {
		List<BulletinShort> results = jdbcTemplate.query(ALL_OPEN_BULLETINS, new BulletinShortRowMapper());
		
		return results;
	}
	
	public List<BulletinShort> allTargetedBulletins(long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		
		List<BulletinShort> results = namedParameterJdbcTemplate.query(TARGETED_OPEN_BULLETINS, parameters, new BulletinShortRowMapper());
		return results;
	}
	
	public List<BulletinShort> allAvailableBulletinsForMember(Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		
		List<BulletinShort> results = namedParameterJdbcTemplate.query(ALL_AVAILABLE_TO_MEMBER, parameters, new BulletinShortRowMapper());
		return results;
	}
	
	public long findBulletinIdByDescription(String description) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("description", description);
		
		long id = namedParameterJdbcTemplate.queryForObject(BULLETIN_ID_BY_DESCRIPTION, parameters, Long.class);
		
		return id;
	}
	
	public List<Bulletin> getBulletinsByMember(long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		
		List<Bulletin> bulletinsByMember = namedParameterJdbcTemplate.query(BULLETINS_BY_MEMBER,parameters, new BulletinRowMapper());
		
		return bulletinsByMember;
	}

}
