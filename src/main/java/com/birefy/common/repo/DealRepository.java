package com.birefy.common.repo;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.entity.Deal;
import com.birefy.common.entity.utils.database.DealRowMapper;

@Repository
public class DealRepository extends CommonRepository<Deal>{
	private static final String DEALS_FOR_MEMBER = "SELECT * FROM " + Deal.TABLE_NAME +
			" WHERE " + Deal.CUSTOMER_ID_COLUMN + " = :memberId OR " + Deal.SUPPLIER_ID_COLUMN +
			" = :memberId ORDER BY " + Deal.CREATION_DATE_COLUMN + " DESC";
	
	private static final String CAN_UPDATE_DEAL = "SELECT " + Deal.PAID_COLUMN + " IS NULL AND " +
			Deal.ACTIVE_COLUMN + " AND (" + Deal.CUSTOMER_ID_COLUMN + " = :memberId OR " + Deal.SUPPLIER_ID_COLUMN +
			" = :memberId) AND !" + Deal.CUSTOMER_CONFIRM_COLUMN + " AND !" + Deal.SUPPLIER_CONFIRM_COLUMN
			+ " FROM " + Deal.TABLE_NAME + " WHERE " + Deal.ID_COLUMN + " = :dealId";
	
	private static final String CAN_ADD_INVENTORY = "SELECT " + Deal.SUPPLIER_ID_COLUMN + " = :memberId OR " + 
	Deal.CUSTOMER_ID_COLUMN + " = :memberId FROM " + Deal.TABLE_NAME + " WHERE " + Deal.ID_COLUMN + " = :dealId";
	
	private static final String CUSTOMER_CONFIRM_DEAL = "UPDATE " + Deal.TABLE_NAME + " SET " + Deal.CUSTOMER_CONFIRM_COLUMN +
			" = TRUE WHERE " + Deal.CUSTOMER_ID_COLUMN + " = :memberId AND " + Deal.ID_COLUMN + " = :dealId";
	
	private static final String SUPPLIER_CONFIRM_DEAL = "UPDATE " + Deal.TABLE_NAME + " SET " + Deal.SUPPLIER_CONFIRM_COLUMN +
			" = TRUE WHERE " + Deal.SUPPLIER_ID_COLUMN + " = :memberId AND " + Deal.ID_COLUMN + " = :dealId";
	
	private static final String SUPPLIER_UNCONFIRM = "UPDATE " + Deal.TABLE_NAME + " SET " + Deal.SUPPLIER_CONFIRM_COLUMN + 
			" = FALSE WHERE " + Deal.SUPPLIER_ID_COLUMN + " = :memberId AND " + Deal.ID_COLUMN + " = :dealId AND !" +
			Deal.CUSTOMER_CONFIRM_COLUMN;
	
	private static final String CUSTOMER_UNCONFIRM = "UPDATE " + Deal.TABLE_NAME + " SET " + Deal.CUSTOMER_CONFIRM_COLUMN +
			" = FALSE WHERE " + Deal.CUSTOMER_ID_COLUMN + " = :memberId AND " + Deal.ID_COLUMN + " = :dealId AND !" +
			Deal.SUPPLIER_CONFIRM_COLUMN;
	
	private static final String REJECT_DEAL = "UPDATE " + Deal.TABLE_NAME + " SET " + Deal.ACTIVE_COLUMN + " = FALSE WHERE " +
			Deal.ID_COLUMN + " = :dealId AND (" + Deal.SUPPLIER_ID_COLUMN + " = :memberId OR " + Deal.CUSTOMER_ID_COLUMN + " = :memberId)" +
			" AND "+ Deal.PAID_COLUMN + " IS NULL AND (!" + Deal.CUSTOMER_CONFIRM_COLUMN + " OR !" + Deal.SUPPLIER_CONFIRM_COLUMN + ")"; 
	
	private static final String SET_PAID = "UPDATE " + Deal.TABLE_NAME + " SET " + Deal.PAID_COLUMN + " = :paidDate " +
			"WHERE " + Deal.ID_COLUMN + " = :id";
	
	private static final String DEAL_PRICE_AND_NAME = "SELECT " + Deal.CUSTOMER_DETAILS_COLUMN + ", " + Deal.PRICE_COLUMN + " FROM " +
			Deal.TABLE_NAME + " WHERE " + Deal.ID_COLUMN + " = :id";
	
	private static final String DEAL_SUPPLIER_ID = "SELECT " + Deal.SUPPLIER_ID_COLUMN + " FROM " + Deal.TABLE_NAME +
			" WHERE " + Deal.ID_COLUMN + " = :id";
	
	public Map<String, Float> dealDetailsAndPrice(Long dealId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", dealId);
		
		Map<String, Object> results = namedParameterJdbcTemplate.queryForMap(DEAL_PRICE_AND_NAME, parameters);
		
		Map<String, Float> detailsAndPrice = new HashMap<>();
		detailsAndPrice.put((String)results.get(Deal.CUSTOMER_DETAILS_COLUMN), ((BigDecimal)results.get(Deal.PRICE_COLUMN)).floatValue());
		
		return detailsAndPrice;
	}
	
	public Long dealSupplierId(Long dealId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", dealId);
		
		Long customerId = namedParameterJdbcTemplate.queryForObject(DEAL_SUPPLIER_ID, parameters, Long.class);
		return customerId;
	}
	public List<Deal> dealsWithMember(Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		
		List<Deal> results = namedParameterJdbcTemplate.query(DEALS_FOR_MEMBER, parameters, new DealRowMapper());
		
		return results;
	}
	
	public Boolean canUpdateDeal(Long dealId, Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("dealId", dealId);
		
		Boolean canUpdate = namedParameterJdbcTemplate.queryForObject(CAN_UPDATE_DEAL, parameters, Boolean.class);
		
		return canUpdate;
	}
	
	public boolean customerConfirmDeal(Long dealId, Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("dealId", dealId);
		
		int updates = namedParameterJdbcTemplate.update(CUSTOMER_CONFIRM_DEAL, parameters);
		return updates > 0;
	}
	
	public boolean supplierConfirmDeal(Long dealId, Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("dealId", dealId);
		
		int updates = namedParameterJdbcTemplate.update(SUPPLIER_CONFIRM_DEAL, parameters);
		return updates > 0;
	}
	
	public boolean customerUnconfirm(Long dealId, Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("dealId", dealId);
		parameters.addValue("memberId", memberId);
		
		int updates = namedParameterJdbcTemplate.update(CUSTOMER_UNCONFIRM, parameters);
		return updates > 0;
	}
	
	public boolean supplierUnconfirm(Long dealId, Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("dealId", dealId);
		parameters.addValue("memberId", memberId);
		
		int updates = namedParameterJdbcTemplate.update(SUPPLIER_UNCONFIRM, parameters);
		return updates > 0;
	}
	
	public boolean rejectDeal(Long dealId, Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("dealId", dealId);
		parameters.addValue("memberId", memberId);
		
		int updates = namedParameterJdbcTemplate.update(REJECT_DEAL, parameters);
		return updates > 0;
	}
	
	public boolean setDealPaidDate(Long dealId, LocalDateTime date) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", dealId);
		parameters.addValue("paidDate", date);
		
		int updates = namedParameterJdbcTemplate.update(SET_PAID, parameters);
		return updates > 0;
	}
	
	public boolean canAddInventory(Long memberId, Long dealId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("dealId", dealId);
		
		boolean canAdd = namedParameterJdbcTemplate.queryForObject(CAN_ADD_INVENTORY, parameters, Boolean.class);
		
		return canAdd;
	}
	
}
