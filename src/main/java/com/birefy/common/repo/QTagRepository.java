package com.birefy.common.repo;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.birefy.common.dto.topic.QTagWithTopic;
import com.birefy.common.entity.QTag;
import com.birefy.common.entity.QTopicTag;
import com.birefy.common.entity.utils.database.QTagRowMapper;
import com.birefy.common.entity.utils.database.QTagWithTopicRowMapper;

@Repository
public class QTagRepository extends CommonRepository<QTag> {
	private static final String FIND_ALL = "SELECT * FROM " + QTag.TABLE_NAME + " ORDER BY " + QTag.TAG_NAME_COLUMN;
	
	private static final String SEARCH_BY_TITLE = "SELECT * FROM " + QTag.TABLE_NAME + " WHERE LOWER("
			+ QTag.TAG_NAME_COLUMN + ") LIKE LOWER('%%%s%%') ORDER BY " + QTag.TAG_NAME_COLUMN;
	
	private static final String TAGS_FOR_TOPIC = "SELECT * FROM " + QTag.TABLE_NAME + " INNER JOIN "
			+ QTopicTag.TABLE_NAME + " ON " + QTopicTag.TAG_ID_COLUMN + " = " + QTag.ID_COLUMN + " WHERE "
			+ QTopicTag.TOPIC_ID_COLUMN + " = :topicId";
	
	private static final String TAGS_FOR_TOPICS = "SELECT " + 
			QTag.TABLE_NAME + "." + QTag.ID_COLUMN + ", " +
			QTag.TABLE_NAME + "." + QTag.TAG_NAME_COLUMN + ", " +
			QTopicTag.TABLE_NAME + "." + QTopicTag.TOPIC_ID_COLUMN + 
			" FROM " + QTag.TABLE_NAME + " INNER JOIN " + QTopicTag.TABLE_NAME + 
			" ON " + QTag.TABLE_NAME + "." + QTag.ID_COLUMN + " = " +
			QTopicTag.TABLE_NAME + "." + QTopicTag.TAG_ID_COLUMN + 
			" WHERE " + QTopicTag.TABLE_NAME + "." + QTopicTag.TOPIC_ID_COLUMN + 
			" IN (:topicIds)";
	
	private static final String KNOWN_TAGS = "SELECT * FROM " + QTag.TABLE_NAME + " WHERE " +
			QTag.TAG_NAME_COLUMN + " IN (:tagNames)";

	public List<QTag> findAll() {
		List<QTag> tags = jdbcTemplate.query(FIND_ALL, new QTagRowMapper());
		return tags;
	}

	public List<QTag> searchByTitle(String title) {
		List<QTag> tags = jdbcTemplate.query(String.format(SEARCH_BY_TITLE, title), new QTagRowMapper());
		return tags;
	}
	
	public List<QTag> getTagsForTopic(long topicId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("topicId", topicId);
		List<QTag> tags = namedParameterJdbcTemplate.query(TAGS_FOR_TOPIC, parameters, new QTagRowMapper());
		return tags;
	}
	
	public List<QTagWithTopic> getTagsForTopics(Set<Long> topicIds) {
		List<QTagWithTopic> results;
		if(CollectionUtils.isEmpty(topicIds)) {
			results = new ArrayList<>();
		} else {
			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("topicIds", topicIds);
			results = namedParameterJdbcTemplate.query(TAGS_FOR_TOPICS, parameters, new QTagWithTopicRowMapper());
		}
		
		return results;
	}
	
	public List<QTag> findExistingTags(List<String> tagNames) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tagNames", tagNames);
		List<QTag> existingTags = namedParameterJdbcTemplate.query(KNOWN_TAGS, parameters, new QTagRowMapper());
		return existingTags;
	}
}
