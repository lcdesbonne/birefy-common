package com.birefy.common.repo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.birefy.common.dto.topic.TopicContributor;
import com.birefy.common.entity.QMemberTopic;
import com.birefy.common.entity.utils.database.MemberTopicRowMapper;

@Repository
public class QMemberTopicRepository extends CommonRepository<QMemberTopic>{
	private static String FIND_CREATORS_OF_TOPIC = "SELECT * FROM " + 
			QMemberTopic.TABLE_NAME + " WHERE " + QMemberTopic.TOPIC_ID_COLUMN + " IN (:topicId)";
	
	private static final String REMOVE_CONTRIBUTORS_FROM_TOPIC = "DELETE FROM " + QMemberTopic.TABLE_NAME + 
			" WHERE " + QMemberTopic.TOPIC_ID_COLUMN + " = :topicId AND " + QMemberTopic.MEMBER_ID_COLUMN +
			" IN (:memberIds)";
	
	public List<QMemberTopic> findCreatorsForTopic(List<Long> topicIds) {
		List<QMemberTopic> memberAndTopics;
		if(CollectionUtils.isEmpty(topicIds)) {
			memberAndTopics = new ArrayList<>();
		} else {
			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("topicId", topicIds);
			
			memberAndTopics = namedParameterJdbcTemplate.query(FIND_CREATORS_OF_TOPIC, 
					parameters, new MemberTopicRowMapper());
		}
		
		return memberAndTopics;
	}
	
	public void deleteContributorsFromTopic(long topicId, List<Long> memberIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("topicId", topicId);
		parameters.addValue("memberIds", memberIds);
		
		namedParameterJdbcTemplate.update(REMOVE_CONTRIBUTORS_FROM_TOPIC, parameters);
	}

}
