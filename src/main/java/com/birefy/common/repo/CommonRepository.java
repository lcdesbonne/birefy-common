package com.birefy.common.repo;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.birefy.common.entity.CommonEntity;
import com.birefy.common.entity.utils.database.MemberRowMapper;

@Repository
public class CommonRepository<T extends CommonEntity> {
	public static Logger LOGGER = LoggerFactory.getLogger(CommonRepository.class);
	
	protected static final String INSERT_STATEMENT = "INSERT INTO %s (%s) VALUES (%s)";
	protected static final String UPDATE_STATEMENT = "UPDATE %s SET %s WHERE id = %d";
	private static final String ID_COLUMN = "id";
	
	@Autowired
	@Qualifier("jdbcTemplate")
	protected JdbcTemplate jdbcTemplate;
	@Autowired
	@Qualifier("namedParameterJdbcTemplate")
	protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	/**
	 * Constructs an insert statement for the given entity class
	 * @param entity
	 */
	public void addNewRecord(T entity) throws NoSuchFieldException, IllegalAccessException {
		StringBuilder columnList = new StringBuilder();
		StringBuilder valueList = new StringBuilder();
		String tableName = entity.getTableName();
		
		populateColumnAndValueLists(entity, columnList, valueList);
		
		String query = String.format(INSERT_STATEMENT, tableName, columnList, valueList);
		
		//Execute insert statement
		jdbcTemplate.execute(query);
		
		LOGGER.info("Added new record into table: "+entity.getTableName());		
	}
	
	public void update(T entity) throws IllegalAccessException, NoSuchFieldException {
		Field idField = entity.getClass().getDeclaredField("id");
		idField.setAccessible(true);
		Long entityId = (Long) idField.get(entity);
		String tableName = entity.getTableName();
		String setClause = generateSetClause(entity.getColumnToFieldMap());
		String query = String.format(UPDATE_STATEMENT, tableName, setClause, entityId);
		
		jdbcTemplate.update(query);
		
		LOGGER.info("Updated table: "+ entity.getTableName());
	}
	
	protected void populateColumnAndValueLists(T entity, StringBuilder columnList, StringBuilder valueList) throws IllegalAccessException{
		Map<String, String> propertiesMap = entity.getColumnToFieldMap();
		
		Set<String> columns = propertiesMap.keySet();
		for (String column : columns) {
			if (propertiesMap.get(column) != null) {
				valueList.append(propertiesMap.get(column)).append(",");
				columnList.append(column).append(",");
			}
		}
		
		//Remove trailing ','
		columnList.deleteCharAt(columnList.lastIndexOf(","));
		valueList.deleteCharAt(valueList.lastIndexOf(","));
	}
	
	protected String generateSetClause(Map<String, String> propertiesMap) {
		Set<String> columns = propertiesMap.keySet();
		StringBuilder setStatementBuilder = new StringBuilder();
		for (String column : columns) {
			if (column.equalsIgnoreCase(ID_COLUMN)) {
				continue;
			}
			
			setStatementBuilder.append(column+"="+propertiesMap.get(column)+",");
		}
		setStatementBuilder.deleteCharAt(setStatementBuilder.lastIndexOf(","));
		return setStatementBuilder.toString();
	}

}
