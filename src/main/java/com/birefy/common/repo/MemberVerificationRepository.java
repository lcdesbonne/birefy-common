package com.birefy.common.repo;

import java.sql.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.entity.MemberVerification;
import com.birefy.common.entity.utils.database.MemberVerificationRowMapper;

@Repository
public class MemberVerificationRepository extends CommonRepository<MemberVerification> {
	public static final Logger LOGGER = LoggerFactory.getLogger(MemberVerificationRepository.class);
	
	private static final String UPDATE = "UPDATE "+MemberVerification.TABLE_NAME+" SET "+MemberVerification.TOKEN_COLUMN+" = :token ,"
			+ MemberVerification.CREATION_DATE_COLUMN+" = :date WHERE "+MemberVerification.MEMBER_COLUMN + " = :memberId";
	
	private static final String GET_VERIFICATION_FOR_MEMBER = "SELECT * FROM " + MemberVerification.TABLE_NAME + " WHERE " + MemberVerification.MEMBER_COLUMN + " = :memberId";
	
	private static final String GET_VERIFICATION_BY_TOKEN = "SELECT * FROM " + MemberVerification.TABLE_NAME
			+ "WHERE " + MemberVerification.TOKEN_COLUMN + " = :token";
	
	public Optional<MemberVerification> getVerificationDetails(long memberId) {
		Optional<MemberVerification> result = Optional.empty();
		try {
			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("memberId", memberId);
			MemberVerification res = namedParameterJdbcTemplate.queryForObject(GET_VERIFICATION_FOR_MEMBER, parameters, new MemberVerificationRowMapper());
			
			result = Optional.of(res);
		} catch (IncorrectResultSizeDataAccessException e) {
			LOGGER.debug("Verification details for member: " + memberId + " not found.");
		}
		
		return result;
	}
	
	public Optional<MemberVerification> getVerificationByToken(String token) {
		Optional<MemberVerification> result = Optional.empty();
		
		try {
			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("token", token);
			MemberVerification res = namedParameterJdbcTemplate.queryForObject(GET_VERIFICATION_BY_TOKEN, parameters, new MemberVerificationRowMapper());
			
			result = Optional.of(res);
		} catch (IncorrectResultSizeDataAccessException e) {
			LOGGER.debug("Verification details for token: " + token + " not found");
		}
		
		return result;
	}
	
	@Override
	public void update(MemberVerification memberVerification) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("token", memberVerification.getToken());
		parameters.addValue("date", Date.valueOf(memberVerification.getCreationDate()));
		parameters.addValue("memberId", memberVerification.getMemberId());
		
		namedParameterJdbcTemplate.update(UPDATE, parameters);
	}
}
