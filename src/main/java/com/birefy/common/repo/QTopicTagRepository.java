package com.birefy.common.repo;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.entity.QTopicTag;

@Repository
public class QTopicTagRepository extends CommonRepository<QTopicTag> {
	private static final String DELETE_TAG_FROM_TOPIC = "DELETE FROM " + QTopicTag.TABLE_NAME + "WHERE "
			+ QTopicTag.TOPIC_ID_COLUMN + " = :topicId " + QTopicTag.TAG_ID_COLUMN + " IN :tagIds";

	public void deleteTagsFromTopic(List<Long> tagIds, Long topicId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tagId", tagIds);
		parameters.addValue("topicId", topicId);
		
		namedParameterJdbcTemplate.update(DELETE_TAG_FROM_TOPIC, parameters);
	}

}
