package com.birefy.common.repo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.entity.MemberBeneficiary;

@Repository
public class MemberBeneficiaryRepository extends CommonRepository<MemberBeneficiary>{
	private static final String FIND_BENEFICIARY_IDS_FOR_MEMBER = "SELECT " + MemberBeneficiary.BENEFICIARY_ID_COLUMN +
			" FROM " + MemberBeneficiary.TABLE_NAME + " WHERE " + MemberBeneficiary.MEMBER_ID_COLUMN + " = :id";
	
	public List<String> findBeneficiaryIdsUsedByMember(Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource(); 
		parameters.addValue("id", memberId);
		
		List<String> results = namedParameterJdbcTemplate.queryForList(FIND_BENEFICIARY_IDS_FOR_MEMBER, parameters, String.class);
		
		return results == null ? new ArrayList<String>() : results;
	}
}
