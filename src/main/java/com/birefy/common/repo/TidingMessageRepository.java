package com.birefy.common.repo;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.birefy.common.dto.tiding.TidingAndMemberID;
import com.birefy.common.dto.tiding.TidingIdDTO;
import com.birefy.common.dto.tiding.TidingIsGroup;
import com.birefy.common.entity.tiding.BlockedList;
import com.birefy.common.entity.tiding.MemberChannel;
import com.birefy.common.entity.tiding.Message;
import com.birefy.common.entity.utils.database.IsChannelGroupMapper;
import com.birefy.common.entity.utils.database.MemberChannelRowMapper;
import com.birefy.common.entity.utils.database.MessageRowMapper;
import com.birefy.common.entity.utils.database.TidingAndMemberIdRowMapper;
import com.birefy.common.entity.utils.database.TidingIdRowMapper;
import com.birefy.common.tidings.Tiding;
import com.birefy.common.tidings.Tiding.MessageType;

@Repository
public class TidingMessageRepository {
	private final Logger LOGGER = LoggerFactory.getLogger(TidingMessageRepository.class);
	
	private static final String AUTHORITY_SEARCH_CLAUSE = "(" + MemberChannel.MEMBER_AUTHORITY_COLUMN + " > 0 OR " + MemberChannel.ID_COLUMN + " IS NULL)";
	
	private static final String ADD_MEMBER_CHANNEL = "INSERT INTO " + MemberChannel.TABLE_NAME + " ("+MemberChannel.MEMBER_ID_COLUMN+","
	+MemberChannel.TIDING_ID_COLUMN+","+MemberChannel.ACTIVE_COLUMN+","+ MemberChannel.MEMBER_AUTHORITY_COLUMN + ") "
			+ "VALUES (:memberId, :tidingId, :active, 0)";
	
	private static final String ADD_GROUP_CHANNEL = "INSERT INTO " + MemberChannel.TABLE_NAME + "("+MemberChannel.TIDING_ID_COLUMN+","
			+MemberChannel.ACTIVE_COLUMN+") VALUES (:tidingId, :active)";
	
	private static final String FIND_CHANNEL_FOR_MEMBER = "SELECT * FROM "+MemberChannel.TABLE_NAME+" WHERE "+MemberChannel.MEMBER_ID_COLUMN+
			" IN (:memberIds)";
	
	private static final String FIND_CHANNEL_BY_CHANNEL_ID = "SELECT * FROM "+MemberChannel.TABLE_NAME+" WHERE "+MemberChannel.ID_COLUMN+
			" = :channelId)";
	
	private static final String FIND_CHANNELS_BY_CHANNEL_IDS = "SELECT * FROM " + MemberChannel.TABLE_NAME + " WHERE " + MemberChannel.ID_COLUMN +
			" IN (:channelIds)";
	
	private static final String ALTER_MEMBER_ACTIVE = "UPDATE "+MemberChannel.TABLE_NAME+" SET "+MemberChannel.ACTIVE_COLUMN+" = "+
			":active WHERE "+MemberChannel.ID_COLUMN+" IN (:channelIds)";
	
	private static final String ADD_MESSAGE = "INSERT INTO "+Message.TABLE_NAME+" ("+Message.CONTENT_COLUMN+","+Message.CREATOR_COLUMN+","
			+ Message.RECIPIENT_COLUMN + "," + Message.CREATION_DATE_COLUMN+") VALUES (:content, :creator, :recipient, :creationDate)";
	
	private static final String ADD_TO_BLOCKED = "INSERT INTO "+BlockedList.TABLE_NAME+" ("+BlockedList.BLOCKED_BY_COLUMN+","+BlockedList.BLOCKED_COLUMN+")"
			+ " VALUES (:blockedBy, :blocked)";
	
	private static final String UPDATE_BLOCKED = "UPDATE "+BlockedList.TABLE_NAME+" SET "+BlockedList.BLOCKED_COLUMN+" = :blockedTidingId WHERE "
			+BlockedList.BLOCKED_BY_COLUMN + " = :blockedByTidingId";
	
	private static final String USERS_BLOCKED_LIST = "SELECT "+BlockedList.BLOCKED_COLUMN+" FROM " + BlockedList.TABLE_NAME + " WHERE "
			+ BlockedList.BLOCKED_BY_COLUMN + " = :blockedByTidingId";
	
	private static final String IS_USER_ACTIVE = "SELECT "+MemberChannel.ACTIVE_COLUMN+" FROM "+MemberChannel.TABLE_NAME+" WHERE "+
			MemberChannel.ID_COLUMN + " = :channelId";
	
	private static final String RETRIEVE_MESSAGES = "SELECT * FROM " + Message.TABLE_NAME + " WHERE (" + Message.CREATOR_COLUMN +
			" = :channelId OR " + Message.RECIPIENT_COLUMN + " = :channelId) AND " + Message.CREATION_DATE_COLUMN + " >= :from AND " +
			Message.CREATION_DATE_COLUMN + " <= :to AND "+Message.CREATOR_COLUMN + " NOT IN ("+USERS_BLOCKED_LIST+") AND " +
			Message.RECIPIENT_COLUMN + " NOT IN ("+USERS_BLOCKED_LIST+")";
	
	private static final String RETRIEVE_CONVERSATION = "SELECT * FROM " + Message.TABLE_NAME + " WHERE (" + Message.CREATOR_COLUMN + " = :sender OR " + Message.RECIPIENT_COLUMN +
			" = :sender) AND (" + Message.CREATOR_COLUMN + " = :receiver OR " + Message.RECIPIENT_COLUMN + " = :receiver ) AND " +
			Message.CREATOR_COLUMN + " NOT IN ("+USERS_BLOCKED_LIST+") AND "+Message.RECIPIENT_COLUMN+" NOT IN ("+USERS_BLOCKED_LIST+")";
	
	private static final String IS_GROUP = "SELECT " + MemberChannel.ID_COLUMN + "," + MemberChannel.MEMBER_ID_COLUMN + " IS NULL FROM " + MemberChannel.TABLE_NAME +
			" WHERE " + MemberChannel.ID_COLUMN + " IN (:channelIds)";
	
	private static final String TIDING_ID_IS_GROUP = "SELECT " + MemberChannel.MEMBER_ID_COLUMN + " IS NULL FROM " + MemberChannel.TABLE_NAME + 
			" WHERE " + MemberChannel.TIDING_ID_COLUMN + " = :tidingId";
	
	private static final String SEARCH_USERS = "SELECT " + MemberChannel.TIDING_ID_COLUMN +", " + MemberChannel.ID_COLUMN+ " FROM " + MemberChannel.TABLE_NAME +
			" WHERE lower(" + MemberChannel.TIDING_ID_COLUMN + ") LIKE lower(:tidingId) AND " + MemberChannel.ID_COLUMN +
			" NOT IN ("+USERS_BLOCKED_LIST+") AND " + MemberChannel.ACTIVE_COLUMN + " AND " + AUTHORITY_SEARCH_CLAUSE + " LIMIT 15";
	
	private static final String SEARCH_USERS_LIKE_TIDING = "SELECT " + MemberChannel.TIDING_ID_COLUMN + ", " + MemberChannel.ID_COLUMN + " FROM " + MemberChannel.TABLE_NAME +
			" WHERE lower(" + MemberChannel.TIDING_ID_COLUMN + ") LIKE lower(:tidingId) AND " + MemberChannel.ACTIVE_COLUMN + " AND " + AUTHORITY_SEARCH_CLAUSE + " LIMIT 15";
	
	private static final String GET_TIDING_IDS = "SELECT " + MemberChannel.TIDING_ID_COLUMN + ", " + MemberChannel.ID_COLUMN + " FROM " + MemberChannel.TABLE_NAME + 
			" WHERE " + MemberChannel.ID_COLUMN + " IN (:channelIds)"; 
	
	private static final String GET_TIDING_ID_FROM_MEMBER_ID = "SELECT " + MemberChannel.TIDING_ID_COLUMN + " FROM " + MemberChannel.TABLE_NAME +
			" WHERE " + MemberChannel.MEMBER_ID_COLUMN + " = :memberId";
	
	private static final String GET_ACTIVE_MEMBER_ID_AND_TIDING_ID = "SELECT " + MemberChannel.TIDING_ID_COLUMN + ", " + MemberChannel.MEMBER_ID_COLUMN + 
			" FROM " + MemberChannel.TABLE_NAME + " WHERE " + MemberChannel.ACTIVE_COLUMN + " AND " + MemberChannel.MEMBER_ID_COLUMN + " IS NOT NULL" +
			" AND " + MemberChannel.MEMBER_ID_COLUMN + " IN (:memberIds)";
	
	private static final String FIND_MEMBERS_LIKE_TIDING_ID = "SELECT " + MemberChannel.TIDING_ID_COLUMN + ", " + MemberChannel.MEMBER_ID_COLUMN +
			" FROM " + MemberChannel.TABLE_NAME + " WHERE " + MemberChannel.ACTIVE_COLUMN + " AND " + MemberChannel.MEMBER_ID_COLUMN + " IS NOT NULL" +
			" AND LOWER(" + MemberChannel.TIDING_ID_COLUMN + ") LIKE LOWER(:tidingId) AND " + AUTHORITY_SEARCH_CLAUSE;
	
	private static final String TIDING_EXISTS = "SELECT " + MemberChannel.TIDING_ID_COLUMN + " FROM " + MemberChannel.TABLE_NAME + " WHERE " + MemberChannel.TIDING_ID_COLUMN +
			" IN (:tidingIds)";
	
	private static final String UPDATE_TIDING_ID = "UPDATE " + MemberChannel.TABLE_NAME + " SET " + MemberChannel.TIDING_ID_COLUMN + " = :tidingId WHERE " + MemberChannel.MEMBER_ID_COLUMN +
			" = :memberId";
	
	@Autowired
	@Qualifier("tidingsNamedParameterJdbcTemplate")
	private NamedParameterJdbcTemplate tidingsNamedParameterJdbcTemplate;
	
	@Autowired
	@Qualifier("tidingsJdbcTemplate")
	private JdbcTemplate tidingsJdbcTemplate;
	
	public List<MemberChannel> findChannelForMembers(List<Long> memberIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberIds", memberIds);
		
		List<MemberChannel> memberDetails = tidingsNamedParameterJdbcTemplate.query(FIND_CHANNEL_FOR_MEMBER,
				parameters, new MemberChannelRowMapper());
		
		return memberDetails;
	}
	
	public MemberChannel findChannelByChannelId(Long channelId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("channelId", channelId);
		
		MemberChannel memberDetails = tidingsNamedParameterJdbcTemplate.queryForObject(FIND_CHANNEL_BY_CHANNEL_ID, parameters,
				new MemberChannelRowMapper());
		
		return memberDetails;
	}
	
	public List<MemberChannel> findChannelsByChannelIds(List<Long> channelIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("channelIds", channelIds);
		
		List<MemberChannel> memberDetails = tidingsNamedParameterJdbcTemplate.query(FIND_CHANNELS_BY_CHANNEL_IDS, parameters, new MemberChannelRowMapper());
		
		return memberDetails;
	}
	
	public void addMemberToTidings(MemberChannel memberChannel) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberChannel.getMemberId());
		parameters.addValue("tidingId", memberChannel.getTidingId());
		parameters.addValue("active", memberChannel.isActive());
		
		int updates = tidingsNamedParameterJdbcTemplate.update(ADD_MEMBER_CHANNEL, parameters);
		if(updates != 1) {
			throw new RuntimeException("Failed to add tiding details for user " + memberChannel.getMemberId() + 
					" with tiding id " + memberChannel.getTidingId());
		}
	}
	
	public void addGroupToTidings(MemberChannel memberChannel) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tidingId", memberChannel.getTidingId());
		parameters.addValue("active", memberChannel.isActive());
		
		tidingsNamedParameterJdbcTemplate.update(ADD_GROUP_CHANNEL, parameters);
	}
	
	public void alterMemberActive(List<Long> channelIds, boolean active) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tidingIds", channelIds);
		parameters.addValue("active", active);
		
		tidingsNamedParameterJdbcTemplate.update(ALTER_MEMBER_ACTIVE, parameters);
	}
	
	public void addNewMessage(Message message) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("content", message.getContent());
		parameters.addValue("creator", message.getCreator());
		parameters.addValue("recipient", message.getRecipient());
		parameters.addValue("creationDate", message.getCreationDate());
		
		tidingsNamedParameterJdbcTemplate.update(ADD_MESSAGE, parameters);
	}
	
	public void addToBlocked(BlockedList blockedList) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("blockedBy", blockedList.getBlockedBy());
		parameters.addValue("blocked", blockedList.getBlocked());
		
		tidingsNamedParameterJdbcTemplate.update(ADD_TO_BLOCKED, parameters);
	}
	
	public void updateBlocked(BlockedList blockedList) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("blockedTidingId", blockedList.getBlocked());
		parameters.addValue("blockedByTidingId", blockedList.getBlockedBy());
		
		tidingsNamedParameterJdbcTemplate.update(UPDATE_BLOCKED, parameters);
	}
	
	public String usersBlockedList(String tidingId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("blockedByTidingId", tidingId);
		
		String blockedListAsString = tidingsNamedParameterJdbcTemplate.queryForObject(USERS_BLOCKED_LIST, parameters, String.class);
		return blockedListAsString;
	}
	
	public boolean isUserActive(Long channelId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("channelId", channelId);
		
		boolean active = tidingsNamedParameterJdbcTemplate.queryForObject(IS_USER_ACTIVE, parameters, Boolean.class);
		
		return active;
	}
	
	public Set<Message> retrieveMessagesForUser(Long channelId, LocalDateTime from, LocalDateTime to) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("channelId", channelId);
		parameters.addValue("from", from.toString());
		parameters.addValue("to", to.toString());
		parameters.addValue("blockedByTidingId", channelId);
		
		List<Message> messages = tidingsNamedParameterJdbcTemplate.query(RETRIEVE_MESSAGES, parameters, new MessageRowMapper());
		
		return new TreeSet<Message>(messages);
	}
	
	public List<TidingIsGroup> isGroup(Set<Long> channelIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("channelIds", channelIds);
		
		List<TidingIsGroup> isGroup = tidingsNamedParameterJdbcTemplate.query(IS_GROUP, parameters, new IsChannelGroupMapper());
		
		return isGroup;
	}
	
	public boolean isGroup(String tidingId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tidingId", tidingId);
		
		boolean isGroup = tidingsNamedParameterJdbcTemplate.queryForObject(TIDING_ID_IS_GROUP, parameters, Boolean.class);
		
		return isGroup;
	}
	
	public List<TidingIdDTO> findTidingIds(String tidingId, Long requestedByTidingId) {
		//TODO restrict this to lord users and above
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tidingId", "%"+tidingId+"%");
		parameters.addValue("blockedByTidingId", requestedByTidingId);
		
		List<TidingIdDTO> tidings = tidingsNamedParameterJdbcTemplate.query(SEARCH_USERS, parameters, new TidingIdRowMapper());
		
		return tidings;
	}
	
	public List<TidingIdDTO> findTidingIdsLikeTiding(String tidingId) {
		//TODO restrict this to only search for lord users and above
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tidingId", "%"+tidingId+"%");
		
		List<TidingIdDTO> tidings = tidingsNamedParameterJdbcTemplate.query(SEARCH_USERS_LIKE_TIDING, parameters, new TidingIdRowMapper());
		
		return tidings;
	}
	
	public TidingIdDTO findTidingId(String tidingId, Long requestedByChannelId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tidingId", tidingId);
		parameters.addValue("blockedByTidingId", requestedByChannelId);
		
		TidingIdDTO tiding = tidingsNamedParameterJdbcTemplate.queryForObject(SEARCH_USERS, parameters, new TidingIdRowMapper());
		return tiding;
	}
	
	public List<Tiding> findConversation(Long userTidingId, Long withTidingId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("sender", userTidingId);
		parameters.addValue("receiver", withTidingId);
		parameters.addValue("blockedByTidingId", userTidingId);
		
		List<Message> messages = tidingsNamedParameterJdbcTemplate.query(RETRIEVE_CONVERSATION, parameters, new MessageRowMapper());
		
		//List all the channelIds
		Set<Long> channelIds = messages.stream().map(msg -> msg.getCreator()).collect(Collectors.toSet());
		channelIds.addAll(messages.stream().map(msg -> msg.getRecipient()).collect(Collectors.toSet()));
		
		List<TidingIdDTO> tidingIds = findTidingIdsByChannelId(channelIds.stream().collect(Collectors.toList()));
		
		Map<Long, String> channelIdToTidingIdMap = tidingIds.stream().collect(Collectors.toMap(TidingIdDTO::getChannelId, TidingIdDTO::getTidingId));
		
		List<Tiding> tidings = messages.stream().map(mes -> {
			return new Tiding(MessageType.CHAT, mes.getContent(), new TidingIdDTO(
					channelIdToTidingIdMap.get(mes.getCreator()), mes.getCreator()));
		}).collect(Collectors.toList());
		
		return tidings;
	}
	
	public List<TidingIdDTO> findTidingIdsByChannelId(List<Long> channelIds) {
		List<TidingIdDTO> tidingIds;
		if(!channelIds.isEmpty()) {
			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("channelIds", channelIds);
			
			tidingIds = tidingsNamedParameterJdbcTemplate.query(GET_TIDING_IDS, parameters, new TidingIdRowMapper());
		} else {
			tidingIds = new ArrayList<>();
		}

		return tidingIds;
	}
	
	public Map<String, Boolean> tidingIdsExist(List<String> tidingIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tidingIds", tidingIds);
		
		List<String> foundIds = tidingsNamedParameterJdbcTemplate.queryForList(TIDING_EXISTS, parameters, String.class);
		
		Map<String, Boolean> tidingIdsMap = new HashMap<>();
		
		for (String tidingId : tidingIds) {
			tidingIdsMap.put(tidingId, foundIds.contains(tidingId));
		}
		
		return tidingIdsMap;
	}
	
	public boolean updateTidingIdForMember(String tidingId, long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tidingId", tidingId);
		parameters.addValue("memberId", memberId);
		
		int updates = tidingsNamedParameterJdbcTemplate.update(UPDATE_TIDING_ID, parameters);
		LOGGER.info("Updated " + updates + " rows with new tiding Id " + tidingId);
		return updates == 1;
	}
	
	public String getTidingIdForMember(Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		
		String tidingId = tidingsNamedParameterJdbcTemplate.queryForObject(GET_TIDING_ID_FROM_MEMBER_ID, parameters, String.class);
		
		return tidingId;
	}
	
	public List<TidingAndMemberID> getAllActiveMemberAndTidingIds(Set<Long> memberIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberIds", memberIds);
		List<TidingAndMemberID> result = CollectionUtils.isEmpty(memberIds) ? new ArrayList<>() : tidingsNamedParameterJdbcTemplate.query(GET_ACTIVE_MEMBER_ID_AND_TIDING_ID, parameters, new TidingAndMemberIdRowMapper());
		return result;
	}
	
	public List<TidingAndMemberID> findActiveMembersByTidingId(String tidingId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tidingId", "%"+tidingId+"%");
		List<TidingAndMemberID> result = tidingsNamedParameterJdbcTemplate.query(FIND_MEMBERS_LIKE_TIDING_ID, parameters, new TidingAndMemberIdRowMapper());
		return result;
	}
	
}
