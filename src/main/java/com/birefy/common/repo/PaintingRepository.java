package com.birefy.common.repo;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.dto.painting.PaintingNameAndLocation;
import com.birefy.common.dto.painting.PaintingShort;
import com.birefy.common.entity.Painting;
import com.birefy.common.entity.PaintingDescriptiveTag;
import com.birefy.common.entity.utils.database.PaintingNameAndLocationMapper;
import com.birefy.common.entity.utils.database.PaintingRowMapper;

@Repository
public class PaintingRepository extends CommonRepository<Painting> {
	private static final String KEY_COLUMNS = Painting.ID_COLUMN + "," + Painting.CATEGORY_COLUMN +
			"," + Painting.LOCATION_COLUMN + "," + Painting.NAME_COLUMN + "," + Painting.MEMBER_COLUMN;
	
	private static final String RECENT_PAINTINGS_BY_CATEGORY = "SELECT " + KEY_COLUMNS + " FROM " + Painting.TABLE_NAME +
			" WHERE " + Painting.CATEGORY_COLUMN + " = :category ORDER BY "+ Painting.CREATION_DATE_COLUMN + " DESC ";
	
	private static final String RECENT_PAINTINGS_BY_CATEGORY_TAGS = "SELECT " + KEY_COLUMNS + " FROM " + Painting.TABLE_NAME +
			" INNER JOIN " + PaintingDescriptiveTag.TABLE_NAME + " ON " + Painting.ID_COLUMN + " = " + PaintingDescriptiveTag.PAINTING_COLUMN +
			" WHERE " + Painting.CATEGORY_COLUMN + " = :category AND " + PaintingDescriptiveTag.TAG_COLUMN + " IN (:tagIds) " +
			" GROUP BY "+ KEY_COLUMNS + " ORDER BY "+ Painting.CREATION_DATE_COLUMN + " DESC ";
			
	private static final String PAINTINGS_BY_MEMBER_CATEGORY = "SELECT " + KEY_COLUMNS + " FROM " + Painting.TABLE_NAME + 
			" WHERE " + Painting.MEMBER_COLUMN + " = :memberId AND " + Painting.CATEGORY_COLUMN + " = :category";
	
	private static final String PAINTINGS_BY_MEMBER_CATEGORY_TAGS = "SELECT " + KEY_COLUMNS + " FROM " + Painting.TABLE_NAME + 
			" INNER JOIN " + PaintingDescriptiveTag.TABLE_NAME + " ON " + Painting.ID_COLUMN + " = " + PaintingDescriptiveTag.PAINTING_COLUMN +
			" WHERE " + Painting.MEMBER_COLUMN + " = :memberId AND " + Painting.CATEGORY_COLUMN + " = :category" +
			" AND " + PaintingDescriptiveTag.TAG_COLUMN + " IN (:tagids)" +
			" GROUP BY " + KEY_COLUMNS;
	
	private static final String PAINTINGS_BY_MEMBER = "SELECT " + KEY_COLUMNS + ", " + Painting.PRICE_COLUMN + " FROM " + Painting.TABLE_NAME +
			" WHERE " + Painting.MEMBER_COLUMN + " = :memberId";
	
	private static final String PAINTINGS_BY_MEMBER_TAGS = "SELECT " + KEY_COLUMNS + " FROM " + Painting.TABLE_NAME +
			" INNER JOIN " + PaintingDescriptiveTag.TABLE_NAME + " ON " + Painting.ID_COLUMN + " = " + PaintingDescriptiveTag.PAINTING_COLUMN +
			" WHERE " + Painting.MEMBER_COLUMN + " = :memberId AND " + PaintingDescriptiveTag.TAG_COLUMN + " IN (:tagIds)" +
			" GROUP BY " + KEY_COLUMNS;
	
	private static final String PAINTINGS_BY_MEMBER_AND_TITLE = "SELECT " + KEY_COLUMNS + " FROM " + Painting.TABLE_NAME +
			" WHERE " + Painting.MEMBER_COLUMN + " = :memberId AND lower(" + Painting.NAME_COLUMN + ") LIKE lower(:name)";
	
	private static final String PAINTINGS_BY_MEMBER_AND_TITLE_TAGS = "SELECT " + KEY_COLUMNS + " FROM " + Painting.TABLE_NAME +
			" INNER JOIN " + PaintingDescriptiveTag.TABLE_NAME + " ON " + Painting.ID_COLUMN + " = " + PaintingDescriptiveTag.PAINTING_COLUMN +
			" WHERE " + Painting.MEMBER_COLUMN + " = :memberId AND lower(" + Painting.NAME_COLUMN + ") LIKE lower(:name)" +
			" AND " + PaintingDescriptiveTag.TAG_COLUMN + " IN (:tagIds)" +
			" GROUP BY " + KEY_COLUMNS;
	
	private static final String FIND_PAINTING_BY_NAME_MEMBER_CATEGORY = "SELECT " + KEY_COLUMNS + " FROM " + Painting.TABLE_NAME +
			" WHERE lower(" + Painting.NAME_COLUMN + ") LIKE lower(:name) AND " + Painting.CATEGORY_COLUMN + " = :category " +
			" AND "+ Painting.MEMBER_COLUMN + " :memberId";
	
	private static final String FIND_PAINTING_BY_NAME_MEMBER_CATEGORY_TAGS = "SELECT " + KEY_COLUMNS + " FROM " + Painting.TABLE_NAME +
			" INNER JOIN " + PaintingDescriptiveTag.TABLE_NAME + " ON " + Painting.ID_COLUMN + " = " + PaintingDescriptiveTag.PAINTING_COLUMN +
			" WHERE lower(" + Painting.NAME_COLUMN + ") LIKE lower(:name) AND " + Painting.CATEGORY_COLUMN + " = :category " +
			" AND "+ Painting.MEMBER_COLUMN + " :memberId AND " + PaintingDescriptiveTag.TAG_COLUMN + " IN (:tagIds)" +
			" GROUP BY " + KEY_COLUMNS;
	
	private static final String FIND_PAINTING_BY_NAME_AND_CATEGORY = "SELECT " + KEY_COLUMNS + " FROM " + Painting.TABLE_NAME + 
			" WHERE lower(" + Painting.NAME_COLUMN + ") LIKE lower(:name) AND " + Painting.CATEGORY_COLUMN + " = :category ";
	
	private static final String FIND_PAINTING_BY_NAME_AND_CATEGORY_TAGS = "SELECT " + KEY_COLUMNS + " FROM " + Painting.TABLE_NAME +
			" INNER JOIN " + PaintingDescriptiveTag.TABLE_NAME + " ON " + Painting.ID_COLUMN + " = " + PaintingDescriptiveTag.PAINTING_COLUMN + 
			" WHERE lower(" + Painting.NAME_COLUMN + ") LIKE lower(:name) AND " + Painting.CATEGORY_COLUMN + " = :category " +
			" AND " + PaintingDescriptiveTag.TAG_COLUMN + " IN (:tagIds)" +
			" GROUP BY " + KEY_COLUMNS;
	
	private static final String FIND_PAINTING_BY_NAME = "SELECT " + KEY_COLUMNS + " FROM " + Painting.TABLE_NAME + 
			" WHERE lower(" + Painting.NAME_COLUMN + ") LIKE lower(:name)";
	
	private static final String FIND_PAINTING_BY_NAME_TAGS = "SELECT " + KEY_COLUMNS + " FROM " + Painting.TABLE_NAME + 
			" INNER JOIN " + PaintingDescriptiveTag.TABLE_NAME + " ON " + Painting.ID_COLUMN + " = " + PaintingDescriptiveTag.PAINTING_COLUMN +
			" WHERE lower(" + Painting.NAME_COLUMN + ") LIKE lower(:name)" +
			" AND " + PaintingDescriptiveTag.TAG_COLUMN + " IN (:tagIds)" +
			" GROUP BY " + KEY_COLUMNS;
	
	private static final String ALL_RECENT_PAINTINGS = "SELECT " + KEY_COLUMNS + " FROM " + Painting.TABLE_NAME + 
			" ORDER BY " + Painting.CREATION_DATE_COLUMN + " DESC ";
	
	private static final String FIND_PAINTINGS_FOR_REVIEW = "SELECT " + KEY_COLUMNS + " FROM " + Painting.TABLE_NAME +
			" WHERE " + Painting.REVIEW_COLUMN + " ORDER BY " + Painting.CREATION_DATE_COLUMN + " DESC ";
	
	private static final String ALL_RECENT_PAINTINGS_TAGS = "SELECT " + KEY_COLUMNS + " FROM " + Painting.TABLE_NAME + " INNER JOIN " +
			PaintingDescriptiveTag.TABLE_NAME + " ON " + Painting.TABLE_NAME + "." + Painting.ID_COLUMN + " = " + PaintingDescriptiveTag.TABLE_NAME + "." + PaintingDescriptiveTag.PAINTING_COLUMN
			+ " WHERE " + PaintingDescriptiveTag.TABLE_NAME + "." + PaintingDescriptiveTag.TAG_COLUMN + " IN (:tagIds)" + 
			" GROUP BY " + KEY_COLUMNS;
	
	private static final String GET_PAINTING_BY_ID = "SELECT " + KEY_COLUMNS + ", " + Painting.PRICE_COLUMN + " FROM " + Painting.TABLE_NAME +
			" WHERE " + Painting.ID_COLUMN + " = :id";
	
	private static final String GET_PAINTING_LOCATION = "SELECT " + Painting.LOCATION_COLUMN + " FROM " + Painting.TABLE_NAME +
			" WHERE " + Painting.ID_COLUMN + " = :id";
	
	private static final String FIND_PAINTERS = "SELECT " + Painting.MEMBER_COLUMN + " FROM " + Painting.TABLE_NAME + 
			" WHERE " + Painting.MEMBER_COLUMN + " IN (:memberIds)";
	
	private static final String UPDATE_PAINTING_NAME_AND_CATEGORY = "UPDATE " + Painting.TABLE_NAME + " SET " + Painting.NAME_COLUMN +
			" = :name, " + Painting.CATEGORY_COLUMN + " = :category WHERE " + Painting.ID_COLUMN + " = :id";
	
	private static final String UPDATE_PAINTING_NAME_AND_CATEGORY_AND_PRICE = "UPDATE " + Painting.TABLE_NAME + " SET " + Painting.NAME_COLUMN +
			" = :name, " + Painting.CATEGORY_COLUMN + " = :category, " + Painting.PRICE_COLUMN + " = :price WHERE " + Painting.ID_COLUMN + " = :id";
		
	private static final String UPDATE_PAINTING_NAME = "UPDATE " + Painting.TABLE_NAME + " SET " + Painting.NAME_COLUMN +
			" = :name WHERE " + Painting.ID_COLUMN + " = :id";
	
	private static final String UPDATE_PAINTING_NAME_AND_PRICE = "UPDATE " + Painting.TABLE_NAME + " SET " + Painting.NAME_COLUMN +
			" = :name, " + Painting.PRICE_COLUMN + " = :price WHERE " + Painting.ID_COLUMN + " = :id";
	
	private static final String UPDATE_PAINTING_CATEGORY = "UPDATE " + Painting.TABLE_NAME + " SET " + Painting.CATEGORY_COLUMN +
			" = :category WHERE " + Painting.ID_COLUMN + " = :id";
	
	private static final String UPDATE_PAINTING_CATEGORY_AND_PRICE = "UPDATE " + Painting.TABLE_NAME + " SET " + Painting.CATEGORY_COLUMN +
			" = :category, " + Painting.PRICE_COLUMN + " = :price WHERE " + Painting.ID_COLUMN + " = :id";
	
	private static final String DELETE_PAINTING_BY_ID = "DELETE FROM " + Painting.TABLE_NAME + " WHERE " + Painting.ID_COLUMN +
			" = :id";
	
	private static final String FIND_PAINTING_BY_LOCATION = "SELECT " + Painting.ID_COLUMN + " FROM " + Painting.TABLE_NAME + " WHERE " +
			Painting.LOCATION_COLUMN + " LIKE :fileName AND " + Painting.MEMBER_COLUMN + " = :memberId";
	
	private static final String GET_PAINTING_NAME_AND_LOCATION = "SELECT " + Painting.NAME_COLUMN + ", " + Painting.LOCATION_COLUMN + 
			" FROM " + Painting.TABLE_NAME + " WHERE " + Painting.ID_COLUMN + " = :id";
	
	private static final String PAINTING_OWNER_ID = "SELECT " + Painting.MEMBER_COLUMN + " FROM " + Painting.TABLE_NAME + " WHERE " +
			Painting.ID_COLUMN + " = :id";
	
	private static final String APPROVE_PAINTING = "UPDATE " + Painting.TABLE_NAME + " SET " + Painting.REVIEW_COLUMN + " = FALSE "+ 
			" WHERE " + Painting.ID_COLUMN + " = :id";
	
	public List<PaintingShort> findAllPaintings() {
		List<PaintingShort> results = jdbcTemplate.query(ALL_RECENT_PAINTINGS, new PaintingRowMapper());
		return results;
	}
	
	public List<PaintingShort> findAllPaintingsForReview() {
		List<PaintingShort> results = jdbcTemplate.query(FIND_PAINTINGS_FOR_REVIEW, new PaintingRowMapper());
		return results;
	}
	
	public boolean approvePainting(Long imageId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", imageId);
		
		int updates = namedParameterJdbcTemplate.update(APPROVE_PAINTING, parameters);
		
		return updates > 0;
	}
	
	public PaintingNameAndLocation getPaintingNameAndLocation(Long paintingId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", paintingId);
		PaintingNameAndLocation result = namedParameterJdbcTemplate.queryForObject(GET_PAINTING_NAME_AND_LOCATION, parameters,  new PaintingNameAndLocationMapper());
		return result;
	}
	
	public List<PaintingShort> findAllPaintingsWithTags(List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tagIds", tagIds);
		List<PaintingShort> result = namedParameterJdbcTemplate.query(ALL_RECENT_PAINTINGS_TAGS, parameters, new PaintingRowMapper());
		return result;
	}
	
	public List<PaintingShort> findPaintingByArtistAndTitle(Long memberId, String title) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("name", title);
		List<PaintingShort> results = namedParameterJdbcTemplate.query(PAINTINGS_BY_MEMBER_AND_TITLE, parameters, new PaintingRowMapper());
		return results;
	}
	
	public List<PaintingShort> findPaintingByArtistAndTitleTags(Long memberId, String title, List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("name", title);
		parameters.addValue("tagIds", tagIds);
		List<PaintingShort> results = namedParameterJdbcTemplate.query(PAINTINGS_BY_MEMBER_AND_TITLE_TAGS, parameters, new PaintingRowMapper());
		return results;
	}
	
	public List<PaintingShort> findPaintingByName(String title) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("name", "%"+title+"%");
		List<PaintingShort> results = namedParameterJdbcTemplate.query(FIND_PAINTING_BY_NAME, parameters, new PaintingRowMapper());
		return results;
	}
	
	public List<PaintingShort> findPaintingByNameTags(String title, List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("name", "%"+title+"%");
		parameters.addValue("tagIds", tagIds);
		List<PaintingShort> results = namedParameterJdbcTemplate.query(FIND_PAINTING_BY_NAME_TAGS, parameters, new PaintingRowMapper());
		return results;
	}
	
	public List<PaintingShort> recentPaintingsByCategory(Painting.Category category) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("category", category.toString());
		List<PaintingShort> results = namedParameterJdbcTemplate.query(RECENT_PAINTINGS_BY_CATEGORY, parameters, new PaintingRowMapper());
		return results;
	}
	
	public List<PaintingShort> recentPaintingsByCategoryTags(Painting.Category category, List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("category", category.toString());
		parameters.addValue("tagIds", tagIds);
		List<PaintingShort> results = namedParameterJdbcTemplate.query(RECENT_PAINTINGS_BY_CATEGORY_TAGS, parameters, new PaintingRowMapper());
		return results;
	}
	
	public List<PaintingShort> paintingsByMemberAndCategory(long memberId, Painting.Category category) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("category", category.toString());
		List<PaintingShort> results = namedParameterJdbcTemplate.query(PAINTINGS_BY_MEMBER_CATEGORY, parameters, new PaintingRowMapper());
		return results;
	}
	
	public List<PaintingShort> paintingsByMemberAndCategoryTags(long memberId, Painting.Category category, List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("category", category.toString());
		parameters.addValue("tagIds", tagIds);
		List<PaintingShort> results = namedParameterJdbcTemplate.query(PAINTINGS_BY_MEMBER_CATEGORY_TAGS, parameters, new PaintingRowMapper());
		return results;
	}
	
	public List<PaintingShort> paintingsByMember(long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		List<PaintingShort> results = namedParameterJdbcTemplate.query(PAINTINGS_BY_MEMBER, parameters, new PaintingRowMapper());
		return results;
	}
	
	public List<PaintingShort> paintingsByMemberTags(long memberId, List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("tagIds", tagIds);
		List<PaintingShort> results = namedParameterJdbcTemplate.query(PAINTINGS_BY_MEMBER_TAGS, parameters, new PaintingRowMapper());
		return results;
	}
	
	public List<PaintingShort> paintingByNameMemberAndCategory(long memberId, String name, Painting.Category category) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("name", "%"+name+"%");
		parameters.addValue("category", category.toString());
		List<PaintingShort> results = namedParameterJdbcTemplate.query(FIND_PAINTING_BY_NAME_MEMBER_CATEGORY, parameters, new PaintingRowMapper());
		return results;
	}
	
	public List<PaintingShort> paintingByNameMemberAndCategoryTag(long memberId, String name, Painting.Category category, List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("name", "%"+name+"%");
		parameters.addValue("category", category.toString());
		parameters.addValue("tagIds", tagIds);
		List<PaintingShort> results = namedParameterJdbcTemplate.query(FIND_PAINTING_BY_NAME_MEMBER_CATEGORY_TAGS, parameters, new PaintingRowMapper());
		return results;
	}
	
	public List<PaintingShort> findPaintingByNameAndCategory(String title, Painting.Category category) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("name", "%"+title+"%");
		parameters.addValue("category", category.toString());
		List<PaintingShort> results = namedParameterJdbcTemplate.query(FIND_PAINTING_BY_NAME_AND_CATEGORY, parameters, new PaintingRowMapper());
		return results;
	}
	
	public List<PaintingShort> findPaintingByNameAndCategoryTags(String title, Painting.Category category, List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("name", "%"+title+"%");
		parameters.addValue("category", category.toString());
		parameters.addValue("tagIds", tagIds);
		List<PaintingShort> results = namedParameterJdbcTemplate.query(FIND_PAINTING_BY_NAME_AND_CATEGORY_TAGS, parameters, new PaintingRowMapper());
		return results;
	}
	
	public List<Long> findPainters(List<Long> memberIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberIds", memberIds);
		List<Long> members = namedParameterJdbcTemplate.queryForList(FIND_PAINTERS, parameters, Long.class);
		return members;
	}
	
	public boolean updatePaintingNameAndCategory(Long paintingId, String name, Painting.Category category) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", paintingId);
		parameters.addValue("name", name);
		parameters.addValue("category", category.toString());
		int updatedRows = namedParameterJdbcTemplate.update(UPDATE_PAINTING_NAME_AND_CATEGORY, parameters);
		return updatedRows > 0;
	}
	
	public boolean updatePaintingNameAndCategoryAndPrice(Long paintingId, String name, Painting.Category category, Float price) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", paintingId);
		parameters.addValue("name", name);
		parameters.addValue("category", category.toString());
		parameters.addValue("price", price);
		int updatedRows = namedParameterJdbcTemplate.update(UPDATE_PAINTING_NAME_AND_CATEGORY_AND_PRICE, parameters);
		return updatedRows > 0;
	}
	
	public boolean updatePaintingName(Long paintingId, String name) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", paintingId);
		parameters.addValue("name", name);
		int updatedRows = namedParameterJdbcTemplate.update(UPDATE_PAINTING_NAME, parameters);
		return updatedRows > 0;
	}
	
	public boolean updatePaintingNameAndPrice(Long paintingId, String name, Float price) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", paintingId);
		parameters.addValue("name", name);
		parameters.addValue("price", price);
		int updatedRows = namedParameterJdbcTemplate.update(UPDATE_PAINTING_NAME_AND_PRICE, parameters);
		return updatedRows > 0;
	}
	
	public boolean updatePaintingCategory(Long paintingId, Painting.Category category) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", paintingId);
		parameters.addValue("category", category.toString());
		int updatedRows = namedParameterJdbcTemplate.update(UPDATE_PAINTING_CATEGORY, parameters);
		return updatedRows > 0;
	}
	
	public boolean updatePaintingCategoryAndPrice(Long paintingId, Painting.Category category, Float price) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", paintingId);
		parameters.addValue("category", category.toString());
		parameters.addValue("price", price);
		int updatedRows = namedParameterJdbcTemplate.update(UPDATE_PAINTING_CATEGORY_AND_PRICE, parameters);
		return updatedRows > 0;
	}
	
	public boolean deletePainting(Long paintingId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", paintingId);
		int deletedRows = namedParameterJdbcTemplate.update(DELETE_PAINTING_BY_ID, parameters);
		return deletedRows > 0;
	}
	
	public PaintingShort getPaintingById(Long paintingId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", paintingId);
		PaintingShort painting = namedParameterJdbcTemplate.queryForObject(GET_PAINTING_BY_ID, parameters, new PaintingRowMapper());
		return painting;
	}
	
	public String getPaintingLocation(Long paintingId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", paintingId);
		
		String url = namedParameterJdbcTemplate.queryForObject(GET_PAINTING_LOCATION, parameters, String.class);
		return url;
	}
	
	public Long findPaintingByLocationAndMember(String fileName, Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("fileName", "%" +fileName);
		parameters.addValue("memberId", memberId);
		
		Long paintingId = namedParameterJdbcTemplate.queryForObject(FIND_PAINTING_BY_LOCATION, parameters, Long.class);
		
		return paintingId;
	}
	
	public Long paintingOwnerId(Long paintingId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", paintingId);
		
		Long ownerId = namedParameterJdbcTemplate.queryForObject(PAINTING_OWNER_ID, parameters, Long.class);
		return ownerId;
	}
}
