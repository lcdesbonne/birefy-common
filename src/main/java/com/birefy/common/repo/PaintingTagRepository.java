package com.birefy.common.repo;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.dto.painting.PaintingTagDTO;
import com.birefy.common.entity.PaintingTag;
import com.birefy.common.entity.utils.database.BasicPaintingTagRowMapper;

@Repository
public class PaintingTagRepository extends CommonRepository<PaintingTag>{
	private static final String SEARCH_TAGS = "SELECT * FROM " + PaintingTag.TABLE_NAME + " WHERE lower(" + PaintingTag.TAG_NAME_COLUMN +
			") LIKE lower(:tagName)";
	
	private static final String TAG_EXISTS = "SELECT COUNT(*) > 0 FROM " + PaintingTag.TABLE_NAME + " WHERE lower(" + PaintingTag.TAG_NAME_COLUMN +
			") = lower(:tagName)";
	
	private static final String GET_TAG_BY_NAME = "SELECT * FROM " + PaintingTag.TABLE_NAME + " WHERE lower(" + PaintingTag.TAG_NAME_COLUMN + ") " +
			"= lower(:tagName)";
	
	public List<PaintingTagDTO> searchForTags(String tagName) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tagName", "%"+tagName+"%");
		
		List<PaintingTagDTO> results = namedParameterJdbcTemplate.query(SEARCH_TAGS, parameters, new BasicPaintingTagRowMapper());
		return results;
	}
	
	public boolean tagExists(String tagName) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tagName", tagName);
		
		boolean exists = namedParameterJdbcTemplate.queryForObject(TAG_EXISTS, parameters, Boolean.class);
		
		return exists;
	}
	
	public PaintingTagDTO getTagByName(String tagName) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tagName", tagName);
		
		PaintingTagDTO tag = namedParameterJdbcTemplate.queryForObject(GET_TAG_BY_NAME, parameters, new BasicPaintingTagRowMapper());
		
		return tag;
	}
}
