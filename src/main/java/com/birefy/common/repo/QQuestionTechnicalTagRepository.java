package com.birefy.common.repo;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.entity.QQuestionTechnicalTag;

@Repository
public class QQuestionTechnicalTagRepository extends CommonRepository<QQuestionTechnicalTag> {
	private static final String DELETE_LINK = "DELETE FROM " + QQuestionTechnicalTag.TABLE_NAME + 
			" WHERE " + QQuestionTechnicalTag.QUESTION_ID_COLUMN + " = :questionId " + 
			" AND " + QQuestionTechnicalTag.TAG_ID_COLUMN + " = :tagId";
	
	public void deleteRecord(long questionId, long tagId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("questionId", questionId);
		parameters.addValue("tagId", tagId);
		
		namedParameterJdbcTemplate.update(DELETE_LINK, parameters);
	}
}
