package com.birefy.common.repo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.entity.BulletinTechnicalTag;
import com.birefy.common.entity.TechnicalTag;
import com.birefy.common.entity.utils.QueryUtils;

@Repository
public class BulletinTechnicalTagRepository extends CommonRepository<BulletinTechnicalTag> {
	private static final String GROUP_CONTACT_HEADER_TEMPLATE = "GROUP_CONCAT(%s)";
	
	private static final String TECH_TAGS_FOR_BULLETIN = "SELECT " + BulletinTechnicalTag.TABLE_NAME + "." + BulletinTechnicalTag.BULLETIN_COLUMN +
			", GROUP_CONCAT(" + TechnicalTag.TABLE_NAME + "." + TechnicalTag.NAME_COLUMN + ") FROM " + BulletinTechnicalTag.TABLE_NAME + 
			" INNER JOIN " + TechnicalTag.TABLE_NAME + " ON " + TechnicalTag.TABLE_NAME + "." + TechnicalTag.ID_COLUMN + " = " +
			BulletinTechnicalTag.TABLE_NAME + "." + BulletinTechnicalTag.TECH_COLUMN + 
			" WHERE " + BulletinTechnicalTag.TABLE_NAME + "." + BulletinTechnicalTag.BULLETIN_COLUMN + " IN (:bulletinIds)" +
			" GROUP BY " + BulletinTechnicalTag.TABLE_NAME + "." + BulletinTechnicalTag.BULLETIN_COLUMN;

	public Map<Long, List<String>> tagsForBulletins(List<Long> bulletinIds) {
		MapSqlParameterSource paramMap = new MapSqlParameterSource();
		paramMap.addValue("bulletinIds", bulletinIds);
		
		List<Map<String, Object>> results = namedParameterJdbcTemplate.queryForList(TECH_TAGS_FOR_BULLETIN, paramMap);
		
		Map<Long, List<String>> formattedData = new HashMap<>();
		
		for(Map<String, Object> entry : results) {
			formattedData.put((Long) entry.get(BulletinTechnicalTag.BULLETIN_COLUMN), 
					QueryUtils.sqlGroupConcatToStringList(
							(String) entry.get(String.format(GROUP_CONTACT_HEADER_TEMPLATE, TechnicalTag.TABLE_NAME + "." + TechnicalTag.NAME_COLUMN)))
							);
					
		}
		return formattedData;
	}
}
