package com.birefy.common.repo;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.entity.MemberBulletin;

@Repository
public class MemberBulletinRepository extends CommonRepository<MemberBulletin> {
	private static String CREATORS_OF_BULLETIN = "SELECT "+MemberBulletin.MEMBER_ID_COLUMN + " FROM " +
			MemberBulletin.TABLE_NAME + " WHERE " + MemberBulletin.BULLETIN_ID_COLUMN + " = :bulletinId";
	
	public List<Long> creatorsOfBulletin(Long bulletinId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("bulletinId", bulletinId);
		List<Long> members = namedParameterJdbcTemplate.queryForList(CREATORS_OF_BULLETIN, parameters, Long.class);
		
		return members;
	}
	
}
