package com.birefy.common.repo;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.birefy.common.entity.QMemberTopic;
import com.birefy.common.entity.QTopic;
import com.birefy.common.entity.QTopicTag;
import com.birefy.common.entity.utils.database.QTopicRowMapper;

@Repository
public class QTopicRepository extends CommonRepository<QTopic> {
	private static final String KEY_COLUMNS = QTopic.ID_COLUMN + "," + QTopic.TITLE_COLUMN + "," + QTopic.DETAIL_COLUMN;
	private static final String FIND_ALL = "SELECT " + KEY_COLUMNS + " FROM " + QTopic.TABLE_NAME + " WHERE "
			+ QTopic.ACTIVE_COLUMN + " = %s ORDER BY " + QTopic.TITLE_COLUMN;
	
	private static final String FIND_ALL_BY_TITLE = "SELECT " + KEY_COLUMNS + " FROM " + QTopic.TABLE_NAME + " WHERE LOWER("
			+ QTopic.TITLE_COLUMN +") LIKE LOWER('%%%s%%') ORDER BY " + QTopic.TITLE_COLUMN;
	
	private static final String FIND_BY_TITLE = "SELECT " + KEY_COLUMNS + " FROM " + QTopic.TABLE_NAME + " WHERE "
			+ QTopic.ACTIVE_COLUMN + " = %s AND LOWER(" + QTopic.TITLE_COLUMN + ") LIKE LOWER('%%%S%%') ORDER BY "
			+ QTopic.TITLE_COLUMN;
	
	private static final String FIND_BY_TAG = "SELECT " + KEY_COLUMNS + " FROM " + QTopic.TABLE_NAME + " INNER JOIN "
			+ QTopicTag.TABLE_NAME + " ON " + QTopicTag.TABLE_NAME + "." + QTopicTag.TOPIC_ID_COLUMN + " = "
			+ QTopic.TABLE_NAME + "." + QTopic.ID_COLUMN + " WHERE " + QTopic.ACTIVE_COLUMN + " = %s AND "
			+ QTopicTag.TAG_ID_COLUMN + " IN (:tagIds) ORDER BY " + QTopic.TITLE_COLUMN;
	
	private static final String FIND_BY_TITLE_TAG = "SELECT " + KEY_COLUMNS + " FROM " + QTopic.TABLE_NAME
			+ " INNER JOIN " + QTopicTag.TABLE_NAME + " ON " + QTopicTag.TABLE_NAME + "." + QTopicTag.TOPIC_ID_COLUMN
			+ " = " + QTopic.TABLE_NAME + "." + QTopic.ID_COLUMN + " WHERE " + QTopic.ACTIVE_COLUMN + " = %s AND LOWER("
			+ QTopic.TITLE_COLUMN + ") LIKE LOWER('%%%s%%') AND " + QTopicTag.TAG_ID_COLUMN + " IN (:tagIds) ORDER BY "
			+ QTopic.TITLE_COLUMN;
	
	private static final String FIND_BY_ID = "SELECT " + KEY_COLUMNS +", "+QTopic.ACTIVE_COLUMN+ " FROM " + QTopic.TABLE_NAME + " WHERE "
			+ QTopic.ID_COLUMN + " IN (:topicIds)";
	
	private static final String ALTER_ACTIVE_STATE_TOPIC = "UPDATE " + QTopic.TABLE_NAME + " SET "
			+ QTopic.ACTIVE_COLUMN + " = :active WHERE " + QTopic.ID_COLUMN + " = :topicId";
	
	private static final String TOPIC_STATE = "SELECT " + QTopic.ACTIVE_COLUMN + " FROM " + QTopic.TABLE_NAME + " WHERE " + QTopic.ID_COLUMN + " = :id";
		
	private static final String GET_TOPICS_BY_MEMBER = "SELECT " + KEY_COLUMNS + ", "+ QTopic.ACTIVE_COLUMN + " FROM " + QTopic.TABLE_NAME +
			" INNER JOIN " + QMemberTopic.TABLE_NAME + " ON " + QTopic.ID_COLUMN + " = " + QMemberTopic.TOPIC_ID_COLUMN + 
			" WHERE " + QMemberTopic.MEMBER_ID_COLUMN + " = :memberId";
	
	public List<QTopic> findAll(boolean active) {
		List<QTopic> topics = jdbcTemplate.query(String.format(FIND_ALL, String.valueOf(active)),
				new QTopicRowMapper());
		return topics;
	}

	public List<QTopic> findByTitle(boolean active, String title) {
		List<QTopic> topics = jdbcTemplate.query(String.format(FIND_BY_TITLE, String.valueOf(active), title),
				new QTopicRowMapper());
		return topics;
	}

	public List<QTopic> findByTags(boolean active, List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tagIds", tagIds);
		List<QTopic> topics = namedParameterJdbcTemplate.query(String.format(FIND_BY_TAG, String.valueOf(active)),
				parameters, new QTopicRowMapper());
		return topics;
	}

	public List<QTopic> findByTitleAndTags(boolean active, String title, List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tagIds", tagIds);
		List<QTopic> topics = namedParameterJdbcTemplate.query(
				String.format(FIND_BY_TITLE_TAG, String.valueOf(active), title), parameters, new QTopicRowMapper());
		return topics;

	}

	public List<QTopic> findById(List<Long> topicIds) {
		List<QTopic> topics;
		if(!CollectionUtils.isEmpty(topicIds)) {
			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("topicIds", topicIds);
			topics = namedParameterJdbcTemplate.query(FIND_BY_ID, parameters, new QTopicRowMapper());
		} else {
			topics = new ArrayList<>();
		}
		return topics;
	}
	
	public List<QTopic> findAllByTitle(String title) {
		List<QTopic> topics = jdbcTemplate.query(String.format(FIND_ALL_BY_TITLE, title), new QTopicRowMapper());
		return topics;
	}
	
	public List<QTopic> getTopicsByMember(long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		List<QTopic> topics = namedParameterJdbcTemplate.query(GET_TOPICS_BY_MEMBER, parameters, new QTopicRowMapper());
		return topics;
	}

	public void modifyActiveState(Long topicId, boolean active) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("topicId", topicId);
		parameters.addValue("active", active);
		namedParameterJdbcTemplate.update(ALTER_ACTIVE_STATE_TOPIC, parameters);
	}
	
	public boolean isActive(Long topicId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", topicId);
		return namedParameterJdbcTemplate.queryForObject(TOPIC_STATE, parameters, Boolean.class);
	}
}
