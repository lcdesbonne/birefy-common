package com.birefy.common.repo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.birefy.common.dto.question.TechTagAndQuestion;
import com.birefy.common.entity.MemberTechnicalTag;
import com.birefy.common.entity.QQuestion;
import com.birefy.common.entity.QQuestionTechnicalTag;
import com.birefy.common.entity.TechnicalTag;
import com.birefy.common.entity.utils.database.TechTagAndQuestionRowMapper;
import com.birefy.common.entity.utils.database.TechnicalTagRowMapper;

@Repository
public class TechnicalTagRepository extends CommonRepository<TechnicalTag>{
	private static final String TECH_TAGS_FOR_QUESTION = "SELECT " + TechnicalTag.NAME_COLUMN + "," + QQuestionTechnicalTag.QUESTION_ID_COLUMN + 
			" FROM " + TechnicalTag.TABLE_NAME + " INNER JOIN " + QQuestionTechnicalTag.TABLE_NAME + 
			" ON " + TechnicalTag.ID_COLUMN + " = " + QQuestionTechnicalTag.TAG_ID_COLUMN + 
			" WHERE " + QQuestionTechnicalTag.QUESTION_ID_COLUMN + " IN (:questionId)";
	
	private static final String FIND_TAG_BY_NAME = "SELECT "+TechnicalTag.ID_COLUMN+" FROM "+TechnicalTag.TABLE_NAME+
			" WHERE "+TechnicalTag.NAME_COLUMN+" = :tagName"; 
	
	private static final String SEARCH_TECH_TAGS_BY_NAME = "SELECT * FROM " + TechnicalTag.TABLE_NAME + " WHERE lower(" + TechnicalTag.NAME_COLUMN + ") LIKE LOWER(:tagName)";
	
	private static final String FIND_ALL_TECH_TAGS = "SELECT " + TechnicalTag.NAME_COLUMN +" FROM " + TechnicalTag.TABLE_NAME;
	
	private static final String FIND_ALL_TECH_TAGS_FULL = "SELECT * FROM " + TechnicalTag.TABLE_NAME;
	
	private static final String TECH_TAGS_FOR_QUESTION_BY_ACTIVE = "SELECT " + TechnicalTag.NAME_COLUMN + "," + QQuestionTechnicalTag.QUESTION_ID_COLUMN + 
			" FROM " + TechnicalTag.TABLE_NAME + " INNER JOIN " + QQuestionTechnicalTag.TABLE_NAME + 
			" ON " + TechnicalTag.ID_COLUMN + " = " + QQuestionTechnicalTag.TAG_ID_COLUMN + 
			" INNER JOIN " + QQuestion.TABLE_NAME + " ON " + QQuestion.ID_COLUMN + " = " + QQuestionTechnicalTag.QUESTION_ID_COLUMN +
			" WHERE " + QQuestion.ACTIVE_COLUMN + " = :active";
	
	private static final String TECH_TAGS_FOR_QUESTION_BY_ACTIVE_AND_NAME = "SELECT " + TechnicalTag.NAME_COLUMN + "," + QQuestionTechnicalTag.QUESTION_ID_COLUMN + 
			" FROM " + TechnicalTag.TABLE_NAME + " INNER JOIN " + QQuestionTechnicalTag.TABLE_NAME + 
			" ON " + TechnicalTag.ID_COLUMN + " = " + QQuestionTechnicalTag.TAG_ID_COLUMN + 
			" INNER JOIN " + QQuestion.TABLE_NAME + " ON " + QQuestion.ID_COLUMN + " = " + QQuestionTechnicalTag.QUESTION_ID_COLUMN +
			" WHERE " + QQuestion.ACTIVE_COLUMN + " = :active" + 
			" AND lower(" + QQuestion.TITLE_COLUMN + ") LIKE lower(:title)";
	
	private static final String TECH_TAGS_FOR_QUESTION_FOR_REVIEW = "SELECT " + TechnicalTag.NAME_COLUMN + "," + QQuestionTechnicalTag.QUESTION_ID_COLUMN + 
			" FROM " + TechnicalTag.TABLE_NAME + " INNER JOIN " + QQuestionTechnicalTag.TABLE_NAME + 
			" ON " + TechnicalTag.ID_COLUMN + " = " + QQuestionTechnicalTag.TAG_ID_COLUMN + 
			" INNER JOIN " + QQuestion.TABLE_NAME + " ON " + QQuestion.ID_COLUMN + " = " + QQuestionTechnicalTag.QUESTION_ID_COLUMN +
			" WHERE " + QQuestion.REVIEW_COLUMN + " = TRUE";
	
	private static final String TECH_TAGS_FOR_QUESTION_FOR_REVIEW_AND_TITLE = "SELECT " + TechnicalTag.NAME_COLUMN + "," + QQuestionTechnicalTag.QUESTION_ID_COLUMN + 
			" FROM " + TechnicalTag.TABLE_NAME + " INNER JOIN " + QQuestionTechnicalTag.TABLE_NAME + 
			" ON " + TechnicalTag.ID_COLUMN + " = " + QQuestionTechnicalTag.TAG_ID_COLUMN + 
			" INNER JOIN " + QQuestion.TABLE_NAME + " ON " + QQuestion.ID_COLUMN + " = " + QQuestionTechnicalTag.QUESTION_ID_COLUMN +
			" WHERE " + QQuestion.REVIEW_COLUMN + " = TRUE" + 
			" AND lower(" + QQuestion.TITLE_COLUMN + ") LIKE lower(:title)";
	
	private static final String SPECIFIC_TECH_TAGS_FOR_MEMBER = "SELECT " + TechnicalTag.NAME_COLUMN + " FROM " + TechnicalTag.TABLE_NAME + 
			" INNER JOIN " + MemberTechnicalTag.TABLE_NAME + " ON " + TechnicalTag.ID_COLUMN + " = " + MemberTechnicalTag.TECHNICAL_TAG_COLUMN + 
			" WHERE " + MemberTechnicalTag.MEMBER_ID_COLUMN + " = :memberId";
	
	public List<TechTagAndQuestion> getTechnicalTagsForQuestion(List<Long> questionIds) {
		List<TechTagAndQuestion> tagsForQuestion;
		if(CollectionUtils.isEmpty(questionIds)) {
			tagsForQuestion = new ArrayList<>();
		} else {
			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("questionId", questionIds);
			
			tagsForQuestion = namedParameterJdbcTemplate.query(TECH_TAGS_FOR_QUESTION,
					parameters, new TechTagAndQuestionRowMapper());
		}
		
		return tagsForQuestion;
	}
	
	public Long findTagByName(String tagName) {
		if(!Optional.ofNullable(tagName).isPresent()){
			return null;
		}
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tagName", tagName);
		
		Long tagId;
		try {
			tagId = namedParameterJdbcTemplate.queryForObject(FIND_TAG_BY_NAME, parameters, Long.class);
		} catch(Exception e) {
			LOGGER.info("Tag with name " + tagName + " not found", e);
			tagId = null;
		}
		
		return tagId;
	}
	
	public List<String> findAllTechTags() {
		List<String> availableTechTags = jdbcTemplate.queryForList(FIND_ALL_TECH_TAGS, String.class);
		return availableTechTags;
	}
	
	public List<TechnicalTag> findAllTechTagsFull() {
		List<TechnicalTag> allAvailableTechTags = jdbcTemplate.query(FIND_ALL_TECH_TAGS_FULL, new TechnicalTagRowMapper());
		return allAvailableTechTags;
	}
	
	public List<TechTagAndQuestion> techTagsForQuestionByActive(Boolean active) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("active", active);
		
		List<TechTagAndQuestion> tagsForQuestion = namedParameterJdbcTemplate.query(TECH_TAGS_FOR_QUESTION_BY_ACTIVE, parameters,
				new TechTagAndQuestionRowMapper());
		
		return tagsForQuestion;
	}
	
	public List<TechTagAndQuestion> techTagsForQuestionByActiveAndName(Boolean active, String title) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("active", active);
		parameters.addValue("title", "%"+title+"%");
		
		List<TechTagAndQuestion> tagsForQuestion = namedParameterJdbcTemplate.query(TECH_TAGS_FOR_QUESTION_BY_ACTIVE_AND_NAME, 
				parameters, new TechTagAndQuestionRowMapper());
		
		return tagsForQuestion;
	}
	
	public List<TechTagAndQuestion> techTagsForQuestionsForReview() {
		List<TechTagAndQuestion> tagsForQuestion = jdbcTemplate.query(TECH_TAGS_FOR_QUESTION_FOR_REVIEW, 
				new TechTagAndQuestionRowMapper());
		
		return tagsForQuestion;
	}
	
	public List<TechTagAndQuestion> techTagsForQuestionForReviewByTitle(String title) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("title", title);
		
		List<TechTagAndQuestion> tagsForQuestion = namedParameterJdbcTemplate.query(TECH_TAGS_FOR_QUESTION_FOR_REVIEW_AND_TITLE, parameters,
				new TechTagAndQuestionRowMapper());
		
		return tagsForQuestion;
	}
	
	public List<String> specificTechnicalTagsForMember(long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		
		List<String> techTagsForMember = namedParameterJdbcTemplate.queryForList(SPECIFIC_TECH_TAGS_FOR_MEMBER, parameters, String.class);
		return techTagsForMember;
	}
	
	public List<TechnicalTag> searchTagsByName(String tagName) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tagName", "%" + tagName + "%");
		
		List<TechnicalTag> results = namedParameterJdbcTemplate.query(SEARCH_TECH_TAGS_BY_NAME, parameters,  new TechnicalTagRowMapper());
		
		return results;
	}
}
