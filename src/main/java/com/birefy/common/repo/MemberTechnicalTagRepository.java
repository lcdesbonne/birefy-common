package com.birefy.common.repo;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.birefy.common.entity.MemberTechnicalTag;

public class MemberTechnicalTagRepository extends CommonRepository<MemberTechnicalTag> {
	private static final String DELETE_ENTRY = "DELETE FROM "+ MemberTechnicalTag.TABLE_NAME +
			" WHERE " + MemberTechnicalTag.MEMBER_ID_COLUMN + " = :memberId AND " + MemberTechnicalTag.TECHNICAL_TAG_COLUMN +
			" IN (:technicalTagIds)";
		
	public int deleteSpecificSkillsForMember(long memberId, List<Long> technicalTagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("technicalTagIds", technicalTagIds);
		
		return namedParameterJdbcTemplate.update(DELETE_ENTRY, parameters);
	}
}
