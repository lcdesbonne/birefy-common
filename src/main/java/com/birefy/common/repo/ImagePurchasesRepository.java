package com.birefy.common.repo;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.dto.painting.PaintingNameAndCategory;
import com.birefy.common.entity.ImagePurchases;
import com.birefy.common.entity.Painting;
import com.birefy.common.entity.utils.database.PaintingNameAndCategoryMapper;

@Repository
public class ImagePurchasesRepository extends CommonRepository<ImagePurchases> {
	private static final String HAS_PURCHASED_IMAGE = "SELECT COUNT(*) > 0 FROM " + ImagePurchases.TABLE_NAME +
			" WHERE " + ImagePurchases.MEMBER_COLUMN + " = :memberId AND " + ImagePurchases.PAINTING_COLUMN + " = :paintingId";
	
	private static final String USER_PURCHASES = "SELECT " + Painting.CATEGORY_COLUMN + ", " + Painting.NAME_COLUMN +
			" FROM " + Painting.TABLE_NAME + " INNER JOIN " + ImagePurchases.TABLE_NAME +
			" ON " + Painting.ID_COLUMN + " = " + ImagePurchases.PAINTING_COLUMN +
			" WHERE " + ImagePurchases.TABLE_NAME + "." + ImagePurchases.MEMBER_COLUMN + " = :memberId" +
			" OR " + Painting.TABLE_NAME + "." + Painting.MEMBER_COLUMN + " = :memberId";
	
	public boolean userHasPurchasedImage(long memberId, long paintingId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("paintingId", paintingId);
		
		boolean hasPurchased = namedParameterJdbcTemplate.queryForObject(HAS_PURCHASED_IMAGE, parameters, Boolean.class);
		
		return hasPurchased;
	}
	
	List<PaintingNameAndCategory> purchasedImages(long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		
		List<PaintingNameAndCategory> results =
				namedParameterJdbcTemplate.query(USER_PURCHASES, parameters, new PaintingNameAndCategoryMapper());
		
		return results;
	}
}
