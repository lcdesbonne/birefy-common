package com.birefy.common.repo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.dto.sound.SoundTagDTO;
import com.birefy.common.dto.sound.SoundTagResult;
import com.birefy.common.entity.SoundDescriptiveTag;
import com.birefy.common.entity.SoundTag;
import com.birefy.common.entity.utils.database.SoundTagResultMapper;

@Repository
public class SoundDescriptiveTagRepository extends CommonRepository<SoundDescriptiveTag> {
	private static final String DELETE_TAGS_FROM_SOUNDS = "DELETE FROM " + SoundDescriptiveTag.TABLE_NAME + " WHERE "
			+ SoundDescriptiveTag.SOUND_COLUMN + " = :soundId AND " + SoundDescriptiveTag.TAG_COLUMN + " IN (:tagIds)";
	
	private static final String TAGS_FOR_SOUNDS = "SELECT " + SoundDescriptiveTag.SOUND_COLUMN + ", " + SoundTag.TABLE_NAME + ".*"
			+ " FROM " + SoundDescriptiveTag.TABLE_NAME + " INNER JOIN " + SoundTag.TABLE_NAME
			+ " ON " + SoundDescriptiveTag.TAG_COLUMN + " = " + SoundTag.ID_COLUMN + " WHERE "
			+ SoundDescriptiveTag.SOUND_COLUMN + " IN (:soundIds)";
	
	public void deleteTagsFromSound(Long soundId, List<Long> tagIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("sound", soundId);
		parameters.addValue("tagIds", tagIds);
		
		namedParameterJdbcTemplate.update(DELETE_TAGS_FROM_SOUNDS, parameters);
	}
	
	public Map<Long, List<SoundTagDTO>> findTagsForSounds(List<Long> soundIds) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("soundIds", soundIds);
		
		List<SoundTagResult> results = namedParameterJdbcTemplate.query(TAGS_FOR_SOUNDS, parameters, new SoundTagResultMapper());
		
		Map<Long, List<SoundTagDTO>> mappedResults = new HashMap<>();
		
		Set<Long> uniqueIds = results.stream().map(SoundTagResult::getSoundId).collect(Collectors.toSet());
		
		for(Long soundId : uniqueIds) {
			List<SoundTagDTO> sTag = results.stream().filter(r -> r.getSoundId().equals(soundId)).map(r -> {
				return new SoundTagDTO(r.getTagId(), r.getTag());
			}).collect(Collectors.toList());
			
			mappedResults.put(soundId, sTag);
		}
		
		return mappedResults;
	}
}
