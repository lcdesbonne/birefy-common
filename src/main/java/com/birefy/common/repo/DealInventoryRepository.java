package com.birefy.common.repo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.birefy.common.dto.deal.DealInventoryDTO;
import com.birefy.common.dto.deal.DealInventoryNameAndLocation;
import com.birefy.common.entity.Deal;
import com.birefy.common.entity.DealInventory;
import com.birefy.common.entity.InventoryType;
import com.birefy.common.entity.utils.database.DealInventoryNameAndLocationRowMapper;
import com.birefy.common.entity.utils.database.DealInventoryRowMapper;

@Repository
public class DealInventoryRepository extends CommonRepository<DealInventory>{
	private static final String KEY_COLUMNS = DealInventory.TABLE_NAME + "." + DealInventory.ID_COLUMN + ", " + DealInventory.TITLE_COLUMN + ", " + DealInventory.AUTHOR_COLUMN +
			", " + DealInventory.DEAL_ID_COLUMN + ", " + DealInventory.INVENTORY_TYPE_COLUMN + ", " + DealInventory.CAN_PREVIEW_COLUMN +
			", " + InventoryType.TYPE_COLUMN + ", " + DealInventory.RUN_COMMANDS_COLUMN;
	
	private static final String INVENTORY_FOR_DEALS = "SELECT " + KEY_COLUMNS  + ", substring_index(" + DealInventory.LOCATION_COLUMN + ", '.',-1) AS fExtension," +
			" (" + DealInventory.AUTHOR_COLUMN + " = :userId OR (" + Deal.CUSTOMER_CONFIRM_COLUMN + " AND " + Deal.SUPPLIER_CONFIRM_COLUMN + " AND " + Deal.TABLE_NAME + "." + Deal.CUSTOMER_ID_COLUMN + " = :userId)) AS " 
			+ DealInventoryDTO.CAN_DOWNLOAD_QUERY_HEADER +
			" FROM " + DealInventory.TABLE_NAME + " INNER JOIN " + InventoryType.TABLE_NAME + 
			" ON " + InventoryType.TABLE_NAME + "." + InventoryType.ID_COLUMN + " = " + DealInventory.INVENTORY_TYPE_COLUMN +
			" INNER JOIN " + Deal.TABLE_NAME +
			" ON " + DealInventory.DEAL_ID_COLUMN + " = " + Deal.TABLE_NAME + "." + Deal.ID_COLUMN +
			" WHERE " + DealInventory.DEAL_ID_COLUMN + " IN (:dealIds)";
		
	private static final String DELETE_INVENTORY = "DELETE FROM " + DealInventory.TABLE_NAME + " WHERE " + DealInventory.ID_COLUMN + 
			" = :inventoryId AND " + DealInventory.AUTHOR_COLUMN + " = :memberId";
	
	private static final String IS_AUTHOR = "SELECT " + DealInventory.AUTHOR_COLUMN + " = :memberId FROM " + DealInventory.TABLE_NAME +
			" WHERE " + DealInventory.ID_COLUMN + " = :inventoryId";
	
	private static final String GET_INVENTORY_NAME_AND_LOCATION = "SELECT " + DealInventory.TITLE_COLUMN + ", " + DealInventory.LOCATION_COLUMN + 
			" FROM " + DealInventory.TABLE_NAME + " WHERE " + DealInventory.ID_COLUMN + " = :id";
	
	private static final String CAN_DOWNLOAD_INVENTORY = "SELECT " + DealInventory.AUTHOR_COLUMN + " = :userId OR (" + Deal.CUSTOMER_CONFIRM_COLUMN + " AND " +
			Deal.SUPPLIER_CONFIRM_COLUMN + " AND " + Deal.TABLE_NAME + "." + Deal.CUSTOMER_ID_COLUMN + " = :userId) FROM " + DealInventory.TABLE_NAME + " INNER JOIN " + Deal.TABLE_NAME + " ON " +
			DealInventory.DEAL_ID_COLUMN + " = " + Deal.TABLE_NAME + "." + Deal.ID_COLUMN + " WHERE " + DealInventory.TABLE_NAME + "." + DealInventory.ID_COLUMN + " = :inventoryId";
	
	public List<DealInventoryDTO> inventoryForDeals(List<Long> dealIds, Long userId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("dealIds", dealIds);
		parameters.addValue("userId", userId);
		
		List<DealInventoryDTO> inventory = CollectionUtils.isEmpty(dealIds) ? new ArrayList<>() :
			namedParameterJdbcTemplate.query(INVENTORY_FOR_DEALS, parameters, new DealInventoryRowMapper());
		return inventory;
	}
	
	public boolean deleteInventoryByAuthor(Long inventoryId, Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("inventoryId", inventoryId);
		parameters.addValue("memberId", memberId);
		
		int deletions = namedParameterJdbcTemplate.update(DELETE_INVENTORY, parameters);
		
		return deletions > 0;
	}
	
	public boolean isAuthor(Long memberId, Long inventoryId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("inventoryId", inventoryId);
		
		boolean isAuthor = namedParameterJdbcTemplate.queryForObject(IS_AUTHOR, parameters, Boolean.class);
		
		return isAuthor;
	}
	
	public DealInventoryNameAndLocation getDealInventoryNameAndLocation(Long dealInventoryId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", dealInventoryId);
		DealInventoryNameAndLocation inventoryLocation = namedParameterJdbcTemplate.queryForObject(GET_INVENTORY_NAME_AND_LOCATION, parameters, new DealInventoryNameAndLocationRowMapper());
		
		return inventoryLocation;
	}
	
	public boolean canDownloadInventory(Long inventoryId, Long userId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("userId", userId);
		parameters.addValue("inventoryId", inventoryId);
		
		Boolean canDownload = namedParameterJdbcTemplate.queryForObject(CAN_DOWNLOAD_INVENTORY, parameters, Boolean.class);
		
		return canDownload;
	}
}
