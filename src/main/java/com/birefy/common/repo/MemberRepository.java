package com.birefy.common.repo;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.authentication.AuthorityResolver;
import com.birefy.common.authentication.BirefyPlatformAuthority;
import com.birefy.common.entity.Member;
import com.birefy.common.entity.utils.database.MemberRowMapper;

/**
 * Queries on the Members table
 * 
 * @author Lennon
 *
 */
@Repository
public class MemberRepository extends CommonRepository<Member> {
	private static final String KEY_COLUMNS = Member.ID_COLUMN + "," + Member.FIRST_NAME_COLUMN + ","
			+ Member.FAMILY_NAME_COLUMN + "," + Member.EMAIL_COLUMN + "," + Member.PHONE_COLUMN;
	private static final String SELECT_ALL_MEMBERS = "SELECT * FROM "+Member.TABLE_NAME;
	private static final String FIND_MEMBER_BY_EMAIL = "SELECT " + KEY_COLUMNS + " FROM " + Member.TABLE_NAME + " WHERE LOWER(" + Member.EMAIL_COLUMN + ") LIKE LOWER('%%%s%%')";
	private static final String FIND_MEMBER_BY_EMAIL_STRICT = "SELECT * FROM " + Member.TABLE_NAME + " WHERE " + Member.EMAIL_COLUMN + " = :email AND "+Member.ACTIVE_COLUMN;
	private static final String COUNT_MEMBERS = "SELECT count(*) FROM "+Member.TABLE_NAME;
	private static final String FULL_DETAILS_BY_ID = "SELECT * FROM "+Member.TABLE_NAME+" WHERE " + Member.ID_COLUMN + " = %d";
	private static final String FIND_MEMBERS_BY_NAME = "SELECT " + KEY_COLUMNS + ", " + Member.SUBSCRIPTION_CUSTOMER_COLUMN + ", " + Member.HOST_SERVICE_COLUMN + " FROM " + Member.TABLE_NAME
			+ " WHERE LOWER(" + Member.FIRST_NAME_COLUMN + ") LIKE LOWER(:name) OR LOWER(" + Member.FAMILY_NAME_COLUMN
			+ ") LIKE LOWER(:name) ORDER BY " + Member.FAMILY_NAME_COLUMN;
	private static final String ALTER_AUTHORITY = "UPDATE "+Member.TABLE_NAME+" SET "+Member.AUTHORITY_COLUMN+" = :authority WHERE id = :id";
	
	private static final String AUTHENTICATE_DETAILS = "SELECT " + Member.EMAIL_COLUMN + " as username, " + Member.KEY_COLUMN + " as password, TRUE as enabled FROM " + Member.TABLE_NAME + " WHERE " + Member.EMAIL_COLUMN + " = ?";
	private static final String AUTHORITY_DETAILS = "SELECT " + Member.EMAIL_COLUMN + " as username, "+ Member.AUTHORITY_COLUMN + " as authority FROM " + Member.TABLE_NAME + " WHERE " + Member.EMAIL_COLUMN + " = ?";
	
	private static final String FIND_MEMBER_LAST_PAID = "SELECT " + Member.LAST_PAID_COLUMN + " FROM " + Member.TABLE_NAME + " WHERE " + Member.ID_COLUMN + " = :id";
	
	private static final String GET_MEMBER_PASSPHRASE = "SELECT " + Member.KEY_COLUMN + " FROM " + Member.TABLE_NAME + " WHERE " + Member.ID_COLUMN + " = :id";
	
	private static final String GET_MEMBER_AUTHORITY = "SELECT " + Member.AUTHORITY_COLUMN + " FROM " + Member.TABLE_NAME + " WHERE " + Member.ID_COLUMN + " = :id";
	
	private static final String GET_MEMBER_ACCOUNT = "SELECT " + Member.ACCOUNT_ID_COLUMN + " FROM " + Member.TABLE_NAME + " WHERE " + Member.ID_COLUMN + " = :id";
	
	/**
	 * Find member by email
	 * 
	 * @param email
	 * @return
	 */
	public Member findMemberByEmail(String email) {
		Member memberDetails = jdbcTemplate.queryForObject(String.format(FIND_MEMBER_BY_EMAIL, email), new MemberRowMapper());

		return memberDetails;
	}
	
	/**
	 * Identify user by email
	 * 
	 * @param email
	 * @return Member
	 */
	public Optional<Member> identifyMemberByEmail(String email) {
		Optional<Member> member;
		
		try {
			MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue("email", email);
			Member result = namedParameterJdbcTemplate.queryForObject(FIND_MEMBER_BY_EMAIL_STRICT, params, new MemberRowMapper());
			member = Optional.of(result);
		} catch (EmptyResultDataAccessException e) {
			member = Optional.empty();
		}
		
		return member;
	}
	
	public Optional<String> getMemberAccountId(long memberId) {
		Optional<String> accountId;
		
		try {
			MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue("id", memberId);
			String accountIdResult = namedParameterJdbcTemplate.queryForObject(GET_MEMBER_ACCOUNT, params, String.class);
			accountId = Optional.of(accountIdResult);
		} catch (EmptyResultDataAccessException e) {
			accountId = Optional.empty();
		}
		
		return accountId;
	}

	/**
	 * Number of active members
	 * 
	 * @return long
	 */
	public long countMembers() {
		long memberCount = jdbcTemplate.queryForObject(COUNT_MEMBERS, Long.class);
		return memberCount;
	}

	/**
	 * View full member details
	 * 
	 * @param ids
	 * @return Member
	 */
	public Member selectFullMemberDetailsById(long id) {
		Member member = jdbcTemplate.queryForObject(String.format(FULL_DETAILS_BY_ID, id), new MemberRowMapper());
		return member;
	}

	/**
	 * View all registered members
	 * 
	 * @return List<Member>
	 */
	public List<Member> selectAllMembers() {
		List<Member> members = jdbcTemplate.query(SELECT_ALL_MEMBERS, new MemberRowMapper());
		return members;
	}
	
	/**
	 * Retrieve members by name. Searchers for both first name and family name
	 * @param name
	 * @return List<Member>
	 */
	public List<Member> findMembersByName(String name) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("name", "%"+name+"%");
		List<Member> members = namedParameterJdbcTemplate.query(FIND_MEMBERS_BY_NAME, parameters, new MemberRowMapper());
		return members;
	}
	
	public void alterAuthority(BirefyPlatformAuthority authority, long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("authority", authority.value());
		parameters.addValue("id", memberId);
		
		namedParameterJdbcTemplate.update(ALTER_AUTHORITY, parameters);
	}
	
	public LocalDateTime memberLastPaid(long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", memberId);
		
		LocalDateTime lastPaid = namedParameterJdbcTemplate.queryForObject(FIND_MEMBER_LAST_PAID, parameters, LocalDateTime.class);
		
		return lastPaid;
	}
	
	public String getMemberPassPhrase(long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", memberId);
		
		String passphrase = namedParameterJdbcTemplate.queryForObject(GET_MEMBER_PASSPHRASE, parameters, String.class);
		
		return passphrase;
	}
	
	public BirefyPlatformAuthority getMemberAuthority(long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", memberId);
		
		Byte authority = namedParameterJdbcTemplate.queryForObject(GET_MEMBER_AUTHORITY, parameters, Byte.class);
		
		BirefyPlatformAuthority memberAuthority = null;
		
		switch(authority) {
			case (0): {
				memberAuthority = BirefyPlatformAuthority.CITIZEN;
				break;
			}
			case (1): {
				memberAuthority = BirefyPlatformAuthority.LORD;
				break;
			}
			case (2): {
				memberAuthority = BirefyPlatformAuthority.ROYAL;
				break;
			}
			default: throw new RuntimeException("Unable to identify authority for member with ID: " + memberId);
		}
		
		return memberAuthority;
	}
}
