package com.birefy.common.repo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.birefy.common.dto.painting.PaintingTagDTO;
import com.birefy.common.dto.painting.PaintingTagResult;
import com.birefy.common.entity.PaintingDescriptiveTag;
import com.birefy.common.entity.PaintingTag;
import com.birefy.common.entity.utils.database.PaintingTagResultMapper;

@Repository
public class PaintingDescriptiveTagRepository extends CommonRepository<PaintingDescriptiveTag> {
	private static final String DELETE_TAGS_FROM_PAINTING = "DELETE FROM " + PaintingDescriptiveTag.TABLE_NAME + " WHERE "
			+ PaintingDescriptiveTag.PAINTING_COLUMN + " = :paintingId AND " + PaintingDescriptiveTag.TAG_COLUMN + " IN (:tagIds)";
	
	private static final String TAGS_FOR_PAINTINGS = "SELECT " + PaintingDescriptiveTag.PAINTING_COLUMN + ", " + PaintingTag.TABLE_NAME + ".*" 
	+ " FROM " + PaintingDescriptiveTag.TABLE_NAME + " INNER JOIN " + PaintingTag.TABLE_NAME 
	+ " ON " + PaintingDescriptiveTag.TAG_COLUMN + " = " + PaintingTag.ID_COLUMN + " WHERE "
			+ PaintingDescriptiveTag.PAINTING_COLUMN + " IN (:paintingIds)";
	
	public void deleteTagsFromPainting(Long paintingId, List<Long> tagIds) {
		if(CollectionUtils.isEmpty(tagIds)) {
			return;
		}
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("paintingIs", paintingId);
		parameters.addValue("tagIds", tagIds);
		
		namedParameterJdbcTemplate.update(DELETE_TAGS_FROM_PAINTING, parameters);
	}
	
	public Map<Long, List<PaintingTagDTO>> findTagsForPaintings(List<Long> paintingIds) {
		if(CollectionUtils.isEmpty(paintingIds)) {
			return new HashMap<>();
		}
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("paintingIds", paintingIds);
		
		List<PaintingTagResult> results = namedParameterJdbcTemplate.query(TAGS_FOR_PAINTINGS, parameters, new PaintingTagResultMapper());
		
		Map<Long, List<PaintingTagDTO>> mappedResults = new HashMap<>();
		
		Set<Long> uniqueIds = results.stream().map(PaintingTagResult::getPaintingId).collect(Collectors.toSet());
		
		for(Long paintingId : uniqueIds) {
			List<PaintingTagDTO> pTag = results.stream().filter(r -> r.getPaintingId().equals(paintingId)).map(r -> {
				return new PaintingTagDTO(r.getTagId(), r.getTag());
			}).collect(Collectors.toList());
			
			mappedResults.put(paintingId, pTag);
		}
		
		return mappedResults;
	}

}
