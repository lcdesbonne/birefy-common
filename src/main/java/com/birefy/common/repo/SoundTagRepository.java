package com.birefy.common.repo;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.dto.sound.SoundTagDTO;
import com.birefy.common.entity.SoundTag;
import com.birefy.common.entity.utils.database.SoundTagSimpleRowMapper;

@Repository
public class SoundTagRepository extends CommonRepository<SoundTag> {
	private static final String SEARCH_TAGS = "SELECT * FROM " + SoundTag.TABLE_NAME + " WHERE lower(" + SoundTag.TAG_COLUMN +
			") LIKE lower(:tagName)";
	
	private static final String TAG_EXISTS = "SELECT COUNT(*) > 0 FROM " + SoundTag.TABLE_NAME + " WHERE lower(" + SoundTag.TAG_COLUMN +
			") = lower(:tagName)";
	
	private static final String GET_TAG_BY_NAME = "SELECT * FROM " + SoundTag.TABLE_NAME + " WHERE lower(" + SoundTag.TAG_COLUMN + ") " +
			"= lower(:tagName)";
	
	public List<SoundTagDTO> searchForTags(String tagName) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tagName", "%" + tagName + "%");
		
		List<SoundTagDTO> results = namedParameterJdbcTemplate.query(SEARCH_TAGS, parameters, new SoundTagSimpleRowMapper());
		return results;
	}
	
	public boolean tagExists(String tagName) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tagName", tagName);
		
		boolean exists = namedParameterJdbcTemplate.queryForObject(TAG_EXISTS, parameters, Boolean.class);
		
		return exists;
	}
	
	public SoundTagDTO getTagByName(String tagName) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("tagName", tagName);
		
		SoundTagDTO tag = namedParameterJdbcTemplate.queryForObject(GET_TAG_BY_NAME, parameters, new SoundTagSimpleRowMapper());
		
		return tag;
	}
}
