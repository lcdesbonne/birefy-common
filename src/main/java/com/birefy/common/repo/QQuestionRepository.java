package com.birefy.common.repo;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.cluster.DeploymentRequest;
import com.birefy.common.dto.question.QQuestionTitleAndUrl;
import com.birefy.common.dto.question.QuestionIdAndSource;
import com.birefy.common.entity.QMemberQuestion;
import com.birefy.common.entity.QQuestion;
import com.birefy.common.entity.utils.database.QQuestionRowMapper;
import com.birefy.common.entity.utils.database.QQuestionTitleUrlMapper;
import com.birefy.common.entity.utils.database.DeploymentRequestRowMapper;

@Repository
public class QQuestionRepository extends CommonRepository<QQuestion> {
	private static final String KEY_COLUMNS = QQuestion.ID_COLUMN + "," + QQuestion.TITLE_COLUMN + ","
			+ QQuestion.DETAIL_COLUMN + "," + QQuestion.TOPIC_COLUMN + "," + QQuestion.APP_URL_COLUMN;

	private static final String ALL_QUESTIONS = "SELECT " + KEY_COLUMNS + " FROM " + QQuestion.TABLE_NAME + " WHERE "
			+ QQuestion.ACTIVE_COLUMN + " = %s" + " ORDER BY " + QQuestion.TOPIC_COLUMN + ", "
			+ QQuestion.TITLE_COLUMN + "," + QQuestion.INCEPTION_COLUMN + " DESC";

	private static final String COUNT_QUESTIONS = "SELECT COUNT(*) FROM " + QQuestion.TABLE_NAME + " WHERE "
			+ QQuestion.ACTIVE_COLUMN + " = %s ";

	private static final String SEARCH_BY_TITLE = "SELECT " + KEY_COLUMNS + " FROM " + QQuestion.TABLE_NAME
			+ " WHERE LOWER(" + QQuestion.TITLE_COLUMN + ") LIKE LOWER('%%%s%%') AND " + QQuestion.ACTIVE_COLUMN
			+ "=%s ORDER BY " + QQuestion.TOPIC_COLUMN + "," + QQuestion.TITLE_COLUMN + "," + QQuestion.INCEPTION_COLUMN + " DESC";
	
	private static final String SEARCH_BY_TITLE_WITH_REVIEW_STATUS = "SELECT " + KEY_COLUMNS + "," + QQuestion.REVIEW_COLUMN + " FROM "
			+ QQuestion.TABLE_NAME + " WHERE LOWER(" + QQuestion.TITLE_COLUMN + ") LIKE LOWER('%%%s%%') AND "
			+ QQuestion.REVIEW_COLUMN + " = %s ORDER BY " + QQuestion.TOPIC_COLUMN + "," + QQuestion.TITLE_COLUMN + "," 
			+ QQuestion.INCEPTION_COLUMN + " DESC";
	
	private static final String SEARCH_BY_TITLE_EXTENDED = "SELECT " + KEY_COLUMNS + "," + QQuestion.REVIEW_COLUMN + " FROM "
			+ QQuestion.TABLE_NAME + " WHERE LOWER(" + QQuestion.TITLE_COLUMN + ") LIKE LOWER('%%%s%%') AND " + QQuestion.ACTIVE_COLUMN
			+ "=%s ORDER BY " + QQuestion.TOPIC_COLUMN + "," + QQuestion.TITLE_COLUMN + "," + QQuestion.INCEPTION_COLUMN + " DESC";

	private static final String SEARCH_ALL_BY_TITLE = "SELECT " + KEY_COLUMNS + " FROM " + QQuestion.TABLE_NAME
			+ " WHERE LOWER(" + QQuestion.TITLE_COLUMN + ") LIKE LOWER('%:title%') ORDER BY " + QQuestion.TOPIC_COLUMN
			+ "," + QQuestion.TITLE_COLUMN + "," + QQuestion.INCEPTION_COLUMN + " DESC";
	
	private static final String SEARCH_BY_TOPIC = "SELECT " + KEY_COLUMNS + " FROM " + QQuestion.TABLE_NAME + " WHERE "
			+ QQuestion.TOPIC_COLUMN + " IN (:topicIds) AND " + QQuestion.ACTIVE_COLUMN + " = %s ORDER BY " + QQuestion.TOPIC_COLUMN + ","
			+ QQuestion.TITLE_COLUMN + "," + QQuestion.INCEPTION_COLUMN + " DESC";
	
	private static final String SEARCH_ALL_BY_TOPIC = "SELECT " + KEY_COLUMNS + " FROM " + QQuestion.TABLE_NAME + " WHERE "
			+ QQuestion.TOPIC_COLUMN + " IN (:topicIds) ORDER BY " + QQuestion.TOPIC_COLUMN + "," + QQuestion.TITLE_COLUMN
			+ "," + QQuestion.INCEPTION_COLUMN + " DESC";

	private static final String SEARCH_BY_TOPIC_AND_TITLE = "SELECT " + KEY_COLUMNS + " FROM " + QQuestion.TABLE_NAME
			+ " WHERE " + QQuestion.TOPIC_COLUMN + " IN (:topicIds) AND LOWER(" + QQuestion.TITLE_COLUMN + ") LIKE LOWER('%%%s%%') AND "
			+ QQuestion.ACTIVE_COLUMN + " = %s ORDER BY "+ QQuestion.TOPIC_COLUMN + "," + QQuestion.TITLE_COLUMN + "," 
			+ QQuestion.INCEPTION_COLUMN + " DESC";
	
	private static final String ALTER_ACTIVE_STATE_Q = "UPDATE "+QQuestion.TABLE_NAME+" SET "+QQuestion.ACTIVE_COLUMN+" = :active WHERE "+QQuestion.ID_COLUMN+" = :qId";
	
	private static final String TOPIC_FOR_QUESTION = "SELECT " + QQuestion.TOPIC_COLUMN + " FROM " + QQuestion.TABLE_NAME + " WHERE " + QQuestion.ID_COLUMN + " = :id";
	
	private static final String DEACTIVATE_QUESTIONS_FOR_TOPIC = "UPDATE " + QQuestion.TABLE_NAME + " SET " + QQuestion.ACTIVE_COLUMN + " = FALSE WHERE " + QQuestion.TOPIC_COLUMN + " = :topId";
	
	private static final String URL_FOR_QUESTION = "SELECT "+QQuestion.TITLE_COLUMN+","+QQuestion.APP_URL_COLUMN+" FROM "+QQuestion.TABLE_NAME+" WHERE " + QQuestion.ID_COLUMN + " = :id";
	
	private static final String QUESTIONS_BY_USER = "SELECT "+ QQuestion.TABLE_NAME + "."+QQuestion.ID_COLUMN + ", "
			+ QQuestion.TABLE_NAME + "." + QQuestion.TITLE_COLUMN + ", "
			+ QQuestion.TABLE_NAME + "." + QQuestion.DETAIL_COLUMN + ", "
			+ QQuestion.TABLE_NAME + "." + QQuestion.TOPIC_COLUMN + ", "
			+ QQuestion.TABLE_NAME + "." + QQuestion.APP_URL_COLUMN + ", "
			+ QQuestion.TABLE_NAME + "." + QQuestion.ACTIVE_COLUMN + ","
			+ QQuestion.TABLE_NAME + "." + QQuestion.SOURCE_COLUMN + ","
			+ QQuestion.TABLE_NAME + "." + QQuestion.REVIEW_COLUMN + ","
			+ QQuestion.TABLE_NAME + "." + QQuestion.PORT_COLUMN +
			" FROM " + QQuestion.TABLE_NAME + " INNER JOIN "+ QMemberQuestion.TABLE_NAME + 
			" ON " + QQuestion.ID_COLUMN + " = " + QMemberQuestion.QUESTION_ID_COLUMN + 
			" WHERE " + QMemberQuestion.MEMBER_ID_COLUMN + " = :id";
	
	private static final String UPDATE_QUESTION_FOR_REVIEW = "UPDATE " + QQuestion.TABLE_NAME + " SET " + QQuestion.REVIEW_COLUMN + " = :isReview WHERE " + QQuestion.ID_COLUMN + " = :id";
	
	private static final String RETRIEVE_QUESTION_SOURCE = "SELECT " + QQuestion.ID_COLUMN + ", " + QQuestion.SOURCE_COLUMN + "," + QMemberQuestion.MEMBER_ID_COLUMN + 
			", " + QQuestion.PORT_COLUMN + ", " + QQuestion.TITLE_COLUMN +
			" FROM " + QQuestion.TABLE_NAME + 
			" INNER JOIN " + QMemberQuestion.TABLE_NAME + " ON " + QMemberQuestion.QUESTION_ID_COLUMN + " = " + QQuestion.ID_COLUMN +
			" WHERE NOT " + QQuestion.DELETE_REQUESTED_COLUMN + " AND " + QQuestion.LAUNCH_REQUESTED_COLUMN;
		
	/**
	 * Search all questions based on active or pending
	 * 
	 * @param active
	 * @return List<QQuestion>
	 */
	public List<QQuestion> searchQuestions(boolean active) {
		List<QQuestion> questions = jdbcTemplate.query(String.format(ALL_QUESTIONS, String.valueOf(active)),
				new QQuestionRowMapper());
		return questions;
	}

	/**
	 * Count questions based on active or pending
	 * 
	 * @param active
	 * @return long
	 */
	public long countQuestions(boolean active) {
		long activeQuestionCount = jdbcTemplate.queryForObject(String.format(COUNT_QUESTIONS, String.valueOf(active)),
				Long.class);
		return activeQuestionCount;
	}

	/**
	 * Search for questions with titles containing titleText, based on their
	 * active state
	 * 
	 * @param titleText
	 * @param active
	 * @return List<QQuestion>
	 */
	public List<QQuestion> searchQuestionsByTitle(String titleText, boolean active) {
		List<QQuestion> questions = jdbcTemplate
				.query(String.format(SEARCH_BY_TITLE, titleText, String.valueOf(active)), new QQuestionRowMapper());
		return questions;
	}
	
	public List<QQuestion> searchQuestionsByTitleWithReviewStatus(String titleText, boolean active) {
		List<QQuestion> questions = jdbcTemplate
				.query(String.format(SEARCH_BY_TITLE_WITH_REVIEW_STATUS, titleText, String.valueOf(active)), new QQuestionRowMapper());
		return questions;
	}
	
	public List<QQuestion> searchQuestionByTitleAndIsActiveExtended(String titleText, boolean active) {
		List<QQuestion> questions = jdbcTemplate
				.query(String.format(SEARCH_BY_TITLE_EXTENDED, titleText, String.valueOf(active)), new QQuestionRowMapper());
		return questions;
	}
	
	/**
	 * Search for questions with titles containing the title, regardless of their 
	 * active state
	 * @param title
	 * @return List<QQuestion>
	 */
	public List<QQuestion> searchAllQuestionsByTitle(String title) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("title", title);
		List<QQuestion> questions = namedParameterJdbcTemplate.query(SEARCH_ALL_BY_TITLE, parameters, new QQuestionRowMapper());
		return questions;
	}

	/**
	 * Search questions by topic name
	 * 
	 * @param topicId
	 * @param active
	 * @return List<QQuestion>
	 */
	public List<QQuestion> searchQuestionsByTopic(List<Long> topicId, boolean active) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("topicIds", topicId);
		List<QQuestion> question = namedParameterJdbcTemplate.query(String.format(SEARCH_BY_TOPIC, String.valueOf(active)), parameters,
				new QQuestionRowMapper());
		return question;
	}
	
	/**
	 * Search questions by topic and title
	 * @param topicId
	 * @param titleText
	 * @param active
	 * @return List<QQuestion>
	 */
	public List<QQuestion> searchQuestionsByTopicAndTitle(List<Long> topicId, String titleText, boolean active) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("topicIds", topicId);
		List<QQuestion> question = namedParameterJdbcTemplate.query(
				String.format(SEARCH_BY_TOPIC_AND_TITLE, titleText, String.valueOf(active)), parameters,
				new QQuestionRowMapper());
		return question;
	}
	
	/**
	 * Search questions by title, ignoring their active state
	 * @param topicId
	 * @return List<QQuestion>
	 */
	public List<QQuestion> searchAllQuestionsByTopic(List<Long> topicId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("topicIds", topicId);
		List<QQuestion> question = namedParameterJdbcTemplate.query(SEARCH_ALL_BY_TOPIC, parameters, new QQuestionRowMapper());
		return question;
	}
	
	public void alterActiveState(Long questionId, boolean active) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("qId", questionId);
		parameters.addValue("active", active);
		namedParameterJdbcTemplate.update(ALTER_ACTIVE_STATE_Q, parameters);
	}
	
	public Long topicforQuestion(long questionId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", questionId);
		return namedParameterJdbcTemplate.queryForObject(TOPIC_FOR_QUESTION, parameters, Long.class);
	}
	
	public void deactivateQuestionsForTopic(long topicId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("topId", topicId);
		namedParameterJdbcTemplate.update(DEACTIVATE_QUESTIONS_FOR_TOPIC, parameters);
	}
	
	public QQuestionTitleAndUrl getTitleAndUrlForQuestion(long questionId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", questionId);
		QQuestionTitleAndUrl url = namedParameterJdbcTemplate.queryForObject(URL_FOR_QUESTION, parameters, new QQuestionTitleUrlMapper());
		return url;
	}
	
	public List<QQuestion> allQuestionsByMember(long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", memberId);
		List<QQuestion> questionsByMember = namedParameterJdbcTemplate.query(QUESTIONS_BY_USER, parameters, new QQuestionRowMapper());
		return questionsByMember;
	}
	
	public void alterReviewState(long questionId, boolean isReview) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", questionId);
		parameters.addValue("isReview", isReview);
		namedParameterJdbcTemplate.update(UPDATE_QUESTION_FOR_REVIEW, parameters);
	}
	
	public List<DeploymentRequest> retrieveDeploymentRequests() {
		List<DeploymentRequest> results = 
				jdbcTemplate.query(RETRIEVE_QUESTION_SOURCE, new DeploymentRequestRowMapper());
		
		return results;
	}
}
