package com.birefy.common.repo;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.dto.capital.RevenuePayment;
import com.birefy.common.dto.capital.RevenueSummary;
import com.birefy.common.entity.Revenue;
import com.birefy.common.entity.utils.database.RevenuePaymentRowMapper;
import com.birefy.common.entity.utils.database.RevenueRowMapper;

@Repository
public class RevenueRepository extends CommonRepository<Revenue> {
	private static final String REVENUE_PAYABLE_TO_MEMBER = "SELECT * FROM " + Revenue.TABLE_NAME + " WHERE " +
			Revenue.MEMBER_COLUMN + " = :id AND NOT " + Revenue.RESOLVED_COLUMN;
	
	private static final String ANNUAL_SALES_FOR_MEMBER = "SELECT * FROM " + Revenue.TABLE_NAME + " WHERE " +
			Revenue.MEMBER_COLUMN + " = :id AND " + Revenue.CREATED_COLUMN + " > :yearToDate";
	
	private static final String UPDATE_REVENUES_RESOLVED_FOR_USER = "UPDATE " + Revenue.TABLE_NAME + " SET " +
			Revenue.RESOLVED_COLUMN + " = :resolved, " + Revenue.PAYMENT_ID_COLUMN + " = :paymentId WHERE " +
			Revenue.MEMBER_COLUMN + " = :memberId AND NOT " + Revenue.RESOLVED_COLUMN;
	
	private static final String RETRIEVE_ALL_PENDING_PAYMENTS = "SELECT * FROM " + Revenue.TABLE_NAME + 
			" WHERE NOT " + Revenue.PAYMENT_CONFIRMED_COLUMN;
	
	private static final String UPDATE_PAYMENT_CONFIRMATION_STATUS = "UPDATE " + Revenue.TABLE_NAME + 
			" SET " + Revenue.PAYMENT_CONFIRMED_COLUMN + " = :confirmationStatus WHERE " + Revenue.ID_COLUMN + " = :id";
	
	public List<RevenueSummary> revenuePayableToMember(Long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", memberId);
		
		List<RevenueSummary> results = namedParameterJdbcTemplate.query(REVENUE_PAYABLE_TO_MEMBER, parameters, new RevenueRowMapper());
		Collections.sort(results);
		Collections.reverse(results);
		
		return results;
	}
	
	public List<RevenueSummary> salesOverThePastYear(Long memberId) {
		LocalDate aYearAgo = LocalDate.now().minusYears(1l);
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", memberId);
		parameters.addValue("yearToDate", aYearAgo);
		
		List<RevenueSummary> results = namedParameterJdbcTemplate.query(ANNUAL_SALES_FOR_MEMBER, parameters, new RevenueRowMapper());
		Collections.sort(results);
		Collections.reverse(results);
		
		return results;
	}
	
	public void updatePaymentDetailsForMember(Long memberId, String paymentId, Boolean resolved) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("resolved", resolved);
		parameters.addValue("paymentId", paymentId);
		parameters.addValue("memberId", memberId);
		
		namedParameterJdbcTemplate.update(UPDATE_REVENUES_RESOLVED_FOR_USER, parameters);
	}
	
	public List<RevenuePayment> findOutstandingPayments() {
		List<RevenuePayment> results = jdbcTemplate.query(RETRIEVE_ALL_PENDING_PAYMENTS, new RevenuePaymentRowMapper());
		
		return results;
	}
	
	public void updateRevenuePaymentStatus(long revenueId, boolean paymentConfirmed) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("id", revenueId);
		parameters.addValue("confirmationStatus", paymentConfirmed);
		
		namedParameterJdbcTemplate.update(UPDATE_PAYMENT_CONFIRMATION_STATUS, parameters);
	}

}
