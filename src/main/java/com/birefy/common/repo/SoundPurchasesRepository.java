package com.birefy.common.repo;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.birefy.common.dto.sound.SoundNameAndCategory;
import com.birefy.common.entity.Sound;
import com.birefy.common.entity.SoundPurchases;
import com.birefy.common.entity.utils.database.SoundNameAndCategoryMapper;

@Repository
public class SoundPurchasesRepository extends CommonRepository<SoundPurchases> {
	private static final String HAS_PURCHASED_SOUND = "SELECT COUNT(*) > 0 FROM " + SoundPurchases.TABLE_NAME +
			" WHERE " + SoundPurchases.MEMBER_COLUMN + " = :memberId AND " + SoundPurchases.SOUND_COLUMN + " = :soundId";
	
	private static final String USER_PURCHASES = "SELECT " + Sound.CATEGORY_COLUMN + ", " + Sound.NAME_COLUMN + 
			" FROM " + Sound.TABLE_NAME + " INNER JOIN " + SoundPurchases.TABLE_NAME + 
			" ON " + Sound.ID_COLUMN + " = " + SoundPurchases.SOUND_COLUMN +
			" WHERE " + SoundPurchases.TABLE_NAME + "." + SoundPurchases.MEMBER_COLUMN + " = :memberId" +
			" OR " + Sound.TABLE_NAME + "." + Sound.MEMBER_COLUMN + " = :memberId";
	
	public boolean userHasPurchasedSound(long memberId, long soundId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		parameters.addValue("soundId", soundId);
		
		boolean hasPurchased = namedParameterJdbcTemplate.queryForObject(HAS_PURCHASED_SOUND, parameters, Boolean.class);
		
		return hasPurchased;
	}
	
	public List<SoundNameAndCategory> purchasedSounds(long memberId) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("memberId", memberId);
		
		List<SoundNameAndCategory> results = 
				namedParameterJdbcTemplate.query(USER_PURCHASES, parameters, new SoundNameAndCategoryMapper());
		
		return results;
	}
	
}
