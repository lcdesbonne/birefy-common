package com.birefy.common.exception;

import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.OAuth2ErrorCodes;

public class UserVerificationException extends OAuth2Error {
	
	public UserVerificationException() {
		super(OAuth2ErrorCodes.ACCESS_DENIED);
	}
}
