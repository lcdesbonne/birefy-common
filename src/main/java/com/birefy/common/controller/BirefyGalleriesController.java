package com.birefy.common.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.painting.PaintingShortWithTidingId;
import com.birefy.common.dto.painting.PaintingTagDTO;
import com.birefy.common.entity.Painting;
import com.birefy.common.entity.tiding.MemberChannel;
import com.birefy.common.service.PaintingService;
import com.birefy.common.service.TidingsService;
import com.birefy.common.utils.BirefyApplicationUtils;

@Controller
@RequestMapping("birefy-galleries")
public class BirefyGalleriesController {
	private final Logger LOGGER = LoggerFactory.getLogger(BirefyGalleriesController.class);
	
	@Autowired
	private PaintingService paintingService;
	@Autowired
	private BirefyApplicationUtils birefyApplicationUtils;
	@Autowired
	private TidingsService tidingsService;
	
	
	@RequestMapping()
	public String artPortfolios(Model model) {
		List<PaintingShortWithTidingId> activePaintings = paintingService.allActivePaintings();
		
		if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof CommonUser) {
			CommonUser commonUser = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			long memberId = commonUser.getId();
			model.addAttribute("userTidingId", tidingsService.channelDetailsForMember(memberId));
		}
		
		model.addAttribute("allImages",activePaintings);
		model.addAttribute("galleryCategories", Painting.Category.values());
		model.addAttribute("applicationDomain", birefyApplicationUtils.getApplicationDomain());
		return "birefy-galleries";
	}
	
	@GetMapping("stream-painting")
	@ResponseBody
	public ResponseEntity<StreamingResponseBody> streamPainting(@RequestParam("id") Long id) {
		ResponseEntity<StreamingResponseBody> responseEntity;
		try {
			StreamingResponseBody response = paintingService.streamPaintingFile(id, 0);
			responseEntity = new ResponseEntity<StreamingResponseBody>(response, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("Error streaming image.", e);
			responseEntity = new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@GetMapping("all-paintings")
	@ResponseBody
	public List<PaintingShortWithTidingId> searchAllPaintings() {
		List<PaintingShortWithTidingId> paintings = paintingService.allActivePaintings();
		return paintings;
	}
	
	@GetMapping("all-paintings-with-tags")
	@ResponseBody
	public List<PaintingShortWithTidingId> searchPaintingsWithTags(@RequestParam("tagIds") List<Long> tagIds) {
		List<PaintingShortWithTidingId> paintings = paintingService.allActivePaintingsWithTags(tagIds);
		return paintings;
	}
	
	@GetMapping("find-image-by-title-and-category")
	@ResponseBody
	public List<PaintingShortWithTidingId> searchImageByTitle(@RequestParam("title") String title, @RequestParam("category") Painting.Category category) {
		List<PaintingShortWithTidingId> painting = paintingService.searchPaintingByTitleAndCategory(title, category);
		return painting;
	}
	
	@GetMapping("find-image-by-title-category-and-tags")
	@ResponseBody
	public List<PaintingShortWithTidingId> searchImageByTitleAndTags(@RequestParam("title") String title, @RequestParam("category") Painting.Category category, 
			@RequestParam("tagIds") List<Long> tagIds) {
		List<PaintingShortWithTidingId> paintings = paintingService.searchPaintingByTitleAndCategoryTags(title, category, tagIds);
		return paintings;
	}
	
	@GetMapping("search-painters")
	@ResponseBody
	public List<MemberChannel> searchPainters(@RequestParam("name") String name) {
		return paintingService.searchPainters(name);
	}
	
	@GetMapping("search-by-title-category-and-artist")
	@ResponseBody
	public List<PaintingShortWithTidingId> searchByTitleAndPainter(@RequestParam("title") String title, @RequestParam("category") Painting.Category category, @RequestParam("painter") Long memberId) {
		return paintingService.searchPaintingsByTitleCategoryAndArtist(title, category, memberId);
	}
	
	@GetMapping("search-by-title-category-and-artist-tags")
	@ResponseBody
	public List<PaintingShortWithTidingId> searchByTitleAndPainter(@RequestParam("title") String title, @RequestParam("category") Painting.Category category, @RequestParam("painter") Long memberId
			, @RequestParam("tagIds") List<Long> tagIds) {
		return paintingService.searchPaintingsByTitleCategoryAndArtistTags(title, category, memberId, tagIds);
	}
	
	@GetMapping("all-images-by-painter-category")
	@ResponseBody
	public List<PaintingShortWithTidingId> allImagesByPainter(@RequestParam("painter") Long memberId, @RequestParam("category") Painting.Category category) {
		return paintingService.allPaintingsByPainterAndCategory(memberId, category);
	}
	
	@GetMapping("all-images-by-painter-category-tags")
	@ResponseBody
	public List<PaintingShortWithTidingId> allImagesByPainterTags(@RequestParam("painter") Long memberId, @RequestParam("category") Painting.Category category
			, @RequestParam("tagIds") List<Long> tagIds) {
		return paintingService.allPaintingsByPainterAndCategoryTags(memberId, category, tagIds);
	}
	
	@GetMapping("paintings-by-category")
	@ResponseBody
	public List<PaintingShortWithTidingId> paintingsByCategory(@RequestParam("category") Painting.Category category) {
		return paintingService.allPaintingsByCategory(category);
	}
	
	@GetMapping("paintings-by-category-tags")
	@ResponseBody
	public List<PaintingShortWithTidingId> paintingsByCategoryTags(@RequestParam("category") Painting.Category category, @RequestParam("tagIds") List<Long> tagIds) {
		return paintingService.allPaintingsByCategoryTags(category, tagIds);
	}
	
	@GetMapping("artwork-by-painter")
	@ResponseBody
	public List<PaintingShortWithTidingId> artworkByPainter(@RequestParam("painter") Long memberId) {
		return paintingService.paintingsByArtist(memberId);
	}
	
	@GetMapping("artwork-by-painter-tags")
	@ResponseBody
	public List<PaintingShortWithTidingId> artworkByPainterTags(@RequestParam("painter") Long memberId, @RequestParam("tagIds") List<Long> tagIds) {
		return paintingService.paintingsByArtistTags(memberId, tagIds);
	}
	
	@GetMapping("paintings-by-title")
	@ResponseBody
	public List<PaintingShortWithTidingId> paintingsByTitle(@RequestParam("title") String title) {
		return paintingService.paintingsByTitle(title);
	}
	
	@GetMapping("paintings-by-title-tags")
	@ResponseBody
	public List<PaintingShortWithTidingId> paintingsByTitleTags(@RequestParam("title") String title, @RequestParam("tagIds") List<Long> tagIds) {
		return paintingService.paintingsByTitleTags(title, tagIds);
	}
	
	@GetMapping("paintings-by-artist-title")
	@ResponseBody
	public List<PaintingShortWithTidingId> paintingsByArtistAndTitle(@RequestParam("painter") Long memberId, @RequestParam("title") String title) {
		return paintingService.paintingsByArtistWithTitle(memberId, title);
	}
	
	@GetMapping("paintings-by-artist-title-tags")
	@ResponseBody
	public List<PaintingShortWithTidingId> paintingsByArtistAndTitleTags(@RequestParam("painter") Long memberId, @RequestParam("title") String title
			, @RequestParam("tagIds") List<Long> tagIds) {
		return paintingService.paintingsByArtistWithTitleTags(memberId, title, tagIds);
	}
	
	@GetMapping("search-painting-tags")
	@ResponseBody
	public List<PaintingTagDTO> searchPaintingTags(@RequestParam("tag")String tag) {
		return paintingService.findTagsForPainting(tag);
	}
}
