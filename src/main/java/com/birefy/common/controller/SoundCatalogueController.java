package com.birefy.common.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.birefy.common.authentication.AuthorityResolver;
import com.birefy.common.authentication.BirefyPlatformAuthority;
import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.sound.SoundTagDTO;
import com.birefy.common.dto.sound.SoundUpdate;
import com.birefy.common.dto.sound.SoundUpload;
import com.birefy.common.dto.sound.SoundWithTidingId;
import com.birefy.common.entity.Sound;
import com.birefy.common.service.FileUtilityActions;
import com.birefy.common.service.SoundService;
import com.birefy.common.utils.BirefyApplicationUtils;

@Controller
@RequestMapping("sound-catalogue")
public class SoundCatalogueController {
	@Autowired
	private SoundService soundService;
	@Autowired
	private FileUtilityActions fileUtilityActions;
	@Autowired
	private BirefyApplicationUtils birefyApplicationUtils;
	
	
	@RequestMapping()
	public String soundCatalogue(Model model) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/sound-catalogue");
		
		CommonUser user = (CommonUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		List<SoundWithTidingId> allSoundsByUser = soundService.getAllSoundsByUserWithPricing(user.getId());
		
		List<SoundWithTidingId> instrumentalsByUser = allSoundsByUser.stream().filter(s -> 
		s.getCategory().equals(Sound.Category.INSTRUMENTAL.toString())).collect(Collectors.toList());
		List<SoundWithTidingId> effectsByUser = allSoundsByUser.stream().filter(s -> 
		s.getCategory().equals(Sound.Category.EFFECT.toString())).collect(Collectors.toList());
		
		List<Long> soundIds = new ArrayList<>();
		for(SoundWithTidingId s : instrumentalsByUser) {
			soundIds.add(s.getId());
		}
		
		for(SoundWithTidingId s: effectsByUser) {
			soundIds.add(s.getId());
		}
		
		model.addAttribute("instrumentalsByUser", instrumentalsByUser);
		model.addAttribute("effectsByUser", effectsByUser);
		model.addAttribute("authority", user.getAuthorities().stream().findFirst().get().toString());
		model.addAttribute("soundCategories", Sound.Category.values());
		model.addAttribute("soundUpload", new SoundUpload());
		model.addAttribute("tagsForSounds", soundService.tagsForSounds(soundIds));
		model.addAttribute("maxFileSize", fileUtilityActions.getMaxFileSize());
		model.addAttribute("applicationDomain", birefyApplicationUtils.getApplicationDomain());
		return "sound-catalogue";
	}
	
	@PostMapping("save-sound")
	public String saveSound(@ModelAttribute("soundUpload") SoundUpload soundUpload, Model model) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/sound-catalogue/save-sound");
		
		boolean success = soundService.saveSoundFile(soundUpload);
		if(success) {
			model.addAttribute("message", "Successfully Uploaded Sound");
			
		} else {
			model.addAttribute("message", "Failed to Upload Sound");
		}
		
		model.addAttribute("redirectLocation", "Back To Sound Catalogue");
		model.addAttribute("redirectURL", "sound-catalogue");
		
		return "upload-response";
	}
	
	@PostMapping("delete-sound")
	@ResponseBody
	public boolean deleteSound(@RequestBody Long soundId) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/sound-catalogue/delete-sound");
		
		return soundService.deleteSoundFile(soundId);
	}
	

	@PostMapping("update-sound")
	@ResponseBody
	public boolean updateSound(@RequestBody SoundUpdate sound) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/sound-catalogue/update-sound");
		
		return soundService.updateSound(sound);
	}
}
