package com.birefy.common.controller;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.question.QQuestionTitleAndUrl;
import com.birefy.common.repo.TechnicalTagRepository;
import com.birefy.common.service.QQuestionService;
import com.birefy.common.service.TidingsService;
import com.birefy.common.utils.BirefyApplicationUtils;


@RequestMapping("innovation")
@Controller
public class InnovationController {
	private static final Logger LOGGER = LoggerFactory.getLogger(InnovationController.class);
	
	@Autowired
	private QQuestionService questionService;
	@Autowired
	private TidingsService tidingsService;
	@Autowired
	private TechnicalTagRepository technicalTagRepository;
	@Autowired
	private BirefyApplicationUtils birefyApplicationUtils;
		
	@RequestMapping()
	String innovationDisplay(@RequestParam("id") long id, Model model) {
		QQuestionTitleAndUrl data = questionService.getTitleAndUrlForQuestion(id);
		//model.addAttribute("url", birefyApplicationUtils.getApplicationDomainNoHttps() + ":" + data.getUrl());
		model.addAttribute("url", birefyApplicationUtils.getApplicationDomain() + "/innovation/view-question/q-" + id);
		model.addAttribute("title", data.getTitle());
		
		if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof CommonUser) {
			CommonUser commonUser = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			long memberId = commonUser.getId();
			model.addAttribute("userTidingId", tidingsService.channelDetailsForMember(memberId).getTidingId());
			model.addAttribute("contributorDetails", questionService.retrieveTidingIdsAndSkillsForContributorsToQuestion(id));
		}
		
		model.addAttribute("questionSkills", technicalTagRepository.getTechnicalTagsForQuestion(Arrays.asList(Long.valueOf(id))));
		return "innovation";
	}
	
//	@GetMapping("view-question")
//	HttpResponse viewQuestion(@RequestParam("id") long id) {
//		QQuestionTitleAndUrl data = questionService.getTitleAndUrlForQuestion(id);
//		String questionUrl = data.getUrl();
//		
//		//Use the id of the question to form the service URL.
//				
//		
//	}
	

}
