package com.birefy.common.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LegalDetailsController {
	@RequestMapping("privacy-policy")
	public String showPrivacyPolicy() {
		return "privacy-policy";
	}
	
	@RequestMapping("terms-of-service")
	public String showTermsOfService() {
		return "terms-of-service";
	}
}
