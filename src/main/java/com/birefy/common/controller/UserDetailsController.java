package com.birefy.common.controller;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.member.EditUserDTO;
import com.birefy.common.dto.member.MemberDTO;
import com.birefy.common.dto.tiding.TidingIdDTO;
import com.birefy.common.entity.Member;
import com.birefy.common.entity.utils.mapper.EditUserToMemberDTOMapper;
import com.birefy.common.service.MemberService;
import com.birefy.common.service.TidingsService;

@RequestMapping("editUser")
@Controller
public class UserDetailsController {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailsController.class);
	
	@Autowired
	private MemberService memberService;
	
	@Autowired
	private TidingsService tidingsService;
	
	@RequestMapping
	String userDetailsPage(Model model) {
		//Identify the user
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Member member = memberService.getMemberByEmail(user.getEmail()).get();
		TidingIdDTO tidingId = tidingsService.currentUserTidingId();
		
		EditUserDTO editUserDTO = new EditUserDTO(
				member.getFirstName(),
				member.getFamilyName(),
				member.getEmail(),
				member.getPhone(),
				tidingId.getTidingId()
				);
		
		model.addAttribute("editUserDTO", editUserDTO);
		
		LOGGER.trace("Loading details for member: "+member.getEmail());
		return "edit-user";
	}
	
	@PostMapping("update")
	String updateUserDetails(@Valid EditUserDTO editUserDTO, BindingResult validationResults) {
		String result;
		if(validationResults.hasErrors()) {
			result = "edit-user";
		} else if(memberService.applyDetailUpdates(EditUserToMemberDTOMapper.map(editUserDTO))) {
			result = "redirect:/home"; 
		} else {
			result = "redirect:/error";
		}
		return result;
	}
}
