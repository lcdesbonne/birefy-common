package com.birefy.common.controller;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.birefy.common.dto.CommonUser;
import com.birefy.common.dto.member.MemberDTO;
import com.birefy.common.entity.Member;
import com.birefy.common.events.OnSignUpCompleteEvent;
import com.birefy.common.messages.MessageProperty;
import com.birefy.common.security.Authority;
import com.birefy.common.service.MemberService;

@Controller
@RequestMapping("/sign-up")
public class SignUpController implements WebMvcConfigurer {
	private static Logger LOGGER = LoggerFactory.getLogger(SignUpController.class);
	
	@Autowired
	private MemberService memberService;
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	@RequestMapping
	public String signUp(MemberDTO memberDTO) {
		return "signup";
	}
	
	@PostMapping(value = "new-user")
	public String newUser(@Valid MemberDTO memberDTO, BindingResult validationResults) {
		if (validationResults.hasErrors()) {
			return "signup";
		}
		
		Member member = memberService.addNewMember(memberDTO);
		
		boolean success = member != null;
		
		if(success) {
			//Generate the event that triggers email confirmation
			try {
				CommonUser user = new CommonUser(
						member.getId(),
						member.getFirstName(),
						member.getFamilyName(),
						member.getEmail(),
						member.getKey(),
						Arrays.asList(Authority.CITIZEN),
						true
						);
								
				eventPublisher.publishEvent(new OnSignUpCompleteEvent(user));
			} catch (Exception e) {
				success = false;
				LOGGER.error("Error generating verification details for member: "+memberDTO.getEmail(), e);
			}
		}
		
		return "redirect:/home";
	}
	
//	@RequestMapping("verify")
//	public String verifyMember(@RequestParam("token") String token, Model model) {
//		boolean verified = memberService.verifyMember(token);
//		
//		model.addAttribute("success", verified);
//		
//		return "verifyconfirm";
//	}

}
