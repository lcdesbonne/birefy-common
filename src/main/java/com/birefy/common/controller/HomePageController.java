package com.birefy.common.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.birefy.common.authentication.credentials.CommonUser;

@Controller
public class HomePageController {
	private static final Logger LOGGER = LoggerFactory.getLogger(HomePageController.class);
	
	@RequestMapping("/")
	ModelAndView forwardToHome() {
		return new ModelAndView("home");
	}
	
	@RequestMapping("/home")
	String index(Model model) {
		LOGGER.info("Rendering home.html");
		try {
			CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (user.getId() != 0) {
				model.addAttribute("authenticated", Boolean.TRUE);
				model.addAttribute("authority", user.getAuthorities().stream().findFirst().get().toString());
			}
		} catch (ClassCastException e) {
			LOGGER.info("Unable to cast principal to user details", e);
		}
		
		return "home";
	}

}
