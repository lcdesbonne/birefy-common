package com.birefy.common.controller;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.capital.PaymentStatus;
import com.birefy.common.capital.PurchaseService;
import com.birefy.common.dto.capital.ChargeDetails;
import com.birefy.common.dto.capital.CurrencyValue;
import com.birefy.common.dto.capital.DigitalInventory;
import com.birefy.common.dto.capital.Purchase;
import com.birefy.common.service.DealInventoryDownloadService;
import com.birefy.common.service.PaintingDownloadService;
import com.birefy.common.service.SoundDownloadService;
import com.birefy.common.utils.BirefyApplicationUtils;
import com.stripe.model.PaymentIntent;

//@Controller
//@RequestMapping("purchase")
public class PurchasesController {
	private static final Logger LOGGER = LoggerFactory.getLogger(PurchasesController.class);
	@Autowired
	private PurchaseService purchaseService;
	@Autowired
	private SoundDownloadService soundDownloadService;
	@Autowired
	private PaintingDownloadService paintingDownloadService;
	@Autowired
	private DealInventoryDownloadService dealInventoryDownloadService;
	@Autowired
	private BirefyApplicationUtils birefyUtils;
	
	@GetMapping()
	public String loadPurchasePage(@RequestParam("id") Long id, 
			@RequestParam("category") ChargeDetails.MediaCategory category, Model model) {
		
		//Check to see if the user is logged in
		CommonUser user = (CommonUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		model.addAttribute("itemId", id);
		model.addAttribute("category", category);
		
		PaymentIntent paymentIntent = purchaseService.generateChargeDetails(id, category, user.getId());

		model.addAttribute("productName", paymentIntent.getDescription());
		model.addAttribute("productPrice", paymentIntent.getAmount() + 
				(Optional.ofNullable(paymentIntent.getApplicationFeeAmount()).isPresent() ? paymentIntent.getApplicationFeeAmount() : 0l));
		model.addAttribute("customerId", user.getId());
		model.addAttribute("supplierId", purchaseService.getOwnerOfProduce(id, category));
		model.addAttribute("piId", paymentIntent.getId());
		model.addAttribute("clientSecret", paymentIntent.getClientSecret());
		
		return "checkout";
	}
	
	@PostMapping("action")
	@ResponseBody
	public String purchase(@RequestBody DigitalInventory purchase) { 
//		throw new RuntimeException("Purchasing is currently unsupported.");
		
		boolean purchased = false;
		
		switch (purchase.getCategory()) {
		case DEAL: {
			purchased = dealInventoryDownloadService.userCanDownloadContent(purchase.getId());
			break;
		}
		case PAINTING: {
			purchased = paintingDownloadService.userCanDownloadFile(purchase.getId());
			break;
		}
		case SOUND: {
			purchased = soundDownloadService.userCanDownloadFile(purchase.getId());
		}
		}
		
		if (!purchased) {
			throw new RuntimeException("Content not purchased! " + purchase.toString());
		}
		
		String view;
		if(purchased) {
			switch (purchase.getCategory()) {
			case DEAL: {
				view = birefyUtils.getApplicationDomain() + "/deals";
				break;
			}
			case PAINTING: {
				view = birefyUtils.getApplicationDomain() + "/image-download?imageId=" + purchase.getId();
				break;
			}
			case SOUND: {
				view = birefyUtils.getApplicationDomain() + "/sound-download?soundId=" + purchase.getId();
				break;
			}
			default: {
				LOGGER.error("Unable to establish produce type");
				throw new RuntimeException("Unable to establish produce type for purchase");
			}
			}
		} else {
			throw new RuntimeException("Error purchasing produce. Please contact support. Time: " + LocalDateTime.now().toString());
		}
		return view;
	}
	
	@PostMapping("post-process-payment")
	@ResponseBody
	public PaymentStatus postProcessPayment(@RequestBody PaymentIntent paymentIntent) {
		return purchaseService.postProcessPaymentIntent(paymentIntent);
	}
	
}
