package com.birefy.common.controller;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import com.birefy.common.authentication.BirefyPlatformAuthority;
import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.constants.QuestionState;
import com.birefy.common.dto.question.QuestionTechTags;
import com.birefy.common.dto.question.QuestionUrlUpdate;
import com.birefy.common.dto.topic.QTopicAndQQuestionsFull;
import com.birefy.common.dto.topic.TopicAndTags;
import com.birefy.common.entity.QTag;
import com.birefy.common.service.AdministrationService;
import com.birefy.common.service.TechEstateService;

@RequestMapping("admin")
@Controller
public class AdministrationController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AdministrationController.class);
	
	@Autowired
	private AdministrationService administrationService;
	@Autowired
	private TechEstateService techEstateService;
		
	@RequestMapping()
	public String renderAdminPage() {
		return "admin";
	}
	
	@PostMapping("deleteTagFromTopic")
	@ResponseBody
	void deleteTagFromTopic(@RequestBody TopicAndTags deleteTags) {
		LOGGER.info("Deleting tags from topic with id "+deleteTags.getTopicId());
		administrationService.deleteTagsFromTopic(deleteTags);
	}
	
	@PostMapping("addTagsToTopic")
	@ResponseBody
	void addTagToTopic(@RequestBody TopicAndTags newTags) {
		administrationService.addTagsToTopic(newTags);
	}
	
	@PostMapping("deactivateTopic")
	@ResponseBody
	void deativateTopic(@RequestBody Long topicId) {
		administrationService.alterActiveStateTopic(topicId, false);
	}
	
	@PostMapping("activateTopic")
	@ResponseBody
	void activateTopic(@RequestBody Long topicId) {
		administrationService.alterActiveStateTopic(topicId, true);
	}
	
	@PostMapping("activateQuestion")
	@ResponseBody
	void activateQuestion(@RequestBody Long questionId) {
		administrationService.alterActiveStateQuestion(questionId, true);
	}
	
	@PostMapping("deactivateQuestion")
	@ResponseBody
	void deactivateQuestion(@RequestBody Long questionId) {
		administrationService.alterActiveStateQuestion(questionId, false);
	}
	
	@PostMapping("createTag")
	@ResponseBody
	void createTag(@RequestBody String tagName) {
		administrationService.createNewTag(tagName);
	}
	
	@GetMapping("tagsForTopic")
	@ResponseBody
	List<QTag> tagsForTopic(@RequestParam("topicId") long topicId) {
		return administrationService.getTagsForTopic(topicId);
	}
	
	@GetMapping("pendingQuestionsByTitle")
	@ResponseBody
	QTopicAndQQuestionsFull pendingQuestionsByTitle(@RequestParam("title") String title) {
		return administrationService.findPendingQuestionsByTitle(title);
	}
	
	@GetMapping("reviewQuestionsByTitle")
	@ResponseBody
	QTopicAndQQuestionsFull reviewQuestionsByTitle(@RequestParam("title") String title) {
		return administrationService.findReviewQuestionsByTitle(title);
	}
	
	@GetMapping("pendingTopicsByTitle")
	@ResponseBody
	QTopicAndQQuestionsFull pendingTopicsByTitle(@RequestParam("title") String title) {
		return administrationService.findPendingTopicsByTitle(title);
	}
	
	@GetMapping("pendingQuestionTagInfo")
	@ResponseBody
	List<QuestionTechTags> pendingQuestionTagInfo(@RequestParam("title") String title){
		return administrationService.findTechnicalTagsForQuestionByName(title, QuestionState.pending);
	}
	
	@GetMapping("activeQuestionTagInfo")
	@ResponseBody
	List<QuestionTechTags> activeQuestionTagInfo(@RequestParam("title") String title) {
		return administrationService.findTechnicalTagsForQuestionByName(title, QuestionState.active);
	}
	
	@GetMapping("reviewQuestionTagInfo")
	@ResponseBody
	List<QuestionTechTags> reviewQuestionTagInfo(@RequestParam("title") String title) {
		return administrationService.findTechnicalTagsForQuestionByName(title, QuestionState.review);
	}
	
	@GetMapping("questionTagsByIds")
	@ResponseBody
	List<QuestionTechTags> questionTagsByIds(@RequestParam("questionIds") List<Long> questionIds) {
		return techEstateService.techTagsForQuestions(questionIds);
	}
	
	@GetMapping("allTechTags")
	@ResponseBody
	List<String> allTechnicalTags() {
		List<String> allTechTags = techEstateService.findAvailableTechTags();
		return allTechTags;
	}
	
	@PostMapping("update-question-url")
	@ResponseBody
	Boolean updateQuestionUrl(@RequestBody QuestionUrlUpdate questionUrlUpdate) {
		return administrationService.updateQuestionUrl(questionUrlUpdate);
	}
	
	@PostMapping("update-question-source")
	@ResponseBody
	Boolean updateSourceUrl(@RequestBody QuestionUrlUpdate questionSourceUpdate) {
		return administrationService.updateSourceUrl(questionSourceUpdate);
	}
}
