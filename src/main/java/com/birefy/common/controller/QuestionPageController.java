package com.birefy.common.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.birefy.common.dto.topic.QTopicsAndQQuestions;
import com.birefy.common.entity.QTag;
import com.birefy.common.repo.QTagRepository;
import com.birefy.common.service.QQuestionService;

@RequestMapping("questions")
@Controller
public class QuestionPageController {
	private static final Logger LOGGER = LoggerFactory.getLogger(QuestionPageController.class);
	
	@Autowired
	private QQuestionService questionService;
	
	@Autowired
	private QTagRepository tagRepository;
	
	@RequestMapping()
	String questionSelection(Model model) {
		LOGGER.info("Rendering Question Selection page.");
		//Add a map of topics to questions and questions with details
		QTopicsAndQQuestions topicsAndQuestions = questionService.retrieveLiveQuestionData();
		model.addAttribute("topicsAndQuestions", topicsAndQuestions);
		
		return "questions";
	}
	
	@GetMapping("filterByTopicTitle")
	@ResponseBody
	protected QTopicsAndQQuestions filterByTopicTitle(@RequestParam("title") String title) {
		QTopicsAndQQuestions results = questionService.topicsByTitle(title, true);
		return results;
	}
	
	@GetMapping("filterQuestionByTitle")
	@ResponseBody
	protected QTopicsAndQQuestions filterByQuestionTitle(@RequestParam("title") String title) {
		QTopicsAndQQuestions results = questionService.questionsByTitle(title, true);
		return results;
	}
	
	@GetMapping("filterByTags")
	@ResponseBody
	protected QTopicsAndQQuestions filterByTags(@RequestParam("tags") List<Long> tags) {
		//find all the topics with the matching tags
		QTopicsAndQQuestions results = questionService.topicsByTags(tags, true);
		return results;
	}
	
	@GetMapping("filterByTopicNameAndTags")
	@ResponseBody
	protected QTopicsAndQQuestions filterByTopicNameAndTags(@RequestParam("name") String name, @RequestParam("tags") List<Long> tagIds) {
		QTopicsAndQQuestions results = questionService.questionsByTopicNameAndTags(name, tagIds, true);
		return results;
	}
	
	@GetMapping("filterByQuestionNameAndTags")
	@ResponseBody
	protected QTopicsAndQQuestions filterByQuestionNameAndTags(@RequestParam("title") String title, @RequestParam("tags") List<Long> tagIds) {
		QTopicsAndQQuestions results = questionService.questionsByQuestionNameAndTags(title, tagIds, true);
		return results;
	}
	
	@GetMapping("findTags")
	@ResponseBody
	protected List<QTag> findTags(@RequestParam("tagName") String tagName) {
		List<QTag> tags = new ArrayList<>();
		
		if (tagName.isEmpty()) {
			tags = tagRepository.findAll();
		} else {
			tags = tagRepository.searchByTitle(tagName); 
		}
		
		return tags;
	}
}
