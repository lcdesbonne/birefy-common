package com.birefy.common.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.birefy.common.dto.question.QQuestionShort;
import com.birefy.common.dto.tiding.TagUpdateDTO;
import com.birefy.common.dto.topic.QTopicShort;
import com.birefy.common.dto.topic.QTopicsAndQQuestions;
import com.birefy.common.service.QConceptProcessingService;

@RequestMapping("concepts")
@Controller
public class ConceptSubmissionController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ConceptSubmissionController.class);
	
	@Autowired
	private QConceptProcessingService conceptProcessingService;
	
	@RequestMapping()
	String conceptPage(Model model) {
		LOGGER.info("Rendering concept submission page");
		return "qconcept";
	}
	
	//Will either save or update a topic
	@PostMapping("processTopic")
	@ResponseBody
	Long saveTopic(@RequestBody QTopicShort topic) {
		return conceptProcessingService.processTopicData(topic);
	}
	
	@PostMapping("processQuestion")
	@ResponseBody
	Long saveQuestion(@RequestBody QQuestionShort question) {
		long questionId = conceptProcessingService.processQuestionData(question);
		return questionId;
	}
	
	@PostMapping("addTagsTopic")
	@ResponseBody
	void addTagsToTopic(@RequestBody TagUpdateDTO tagUpdateDetails) {
		conceptProcessingService.saveNewTopicTags(tagUpdateDetails);
	}
	
	@GetMapping("allTopicsByTitle")
	@ResponseBody
	QTopicsAndQQuestions allTopicsByTitle(@RequestParam("title") String title) {
		return conceptProcessingService.findAllTopicsByTitle(title);
	}
	
	@GetMapping("allQuestionsByTitle")
	@ResponseBody
	QTopicsAndQQuestions allQuestionsByTitle(@RequestParam("title") String title) {
		return conceptProcessingService.findAllQuestionsByTitle(title);
	}
}
