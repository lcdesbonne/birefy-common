package com.birefy.common.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.birefy.common.authentication.AuthorityResolver;
import com.birefy.common.authentication.BirefyPlatformAuthority;
import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.capital.MemberPaymentDetails;
import com.birefy.common.dto.capital.PassPhraseDTO;
import com.birefy.common.dto.capital.PassPhraseValidDTO;
import com.birefy.common.service.MemberVerificationService;
import com.birefy.common.service.PayoutService;

//@Controller
//@RequestMapping("revenue-payable")
public class RevenuePayableController {
	@Autowired
	private PayoutService payoutService;
	@Autowired
	private MemberVerificationService memberVerificationService;
	
	private static final String PASSPHRASE_FORM_OBJECT_NAME = "passPhraseValidDTO";
	
	@RequestMapping
	public String loadPassPhraseProtectedPage(Model model) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/revenue-payable");

		model.addAttribute("isConfigured", memberVerificationService.hasConfiguredVerificationDetails());
		model.addAttribute(PASSPHRASE_FORM_OBJECT_NAME, new PassPhraseValidDTO());
		
		return "payable-security-screen";
	}
	
	@PostMapping("load")
	public String loadPage(PassPhraseValidDTO passPhrase, BindingResult validationResults, Model model) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/revenue-payable/load");

		CommonUser user = (CommonUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		String resolvedLocation;
		
		if (!memberVerificationService.verifyPassphrase(user.getId(), passPhrase.getPassphrase())) {
			validationResults.addError(new FieldError(PASSPHRASE_FORM_OBJECT_NAME, "passphrase", "Incorrect password."));
			model.addAttribute("isConfigured", true);
			model.addAttribute(PASSPHRASE_FORM_OBJECT_NAME, passPhrase);
			resolvedLocation = "payable-security-screen";
			
		} else {
			model.addAttribute("revenueBreakdown", payoutService.calculateRevenueByMember(user.getId()));
			resolvedLocation = "revenue-payable";
			
		}
		
		return resolvedLocation;
	}
	
//	@PostMapping("collect-payment")
//	@ResponseBody
//	public Boolean collectPayment(@RequestBody MemberPaymentDetails paymentDetails) {
//		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/revenue-payable/collect-payment");
//
//		boolean success = payoutService.collectPayment(paymentDetails);
//		
//		return success;
//	}
	
	@GetMapping("passphrase-change-request")
	public String setPassphaseChangeRequest() {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/revenue-payable/passphrase-change-request");

		memberVerificationService.requestPasswordChange();
		return "passphrase-change-request";
	}
	
	@GetMapping("passphrase-update")
	public String setPassphrase(@RequestParam("token") String token, @RequestParam("member") Long memberId, Model model) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/revenue-payable/passphrase-update");

		memberVerificationService.setPassphraseUpdateable(token, memberId);
		model.addAttribute("passPhraseDTO", new PassPhraseDTO());
		return "passphrase-update";
	}
	
	@PostMapping("submit-new-passphrase")
	public String verifyPassphrase(@RequestBody @Valid PassPhraseDTO passphraseUpdate, Model model) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/revenue-payable/submit-new-passphrase");

		memberVerificationService.updatePassPhrase(passphraseUpdate.getPassphrase());
		return "verify-passphrase";
	}
}
