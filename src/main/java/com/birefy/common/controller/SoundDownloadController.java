package com.birefy.common.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import com.birefy.common.dto.capital.ChargeDetails;
import com.birefy.common.dto.sound.SoundWithTidingId;
import com.birefy.common.service.SoundDownloadService;
import com.birefy.common.service.SoundService;

@Controller
@RequestMapping("sound-download")
public class SoundDownloadController {
	@Autowired
	private SoundDownloadService soundDownloadService;
	@Autowired
	private SoundService soundService;
	
	private Float SOUND_VALUE = 0F;
	
	@GetMapping("redirect")
	public RedirectView redirectDownLoad(@RequestParam("soundId")Long soundId) {
		RedirectView view;
		if(soundDownloadService.userCanDownloadFile(soundId)) {
			view = new RedirectView("/sound-download?soundId="+soundId);
		} else {
			view = new RedirectView("/purchase?id="+soundId+"&category="+ChargeDetails.MediaCategory.SOUND);
		}
		return view;
	}
	
	@GetMapping
	public String soundDownloadPage(@RequestParam("soundId") Long soundId, Model model) {
		SoundWithTidingId sound = soundService.getSoundById(soundId);
		
		model.addAttribute("soundId", soundId);
		model.addAttribute("title", sound.getName());
		model.addAttribute("artist", sound.getTidingId());
		model.addAttribute("price", SOUND_VALUE);
		
		return "sound-download";
	}
	
	@GetMapping("download")
	@ResponseBody
	public boolean downloadFile(@RequestParam("soundId") Long soundId, HttpServletResponse response) {
		boolean success = soundDownloadService.downloadFile(soundId, response);
		return success;
	}
}
