package com.birefy.common.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.birefy.common.authentication.BirefyPlatformAuthority;
import com.birefy.common.authentication.AuthorityResolver;
import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.painting.PaintingShortWithTidingId;
import com.birefy.common.entity.Painting;
import com.birefy.common.service.PaintingService;
import com.birefy.common.utils.BirefyApplicationUtils;

@Controller
@RequestMapping("graphics-admin")
public class GraphicsAdminController {
	
	@Autowired
	private PaintingService paintingService;
	@Autowired
	private BirefyApplicationUtils birefyApplicationUtils;
	
	@Value("${application-url.domain}")
	private String applicationDomain;
	@Value("${application-url.port}")
	private String applicationPort;
	
	@RequestMapping()
	public String pageDefaults(Model model) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.ROYAL, "/graphics-admin");
		
		List<PaintingShortWithTidingId> recentUploads = paintingService.allPaintingsForReview();
		
		model.addAttribute("allRecentPaintings", recentUploads);
		model.addAttribute("categories", Painting.Category.values());
		model.addAttribute("applicationDomain", birefyApplicationUtils.getApplicationDomain());
		
		return "graphics-admin"; 
	}
	
	@PostMapping("approve")
	@ResponseBody
	public boolean approveImage(@RequestBody Long imageId) {
		return paintingService.approvePainting(imageId);
	}
	
	@GetMapping("review")
	@ResponseBody
	public List<PaintingShortWithTidingId> searchReviewSounds() {
		return paintingService.allPaintingsForReview();
	}
	
	@PostMapping("delete")
	@ResponseBody
	public boolean deletePainting(@RequestBody Long paintingId) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.ROYAL, "/graphics-admin/delete");
		
		return paintingService.deletePainting(paintingId);
	}
}
