package com.birefy.common.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.sound.SoundTagDTO;
import com.birefy.common.dto.sound.SoundWithTidingId;
import com.birefy.common.entity.Sound;
import com.birefy.common.entity.tiding.MemberChannel;
import com.birefy.common.service.SoundService;
import com.birefy.common.service.TidingsService;
import com.birefy.common.utils.BirefyApplicationUtils;

@Controller
@RequestMapping("sound")
public class SoundController {
	private final Logger LOGGER = LoggerFactory.getLogger(SoundController.class);
	
	@Autowired
	private SoundService soundService;
	@Autowired
	private TidingsService tidingsService;
	@Autowired
	private BirefyApplicationUtils birefyApplicationUtils;
	
	@RequestMapping()
	public String birefySoundPage(Model model) {
		List<SoundWithTidingId> recentInstrumentals = soundService.recentSounds(Sound.Category.INSTRUMENTAL);
		Sound.Category[] soundCategories = Sound.Category.values();
		
		if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof CommonUser) {
			CommonUser commonUser = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			long memberId = commonUser.getId();
			model.addAttribute("userTidingId", tidingsService.channelDetailsForMember(memberId));
		}
		
		model.addAttribute("recentInstrumentals", recentInstrumentals);
		model.addAttribute("soundCategories", soundCategories);
		model.addAttribute("applicationDomain", birefyApplicationUtils.getApplicationDomain());
		return "birefy-sound";
	}
	
	@GetMapping("find-recent-sounds")
	@ResponseBody
	public List<SoundWithTidingId> findRecentSounds(@RequestParam("category") Sound.Category category) {
		List<SoundWithTidingId> sounds = soundService.recentSounds(category);
		return sounds;
	}
	
	@GetMapping("find-recent-sounds-tags")
	@ResponseBody
	public List<SoundWithTidingId> findRecentSoundsTags(@RequestParam("category") Sound.Category category, @RequestParam("tagIds")List<Long> tagIds) {
		List<SoundWithTidingId> sounds = soundService.recentSoundsTags(category, tagIds);
		return sounds;
	}
	
	@GetMapping("stream-sound")
	@ResponseBody
	public ResponseEntity<StreamingResponseBody> streamRecent(@RequestParam("id") Long soundId) {
		ResponseEntity<StreamingResponseBody> responseEntity;
		try {
			StreamingResponseBody response = soundService.streamSoundFile(soundId, 0);
			responseEntity = new ResponseEntity<StreamingResponseBody>(response, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("Error streaming sound file with id: " + soundId, e);
			responseEntity = new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return responseEntity;
	}
	
	@GetMapping("find-sound-by-title-and-category")
	@ResponseBody
	public List<SoundWithTidingId> searchSoundsByTitle(@RequestParam("title") String title, @RequestParam("category") Sound.Category category) {
		List<SoundWithTidingId> sounds = soundService.searchSoundsByTitleAndCategory(title, category);
		return sounds;
	}
	
	@GetMapping("find-sound-by-title-and-category-tags")
	@ResponseBody
	public List<SoundWithTidingId> searchSoundsByTitleTags(@RequestParam("title") String title, 
			@RequestParam("category") Sound.Category category, @RequestParam("tagIds") List<Long> tagIds) {
		List<SoundWithTidingId> sounds = soundService.searchSoundsByTitleAndCategoryTags(title, category, tagIds);
		return sounds;
	}
	
	@GetMapping("search-artists")
	@ResponseBody
	public List<MemberChannel> searchArtists(@RequestParam("name") String name) {
		return soundService.searchArtists(name);
	}
	
	@GetMapping("search-by-songName-category-and-artist")
	@ResponseBody
	public List<SoundWithTidingId> searchBySongNameAndArtist(@RequestParam("songName")String songName, @RequestParam("artist") Long memberId, @RequestParam("category") Sound.Category category) {
		return soundService.searchSoundsByTitleCategoryAndArtist(songName, category, memberId);
	}
	
	@GetMapping("search-by-songName-category-and-artist-tags")
	@ResponseBody
	public List<SoundWithTidingId> searchBySongNameAndArtistTags(@RequestParam("songName")String songName, @RequestParam("artist") Long memberId, 
			@RequestParam("category") Sound.Category category, @RequestParam("tagIds") List<Long> tagIds) {
		return soundService.searchSoundsByTitleCategoryAndArtistTags(songName, category, memberId, tagIds);
	}
	
	@GetMapping("all-music-by-artist-category")
	@ResponseBody
	public List<SoundWithTidingId> allMusicByArtistAndCategory(@RequestParam("artist") Long memberId, @RequestParam("category") Sound.Category category) {
		return soundService.allSoundsByArtistAndCategory(memberId, category);
	}
	
	@GetMapping("all-music-by-artist-category-tags")
	@ResponseBody
	public List<SoundWithTidingId> allMusicByArtistAndCategoryTags(@RequestParam("artist") Long memberId, @RequestParam("category") Sound.Category category,
			@RequestParam("tagIds")List<Long> tagIds) {
		return soundService.allSoundsByArtistAndCategoryTags(memberId, category, tagIds);
	}
	
	@GetMapping("all-music-by-artist")
	@ResponseBody
	public List<SoundWithTidingId> allMusicByArtist(@RequestParam("memberId") Long memberId) {
		return soundService.searchSoundsByMember(memberId);
	}
	
	@GetMapping("all-music-by-artist-tags")
	@ResponseBody
	public List<SoundWithTidingId> allMusicByArtistTags(@RequestParam("memberId") Long memberId, @RequestParam("tagIds") List<Long> tagIds) {
		return soundService.searchSoundsByMemberTags(memberId, tagIds);
	}
	
	@GetMapping("all-recent-sounds")
	@ResponseBody List<SoundWithTidingId> allRecentSounds() {
		return soundService.allRecentSounds();
	}
	
	@GetMapping("sounds-by-title")
	@ResponseBody List<SoundWithTidingId> soundsByTitle(@RequestParam("title") String title) {
		return soundService.searchSoundsByTitle(title);
	}
	
	@GetMapping("sounds-by-title-tags")
	@ResponseBody List<SoundWithTidingId> soundsByTitle(@RequestParam("title") String title, @RequestParam("tagIds")List<Long> tagIds) {
		return soundService.searchSoundsByTitleTags(title, tagIds);
	}
	
	@GetMapping("sounds-by-member-title")
	@ResponseBody List<SoundWithTidingId> soundsByMemberWithTitle(@RequestParam("memberId") Long memberId, @RequestParam("title")String title) {
		return soundService.searchSoundsByArtistAndTitle(memberId, title);
	}
	
	@GetMapping("sounds-by-member-title-tags")
	@ResponseBody List<SoundWithTidingId> soundsByMemberWithTitle(@RequestParam("memberId") Long memberId, 
			@RequestParam("title")String title, @RequestParam("tagIds")List<Long> tagIds) {
		return soundService.searchSoundsByArtistAndTitleTags(memberId, title, tagIds);
	}
	
	@GetMapping("search-tags-by-name")
	@ResponseBody List<SoundTagDTO> searchTagsByName(@RequestParam("tagName") String tagName) {
		return soundService.searchForSoundTagsByName(tagName);
	}
}
