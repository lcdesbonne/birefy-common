package com.birefy.common.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.deal.NewDeal;
import com.birefy.common.dto.tiding.TidingAndMemberID;
import com.birefy.common.repo.TidingMessageRepository;
import com.birefy.common.service.BirefyDealsService;

@Controller
@RequestMapping("create-deal")
public class CreateDealController {
	@Autowired
	private TidingMessageRepository tidingMessageRepository;
	@Autowired
	private BirefyDealsService birefyDealsService;
	
	@RequestMapping
	public String createForm(Model model) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String tidingId = tidingMessageRepository.getTidingIdForMember(user.getId());
		
		TidingAndMemberID userDetails = new TidingAndMemberID(user.getId(), tidingId);
		
		model.addAttribute("user", userDetails);
		model.addAttribute("newDeal", new NewDeal());
		return "create-deal";
	}
	
	@PostMapping("create")
	public String createDeal(@Valid NewDeal newDeal, BindingResult validationResults, Model model) {
		if(validationResults.hasErrors()) {
			return "create-deal";
		}
		
		boolean success = birefyDealsService.saveNewDeal(newDeal);
		
		if(success) {
			model.addAttribute("message", "Successfully created deal");
		} else {
			model.addAttribute("message", "Failed to create deal, please try again or contact support");
		}
		
		model.addAttribute("redirectLocation", "To Deals");
		model.addAttribute("redirectUrl", "deals");
		return "upload-response";
	}
	
	@GetMapping("search-members")
	@ResponseBody
	public List<TidingAndMemberID> searchMembersByTidingId(@RequestParam("tidingId") String tidingId) {
		return tidingMessageRepository.findActiveMembersByTidingId(tidingId);
	}

}
