package com.birefy.common.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.birefy.common.authentication.AuthorityResolver;
import com.birefy.common.authentication.BirefyPlatformAuthority;
import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.deal.DealDTO;
import com.birefy.common.dto.deal.DealInventoryDTO;
import com.birefy.common.dto.deal.DealUpdateDTO;
import com.birefy.common.dto.deal.InventoryPreviewMetadata;
import com.birefy.common.dto.deal.InventoryUpdateDTO;
import com.birefy.common.service.BirefyDealsService;
import com.birefy.common.service.DealInventoryDownloadService;

@Controller
@RequestMapping("deals")
public class BirefyDealsController {
	private final Logger LOGGER = LoggerFactory.getLogger(BirefyDealsController.class);
	
	@Autowired
	private BirefyDealsService birefyDealsService;
	@Autowired
	private DealInventoryDownloadService dealInventoryDownloadService;
	
	@RequestMapping()
	public String loadDeals(Model model) {
		List<DealDTO> dealsWithMember = birefyDealsService.loadDealsWithMember();
		
		List<Long> dealIds = dealsWithMember.stream().map(DealDTO::getId).collect(Collectors.toList());
		List<DealInventoryDTO> inventory = birefyDealsService.inventoryForDeals(dealIds);
		
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		model.addAttribute("deals", dealsWithMember);
		model.addAttribute("inventory", inventory);
		model.addAttribute("userId", user.getId());
		return "birefy-deal";
	}
	
	@PostMapping("update-deal")
	@ResponseBody
	public boolean updateDeal(@RequestBody DealUpdateDTO dealUpdate) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/deals/update-deal");

		return birefyDealsService.updateDeal(dealUpdate);
	}
	
	@PostMapping("customer-confirm-deal")
	@ResponseBody
	public boolean customerConfirmDeal(@RequestBody Long dealId) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/deals/customer-confirm-deal");
		
		return birefyDealsService.customerConfirmDeal(dealId);
	}
	
	@PostMapping("supplier-confirm-deal")
	@ResponseBody
	public boolean supplierConfirmDeal(@RequestBody Long dealId) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/deals/supplier-confirm");
		
		return birefyDealsService.supplierConfirmDeal(dealId);
	}
	
	@PostMapping("customer-unconfirm-deal")
	@ResponseBody
	public boolean customerUnconfirmDeal(@RequestBody Long dealId) {
		return birefyDealsService.customerUnconfirmDeal(dealId);
	}
	
	@PostMapping("supplier-unconfirm-deal")
	@ResponseBody
	public boolean supplierUnconfirmDeal(@RequestBody Long dealId) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/deals/supplier-unconfirm-deal");
		
		return birefyDealsService.supplierUnconfirmDeal(dealId);
	}
	
	@PostMapping("reject")
	@ResponseBody
	public boolean rejectDeal(@RequestBody Long dealId) {
		return birefyDealsService.rejectDeal(dealId);
	}
	
	@PostMapping("inventory/update")
	@ResponseBody
	public boolean updateInventory(InventoryUpdateDTO inventoryUpdate) {
		return birefyDealsService.updateInventory(inventoryUpdate);
	}
	
	@PostMapping("delete-inventory")
	@ResponseBody
	public boolean deleteInventory(@RequestBody Long inventoryId) {
		return birefyDealsService.deleteInventory(inventoryId);
	}
	
	@GetMapping("download-inventory")
	@ResponseBody
	public boolean downloadInventory(@RequestParam("inventoryId") Long inventoryId, HttpServletResponse response) {
		return dealInventoryDownloadService.downloadFile(inventoryId, response);
	}
	
	@GetMapping("inventory/preview")
	public String previewInventory(@RequestParam("id") Long inventoryId, Model model) {
		InventoryPreviewMetadata metadata = birefyDealsService.inventoryPreviewMetadata(inventoryId);
		model.addAttribute("isSoundPreview", metadata.isSoundFile());
		model.addAttribute("contentId", metadata.getId());
		model.addAttribute("contentTitle", metadata.getTitle());
		model.addAttribute("contentUrl", metadata.getContentUrl());
		return "inventory-preview";
	}
	
	@RequestMapping("stream-inventory")
	@ResponseBody
	public ResponseEntity<StreamingResponseBody> streamImage(@RequestParam("id") Long inventoryId) {
		ResponseEntity<StreamingResponseBody> responseEntity;
		
		try {
			StreamingResponseBody response = birefyDealsService.previewInventoryContent(inventoryId);
			responseEntity = new ResponseEntity<StreamingResponseBody>(response, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("Error streaming content.", e);
			responseEntity = new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
}
