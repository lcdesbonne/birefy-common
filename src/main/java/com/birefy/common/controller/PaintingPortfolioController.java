package com.birefy.common.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.birefy.common.authentication.AuthorityResolver;
import com.birefy.common.authentication.BirefyPlatformAuthority;
import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.painting.PaintingShortWithTidingId;
import com.birefy.common.dto.painting.PaintingTagDTO;
import com.birefy.common.dto.painting.PaintingUpdate;
import com.birefy.common.dto.painting.PaintingUpload;
import com.birefy.common.entity.Painting;
import com.birefy.common.service.FileUtilityActions;
import com.birefy.common.service.PaintingService;
import com.birefy.common.utils.BirefyApplicationUtils;

@Controller
@RequestMapping("graphic-portfolio")
public class PaintingPortfolioController {
	@Autowired
	private PaintingService paintingService;
	@Autowired
	private FileUtilityActions fileUtilityActions;
	@Autowired
	private BirefyApplicationUtils birefyApplicationUtils;
	
	@RequestMapping()
	public String paintingPortfolio(Model model) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/graphic-portfolio");
		
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		List<PaintingShortWithTidingId> allPaintingsByMember = paintingService.paintingsByArtist(user.getId());
		
		List<PaintingShortWithTidingId> avatars = allPaintingsByMember.stream().filter(p -> p.getCategory().equals(Painting.Category.AVATAR)).collect(Collectors.toList());
		List<PaintingShortWithTidingId> scene = allPaintingsByMember.stream().filter(p -> p.getCategory().equals(Painting.Category.SCENE)).collect(Collectors.toList());;
		List<PaintingShortWithTidingId> prop = allPaintingsByMember.stream().filter(p -> p.getCategory().equals(Painting.Category.PROP)).collect(Collectors.toList());;
		List<PaintingShortWithTidingId> icon = allPaintingsByMember.stream().filter(p -> p.getCategory().equals(Painting.Category.ICON)).collect(Collectors.toList());;
		
		List<Long> allPaintingIds = new ArrayList<>();
		allPaintingIds.addAll(avatars.stream().map(PaintingShortWithTidingId::getId).collect(Collectors.toList()));
		allPaintingIds.addAll(scene.stream().map(PaintingShortWithTidingId::getId).collect(Collectors.toList()));
		allPaintingIds.addAll(prop.stream().map(PaintingShortWithTidingId::getId).collect(Collectors.toList()));
		allPaintingIds.addAll(icon.stream().map(PaintingShortWithTidingId::getId).collect(Collectors.toList()));
		
		Map<Long, List<PaintingTagDTO>> tagsForPaintings = paintingService.tagsForPaintings(allPaintingIds); 
		
		model.addAttribute("avatars",avatars);
		model.addAttribute("scenes", scene);
		model.addAttribute("props", prop);
		model.addAttribute("icon", icon);
		model.addAttribute("authority", user.getAuthorities().stream().findFirst().get().toString());
		model.addAttribute("imageCategories", Painting.Category.values());
		model.addAttribute("tagsForPaintings", tagsForPaintings);
		model.addAttribute("paintingUpload", new PaintingUpload());
		model.addAttribute("maxFileSize", fileUtilityActions.getMaxFileSize());
		model.addAttribute("applicationDomain", birefyApplicationUtils.getApplicationDomain());
		return "graphic-portfolio";
	}
	
	@PostMapping("save-painting")
	public String savePainting(@ModelAttribute("paintingUpload") PaintingUpload paintingUpload, Model model) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/graphic-portfolio/save-painting");
		
		if (StringUtils.isEmpty(paintingUpload.getSoftware()) || paintingUpload.getPaintingFile() == null 
				|| paintingUpload.getPaintingFile().isEmpty() || StringUtils.isEmpty(paintingUpload.getTitle())) {
			model.addAttribute("message", "The form has been completed incorrectly."
					+ "You must provide: "
					+ "title ,"
					+ "file ,"
					+ "and names of the software used to create the image (use commas to separate if more than one software)");
		} else {
			boolean success = paintingService.savePaintingFile(paintingUpload);

			if(success) {
				model.addAttribute("message", "Successfully Uploaded Image");
			} else {
				model.addAttribute("message", "Failed To Upload Image, try contacting support.");
			}
		}
			
		
		model.addAttribute("redirectLocation", "Back To Graphic Portfolio");
		model.addAttribute("redirectURL", "graphic-portfolio");
		return "upload-response";
	}
	
	@PostMapping("delete-painting")
	@ResponseBody
	public boolean deletePainting(@RequestBody Long paintingId) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/graphic-portfolio/delete-painting");

		return paintingService.deletePainting(paintingId);
	}
	
	@PostMapping("update-painting")
	@ResponseBody
	public boolean updatePainting(@RequestBody PaintingUpdate painting) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.LORD, "/graphic-portfolio/save-painting");

		return paintingService.updatePainting(painting);
	}
}
