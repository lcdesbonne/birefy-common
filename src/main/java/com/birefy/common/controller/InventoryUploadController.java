package com.birefy.common.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.deal.NewInventory;
import com.birefy.common.repo.DealRepository;
import com.birefy.common.service.BirefyDealsService;
import com.birefy.common.service.FileUtilityActions;

@Controller
@RequestMapping("inventory-upload")
public class InventoryUploadController {
	@Autowired
	private BirefyDealsService birefyDealsService;
	
	@GetMapping
	public String initiateUpload(@RequestParam("dealId") Long dealId, Model model) {
		birefyDealsService.prepareUploadOfInventory(dealId, model);
		return "upload-inventory";
	}
	
	@PostMapping("upload")
	public String uploadInventory(@Valid @ModelAttribute("newInventory") NewInventory newUpload, BindingResult validationResults, Model model) {
		if(validationResults.hasErrors()) {
			return "upload-inventory";
		}
		
		boolean success = birefyDealsService.saveNewInventory(newUpload);
		if(success) {
			model.addAttribute("message", "Successfully Uploaded Image");
		} else {
			model.addAttribute("message", "Failed To Upload Image");
		}
		
		model.addAttribute("redirectLocation", "Back To Deals");
		model.addAttribute("redirectURL", "deals");
		return "upload-response";
	}
}
