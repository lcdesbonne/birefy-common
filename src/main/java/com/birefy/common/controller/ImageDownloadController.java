package com.birefy.common.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import com.birefy.common.dto.capital.ChargeDetails;
import com.birefy.common.dto.painting.PaintingShortWithTidingId;
import com.birefy.common.service.PaintingDownloadService;
import com.birefy.common.service.PaintingService;

@Controller
@RequestMapping("image-download")
public class ImageDownloadController {
	@Autowired
	private PaintingDownloadService paintingDownloadService;
	@Autowired
	private PaintingService paintingService;
	
	@GetMapping("redirect")
	public RedirectView redirectDownload(@RequestParam("imageId") Long imageId) {
		RedirectView view;
		if(paintingDownloadService.userCanDownloadFile(imageId)) {
			view = new RedirectView("/image-download?imageId="+imageId);
		} else {
			view = new RedirectView("/purchase?id="+imageId+"&category="+ChargeDetails.MediaCategory.PAINTING);
		}
		return view;
	}
	
	@GetMapping
	public String imageDownloadPage(@RequestParam("imageId")Long imageId, Model model) {
		PaintingShortWithTidingId painting = paintingService.getPaintingById(imageId);
		model.addAttribute("title", painting.getName());
		model.addAttribute("artist", painting.getTidingId());
		model.addAttribute("imageId", painting.getId());
		return "image-download";
	}
	
	@GetMapping("download")
	@ResponseBody
	public boolean downloadFile(@RequestParam("imageId") Long imageId, HttpServletResponse response) {
		boolean success = paintingDownloadService.downloadFile(imageId, response);
		return success;
	}
}
