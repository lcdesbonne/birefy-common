package com.birefy.common.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.birefy.common.authentication.BirefyPlatformAuthority;
import com.birefy.common.authentication.AuthorityResolver;
import com.birefy.common.dto.sound.SoundWithTidingId;
import com.birefy.common.entity.Sound;
import com.birefy.common.service.SoundService;
import com.birefy.common.utils.BirefyApplicationUtils;

@Controller
@RequestMapping("sound-admin")
public class SoundAdminController {
	private final Logger LOGGER = LoggerFactory.getLogger(SoundAdminController.class);
	
	@Autowired
	private SoundService soundService;
	@Autowired
	private BirefyApplicationUtils birefyApplicationUtils;
	
	@RequestMapping()
	public String pageDefaults(Model model) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.ROYAL, "/sound-admin");
		
		model.addAttribute("recentSounds", soundService.allSoundsForReview());
		model.addAttribute("categories", Sound.Category.values());
		model.addAttribute("applicationDomain", birefyApplicationUtils.getApplicationUrl());
		
		return "sound-admin";
	}
	
	@PostMapping("approve")
	@ResponseBody
	public boolean approveSound(@RequestBody Long soundId) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.ROYAL, "sound-admin/approve");
		
		return soundService.approveSound(soundId);
	}
	
	@GetMapping("review")
	@ResponseBody
	public List<SoundWithTidingId> searchAllReviewSounds() {
		return soundService.allSoundsForReview();
	}
	
	@PostMapping("delete")
	@ResponseBody
	public boolean deleteSound(@RequestBody Long soundId) {
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.ROYAL, "/sound-admin/delete");
		return soundService.deleteSoundFile(soundId);
	}
}
