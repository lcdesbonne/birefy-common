package com.birefy.common.controller;


import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.birefy.common.dto.bulletin.BulletinShort;
import com.birefy.common.dto.bulletin.BulletinsWithTechTags;
import com.birefy.common.dto.bulletin.NewBulletinDTO;
import com.birefy.common.dto.tiding.Conversation;
import com.birefy.common.dto.tiding.NewGroupResponse;
import com.birefy.common.dto.tiding.TidingIdDTO;
import com.birefy.common.entity.Bulletin;
import com.birefy.common.entity.TechnicalTag;
import com.birefy.common.entity.tiding.Message;
import com.birefy.common.service.BulletinService;
import com.birefy.common.service.TidingsService;
import com.birefy.common.tidings.Tiding;

@Controller
@RequestMapping("tidings")
public class TidingsController {
	private static Logger LOGGER = LoggerFactory.getLogger(TidingsController.class);
	@Autowired
	private BulletinService bulletinService;
	@Autowired
	private TidingsService tidingService;
	
	@RequestMapping()
	String tidingsDashboard(Model model) {
		LOGGER.info("Loading tidings page");
		
		List<BulletinShort> targetedBulletins = bulletinService.allTargetedBulletinsForMember();
		List<BulletinShort> allAvailableBulletinsForMember = bulletinService.allAvailableBulletinsForMember();
		
		Set<BulletinShort> bulletins = new HashSet<>();
		bulletins.addAll(targetedBulletins);
		bulletins.addAll(allAvailableBulletinsForMember);
		
		model.addAttribute("bulletins", bulletins);
		
		List<Long> bulletinIds = bulletins.stream().map(bu -> bu.getId()).collect(Collectors.toList());
		Map<Long, List<String>> bulletinsAndTechTags = bulletinService.bulletinsAndTechTags(bulletinIds);
		model.addAttribute("bulletinAndTechTags", bulletinsAndTechTags);
		
		List<Conversation> recentConversations = tidingService.recentConversationsForUser();
		model.addAttribute("recentConversations", recentConversations);
		
		TidingIdDTO currentUserTidingId = tidingService.currentUserTidingId();
		model.addAttribute("currentUserTidingId", currentUserTidingId);
		
		model.addAttribute("technicalSkills", bulletinService.allAvailableTechnicalTags());
				
		return "tidings";
	}
	
	@GetMapping("conversationIsGroup")
	@ResponseBody
	public boolean conversationIsGroup(@RequestParam("tidingId")String tidingId) {
		return tidingService.tidingIsGroup(tidingId);
	}
	
	@GetMapping("channelForBulletin")
	@ResponseBody
	public List<TidingIdDTO> channelsForBulletin(@RequestParam("bulletinId") Long bulletinId) {
		return tidingService.contactChannelsForBulletin(bulletinId);
	}
	
	@GetMapping("searchUsers")
	@ResponseBody
	public List<TidingIdDTO> searchUsers(@RequestParam("tidingId") String userTidingId) {
		return tidingService.searchTidingUsersByTidingId(userTidingId);
	}
	
	@GetMapping("openConversation")
	@ResponseBody
	public Conversation openConversation(@RequestParam("tidingId") Long tidingId, 
			@RequestParam("receiverId") Long receiverId) {
		return tidingService.openConversation(tidingId, receiverId);
	}
	
	@PutMapping("createGroup")
	@ResponseBody
	public NewGroupResponse createGroup(@RequestBody String newGroup) {
		return tidingService.createGroup(newGroup);
	}
	
	@PutMapping("leaveGroup")
	@ResponseBody
	public boolean leaveGroup(@RequestBody Long groupChannelId) {
		boolean success = tidingService.leaveGroup(groupChannelId);
		return success;
	}
	
	@GetMapping("checkTidingIdExists")
	@ResponseBody
	public Map<String, Boolean> tidingIdExists(@RequestParam("tidingIds") List<String> tidingIds) {
		return tidingService.checkTidingIdExists(tidingIds);
	}
	
	@PostMapping("saveNewBulletin")
	@ResponseBody
	public boolean saveNewBulletin(@RequestBody NewBulletinDTO newBulletin) {
		Bulletin bulletin = new Bulletin();
		bulletin.setDescription(newBulletin.getDescription());
		bulletin.setTargeted(newBulletin.isTargeted());
		
		List<TechnicalTag> techTagsForBulletin = newBulletin.getTechnicalTags();
		boolean success = false;
		try {
			success = bulletinService.createNewBulletin(bulletin, techTagsForBulletin);
		} catch (Exception e) {
			LOGGER.error("Failed to create new Bulletin", e);
			return success;
		}
		return success;
	}
	
	@PostMapping("updateBulletin")
	@ResponseBody
	public Bulletin updateBulletin(@RequestBody Bulletin bulletin) {
		return bulletinService.updateBulletin(bulletin);
	}
	
	@GetMapping("bulletinsByUser")
	@ResponseBody
	public BulletinsWithTechTags getBulletinsByUser() {
		return bulletinService.getBulletinDataForMember();
	}
	
	@MessageMapping("/saveMessage")
	public void saveMessage(@Payload Message message) {
		tidingService.saveMessage(message);
	}
}
