package com.birefy.common.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ResolvableType;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import com.birefy.common.service.MemberService;

/**
 * Controller to authenticate users for login
 * @author Lennon
 *
 */
@Controller
@Order(100)
public class LoginController {
	Map<String, String> oauth2AuthenticationUrls = new HashMap<>();
	
	@Value("${security.oauth2.client.registration.google.authorization-uri}")
	private String authorizationBaseUri;
	
	@Autowired
	private ClientRegistrationRepository clientRegistrationRepository;
	@Autowired
	private OAuth2AuthorizedClientService authorizedClientService;
	@Autowired
	private MemberService memberService;
	
	@RequestMapping("login")
	public String loginPage(Model model) {
		Iterable<ClientRegistration> clientRegistrations = null;
		ResolvableType type = ResolvableType.forInstance(clientRegistrationRepository)
				.as(Iterable.class);
		
		if (type != ResolvableType.NONE && 
				ClientRegistration.class.isAssignableFrom(type.resolveGenerics()[0])) {
			clientRegistrations = (Iterable<ClientRegistration>) clientRegistrationRepository;
		}
		
		clientRegistrations.forEach(registration -> 
				oauth2AuthenticationUrls.put(registration.getClientName(), 
						authorizationBaseUri + "/" + registration.getRegistrationId()));
		
		model.addAttribute("urls", oauth2AuthenticationUrls);
		
		return "login";
	}
	
	@RequestMapping("loginSuccess")
	public String loginSuccess(Model model, OAuth2AuthenticationToken authentication) {
		OAuth2AuthorizedClient client = authorizedClientService.loadAuthorizedClient(
				authentication.getAuthorizedClientRegistrationId(), authentication.getName());
		
		String userInfoEndpointUri = client.getClientRegistration()
				  .getProviderDetails().getUserInfoEndpoint().getUri();
		
		if (!StringUtils.isEmpty(userInfoEndpointUri)) {
		    RestTemplate restTemplate = new RestTemplate();
		    HttpHeaders headers = new HttpHeaders();
		    headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + client.getAccessToken()
		      .getTokenValue());
		    HttpEntity entity = new HttpEntity("", headers);
		    ResponseEntity<Map> response = restTemplate
		      .exchange(userInfoEndpointUri, HttpMethod.GET, entity, Map.class);
		    Map userAttributes = response.getBody();
		    model.addAttribute("name", userAttributes.get("name"));
		}		
				
		return "loginSuccess";
	}
	
	@RequestMapping("loginFailure")
	public String loginFailure() {
		return "loginFailure";
	}
	
}
