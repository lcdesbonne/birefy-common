package com.birefy.common.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.birefy.common.authentication.BirefyPlatformAuthority;
import com.birefy.common.authentication.AuthorityResolver;
import com.birefy.common.authentication.credentials.CommonUser;
import com.birefy.common.dto.question.QQuestionDTO;
import com.birefy.common.dto.question.QuestionContributor;
import com.birefy.common.dto.question.QuestionTagUpdate;
import com.birefy.common.dto.question.QuestionTechTags;
import com.birefy.common.dto.tiding.TidingAndMemberID;
import com.birefy.common.dto.topic.QTagWithTopic;
import com.birefy.common.dto.topic.QTopicAndQQuestionsFull;
import com.birefy.common.dto.topic.TopicContributor;
import com.birefy.common.dto.topic.TopicUpdate;
import com.birefy.common.dto.topic.UpdatedTopicContributor;
import com.birefy.common.entity.QMemberQuestion;
import com.birefy.common.entity.QTag;
import com.birefy.common.entity.QTopic;
import com.birefy.common.entity.TechnicalTag;
import com.birefy.common.service.AdministrationService;
import com.birefy.common.service.MemberService;
import com.birefy.common.service.QQuestionService;
import com.birefy.common.service.TechEstateService;
import com.birefy.common.service.TopicService;

/**
 * The controller for the view displaying all of the projects the member has
 * contributed to
 * @author Lennon
 *
 */
@Controller
@RequestMapping("tech-estate")
public class TechEstateController {
	public static final Logger LOGGER = LoggerFactory.getLogger(TechEstateController.class);
	
	@Autowired
	private TechEstateService techEstateService;
	
	@Autowired
	private QQuestionService questionService;
	
	@Autowired
	private AdministrationService administrationService;
	
	@Autowired
	private MemberService memberService;
	
	@Autowired
	private TopicService topicService;
	
	@RequestMapping
	public String viewTechEstate(Model model) {
		//Identify the user and send their info
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		BirefyPlatformAuthority userAuthority = (BirefyPlatformAuthority) user.getAuthorities().stream().findFirst().get();
		
		AuthorityResolver.accessLevel(BirefyPlatformAuthority.CITIZEN, "/tech-estate");
		
		//Get all the questions and tags for this user
		Map<QTopicAndQQuestionsFull, Map<Long, List<QTag>>> userTechData = techEstateService.retrieveTechByUser(user.getId());
		
		List<Long> questionIds = new ArrayList<>();
		QTopicAndQQuestionsFull topsAndQs = new QTopicAndQQuestionsFull();
		List<QuestionTechTags> questionWithTechTags = new ArrayList<>();
		
		if(userTechData.keySet().stream().findFirst().isPresent()) {
			topsAndQs = userTechData.keySet().stream().findFirst().get();
			questionIds = topsAndQs.getQuestions().stream().map(q -> q.getId()).collect(Collectors.toList());
			questionWithTechTags = techEstateService.techTagsForQuestions(questionIds);
		}
				
		if (!userAuthority.equals(BirefyPlatformAuthority.ROYAL)) {
			final List<QQuestionDTO> questions = topsAndQs.getQuestions();
			//Remove the appUrl information from the data
			questions.forEach(question -> {
				question.setAppUrl(null);
			});
			//TODO refactor
			model.addAttribute("lordUser", true);
		}
		
		Map<Long, List<QuestionContributor>> questionContributorMap = questionService.findQuestionContributors(
				topsAndQs.getQuestions().stream().map(QQuestionDTO::getId).collect(Collectors.toList())
				);
		
		List<QTopic> topicsByMember = techEstateService.topicsByMember(user.getId());
		List<QTagWithTopic> tagsForMemberTopics = techEstateService.findTagsForTopics(
				topicsByMember.stream().map(QTopic::getId).collect(Collectors.toList()));
		Map<Long, List<TopicContributor>> topicContributors = questionService.findTopicContributors(
				topicsByMember.stream().map(QTopic::getId).collect(Collectors.toList()));
		
		//Add the data to the model
		model.addAttribute("topicsAndQuestions", topsAndQs);
		model.addAttribute("contributors", questionContributorMap);
		model.addAttribute("tagsForTopics", userTechData.get(topsAndQs));
		model.addAttribute("tagsForQuestions", questionWithTechTags);
		model.addAttribute("topicsByUser", topicsByMember);
		model.addAttribute("userTopicTags", tagsForMemberTopics);
		model.addAttribute("topicContributors", topicContributors);
		model.addAttribute("hasQuestions", !questionIds.isEmpty());
		
		LOGGER.info("Retrieving technology contributions for user");
		
		return "tech-estate";
	}
	
	@PostMapping("approval")
	@ResponseBody
	public Boolean submitQuestionForApproval(@RequestBody Long questionId) {
		//Check the users permission
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
			//Update the database with the new value
			try{
				administrationService.alterActiveStateQuestion(questionId, true);
			} catch (Exception e) {
				LOGGER.error("Failed to activate question.", e);
				return false;
			}
			if (!user.getAuthorities().stream().findFirst().get().equals(BirefyPlatformAuthority.ROYAL)) {
				return techEstateService.updateQuestionForReview(questionId, true);
			}
			return true;			
	}
	
	@PostMapping("update")
	@ResponseBody
	public Boolean updateQuestions(@RequestBody List<QQuestionDTO> questionsForUpdate) {		
		return techEstateService.applyQuestionUpdates(questionsForUpdate);
	}
	
	@PostMapping("deactivate")
	@ResponseBody
	public Boolean deactivateQuestion(@RequestBody Long questionId) {
		try {
			administrationService.alterActiveStateQuestion(questionId, false);
			return true;
		} catch (Exception e) {
			LOGGER.error("Error altering question state", e);
			return false;
		}
	}
	
	@PostMapping("update-question-tags")
	@ResponseBody
	public Boolean updateQuestionTags(@RequestBody List<QuestionTagUpdate> tagUpdates) {
		administrationService.updateQuestionTags(tagUpdates);
		return true;
	}
	
	@PostMapping("delete-contributors")
	@ResponseBody
	public void deleteContributorsToQuestion(@RequestBody List<QMemberQuestion> contributors) {
		questionService.deleteContributorsFromQuestion(contributors);
	}
	
	@PostMapping("add-contributors")
	@ResponseBody
	public void addContributorsToQuestion(@RequestBody List<QMemberQuestion> contributors) {
		questionService.addContributorsToQuestion(contributors);
	}
	
	@PostMapping("update-contributor-details")
	@ResponseBody
	public void updateContributorDetails(@RequestBody List<QMemberQuestion> contributors) {
		questionService.updateContributorDetails(contributors);
	}
	
	@PostMapping("update-topic-details")
	@ResponseBody
	public void updateChangesToTopic(@RequestBody List<TopicUpdate> topicUpdates) {
		topicService.updateTopicAndTopicTags(topicUpdates);
	}
	
	@PostMapping("add-topic-contributors")
	@ResponseBody
	public void addContributorsToTopic(List<UpdatedTopicContributor> addedContributors) {
		topicService.addNewContributorsToTopc(addedContributors);
	}
	
	@PostMapping("remove-topic-contributors")
	@ResponseBody
	public void removeContributorsFromTopic(List<UpdatedTopicContributor> removedContributors) {
		topicService.removeContributorsFromTopic(removedContributors);
	}
	
	@GetMapping("search-tech-tags")
	@ResponseBody
	public List<TechnicalTag> searchTechnicalTagsByName(@RequestParam("tagName") String tagName) {
		return techEstateService.findTechTagsByName(tagName);
	}
	
	@GetMapping("search-topic-tags")
	@ResponseBody
	public List<QTag> searchTagsForTopics(@RequestParam("tagName") String tagName) {
		return topicService.searchTopicTagsByName(tagName);
	}
	
	@GetMapping("search-members")
	@ResponseBody
	public List<TidingAndMemberID> findMemberByTidingId(@RequestParam("tiding") String tidingSearch) {
		return techEstateService.searcMembersByTidingId(tidingSearch);
		
	}
}
