package com.birefy.common.security;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;

import com.birefy.common.dto.CommonUser;

public class AuthorityResolver {
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthorityResolver.class);
	
	public static void accessLevel(Authority authority, String url) {
		CommonUser user = (CommonUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(!Optional.ofNullable(user).isPresent()) {
			switch ((Authority) user.getAuthorities().stream().findFirst().get()) {
			case ROYAL: return;
			case LORD : {
				if (authority == Authority.LORD) {
					return;
				} else {
					generateException(user, url);
				}
			};
			case CITIZEN : {
				if (authority == Authority.CITIZEN) {
					return;
				} else {
					generateException(user, url);
				}
			};
			default: generateException(user, url);
			}
		}
	}
	
	private static void generateException(CommonUser user, String url) {
		String userId = Optional.ofNullable(user).isPresent() ? String.valueOf(user.getId()) : "unknown";
		LOGGER.error("Permission denied for /graphics-admin for user " + userId);
		throw new AccessDeniedException("Permission denied for " + url + " " + userId);
	}
}
