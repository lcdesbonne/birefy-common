package com.birefy.common.security;

import org.springframework.security.core.GrantedAuthority;

public enum Authority implements GrantedAuthority {
	//Administration privileges
	ROYAL(2),
	//Allowed to modify project content they have created
	LORD(1), 
	//Allowed to view approved content only, possibly omit this
	CITIZEN(0);
	
	private int value;
	
	private Authority(int value) {
		this.value = value;
	}
	
	public int value() {
		return value;
	}
	
	@Override
	public String getAuthority() {
		return this.name();
	}
	
}
