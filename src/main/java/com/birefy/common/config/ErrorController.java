package com.birefy.common.config;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorController {
	private static Logger LOGGER = LoggerFactory.getLogger(ErrorController.class);
	
	@ExceptionHandler(Throwable.class)
	public String exception(final Throwable throwable, final Model model, HttpServletResponse response) {
		LOGGER.error("ERROR processing request " + throwable.getMessage());
		response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		return "error";
	}
}
