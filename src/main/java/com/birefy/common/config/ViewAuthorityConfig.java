package com.birefy.common.config;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.oauth2.client.InMemoryOAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.endpoint.DefaultAuthorizationCodeTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.client.web.HttpSessionOAuth2AuthorizationRequestRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

import com.birefy.common.authentication.credentials.CommonUser;

@Configuration
@EnableWebSecurity
public class ViewAuthorityConfig extends WebSecurityConfigurerAdapter {
	private static final List<String> clients = Arrays.asList("google");
	private static String CLIENT_PROPERTY_KEY = "security.oauth2.client.registration.";
	private static String APPLICATION_DOMAIN_KEY = "application-url.domain";
	private static String APPLICATION_URL_TEMPLATE = "https://%s/";
	private static String GOOGLE_CLIENT_ID_KEY = "security.oauth2.client.registration.google.client-id";
	
	@Resource
	public Environment env;
		
	@Autowired
	@Qualifier("birefyPlatformAuthenticationProvider")
	private OAuth2UserService<OidcUserRequest, OidcUser> birefyPlatformAuthenticationProvider;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {			
		http.authorizeRequests()
		.antMatchers("/webjars/**").permitAll()
		.antMatchers("/admin/**", "/concepts/**", "/tech-estate/**", "/editUser/**", "/tidings/**")
		.authenticated()
		.and()
		.oauth2Login()
		.loginPage("/login")
		.defaultSuccessUrl("/loginSuccess")
		.failureUrl("/loginFailure")
		.authorizationEndpoint()
		.baseUri(env.getProperty("security.oauth2.client.registration.google.authorization-uri"))
		.authorizationRequestRepository(authorizationRequestRepository())
		.and()
		.tokenEndpoint()
		.accessTokenResponseClient(accessTokenResponseClient())
		.and()
		.redirectionEndpoint()
		.baseUri(env.getProperty("security.oauth2.client.registration.google.authorize-redirect"))
		.and().userInfoEndpoint().customUserType(CommonUser.class, env.getProperty(GOOGLE_CLIENT_ID_KEY))
		.oidcUserService(birefyPlatformAuthenticationProvider);
		
		http.logout().logoutUrl("/logout");
	}
	
	@Bean
	public OAuth2AuthorizedClientService authorizedClientService() {
		return new InMemoryOAuth2AuthorizedClientService(
				clientRegistrationRepository()
				);
	}
	
	@Bean
	public ClientRegistrationRepository clientRegistrationRepository() {
		List<ClientRegistration> registrations = clients.stream()
				.map(c -> getRegistration(c))
				.filter(registration -> registration != null)
				.collect(Collectors.toList());
		
		return new InMemoryClientRegistrationRepository(registrations);
	}
	
	@Bean
	public AuthorizationRequestRepository<OAuth2AuthorizationRequest> authorizationRequestRepository() {
		return new HttpSessionOAuth2AuthorizationRequestRepository();
	}
	
	@Bean
	public OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> accessTokenResponseClient() {
		return new DefaultAuthorizationCodeTokenResponseClient();
	}
	
	private ClientRegistration getRegistration(String client) {
		String clientId = env.getProperty(CLIENT_PROPERTY_KEY + client + ".client-id");
		
		if (clientId == null) {
			return null;
		}
		
		String clientSecret = env.getProperty(CLIENT_PROPERTY_KEY + client + ".client-secret");
		
		if (client.equals("google")) {
			ClientRegistration clientRegistration = CommonOAuth2Provider.GOOGLE.getBuilder(client)
					.clientId(clientId).clientSecret(clientSecret).redirectUriTemplate(
							constructApplicationUrl() + env.getProperty("security.oauth2.client.registration.google.authorize-redirect"))
					.build();
			
			return clientRegistration;
		}
		
		return null;
	}
	
	private String constructApplicationUrl() {
		return String.format(APPLICATION_URL_TEMPLATE, 
				env.getProperty(APPLICATION_DOMAIN_KEY));
	}
	
 }
