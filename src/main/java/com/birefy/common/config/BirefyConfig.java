package com.birefy.common.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Locale;
import java.util.Properties;

import javax.mail.Session;
import javax.sql.DataSource;

import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.birefy.common.messages.BirefyMessageResolver;
import com.currencycloud.client.CurrencyCloudClient;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.gmail.GmailScopes;

import freemarker.template.Version;

@Configuration
public class BirefyConfig {
	private static String GMAIL_CREDENTIALS_PATH = "birefy-common-gsuite-cred.json";
	private static String GMAIL_TOKENS_DIRECTORY = "tokens";

	private JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
	
	private static Logger LOGGER = LoggerFactory.getLogger(BirefyConfig.class);
	
	@Value("${template.location}")
	private String templatesLocation;
	
	@Value("${email.location}")
	private String emailsLocation;
	
	@Value("${birefy.email.address}")
	private String birefyEmailAddress;
	
	@Value("${birefy.email.password}")
	private String birefyEmailPassword;
	
	@Value("${currencycloud.login.id}")
	private String currencyCloudLoginId;
	
	@Value("${currencycloud.api.key}")
	private String currencyCloudApiKey;
	
	@Value("${server.port.http}")
	private int serverPortHttp;
	
	@Value("${server.port}")
	private int serverPortHttps;
	
	@Bean("mailSender")
	public JavaMailSender mailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost("smtp.gmail.com");
		mailSender.setPort(587);
		
		mailSender.setUsername(birefyEmailAddress);
		mailSender.setPassword(birefyEmailPassword);
		
		Properties props = mailSender.getJavaMailProperties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.debug", "true");
		
		mailSender.setJavaMailProperties(props);
		
		return mailSender;
	}
	
	@Bean("mailSession")
	public Session mailSession() {
		Properties props = new Properties();
		
		return Session.getDefaultInstance(props);
	}
	
//	@Bean("gmailClient")
//	public Gmail gmailClient() {
//		//The recommendation from the docs is UrlFetchtransport from the app engine.
//		HttpTransport HTTP_TRANSPORT = new NetHttpTransport(); 
//		Gmail gmailService = null;
//		
//		try {
//			gmailService = new Gmail.Builder(HTTP_TRANSPORT, jsonFactory, gmailCredentials())
//					.setApplicationName("birefyapp")
//					.build();	
//		} catch (Exception e) {
//			LOGGER.error(e.getMessage());
//			System.err.print(e.getMessage());
//		}
//		
//		return gmailService;			
//	}
	
	@Deprecated
	private Credential gmailCredentials() throws IOException, GeneralSecurityException {
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		
		InputStream credentialsStream = classLoader.getResourceAsStream(GMAIL_CREDENTIALS_PATH);
		
		if (credentialsStream == null) {
			throw new FileNotFoundException("Unable to find message credentials: " + GMAIL_CREDENTIALS_PATH);
		}
		
		GoogleCredential credential = GoogleCredential.fromStream(credentialsStream, HTTP_TRANSPORT, jsonFactory)
				.createScoped(Arrays.asList(GmailScopes.GMAIL_SEND));
		
		return credential;
	}
	
	@Bean("verificationMessage")
	public MessageSource verificationMessage() {
		MessageSource verificiationMessage = new BirefyMessageResolver(emailContentTemplateProcessor());
		return verificiationMessage;
	}
	
	@ConfigurationProperties(prefix = "spring.datasource")
	@Bean("dataSource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}
	
	@Bean("jdbcTemplate")
	public JdbcTemplate jdbcTemplate() {
		return new JdbcTemplate(dataSource());
	}
	
	@Bean("namedParameterJdbcTemplate")
	public NamedParameterJdbcTemplate namedParameterJdbcTemplate() {
		return new NamedParameterJdbcTemplate(dataSource());
	}
	
	@ConfigurationProperties(prefix = "tidings.datasource")
	@Bean("tidingsDataSource")
	public DataSource tidingsDataSource() {
		return DataSourceBuilder.create().build();
	}
	
	@Bean("tidingsJdbcTemplate")
	public JdbcTemplate tidingsJdbcTemplate() {
		return new JdbcTemplate(tidingsDataSource());
	}
	
	@Bean("tidingsNamedParameterJdbcTemplate")
	public NamedParameterJdbcTemplate tidingsNamedParameterJdbcTemplate() {
		NamedParameterJdbcTemplate tidingsJdbcTemplate = new NamedParameterJdbcTemplate(tidingsDataSource());
		return tidingsJdbcTemplate;
	}
	
	@Bean("freeMarkerTemplateProcessor")
	public freemarker.template.Configuration freeMarkerTemplateProcessor() {
		freemarker.template.Configuration conf = new freemarker.template.Configuration(new Version("2.3.20"));//Version at the time of writing
		conf.setClassLoaderForTemplateLoading(ClassLoader.getSystemClassLoader(), templatesLocation);
		conf.setDefaultEncoding("UTF-8");
		conf.setLocale(Locale.ENGLISH);
		return conf;
	}; 
	
	@Bean("emailContentTemplateProcessor")
	public freemarker.template.Configuration emailContentTemplateProcessor() {
		freemarker.template.Configuration conf = new freemarker.template.Configuration(new Version("2.3.20"));//Version at the time of writing
		conf.setClassLoaderForTemplateLoading(ClassLoader.getSystemClassLoader(), emailsLocation);
		conf.setDefaultEncoding("UTF-8");
		conf.setLocale(Locale.ENGLISH);
		return conf;
	}
	
	@Bean
	public CurrencyCloudClient currencyCloudClient() {
		CurrencyCloudClient currencyCloud = new CurrencyCloudClient(
				CurrencyCloudClient.Environment.demo, currencyCloudLoginId, currencyCloudApiKey);
		
		return currencyCloud;
	}
	
	@Bean
	@Profile("develop")
	public ServletWebServerFactory servletContainer() {
		TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory() {
			@Override
			protected void postProcessContext(Context context) {
				SecurityConstraint securityConstraint = new SecurityConstraint();
				securityConstraint.setUserConstraint("CONFIDENTIAL");
				
				SecurityCollection collection = new SecurityCollection();
				collection.addPattern("/*");
				
				securityConstraint.addCollection(collection);
				context.addConstraint(securityConstraint);
			}
		};
		tomcat.addAdditionalTomcatConnectors(redirectConnector());
		
		return tomcat;
	}
	
	private Connector redirectConnector() {
		Connector connector = new Connector(
				TomcatServletWebServerFactory.DEFAULT_PROTOCOL);
		connector.setScheme("http");
		connector.setPort(serverPortHttp);
		connector.setSecure(false);
		connector.setRedirectPort(serverPortHttps);
		
		return connector;
	}

}
