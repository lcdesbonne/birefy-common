package com.birefy.common.entity.tiding;

import java.time.LocalDateTime;

public class Message implements Comparable<Message>{
	public static final String TABLE_NAME = "message";
	public static final String CONTENT_COLUMN = "content";
	public static final String CREATOR_COLUMN = "creator";
	public static final String RECIPIENT_COLUMN = "recipient";
	public static final String CREATION_DATE_COLUMN = "creation_date";
	
	private String content;
	private Long creator;
	private Long recipient;
	private LocalDateTime creationDate;
	
	public Message() {
		
	}
	
	public Message(String content, Long creator, Long recipient, LocalDateTime creationDate) {
		this.content = content;
		this.creator = creator;
		this.recipient = recipient;
		this.creationDate = creationDate;
	}
	
	public Message(String content, Long creator, Long recipient) {
		this.content = content;
		this.creator = creator;
		this.recipient = recipient;
		this.creationDate = LocalDateTime.now();
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator = creator;
	}

	public Long getRecipient() {
		return recipient;
	}

	public void setRecipient(Long recipient) {
		this.recipient = recipient;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public int compareTo(Message otherMessage) {
		int comparison;
				
		if (this.creationDate.equals(otherMessage.getCreationDate())) {
			comparison = 0;
		} else if (this.creationDate.isBefore(otherMessage.getCreationDate())) {
			comparison = 1;
		} else {
			comparison = -1;
		}
		
		return comparison;
	}
	
	@Override
	public boolean equals(Object other) {
		boolean isEqual = false;
		if (other instanceof Message) {
			Message otherMessage = (Message) other;
			
			if (
					this.creator == otherMessage.getCreator() &&
					this.recipient == otherMessage.getRecipient() && 
					this.content == otherMessage.getContent() &&
					this.creationDate.equals(otherMessage.getCreationDate())) {
				isEqual = true;
			}
		}
		
		return isEqual;
	}

}
