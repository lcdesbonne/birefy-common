package com.birefy.common.entity.tiding;

public class MemberChannel {
	public static final String TABLE_NAME = "member_channel";
	public static final String ID_COLUMN = "id";
	public static final String MEMBER_ID_COLUMN = "member_id";
	public static final String TIDING_ID_COLUMN = "tiding_id";
	public static final String ACTIVE_COLUMN = "active";
	public static final String MEMBER_AUTHORITY_COLUMN = "member_authority";
	
	private Long id;
	private Long memberId;
	private String tidingId;
	private boolean active;
	private Integer memberAuthority;
	
	public MemberChannel(Long memberId, String tidingId) {
		this.memberId = memberId;
		this.tidingId = tidingId;
	}
	
	public MemberChannel(Long id, Long memberId, String tidingId, boolean active) {
		this.id = id;
		this.memberId = memberId;
		this.tidingId = tidingId;
		this.active = active;
	}
	
	public MemberChannel() {
		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getMemberId() {
		return memberId;
	}
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	public String getTidingId() {
		return tidingId;
	}
	public void setTidingId(String tidingId) {
		this.tidingId = tidingId;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public Integer getMemberAuthority() {
		return memberAuthority;
	}
	
	public void setMemberAuthority(Integer memberAuthority) {
		this.memberAuthority = memberAuthority;
	}
}
