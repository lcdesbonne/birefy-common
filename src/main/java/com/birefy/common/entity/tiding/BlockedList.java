package com.birefy.common.entity.tiding;

public class BlockedList {
	public static final String TABLE_NAME = "blocked_list";
	public static final String BLOCKED_BY_COLUMN = "blocked_by";
	public static final String BLOCKED_COLUMN = "blocked";
	
	private Long blockedBy;
	private Long blocked;
	
	public BlockedList(Long blockedBy, Long blocked) {
		this.blockedBy = blockedBy;
		this.blocked = blocked;
	}
		
	public Long getBlockedBy() {
		return blockedBy;
	}

	public void setBlockedBy(Long blockedBy) {
		this.blockedBy = blockedBy;
	}

	public Long getBlocked() {
		return blocked;
	}
	

	public void setBlocked(Long blocked) {
		this.blocked = blocked;
	}
}
