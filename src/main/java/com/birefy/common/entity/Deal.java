package com.birefy.common.entity;

import java.time.LocalDateTime;

public class Deal extends CommonEntity {
	public static final String TABLE_NAME = "deal";
	public static final String ID_COLUMN = "id";
	public static final String CUSTOMER_ID_COLUMN = "customer_id";
	public static final String SUPPLIER_ID_COLUMN = "supplier_id";
	public static final String SUPPLIER_CONFIRM_COLUMN = "supplier_confirm";
	public static final String CUSTOMER_CONFIRM_COLUMN = "customer_confirm";
	public static final String SUPPLIER_DETAILS_COLUMN = "supplier_details";
	public static final String CUSTOMER_DETAILS_COLUMN = "customer_details";
	public static final String CREATION_DATE_COLUMN = "creation_date";
	public static final String ACTIVE_COLUMN = "active";
	public static final String PAID_COLUMN = "paid";
	public static final String PRICE_COLUMN = "price";
	
	private Long id;
	private Long customerId;
	private Long supplierId;
	private boolean supplierConfirm;
	private boolean customerConfirm;
	private String supplierDetails;
	private String customerDetails;
	private LocalDateTime creationDate;
	private Boolean active;
	private LocalDateTime paid;
	private Float price;
	
	public Deal(Long customerId, Long supplierId, String supplierDetails, String customerDetails,
			LocalDateTime creationDate, Boolean active, Float price) {
		this.customerId = customerId;
		this.supplierId = supplierId;
		this.supplierDetails = supplierDetails;
		this.customerDetails = customerDetails;
		this.creationDate = creationDate;
		this.active = active;
		this.price = price;
	}

	public Deal(Long customerId, Long supplierId, boolean supplierConfirm, boolean customerConfirm,
			String supplierDetails, String customerDetails, LocalDateTime creationDate, Boolean active,
			LocalDateTime paid, Float price) {
		this.customerId = customerId;
		this.supplierId = supplierId;
		this.supplierConfirm = supplierConfirm;
		this.customerConfirm = customerConfirm;
		this.supplierDetails = supplierDetails;
		this.customerDetails = customerDetails;
		this.creationDate = creationDate;
		this.active = active;
		this.paid = paid;
		this.price = price;
	}

	public Deal(Long id, Long customerId, Long supplierId, boolean supplierConfirm, boolean customerConfirm,
			String supplierDetails, String customerDetails, LocalDateTime creationDate, Boolean active,
			LocalDateTime paid, Float price) {
		this.id = id;
		this.customerId = customerId;
		this.supplierId = supplierId;
		this.supplierConfirm = supplierConfirm;
		this.customerConfirm = customerConfirm;
		this.supplierDetails = supplierDetails;
		this.customerDetails = customerDetails;
		this.creationDate = creationDate;
		this.active = active;
		this.paid = paid;
		this.price = price;
	}
	
	public Deal() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public boolean isSupplierConfirm() {
		return supplierConfirm;
	}

	public void setSupplierConfirm(boolean supplierConfirm) {
		this.supplierConfirm = supplierConfirm;
	}

	public boolean isCustomerConfirm() {
		return customerConfirm;
	}

	public void setCustomerConfirm(boolean customerConfirm) {
		this.customerConfirm = customerConfirm;
	}

	public String getSupplierDetails() {
		return supplierDetails;
	}

	public void setSupplierDetails(String supplierDetails) {
		this.supplierDetails = supplierDetails;
	}

	public String getCustomerDetails() {
		return customerDetails;
	}

	public void setCustomerDetails(String customerDetails) {
		this.customerDetails = customerDetails;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public LocalDateTime getPaid() {
		return paid;
	}

	public void setPaid(LocalDateTime paid) {
		this.paid = paid;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}
