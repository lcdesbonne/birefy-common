package com.birefy.common.entity;

public class TechnicalTag extends CommonEntity {
	public transient static final String TABLE_NAME = "technical_tag";
	public transient static final String ID_COLUMN = "id";
	public transient static final String NAME_COLUMN = "name";
	
	private Long id;
	private String name;
	

	public TechnicalTag(Long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public TechnicalTag() {
		
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}



	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}
