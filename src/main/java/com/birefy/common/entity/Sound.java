package com.birefy.common.entity;

import java.time.LocalDateTime;

public class Sound extends CommonEntity {
	public static final String TABLE_NAME = "sound";
	public static final String ID_COLUMN = "id";
	public static final String MEMBER_COLUMN = "member_id";
	public static final String LOCATION_COLUMN = "location";
	public static final String CATEGORY_COLUMN = "category";
	public static final String NAME_COLUMN = "name";
	public static final String CREATION_DATE_COLUMN = "creation_date";
	public static final String PRICE_COLUMN = "price";
	public static final String SOFTWARE_COLUMN = "software";
	public static final String REVIEW_COLUMN = "review";
	
	private Long id;
	private Long memberId;
	private String location;
	private Category category;
	private String name;
	private LocalDateTime creationDate;
	private Float price;
	private String software;
	private Boolean review;
	
	public Sound() {
		this.creationDate = LocalDateTime.now();
	}
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Category getCategory() {
		return category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
	
	public String getSoftware() {
		return this.software;
	}
	
	public void setSoftware(String software) {
		this.software = software;
	}
	
	public boolean getReview() {
		return this.review;
	}
	
	public void setReview(boolean review) {
		this.review = review;
	}

	@Override
	public String getTableName() {
		return this.TABLE_NAME;
	}
	
	public enum Category {
		INSTRUMENTAL, EFFECT
	}

}
