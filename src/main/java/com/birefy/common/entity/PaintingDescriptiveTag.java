package com.birefy.common.entity;

public class PaintingDescriptiveTag extends CommonEntity {
	public static final String TABLE_NAME = "painting_descriptive_tag";
	public static final String PAINTING_COLUMN = "painting_id";
	public static final String TAG_COLUMN = "painting_tag_id";
	
	private Long paintingId;
	private Long paintingTagId;
	
	
	public PaintingDescriptiveTag() {
	}

	public PaintingDescriptiveTag(Long paintingId, Long paintingTagId) {
		this.paintingId = paintingId;
		this.paintingTagId = paintingTagId;
	}

	public Long getPaintingId() {
		return paintingId;
	}

	public void setPaintingId(Long paintingId) {
		this.paintingId = paintingId;
	}

	public Long getPaintingTagId() {
		return paintingTagId;
	}

	public void setPaintingTagId(Long paintingTagId) {
		this.paintingTagId = paintingTagId;
	}


	@Override
	public String getTableName() {
		return TABLE_NAME;
	}
}
