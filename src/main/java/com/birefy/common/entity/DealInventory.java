package com.birefy.common.entity;

public class DealInventory extends CommonEntity {
	public static final String TABLE_NAME = "deal_inventory";
	public static final String ID_COLUMN = "id";
	public static final String TITLE_COLUMN = "title";
	public static final String AUTHOR_COLUMN = "author_id";
	public static final String DEAL_ID_COLUMN = "deal_id";
	public static final String INVENTORY_TYPE_COLUMN = "inventory_type_id";
	public static final String LOCATION_COLUMN = "location";
	public static final String CAN_PREVIEW_COLUMN = "can_preview";
	public static final String APP_URL_COLUMN = "app_url";
	public static final String RUN_COMMANDS_COLUMN = "run_commands";
	public static final String ACTIVE_COLUMN = "active";
	
	private Long id;
	private Long dealId;
	private String title;
	private Long authorId;
	private Integer inventoryTypeId;
	private String location;
	private Boolean canPreview;
	private String appUrl;
	private String runCommands;
	private Boolean active;
	
	public Long getId() {
		return id;
	}

	public Long getDealId() {
		return dealId;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDealId(Long dealId) {
		this.dealId = dealId;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Integer getInventoryTypeId() {
		return inventoryTypeId;
	}

	public void setInventoryTypeId(Integer inventoryTypeId) {
		this.inventoryTypeId = inventoryTypeId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Boolean getCanPreview() {
		return canPreview;
	}

	public void setCanPreview(Boolean canPreview) {
		this.canPreview = canPreview;
	}

	public String getAppUrl() {
		return appUrl;
	}

	public void setAppUrl(String appUrl) {
		this.appUrl = appUrl;
	}

	public String getRunCommands() {
		return runCommands;
	}

	public void setRunCommands(String runCommands) {
		this.runCommands = runCommands;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	
	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
	
	public DealInventory() {
	}

	public DealInventory(Long id, String title, Long dealId, Long authorId, Integer inventoryTypeId, String location, Boolean canPreview, String appUrl,
			String runCommands, Boolean active) {
		this.id = id;
		this.title = title;
		this.dealId = dealId;
		this.authorId = authorId;
		this.inventoryTypeId = inventoryTypeId;
		this.location = location;
		this.canPreview = canPreview;
		this.appUrl = appUrl;
		this.runCommands = runCommands;
		this.active = active;
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}
