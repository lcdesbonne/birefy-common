package com.birefy.common.entity;

public class MemberBeneficiary extends CommonEntity {
	public static final String TABLE_NAME = "member_beneficiary";
	public static final String MEMBER_ID_COLUMN = "member_id";
	public static final String BENEFICIARY_ID_COLUMN = "beneficiary_id";
	
	private Long memberId;
	private String beneficiaryId;
	
	public MemberBeneficiary(Long memberId, String beneficiaryId) {
		this.memberId = memberId;
		this.beneficiaryId = beneficiaryId;
	}
	
	public Long getMemberId() {
		return memberId;
	}
	
	public String getBeneficiaryId() {
		return beneficiaryId;
	}
	
	@Override
	public String getTableName() {
		return TABLE_NAME;
	}
}
