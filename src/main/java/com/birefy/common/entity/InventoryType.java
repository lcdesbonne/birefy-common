package com.birefy.common.entity;

public class InventoryType extends CommonEntity {
	public static final String TABLE_NAME = "inventory_type";
	public static final String ID_COLUMN = "id";
	public static final String TYPE_COLUMN = "type";
	
	private Integer id;
	private String type;
	
	public InventoryType() {
	}

	public InventoryType(Integer id, String type) {
		this.id = id;
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}
