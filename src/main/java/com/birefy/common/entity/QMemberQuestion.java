package com.birefy.common.entity;

import javax.validation.constraints.NotNull;

public class QMemberQuestion extends CommonEntity {
	public static final String TABLE_NAME = "q_member_question";
	public static final String MEMBER_ID_COLUMN = "member_id";
	public static final String QUESTION_ID_COLUMN = "question_id";
	public static final String CONTRIBUTION_DETAILS_COLUMN = "details";
	
	@NotNull
	private Long memberId;
	@NotNull
	private Long questionId;
	private String details;
	
	public QMemberQuestion() {
		
	}

	public QMemberQuestion(Long memberId, Long questionId) {
		this.memberId = memberId;
		this.questionId = questionId;
	}
	
	public QMemberQuestion(Long memberId, Long questionId, String details) {
		this.memberId = memberId;
		this.questionId = questionId;
		this.details = details;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}
	
	public String getDetails() {
		return details;
	}
	
	public void setDetails(String details) {
		this.details = details;
	}
	
	@Override
	public String getTableName() {
		return TABLE_NAME;
	}
	
}
