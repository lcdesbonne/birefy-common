package com.birefy.common.entity;

import java.time.LocalDate;

public class MemberVerification extends CommonEntity{
	public static final String TABLE_NAME = "member_verification";
	public static final String MEMBER_COLUMN = "member_id";
	public static final String TOKEN_COLUMN = "token";
	public static final String CREATION_DATE_COLUMN = "creation_date";
	public static final String PASSWORD_CHANGE_REQUEST_COLUMN = "change_password";
	
	private Long memberId;
	private String token;
	private LocalDate creationDate;
	private boolean changePassword;
	
	public MemberVerification() {
	}
	
	public MemberVerification(Long memberId, String token) {
		this.token = token;
		this.memberId = memberId;
		creationDate = LocalDate.now();
	}
	
	public MemberVerification(Long memberId, String token, LocalDate date) {
		this.memberId = memberId;
		this.token = token;
		this.creationDate = date;
	}
	
	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	public LocalDate getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDate date) {
		this.creationDate = date;
	}
	
	public boolean getChangePassword() {
		return changePassword;
	}
	
	public void setChangePassword(boolean changePassword) {
		this.changePassword = changePassword;
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}
