package com.birefy.common.entity;

import javax.validation.constraints.NotNull;

public class QTopicTag extends CommonEntity {
	public static final String TABLE_NAME = "q_topic_tag";
	public static final String TOPIC_ID_COLUMN = "topic_id";
	public static final String TAG_ID_COLUMN = "tag_id";
	
	@NotNull
	private Long topicId;
	@NotNull
	private Long tagId;
	
	public QTopicTag() {
		
	}
	
	public QTopicTag(Long topicId, Long tagId) {
		this.topicId = topicId;
		this.tagId = tagId;
	}

	public Long getTopicId() {
		return topicId;
	}

	public void setTopicId(Long topicId) {
		this.topicId = topicId;
	}

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}
	
	@Override
	public String getTableName() {
		return TABLE_NAME;
	}
	
}
