package com.birefy.common.entity;

import java.time.LocalDateTime;

public class Revenue extends CommonEntity {
	public static final String TABLE_NAME = "revenue";
	public static final String ID_COLUMN = "id";
	public static final String MEMBER_COLUMN = "member_id";
	public static final String PRODUCT_COLUMN = "product_id";
	public static final String DESCRIPTION_COLUMN = "description";
	public static final String CATEGORY_COLUMN = "category";
	public static final String INVENTORY_REQUEST_PRICE_COLUMN = "inventory_request_price";
	public static final String BIREFY_CHARGE_COLUMN = "birefy_charge";
	public static final String CREATED_COLUMN = "created";
	public static final String RESOLVED_COLUMN = "resolved";
	public static final String PAYMENT_ID_COLUMN = "payment_id";
	public static final String PAYMENT_CONFIRMED_COLUMN = "payment_confirmed";
	
	private Long id;
	private Long memberId;
	private Long productId;
	private String description;
	private String category;
	private Integer inventoryRequestPrice;
	private Integer birefyCharge;
	private LocalDateTime created;
	private Boolean resolved;
	private String paymentId;
	private Boolean paymentConfirmed;
	
	public Revenue() {
	}

	public Revenue(Long id, Long memberId, Long productId, String description, String category,
			Integer inventoryRequestPrice, Integer birefyCharge, LocalDateTime created, Boolean resolved) {
		this.id = id;
		this.memberId = memberId;
		this.productId = productId;
		this.description = description;
		this.category = category;
		this.inventoryRequestPrice = inventoryRequestPrice;
		this.birefyCharge = birefyCharge;
		this.created = created;
		this.resolved = resolved;
		this.paymentConfirmed = false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Integer getInventoryRequestPrice() {
		return inventoryRequestPrice;
	}

	public void setInventoryRequestPrice(Integer inventoryRequestPrice) {
		this.inventoryRequestPrice = inventoryRequestPrice;
	}

	public Integer getBirefyCharge() {
		return birefyCharge;
	}

	public void setBirefyCharge(Integer birefyCharge) {
		this.birefyCharge = birefyCharge;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public Boolean getResolved() {
		return resolved;
	}

	public void setResolved(Boolean resolved) {
		this.resolved = resolved;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	
	public Boolean isPaymentConfirmed() {
		return paymentConfirmed;
	}
	
	public void setIsPaymentConfirmed(Boolean paymentConfirmed) {
		this.paymentConfirmed = paymentConfirmed;
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}
	
	@Override
	public String toString() {
		return "{" +
				id +"," +
				memberId + "," +
				productId + "," +
				description + "," +
				category + "," +
				inventoryRequestPrice + "," +
				birefyCharge + "," +
				created + "," +
				resolved + "," +
				paymentId + "," +
				paymentConfirmed +
				"}";
	}
}
