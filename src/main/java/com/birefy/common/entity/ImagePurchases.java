package com.birefy.common.entity;

public class ImagePurchases extends CommonEntity {
	public static final String TABLE_NAME = "image_purchases";
	public static final String MEMBER_COLUMN = "member_id";
	public static final String PAINTING_COLUMN = "painting_id";
	
	private Long memberId;
	private Long paintingId;
	
	public ImagePurchases(Long memberId, Long paintingId) {
		this.memberId = memberId;
		this.paintingId = paintingId;
	}
	
	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Long getPaintingId() {
		return paintingId;
	}

	public void setPaintingId(Long paintingId) {
		this.paintingId = paintingId;
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}
