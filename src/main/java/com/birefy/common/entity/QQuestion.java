package com.birefy.common.entity;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

public class QQuestion extends CommonEntity {
	public static final String TABLE_NAME = "q_question";
	public static final String ID_COLUMN = "id";
	public static final String TITLE_COLUMN = "title";
	public static final String DETAIL_COLUMN = "detail";
	public static final String TOPIC_COLUMN = "topic_id";
	public static final String ACTIVE_COLUMN = "active";
	public static final String APP_URL_COLUMN = "app_url";
	public static final String SOURCE_COLUMN = "source";
	public static final String REVIEW_COLUMN = "review";
	public static final String INCEPTION_COLUMN = "inception";
	public static final String SUBSCRIPTION_COLUMN = "subscription";
	public static final String PORT_COLUMN = "port";
	public static final String DELETE_REQUESTED_COLUMN = "delete_requested";
	public static final String LAUNCH_REQUESTED_COLUMN = "launch_requested";
	
	@NotNull
	private Long id;
	@NotNull
	private String title;
	
	private String detail;
	
	private Long topicId;
	
	private Boolean active;
	
	private String appUrl;
	
	private String source;
	
	private Boolean review;
	
	private LocalDate inception;
	
	private Boolean subscription;
	
	private Integer port;
	
	private boolean deleteRequested;
	
	private boolean launchRequested;
	
	public QQuestion() {
		this.appUrl = "#";
	}
	
	public QQuestion(Long id, String title, Long topicId, String appUrl) {
		this.id = id;
		this.title = title;
		this.topicId = topicId;
		this.appUrl = appUrl;
	}


	public QQuestion(Long id, String title, String detail, Long topicId, Boolean active, String appUrl, String source) {
		this.id = id;
		this.title = title;
		this.detail = detail;
		this.topicId = topicId;
		this.active = active;
		this.appUrl = appUrl;
		this.source = source;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Long getTopicId() {
		return topicId;
	}

	public void setTopicId(Long topicId) {
		this.topicId = topicId;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getAppUrl() {
		return appUrl;
	}

	public void setAppUrl(String appUrl) {
		this.appUrl = appUrl;
	}
	
	public String getSource() {
		return source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	
	public boolean getReview() {
		return this.review;
	}
	
	public void setReview(boolean review) {
		this.review = review;
	}
	
	public LocalDate getInception() {
		return inception;
	}

	public void setInception(LocalDate inception) {
		this.inception = inception;
	}

	public Boolean getSubscription() {
		return subscription;
	}

	public void setSubscription(Boolean subscription) {
		this.subscription = subscription;
	}

	public void setReview(Boolean review) {
		this.review = review;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public boolean isDeleteRequested() {
		return deleteRequested;
	}

	public void setDeleteRequested(boolean deleteRequested) {
		this.deleteRequested = deleteRequested;
	}

	public boolean isLaunchRequested() {
		return launchRequested;
	}

	public void setLaunchRequested(boolean launchRequested) {
		this.launchRequested = launchRequested;
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}
	
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof QQuestion)) {
			return false;
		}
		
		QQuestion otherQ = (QQuestion) other;
		
		if (otherQ.getId() != this.id) {
			return false;
		}
		if (!otherQ.getTitle().equalsIgnoreCase(this.title)){
			return false;
		}
		if (!otherQ.getAppUrl().equals(this.appUrl)) {
			return false;
		}
		if (otherQ.getTopicId() != this.topicId) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		return "{id:"+this.id+", title: "+this.title+", topic_id:"+this.topicId+"}";
	}
	
}
