package com.birefy.common.entity;

public class BulletinTechnicalTag extends CommonEntity {
	public static final String TABLE_NAME = "bulletin_tech_tag";
	public static final String BULLETIN_COLUMN = "bulletin_id";
	public static final String TECH_COLUMN = "tag_id";
	
	private Long bulletinId;
	private Long tagId;
	
	public BulletinTechnicalTag(Long bulletinId, Long tagId) {
		this.bulletinId = bulletinId;
		this.tagId = tagId;
	}
	

	public Long getBulletinId() {
		return bulletinId;
	}



	public void setBulletinId(Long bulletinId) {
		this.bulletinId = bulletinId;
	}



	public Long getTagId() {
		return tagId;
	}



	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}



	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}
