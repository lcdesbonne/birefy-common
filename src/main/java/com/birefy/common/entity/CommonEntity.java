package com.birefy.common.entity;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import com.birefy.common.entity.utils.QueryUtils;

public abstract class CommonEntity implements ICommonEntity {
	
	private final String SPLIT_BY_CAPITALS_REG_EX = "(?=\\p{Upper})";
	private final String IGNORE_TABLE_NAME_FIELD = "TABLE_NAME";
	private final String IGNORE_COLUMN_FIELD = "_COLUMN";
	
	/**
	 * Specifies the name of the table in the database
	 */
	@Override
	public abstract String getTableName();
	
	
	/**
	 * Default implementation for field and column mapping. Null values are removed 
	 * from the column to field map.
	 */
	@Override
	public Map<String, String> getColumnToFieldMap() throws IllegalAccessException {
		Map<String, String> fieldToColumnMap = new HashMap<>();
		//Get all the fields
		Field[] fields = this.getClass().getDeclaredFields();
		int i = 0;
		for (i = 0; i < fields.length ; i++) {
			Field currentField = fields[i];
			String name = currentField.getName();
			if (name.equals(IGNORE_TABLE_NAME_FIELD) || name.toUpperCase().contains(IGNORE_COLUMN_FIELD)) {
				continue;
			}
			currentField.setAccessible(true);
			if (currentField.get(this) == null) {
				continue;
			}
			fieldToColumnMap.put(
					parseByCapitalLetters(name),
					QueryUtils.prepareValueForQuery(currentField.get(this)));
		}
		return fieldToColumnMap;
	};
	
	/**
	 * Splits name according to capital letters seperating words with an
	 * underscore
	 * @param camelCaseName
	 * @return
	 */
	private String parseByCapitalLetters(String camelCaseName) {
		String[] words = camelCaseName.split(SPLIT_BY_CAPITALS_REG_EX);
		StringBuilder nameBuilder = new StringBuilder();
		for (String word : words) {
			nameBuilder.append(word).append("_");
		}
		nameBuilder.deleteCharAt(nameBuilder.lastIndexOf("_"));
		return nameBuilder.toString().toLowerCase();
	}
}
