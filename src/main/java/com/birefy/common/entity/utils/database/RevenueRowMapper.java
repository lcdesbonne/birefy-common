package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.capital.RevenueSummary;
import com.birefy.common.entity.Revenue;

public class RevenueRowMapper implements RowMapper<RevenueSummary>{

	@Override
	public RevenueSummary mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new RevenueSummary(
				rs.getLong(Revenue.ID_COLUMN),
				rs.getLong(Revenue.MEMBER_COLUMN),
				rs.getLong(Revenue.PRODUCT_COLUMN),
				rs.getString(Revenue.DESCRIPTION_COLUMN),
				rs.getString(Revenue.CATEGORY_COLUMN),
				rs.getInt(Revenue.INVENTORY_REQUEST_PRICE_COLUMN),
				rs.getInt(Revenue.BIREFY_CHARGE_COLUMN),
				rs.getTimestamp(Revenue.CREATED_COLUMN).toLocalDateTime(),
				rs.getBoolean(Revenue.RESOLVED_COLUMN)
				);
	}

}
