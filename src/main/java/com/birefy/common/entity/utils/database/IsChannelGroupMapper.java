package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.tiding.TidingIsGroup;
import com.birefy.common.entity.tiding.MemberChannel;

public class IsChannelGroupMapper implements RowMapper<TidingIsGroup> {

	@Override
	public TidingIsGroup mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		return new TidingIsGroup(resultSet.getLong(MemberChannel.ID_COLUMN), resultSet.getBoolean(2));
	}

}
