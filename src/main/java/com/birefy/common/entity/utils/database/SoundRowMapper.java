package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.sound.SoundShort;
import com.birefy.common.entity.Sound;

public class SoundRowMapper implements RowMapper<SoundShort>{

	@Override
	public SoundShort mapRow(ResultSet result, int rowNum) throws SQLException {
		SoundShort soundShort = new SoundShort(
				result.getLong(Sound.ID_COLUMN),
				result.getLong(Sound.MEMBER_COLUMN),
				result.getString(Sound.LOCATION_COLUMN),
				result.getString(Sound.CATEGORY_COLUMN),
				result.getString(Sound.NAME_COLUMN)
				);
		
		try {
			soundShort.setPrice(result.getFloat(Sound.PRICE_COLUMN));
		} catch (Exception e) {
			
		}
		return soundShort;
	}

}
