package com.birefy.common.entity.utils.mapper;

import com.birefy.common.dto.member.EditUserDTO;
import com.birefy.common.dto.member.MemberDTO;

public class EditUserToMemberDTOMapper {
	public static MemberDTO map(EditUserDTO editUser) {
		MemberDTO member = new MemberDTO();
		member.setFirstName(editUser.getFirstName());
		member.setLastName(editUser.getLastName());
		member.setContactNumber(editUser.getContactNumber());
		member.setEmail(editUser.getEmail());
		member.setTidingId(editUser.getTidingId());
		
		return member;
	}
}
