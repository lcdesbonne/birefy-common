package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.entity.Member;
public class MemberRowMapper implements RowMapper<Member> {
	public static final Logger LOGGER = LoggerFactory.getLogger(MemberRowMapper.class);

	@Override
	public Member mapRow(ResultSet rs, int rowNum) throws SQLException {
		Member member = new Member();
		member.setId(rs.getLong(Member.ID_COLUMN));
		member.setFirstName(rs.getString(Member.FIRST_NAME_COLUMN));
		member.setFamilyName(rs.getString(Member.FAMILY_NAME_COLUMN));
		member.setEmail(rs.getString(Member.EMAIL_COLUMN));
		member.setPhone(rs.getString(Member.PHONE_COLUMN));
		
		try {
			member.setEnabled(rs.getBoolean(Member.ENABLED_COLUMN));
			member.setActive(rs.getBoolean(Member.ACTIVE_COLUMN));
		} catch (SQLException e) {
			LOGGER.debug("Enabled and active data not requested.");
		}
		
		try {
			member.setAuthority(rs.getInt(Member.AUTHORITY_COLUMN));
			member.setKey(rs.getString(Member.KEY_COLUMN));
		} catch (SQLException e) {
			LOGGER.debug("some colummns not present in the output of query.");
		}
		
		try {
			member.setSubscriptionReferenceId(rs.getString(Member.SUBSCRIPTION_CUSTOMER_COLUMN));
			member.setHostService(rs.getBoolean(Member.HOST_SERVICE_COLUMN));
		} catch (SQLException e) {
			LOGGER.debug("Subscription service, and project hosting information not present");
		}
		
		return member;
	}

}
