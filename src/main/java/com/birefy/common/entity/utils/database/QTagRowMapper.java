package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.entity.QTag;

public class QTagRowMapper implements RowMapper<QTag> {
	@Override
	public QTag mapRow(ResultSet rs, int rowNum) throws SQLException {
		QTag tag = new QTag();
		tag.setId(rs.getLong(QTag.ID_COLUMN));
		tag.setTagName(rs.getString(QTag.TAG_NAME_COLUMN));
		return tag;
	}

}
