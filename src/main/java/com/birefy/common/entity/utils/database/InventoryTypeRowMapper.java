package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.entity.InventoryType;

public class InventoryTypeRowMapper implements RowMapper<InventoryType>{

	@Override
	public InventoryType mapRow(ResultSet row, int rowNumber) throws SQLException {
		return new InventoryType(
				row.getInt(InventoryType.ID_COLUMN),
				row.getString(InventoryType.TYPE_COLUMN)
				);
	}

}
