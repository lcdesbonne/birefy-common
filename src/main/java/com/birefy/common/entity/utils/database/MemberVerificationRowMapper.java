package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.entity.MemberVerification;

public class MemberVerificationRowMapper implements RowMapper<MemberVerification> {

	@Override
	public MemberVerification mapRow(ResultSet rs, int rowNum) throws SQLException {
		MemberVerification veriDetails = new MemberVerification(
				rs.getLong(MemberVerification.MEMBER_COLUMN),
				rs.getString(MemberVerification.TOKEN_COLUMN),
				rs.getDate(MemberVerification.CREATION_DATE_COLUMN).toLocalDate()
				);
		return veriDetails;
	}

}
