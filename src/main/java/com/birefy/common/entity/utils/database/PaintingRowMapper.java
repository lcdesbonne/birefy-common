package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.painting.PaintingShort;
import com.birefy.common.entity.Painting;

import ch.qos.logback.classic.Logger;

public class PaintingRowMapper implements RowMapper<PaintingShort>{

	@Override
	public PaintingShort mapRow(ResultSet row, int rowNum) throws SQLException {
		PaintingShort painting = new PaintingShort(
				row.getLong(Painting.ID_COLUMN),
				row.getString(Painting.NAME_COLUMN),
				row.getString(Painting.LOCATION_COLUMN),
				row.getLong(Painting.MEMBER_COLUMN),
				Painting.Category.valueOf(row.getString(Painting.CATEGORY_COLUMN))
				);
		
		try {
			painting.setPrice(row.getFloat(Painting.PRICE_COLUMN));
		} catch(Exception e) {
		}
		return painting;
	}

}
