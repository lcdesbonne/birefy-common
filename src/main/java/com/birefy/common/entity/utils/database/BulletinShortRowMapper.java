package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.bulletin.BulletinShort;
import com.birefy.common.entity.Bulletin;

public class BulletinShortRowMapper implements RowMapper<BulletinShort> {
	public static final Logger LOGGER = LoggerFactory.getLogger(BulletinShortRowMapper.class);
	
	@Override
	public BulletinShort mapRow(ResultSet rs, int rowNum) throws SQLException {
		BulletinShort bulletinShort = new BulletinShort(
				rs.getLong(Bulletin.ID_COLUMN),
				rs.getString(Bulletin.DESCRIPTION_COLUMN),
				rs.getDate(Bulletin.DATE_COLUMN).toLocalDate().toString());
		
		return bulletinShort;	
	}
}
