package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.deal.DealInventoryNameAndLocation;
import com.birefy.common.entity.DealInventory;

public class DealInventoryNameAndLocationRowMapper implements RowMapper<DealInventoryNameAndLocation>{

	@Override
	public DealInventoryNameAndLocation mapRow(ResultSet row, int rowNum) throws SQLException {
		return new DealInventoryNameAndLocation(
				row.getString(DealInventory.TITLE_COLUMN),
				row.getString(DealInventory.LOCATION_COLUMN)
				);
	}

}
