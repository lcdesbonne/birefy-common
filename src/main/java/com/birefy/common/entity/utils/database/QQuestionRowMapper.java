package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import org.springframework.jdbc.core.RowMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.birefy.common.entity.QQuestion;

public class QQuestionRowMapper implements RowMapper<QQuestion> {
	private static final Logger LOGGER = LoggerFactory.getLogger(QQuestion.class);

	@Override
	public QQuestion mapRow(ResultSet rs, int rowNum) throws SQLException {
		QQuestion question = new QQuestion();
		question.setId(rs.getLong(QQuestion.ID_COLUMN));
		question.setTitle(rs.getString(QQuestion.TITLE_COLUMN));
		question.setDetail(rs.getString(QQuestion.DETAIL_COLUMN));
		question.setTopicId(rs.getLong(QQuestion.TOPIC_COLUMN));
		question.setAppUrl(rs.getString(QQuestion.APP_URL_COLUMN));
		try {
			question.setActive(rs.getBoolean(QQuestion.ACTIVE_COLUMN));
		} catch (SQLException s) {
			LOGGER.trace("Column is not present in the result set.", s);
		}
		try {
			question.setSource(rs.getString(QQuestion.SOURCE_COLUMN));
		} catch (SQLException s) {
			LOGGER.trace("Column is not present in the result set.", s);
		}
		try {
			question.setReview(rs.getBoolean(QQuestion.REVIEW_COLUMN));
		} catch (SQLException s) {
			LOGGER.trace("Column is not present in the result set.", s);
		}
		try {
			question.setSubscription(rs.getBoolean(QQuestion.SUBSCRIPTION_COLUMN));
		} catch (SQLException s) {
			LOGGER.trace("Subscription is not present in the result set.",s);
		}
		try {
			question.setPort(rs.getInt(QQuestion.PORT_COLUMN));
		} catch (SQLException s) {
			LOGGER.trace("Port not present in result set.", s);
		}

		return question;
	}

}
