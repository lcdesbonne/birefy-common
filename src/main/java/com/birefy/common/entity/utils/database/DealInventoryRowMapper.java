package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.deal.DealInventoryDTO;
import com.birefy.common.dto.deal.InventoryTypeDTO;
import com.birefy.common.entity.DealInventory;
import com.birefy.common.entity.InventoryType;

public class DealInventoryRowMapper implements RowMapper<DealInventoryDTO>{
	private static final Logger LOGGER = LoggerFactory.getLogger(DealInventoryRowMapper.class);
	
	private static final String FILE_EXTENSION_COLUMN = "fExtension";
	
	@Override
	public DealInventoryDTO mapRow(ResultSet row, int rowNumber) throws SQLException {
		DealInventoryDTO inventory = new DealInventoryDTO();
		inventory.setInventoryId(row.getLong(DealInventory.ID_COLUMN));
		inventory.setTitle(row.getString(DealInventory.TITLE_COLUMN));
		inventory.setAuthorId(row.getLong(DealInventory.AUTHOR_COLUMN));
		inventory.setDealId(row.getLong(DealInventory.DEAL_ID_COLUMN));
		inventory.setCanPreview(row.getBoolean(DealInventory.CAN_PREVIEW_COLUMN));
		inventory.setCanDownload(row.getBoolean(DealInventoryDTO.CAN_DOWNLOAD_QUERY_HEADER));
		inventory.setType(
					new InventoryTypeDTO(row.getInt(DealInventory.INVENTORY_TYPE_COLUMN),
							row.getString(InventoryType.TYPE_COLUMN)
							));
		inventory.setRunCommands(row.getString(DealInventory.RUN_COMMANDS_COLUMN));
		
		inventory.setFileExtension(row.getString(FILE_EXTENSION_COLUMN));
		
		return inventory;
	}

}
