package com.birefy.common.entity.utils.mapper;

import com.birefy.common.dto.topic.QTopicShort;
import com.birefy.common.entity.QTopic;

/**
 * Maps QTopic to QTopicShort
 * @author Lennon
 *
 */
public class QTopicToShortMapper {
	
	public static QTopicShort map(QTopic topic) {
		QTopicShort mappedTopic = new QTopicShort(
				topic.getId(),
				topic.getTitle(),
				topic.getDetail()
				);
		return mappedTopic;
	}

}
