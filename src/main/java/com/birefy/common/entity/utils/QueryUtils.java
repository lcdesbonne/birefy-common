package com.birefy.common.entity.utils;

import java.util.ArrayList;
import java.util.List;

public class QueryUtils {
	private static String ONLY_NUMBERS_REGEX = "\\d+";
	private static int PHONE_NUMBER_LENGTH = 11;
	private static String DEFAULT_SQL_GROUP_CONCAT_SEPARATOR = ",";
	
	/**
	 * Returns the value as a string expression for the SQL
	 * query. Numbers are returned without quotes
	 * @param value
	 * @return
	 */
	public static String prepareValueForQuery(Object data) {
		String value = data.toString();
		
		if (data instanceof Boolean) {
			return value.toUpperCase();
		}
		
		if (value.matches(ONLY_NUMBERS_REGEX) && value.length() != PHONE_NUMBER_LENGTH) {
			return value;
		}
		
		return "'"+value+"'";
	}
	
	public static String prepareValueForQuery(Object data, boolean forceTextRepresentation) {
		String value = data.toString();
		if (!forceTextRepresentation) {
			return prepareValueForQuery(data);
		} else {
			return "'"+value+"'";
		}
	}
	
	public static List<String> sqlGroupConcatToStringList(String expression) {
		List<String> valueList = new ArrayList<String>();
		
		String[] values = expression.split(DEFAULT_SQL_GROUP_CONCAT_SEPARATOR);
		
		for (String value : values) {
			valueList.add(value);
		}
		
		return valueList;
	}
	
	public static List<String> sqlGroupConcatToStringList(String expression, String separator) {
		List<String> valueList = new ArrayList<String>();
		
		String[] values = expression.split(separator);
		
		for (String value : values) {
			valueList.add(value);
		}
		
		return valueList;
	}
}
