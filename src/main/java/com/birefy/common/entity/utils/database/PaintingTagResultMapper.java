package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.painting.PaintingTagResult;
import com.birefy.common.entity.PaintingDescriptiveTag;
import com.birefy.common.entity.PaintingTag;

public class PaintingTagResultMapper implements RowMapper<PaintingTagResult>{

	@Override
	public PaintingTagResult mapRow(ResultSet result, int rowNum) throws SQLException {
		return new PaintingTagResult(
				result.getLong(PaintingDescriptiveTag.PAINTING_COLUMN),
				result.getLong(PaintingTag.ID_COLUMN),
				result.getString(PaintingTag.TAG_NAME_COLUMN)
				);
	}

}
