package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.topic.TopicContributor;
import com.birefy.common.entity.QMemberTopic;

public class MemberTopicRowMapper implements RowMapper<QMemberTopic>{

	@Override
	public QMemberTopic mapRow(ResultSet row, int rowNum) throws SQLException {
		return new QMemberTopic(
				row.getLong(QMemberTopic.MEMBER_ID_COLUMN),
				row.getLong(QMemberTopic.TOPIC_ID_COLUMN)
				);
	}

}
