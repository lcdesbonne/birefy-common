package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.painting.PaintingNameAndLocation;
import com.birefy.common.entity.Painting;

public class PaintingNameAndLocationMapper implements RowMapper<PaintingNameAndLocation>{

	@Override
	public PaintingNameAndLocation mapRow(ResultSet result, int rowNum) throws SQLException {
		return new PaintingNameAndLocation(result.getString(Painting.NAME_COLUMN),
				result.getString(Painting.LOCATION_COLUMN));
	}

}
