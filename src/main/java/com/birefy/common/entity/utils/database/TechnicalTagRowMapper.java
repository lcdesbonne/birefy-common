package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.entity.TechnicalTag;

public class TechnicalTagRowMapper implements RowMapper<TechnicalTag> {

	@Override
	public TechnicalTag mapRow(ResultSet result, int rowNum) throws SQLException {
		TechnicalTag techTag = new TechnicalTag(
				result.getLong(TechnicalTag.ID_COLUMN),
				result.getString(TechnicalTag.NAME_COLUMN)
				);
		return techTag;
	}

}
