package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Optional;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.entity.Deal;

public class DealRowMapper implements RowMapper<Deal> {

	@Override
	public Deal mapRow(ResultSet row, int rowNumber) throws SQLException {
		Deal deal = new Deal();
		deal.setId(row.getLong(Deal.ID_COLUMN));
		deal.setCustomerId(row.getLong(Deal.CUSTOMER_ID_COLUMN));
		deal.setSupplierId(row.getLong(Deal.SUPPLIER_ID_COLUMN));
		deal.setSupplierConfirm(row.getBoolean(Deal.SUPPLIER_CONFIRM_COLUMN));
		deal.setCustomerConfirm(row.getBoolean(Deal.CUSTOMER_CONFIRM_COLUMN));
		deal.setSupplierDetails(row.getString(Deal.SUPPLIER_DETAILS_COLUMN));
		deal.setCustomerDetails(row.getString(Deal.CUSTOMER_DETAILS_COLUMN));
		deal.setCreationDate(LocalDateTime.ofInstant(row.getTimestamp(Deal.CREATION_DATE_COLUMN).toInstant(), ZoneOffset.UTC));
		deal.setActive(row.getBoolean(Deal.ACTIVE_COLUMN));
		if(Optional.ofNullable(row.getTimestamp(Deal.PAID_COLUMN)).isPresent()) {
			deal.setPaid(LocalDateTime.ofInstant(row.getTimestamp(Deal.PAID_COLUMN).toInstant(), ZoneOffset.UTC));
		}
		deal.setPrice(row.getFloat(Deal.PRICE_COLUMN));
		
		return deal;
	}
}
