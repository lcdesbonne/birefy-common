package com.birefy.common.entity.utils.database;

import java.sql.Timestamp;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.entity.tiding.Message;

public class MessageRowMapper implements RowMapper<Message> {

	@Override
	public Message mapRow(ResultSet result, int rowNum) throws SQLException {
		Message message = new Message(
				result.getString(Message.CONTENT_COLUMN),
				result.getLong(Message.CREATOR_COLUMN),
				result.getLong(Message.RECIPIENT_COLUMN),
				Timestamp.valueOf(result.getString(Message.CREATION_DATE_COLUMN)).toLocalDateTime()
				);
		return message;
	}

}
