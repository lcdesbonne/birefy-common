package com.birefy.common.entity.utils.mapper;

import com.birefy.common.dto.question.QQuestionShortExtended;
import com.birefy.common.entity.QQuestion;

public class QQuestionToExtendedMapper {
	
	public static QQuestionShortExtended map(QQuestion question) {
		return new QQuestionShortExtended(
				question.getId(),
				question.getTitle(),
				question.getDetail(),
				question.getTopicId(),
				question.getAppUrl(),
				question.getReview()
				);
	}
	
}
