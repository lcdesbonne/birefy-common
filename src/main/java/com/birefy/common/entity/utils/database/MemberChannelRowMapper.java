package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.entity.tiding.MemberChannel;

public class MemberChannelRowMapper implements RowMapper<MemberChannel> {
	public static final Logger LOGGER = LoggerFactory.getLogger(MemberChannelRowMapper.class);
	
	@Override
	public MemberChannel mapRow(ResultSet rs, int rowNum) throws SQLException {
		MemberChannel memberChannel = new MemberChannel(
				rs.getLong(MemberChannel.ID_COLUMN),
				rs.getLong(MemberChannel.MEMBER_ID_COLUMN),
				rs.getString(MemberChannel.TIDING_ID_COLUMN),
				rs.getBoolean(MemberChannel.ACTIVE_COLUMN)
				);
		
		return memberChannel;
	}
}
