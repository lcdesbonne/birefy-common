package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.topic.QTagWithTopic;

public class QTagWithTopicRowMapper implements RowMapper<QTagWithTopic>{
	public static final Logger LOGGER = LoggerFactory.getLogger(QTagWithTopicRowMapper.class);
	
	@Override
	public QTagWithTopic mapRow(ResultSet rs, int rowNum) throws SQLException {
		QTagWithTopic tagWithTopic = new QTagWithTopic(rs.getLong(1), rs.getString(2), rs.getLong(3));
		return tagWithTopic;
	}
}
