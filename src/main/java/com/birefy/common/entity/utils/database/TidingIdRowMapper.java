package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.tiding.TidingIdDTO;
import com.birefy.common.entity.tiding.MemberChannel;

public class TidingIdRowMapper implements RowMapper<TidingIdDTO>{

	@Override
	public TidingIdDTO mapRow(ResultSet row, int rowNumber) throws SQLException {
		TidingIdDTO tidingIdDTO = new TidingIdDTO(
				row.getString(MemberChannel.TIDING_ID_COLUMN),
				row.getLong(MemberChannel.ID_COLUMN)
				);
		return tidingIdDTO;
	}

}
