package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.sound.SoundTagResult;
import com.birefy.common.entity.SoundDescriptiveTag;
import com.birefy.common.entity.SoundTag;

public class SoundTagResultMapper implements RowMapper<SoundTagResult>{

	@Override
	public SoundTagResult mapRow(ResultSet result, int rowNum) throws SQLException {
		return new SoundTagResult(
				result.getLong(SoundDescriptiveTag.SOUND_COLUMN),
				result.getLong(SoundTag.ID_COLUMN),
				result.getString(SoundTag.TAG_COLUMN)
				);
	}

}
