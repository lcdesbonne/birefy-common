package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.entity.Bulletin;

public class BulletinRowMapper implements RowMapper<Bulletin>{

	@Override
	public Bulletin mapRow(ResultSet result, int rowNum) throws SQLException {
		Bulletin bulletin = new Bulletin(
				result.getLong(Bulletin.ID_COLUMN),
				result.getString(Bulletin.DESCRIPTION_COLUMN),
				result.getBoolean(Bulletin.OPEN_COLUMN),
				result.getBoolean(Bulletin.TARGETED_COLUMN),
				LocalDateTime.ofInstant(result.getTimestamp(Bulletin.DATE_COLUMN).toInstant(), ZoneOffset.UTC)
				);
		
		return bulletin;
	}

}
