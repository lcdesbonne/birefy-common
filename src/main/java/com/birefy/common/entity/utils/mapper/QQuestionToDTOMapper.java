package com.birefy.common.entity.utils.mapper;

import com.birefy.common.dto.question.QQuestionDTO;
import com.birefy.common.entity.QQuestion;

public class QQuestionToDTOMapper {
	public static QQuestionDTO map(QQuestion question) {
		QQuestionDTO mappedQuestion = new QQuestionDTO(
				question.getId(),
				question.getTitle(),
				question.getDetail(),
				question.getTopicId(),
				question.getAppUrl(),
				question.getActive(),
				question.getSource(),
				question.getPort()
				);
		return mappedQuestion;
	}
}
