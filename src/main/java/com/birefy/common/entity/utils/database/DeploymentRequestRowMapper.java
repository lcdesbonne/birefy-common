package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.cluster.DeploymentRequest;
import com.birefy.common.dto.question.QuestionIdAndSource;
import com.birefy.common.entity.QMemberQuestion;
import com.birefy.common.entity.QQuestion;

public class DeploymentRequestRowMapper implements RowMapper<DeploymentRequest>{

	@Override
	public DeploymentRequest mapRow(ResultSet row, int rowNumber) throws SQLException {
		return new DeploymentRequest(
				row.getString(QQuestion.TITLE_COLUMN),
				row.getInt(QQuestion.PORT_COLUMN),
				row.getString(QQuestion.SOURCE_COLUMN),
				row.getLong(QMemberQuestion.MEMBER_ID_COLUMN),
				row.getLong(QQuestion.ID_COLUMN)
				);
	}

}
