package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.question.QQuestionTitleAndUrl;
import com.birefy.common.entity.QQuestion;

public class QQuestionTitleUrlMapper implements RowMapper<QQuestionTitleAndUrl>{
	private static final Logger LOGGER = LoggerFactory.getLogger(QQuestionTitleUrlMapper.class);
	
	@Override
	public QQuestionTitleAndUrl mapRow(ResultSet rs, int rowNum) throws SQLException {
		QQuestionTitleAndUrl result = new QQuestionTitleAndUrl();
		result.setTitle(rs.getString(QQuestion.TITLE_COLUMN));
		result.setUrl(rs.getString(QQuestion.APP_URL_COLUMN));
		return result;
	}
}
