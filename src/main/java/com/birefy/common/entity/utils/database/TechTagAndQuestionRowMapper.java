package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.question.TechTagAndQuestion;
import com.birefy.common.entity.QQuestionTechnicalTag;
import com.birefy.common.entity.TechnicalTag;

public class TechTagAndQuestionRowMapper implements RowMapper<TechTagAndQuestion>{
	@Override
	public TechTagAndQuestion mapRow(ResultSet rs, int rowNum) throws SQLException {
		TechTagAndQuestion tagAndQuestion = new TechTagAndQuestion(
				rs.getLong(QQuestionTechnicalTag.QUESTION_ID_COLUMN),
				rs.getString(TechnicalTag.NAME_COLUMN)
				);
		return tagAndQuestion;
	}
}
