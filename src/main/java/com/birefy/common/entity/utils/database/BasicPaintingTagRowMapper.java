package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.painting.PaintingTagDTO;
import com.birefy.common.entity.PaintingTag;

public class BasicPaintingTagRowMapper implements RowMapper<PaintingTagDTO> {

	@Override
	public PaintingTagDTO mapRow(ResultSet result, int rowNumber) throws SQLException {
		
		return new PaintingTagDTO(
				result.getLong(PaintingTag.ID_COLUMN),
				result.getString(PaintingTag.TAG_NAME_COLUMN)
				);
	}

}
