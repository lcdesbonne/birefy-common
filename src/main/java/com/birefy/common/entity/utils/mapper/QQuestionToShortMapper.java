package com.birefy.common.entity.utils.mapper;

import com.birefy.common.dto.question.QQuestionShort;
import com.birefy.common.entity.QQuestion;

/**
 * Maps QQuestion opbects to QQuestionShort objects
 * @author Lennon
 *
 */
public class QQuestionToShortMapper {
	
	public static QQuestionShort map(QQuestion question) {
		QQuestionShort mappedQuestion = new QQuestionShort(
				question.getId(),
				question.getTitle(),
				question.getDetail(),
				question.getTopicId(),
				question.getAppUrl()
				);
		
		return mappedQuestion;
	}

}
