package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.tiding.TidingAndMemberID;
import com.birefy.common.entity.tiding.MemberChannel;

public class TidingAndMemberIdRowMapper implements RowMapper<TidingAndMemberID>{

	@Override
	public TidingAndMemberID mapRow(ResultSet row, int rowNum) throws SQLException {
		return new TidingAndMemberID(
				row.getLong(MemberChannel.MEMBER_ID_COLUMN),
				row.getString(MemberChannel.TIDING_ID_COLUMN)
				);
	}

}
