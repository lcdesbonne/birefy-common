package com.birefy.common.entity.utils.database;

import com.birefy.common.entity.QTopic;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class QTopicRowMapper implements RowMapper<QTopic> {

	@Override
	public QTopic mapRow(ResultSet rs, int rowNum) throws SQLException {
		QTopic topic = new QTopic();
		topic.setId(rs.getLong(QTopic.ID_COLUMN));
		topic.setTitle(rs.getString(QTopic.TITLE_COLUMN));
		topic.setDetail(rs.getString(QTopic.DETAIL_COLUMN));
		try {
			topic.setActive(rs.getBoolean(QTopic.ACTIVE_COLUMN));
		} catch (Exception e){}
		return topic;
	}
	
}
