package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.capital.RevenuePayment;
import com.birefy.common.entity.Revenue;

public class RevenuePaymentRowMapper implements RowMapper<RevenuePayment>{
	
	@Override
	public RevenuePayment mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new RevenuePayment(
				rs.getLong(Revenue.ID_COLUMN),
				rs.getString(Revenue.PAYMENT_ID_COLUMN)
				);
	}

}
