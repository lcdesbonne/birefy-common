package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.sound.SoundTagDTO;
import com.birefy.common.entity.SoundTag;

public class SoundTagSimpleRowMapper implements RowMapper<SoundTagDTO> {

	@Override
	public SoundTagDTO mapRow(ResultSet row, int rowNum) throws SQLException {
		return new SoundTagDTO(
				row.getLong(SoundTag.ID_COLUMN),
				row.getString(SoundTag.TAG_COLUMN)
				);
	}

}
