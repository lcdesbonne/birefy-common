package com.birefy.common.entity.utils.mapper;

import java.util.Optional;

import com.birefy.common.dto.question.QQuestionDTO;
import com.birefy.common.entity.QQuestion;

public class QuestionDTOToQuestionMapper {
	
	public static QQuestion map(QQuestionDTO questionDTO) {
		QQuestion question = new QQuestion();
		question.setId(questionDTO.getId());
		question.setTopicId(Optional.ofNullable(questionDTO.getTopicId()).orElse(null));
		question.setActive(Optional.ofNullable(questionDTO.getActive()).orElse(null));
		question.setAppUrl(Optional.ofNullable(questionDTO.getAppUrl()).orElse(null));
		question.setDetail(Optional.ofNullable(questionDTO.getDetail()).orElse(null));
		question.setSource(Optional.ofNullable(questionDTO.getSource()).orElse(null));
		question.setTitle(Optional.ofNullable(questionDTO.getTitle()).orElse(null));
		question.setPort(Optional.ofNullable(questionDTO.getPort()).orElse(null));
		return question;
	}

}
