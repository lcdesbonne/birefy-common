package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.sound.SoundNameAndCategory;
import com.birefy.common.entity.Sound;

public class SoundNameAndCategoryMapper implements RowMapper<SoundNameAndCategory>{

	@Override
	public SoundNameAndCategory mapRow(ResultSet row, int rowNum) throws SQLException {
		return new SoundNameAndCategory(row.getString(Sound.NAME_COLUMN), row.getString(Sound.CATEGORY_COLUMN));
	}

}
