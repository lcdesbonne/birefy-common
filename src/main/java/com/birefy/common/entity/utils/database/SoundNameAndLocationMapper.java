package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.sound.SoundNameAndLocation;
import com.birefy.common.entity.Sound;

public class SoundNameAndLocationMapper implements RowMapper<SoundNameAndLocation> {

	@Override
	public SoundNameAndLocation mapRow(ResultSet result, int rowNum) throws SQLException {
		return new SoundNameAndLocation(result.getString(Sound.NAME_COLUMN),
				result.getString(Sound.LOCATION_COLUMN));
	}

}
