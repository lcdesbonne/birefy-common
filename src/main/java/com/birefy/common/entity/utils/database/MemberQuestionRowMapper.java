package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.entity.QMemberQuestion;

public class MemberQuestionRowMapper implements RowMapper<QMemberQuestion>{

	@Override
	public QMemberQuestion mapRow(ResultSet result, int rowNumber) throws SQLException {
		return new QMemberQuestion(
				result.getLong(QMemberQuestion.MEMBER_ID_COLUMN),
				result.getLong(QMemberQuestion.QUESTION_ID_COLUMN),
				result.getString(QMemberQuestion.CONTRIBUTION_DETAILS_COLUMN)
				);
	}

}
