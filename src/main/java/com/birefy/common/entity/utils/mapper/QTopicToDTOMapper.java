package com.birefy.common.entity.utils.mapper;

import com.birefy.common.dto.topic.QTopicDTO;
import com.birefy.common.entity.QTopic;

public class QTopicToDTOMapper {
	
	public static QTopicDTO map(QTopic topic) {
		return new QTopicDTO(
				topic.getId(),
				topic.getTitle(),
				topic.getDetail(),
				topic.getActive()
				);
	}

}
