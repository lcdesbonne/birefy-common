package com.birefy.common.entity.utils.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.birefy.common.dto.painting.PaintingNameAndCategory;
import com.birefy.common.entity.Painting;

public class PaintingNameAndCategoryMapper implements RowMapper<PaintingNameAndCategory>{

	@Override
	public PaintingNameAndCategory mapRow(ResultSet row, int rowNum) throws SQLException {
		return new PaintingNameAndCategory(row.getString(Painting.NAME_COLUMN), row.getString(Painting.CATEGORY_COLUMN));
	}

}
