package com.birefy.common.entity;

public class SoundDescriptiveTag extends CommonEntity {
	public static final String TABLE_NAME = "sound_descriptive_tag";
	public static final String SOUND_COLUMN = "sound_id";
	public static final String TAG_COLUMN = "sound_tag_id";
	
	private Long soundId;
	private Long soundTagId;
	
	public SoundDescriptiveTag() {
	}

	public SoundDescriptiveTag(Long soundId, Long soundTagId) {
		this.soundId = soundId;
		this.soundTagId = soundTagId;
	}

	public Long getSoundId() {
		return soundId;
	}

	public void setSoundId(Long soundId) {
		this.soundId = soundId;
	}

	public Long getSoundTagId() {
		return soundTagId;
	}

	public void setSoundTagId(Long soundTagId) {
		this.soundTagId = soundTagId;
	}


	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}
