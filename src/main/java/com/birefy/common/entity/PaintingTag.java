package com.birefy.common.entity;

public class PaintingTag extends CommonEntity {
	public static final String TABLE_NAME = "painting_tag";
	public static final String ID_COLUMN = "id";
	public static final String TAG_NAME_COLUMN = "tag";
	
	private Long id;
	private String tag;
	
	
	public PaintingTag() {
	}

	public PaintingTag(Long id, String tag) {
		this.id = id;
		this.tag = tag;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}



	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}
