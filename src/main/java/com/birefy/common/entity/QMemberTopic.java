package com.birefy.common.entity;

import javax.validation.constraints.NotNull;

public class QMemberTopic extends CommonEntity {
	public static final String TABLE_NAME = "q_member_topic";
	public static final String MEMBER_ID_COLUMN = "member_id";
	public static final String TOPIC_ID_COLUMN = "topic_id";
	
	@NotNull
	private Long memberId;
	@NotNull
	private Long topicId;
	
	public QMemberTopic() {
		
	}
	
	public QMemberTopic(Long memberId, Long topicId) {
		this.memberId = memberId;
		this.topicId = topicId;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Long getTopicId() {
		return topicId;
	}

	public void setTopicId(Long topicId) {
		this.topicId = topicId;
	}
	
	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}
