package com.birefy.common.entity;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.birefy.common.entity.utils.QueryUtils;

public class Bulletin extends CommonEntity {
	public static final String TABLE_NAME = "bulletin";
	public static final String ID_COLUMN = "id";
	public static final String DESCRIPTION_COLUMN = "description"; //This should be unique
	public static final String OPEN_COLUMN = "open";
	public static final String TARGETED_COLUMN = "targeted";
	public static final String DATE_COLUMN = "creation_date";
	
	private Long id;
	private String description;
	private boolean open;
	private boolean targeted;
	private LocalDateTime date;
	
	public Bulletin() {
		open = true;
		targeted = true;
		date = LocalDateTime.now();
	}
	
	public Bulletin(Long id, String description, boolean open, boolean targeted) {
		this.id = id;
		this.description = description;
		this.open = open;
		this.targeted = targeted;
		this.date = LocalDateTime.now();
	}
	
	

	public Bulletin(Long id, String description, boolean open, boolean targeted, LocalDateTime date) {
		this.id = id;
		this.description = description;
		this.open = open;
		this.targeted = targeted;
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public boolean isTargeted() {
		return targeted;
	}

	public void setTargeted(boolean targeted) {
		this.targeted = targeted;
	}

	public LocalDateTime getCreationDate() {
		return date;
	}

	public void setCreationDate(LocalDateTime date) {
		this.date = date;
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}
	
	@Override
	public Map<String, String> getColumnToFieldMap() throws IllegalAccessException {
		Map<String, String> columnToFieldMap = new HashMap<>();
		columnToFieldMap.put("creation_date", QueryUtils.prepareValueForQuery(date));
		if (Optional.ofNullable(id).isPresent()) {
			columnToFieldMap.put("id", QueryUtils.prepareValueForQuery(id));
		}
		columnToFieldMap.put("description", QueryUtils.prepareValueForQuery(description));
		columnToFieldMap.put("open", QueryUtils.prepareValueForQuery(open));
		columnToFieldMap.put("targeted", QueryUtils.prepareValueForQuery(targeted));
		return columnToFieldMap;
	}

}
