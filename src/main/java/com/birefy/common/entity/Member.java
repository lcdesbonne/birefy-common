package com.birefy.common.entity;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

public class Member extends CommonEntity{
	public static final String TABLE_NAME = "member";
	public static final String ID_COLUMN = "id";
	public static final String FIRST_NAME_COLUMN = "first_name";
	public static final String FAMILY_NAME_COLUMN = "family_name";
	public static final String EMAIL_COLUMN = "email";
	public static final String PHONE_COLUMN = "phone";
	public static final String KEY_COLUMN = "access_key";
	public static final String AUTHORITY_COLUMN = "authority";
	public static final String ACTIVE_COLUMN = "active";
	public static final String ENABLED_COLUMN = "enabled";
	public static final String SUBSCRIPTION_CUSTOMER_COLUMN = "subscription_customer_id";
	public static final String HOST_SERVICE_COLUMN = "host_service";
	public static final String LAST_PAID_COLUMN = "last_paid";
	public static final String ACCOUNT_ID_COLUMN = "account_id";
	
	@NotNull
	private Long id;
	@NotNull
	private String firstName;
	@NotNull
	private String familyName;
	private String accessKey;
	@NotNull
	private String email;
	private String phone;
	private Integer authority;
	private Boolean active;
	private Boolean enabled;
	private String subscriptionCustomerId;
	private Boolean hostService;
	private LocalDateTime lastPaid;
	private String accountId;
	
	public Member() {
		
	}
	
	public Member(Long id, String firstName, String familyName, String email) {
		this.id = id;
		this.firstName = firstName;
		this.familyName = familyName;
		this.email = email;
	}

	public Member(Long id, String firstName, String familyName, String email, String phone) {
		this.id = id;
		this.firstName = firstName;
		this.familyName = familyName;
		this.email = email;
		this.phone = phone;
	}
	
	public Member(Long id, String firstName, String familyName, String email, String phone, String key) {
		this.id = id;
		this.firstName = firstName;
		this.familyName = familyName;
		this.accessKey = key;
		this.email = email;
		this.phone = phone;
	}
	
	public Member(Long id, String firstName, String familyName, String email, String phone, String key, int authority) {
		this.id = id;
		this.firstName = firstName;
		this.familyName = familyName;
		this.email = email;
		this.phone = phone;
		this.accessKey = key;
		this.authority = authority;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getKey() {
		return accessKey;
	}

	public void setKey(String key) {
		this.accessKey = key;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public Integer getAuthority() {
		return authority;
	}

	public void setAuthority(Integer authority) {
		this.authority = authority;
	}
	
	

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getSubscriptionReferenceId() {
		return subscriptionCustomerId;
	}

	public void setSubscriptionReferenceId(String subscriptionReferenceId) {
		this.subscriptionCustomerId = subscriptionReferenceId;
	}

	public Boolean getHostService() {
		return hostService;
	}

	public void setHostService(Boolean hostService) {
		this.hostService = hostService;
	}

	public LocalDateTime getLastPaid() {
		return lastPaid;
	}

	public void setLastPaid(LocalDateTime lastPaid) {
		this.lastPaid = lastPaid;
	}
	
	public String getAccountId() {
		return accountId;
	}
	
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	@Override
	public boolean equals(Object object) {
		if (! (object instanceof Member)) {
			return false;
		}
		Member other = (Member) object;
		
		if (this.id != other.getId()) {
			return false;
		}
		if (this.firstName != null) {
			if(!this.firstName.equals(other.getFirstName())){
				return false;
			}
		} else if (this.firstName != other.getFirstName()) {
			return false;
		}
		if (this.familyName != null) {
			if(!this.familyName.equals(other.getFamilyName())) {
				return false;
			}
		} else if (this.familyName != other.getFamilyName()) {
			return false;
		}
		if (this.email != null) {
			if(!this.email.equals(other.getEmail())) {
				return false;
			}
		} else if (this.email != other.getEmail()) {
			return false;
		}
		if (this.phone != null) {
			if(!this.phone.equals(other.getPhone())) {
				return false;
			}
		} else if (this.phone != other.getPhone()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}
