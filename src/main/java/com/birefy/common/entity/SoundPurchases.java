package com.birefy.common.entity;

public class SoundPurchases extends CommonEntity {
	public static final String TABLE_NAME = "sound_purchases";
	public static final String MEMBER_COLUMN = "member_id";
	public static final String SOUND_COLUMN = "sound_id";
	
	private Long memberId;
	private Long soundId;
	
	
	public SoundPurchases(Long memberId, Long soundId) {
		this.memberId = memberId;
		this.soundId = soundId;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Long getSoundId() {
		return soundId;
	}

	public void setSoundId(Long soundId) {
		this.soundId = soundId;
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}
