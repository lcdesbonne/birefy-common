package com.birefy.common.entity;

import java.util.Map;

/**
 * All objects which represent tables should implement this
 * to ensure column to field mapping
 * @author Lennon
 *
 */
public interface ICommonEntity {
	
	/**
	 * Returns the name of the table the entity represents
	 * @return
	 */
	public String getTableName();
	
	/**
	 * Should return a map of the format
	 * key: java field name
	 * value: database column name
	 * @return
	 */
	public Map<String, String> getColumnToFieldMap() throws IllegalAccessException;

}
