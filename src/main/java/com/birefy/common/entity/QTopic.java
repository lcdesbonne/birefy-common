package com.birefy.common.entity;

import javax.validation.constraints.NotNull;

public class QTopic extends CommonEntity {
	public static final String TABLE_NAME = "q_topic";
	public static final String ID_COLUMN = "id";
	public static final String TITLE_COLUMN = "title";
	public static final String DETAIL_COLUMN = "detail";
	public static final String ACTIVE_COLUMN = "active";
	
	@NotNull
	private Long id;
	@NotNull
	private String title;

	private String detail;
	
	private Boolean active;
	
	public QTopic() {
		
	}
	
	public QTopic(Long id, String title) {
		this.id = id;
		this.title = title;
	}
	
	public QTopic(Long id, String title, String detail, Boolean active) {
		this.id = id;
		this.title = title;
		this.detail = detail;
		this.active = active;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	
	@Override
	public String getTableName() {
		return TABLE_NAME;
	}
	
	@Override
	public boolean equals(Object object) {
		if (! (object instanceof QTopic)) {
			return false;
		}
		QTopic other = (QTopic) object;
		if (this.id != other.getId()) {
			return false;
		}
		if (this.detail != null) {
			if (!this.detail.equalsIgnoreCase(other.getDetail())) {
				return false;
			}
		}
		if (this.title != null) {
			if (!this.title.equalsIgnoreCase(other.getTitle())) {
				return false;
			}
		}
		
		return true;
	}
}
