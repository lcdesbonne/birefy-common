package com.birefy.common.entity;

public class MemberTechnicalTag extends CommonEntity {
	public static final String TABLE_NAME = "member_technical_tag";
	
	public static final String MEMBER_ID_COLUMN = "member_id";
	public static final String TECHNICAL_TAG_COLUMN = "technical_tag_id";
	
	private Long memberId;
	private Long technicalTagId;
	
	public MemberTechnicalTag() {
		
	}
	
	public MemberTechnicalTag(Long memberId, Long technicalTagId) {
		this.memberId = memberId;
		this.technicalTagId = technicalTagId;
	}
	
	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Long getTechnicalTagId() {
		return technicalTagId;
	}

	public void setTechnicalTagId(Long technicalTagId) {
		this.technicalTagId = technicalTagId;
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}
