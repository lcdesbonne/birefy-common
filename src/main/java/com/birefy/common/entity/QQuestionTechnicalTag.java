package com.birefy.common.entity;

public class QQuestionTechnicalTag extends CommonEntity {
	public static final String TABLE_NAME = "q_question_technical_tag";
	public static final String QUESTION_ID_COLUMN = "question_id";
	public static final String TAG_ID_COLUMN = "tag_id";
	
	private Long questionId;
	private Long tagId;
	
	
	public QQuestionTechnicalTag(Long questionId, Long tagId) {
		this.questionId = questionId;
		this.tagId = tagId;
	}


	public Long getQuestionId() {
		return questionId;
	}


	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}


	public Long getTagId() {
		return tagId;
	}


	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}


	@Override
	public String getTableName() {
		return TABLE_NAME;
	}
	
}
