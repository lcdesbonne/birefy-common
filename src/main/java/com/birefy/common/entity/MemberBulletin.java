package com.birefy.common.entity;

public class MemberBulletin extends CommonEntity {
	public static final String TABLE_NAME = "member_bulletin";
	public static final String BULLETIN_ID_COLUMN = "bulletin_id";
	public static final String MEMBER_ID_COLUMN = "member_id";
	
	private Long bulletinId;
	private Long memberId;
	
	public MemberBulletin(Long bulletinId, Long memberId) {
		this.bulletinId = bulletinId;
		this.memberId = memberId;
	}

	public Long getBulletinId() {
		return bulletinId;
	}


	public void setBulletinId(Long bulletinId) {
		this.bulletinId = bulletinId;
	}


	public Long getMemberId() {
		return memberId;
	}


	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}


	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}
