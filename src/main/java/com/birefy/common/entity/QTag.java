package com.birefy.common.entity;

import javax.validation.constraints.NotNull;

/**
 * Represents tags associated with topics
 * @author Lennon
 *
 */
public class QTag extends CommonEntity {
	public static final String TABLE_NAME = "q_tag";
	public static final String ID_COLUMN = "id";
	public static final String TAG_NAME_COLUMN = "tag_name";
	
	@NotNull
	private Long id;
	@NotNull
	private String tagName;
	
	public QTag() {
		
	}
	
	public QTag(String tagName) {
		this.tagName = tagName;
	}
	
	public QTag(Long id, String tagName) {
		this.id = id;
		this.tagName = tagName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	
	@Override
	public boolean equals(Object object) {
		if (! (object instanceof QTag)) {
			return false;
		}
		QTag other = (QTag) object;
		if (this.id != other.getId()) {
			return false;
		}
		if (this.tagName != null) {
			if (!this.tagName.equals(other.getTagName())) {
				return false;
			}
		}
		return true;
	}
	
	@Override
	public String getTableName() {
		return TABLE_NAME;
	}
}
