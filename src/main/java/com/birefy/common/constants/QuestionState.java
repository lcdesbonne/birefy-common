package com.birefy.common.constants;

public enum QuestionState {
	active, pending, review
}
