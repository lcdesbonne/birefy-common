package com.birefy.common.dto.bulletin;

import java.util.List;

import com.birefy.common.entity.TechnicalTag;

public class NewBulletinDTO {
	private String description;
	private boolean targeted;
	private List<TechnicalTag> technicalTags;
	
	public NewBulletinDTO(String description, boolean targeted, List<TechnicalTag> technicalTags) {
		this.description = description;
		this.technicalTags = technicalTags;
		this.targeted = targeted;
	}
	
	public NewBulletinDTO() {
		
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<TechnicalTag> getTechnicalTags() {
		return technicalTags;
	}

	public void setTechnicalTags(List<TechnicalTag> technicalTags) {
		this.technicalTags = technicalTags;
	}

	public boolean isTargeted() {
		return targeted;
	}

	public void setTargeted(boolean targeted) {
		this.targeted = targeted;
	}
	
	
}
