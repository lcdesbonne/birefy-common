package com.birefy.common.dto.bulletin;

import java.util.Objects;

public class BulletinShort {
	private Long id;
	private String description;
	private String date;
	
	public BulletinShort(Long id, String description, String date) {
		this.id = id;
		this.description = description;
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getDate() {
		return this.date;
	}
	
	@Override
	public boolean equals(Object other) {
		boolean isEqual;
		if(other instanceof BulletinShort) {
			BulletinShort otherBulletin = (BulletinShort) other;
			
			isEqual = otherBulletin.getId().equals(this.id);
		} else {
			isEqual = false;
		}
		return isEqual;
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}
}
