package com.birefy.common.dto.bulletin;

import java.util.List;
import java.util.Map;

import com.birefy.common.entity.Bulletin;

public class BulletinsWithTechTags {
	private List<Bulletin> bulletins;
	private Map<Long, List<String>> techTags;
	
	public BulletinsWithTechTags(List<Bulletin> bulletins, Map<Long, List<String>> techTags) {
		this.bulletins = bulletins;
		this.techTags = techTags;
	}

	public List<Bulletin> getBulletins() {
		return bulletins;
	}

	public void setBulletins(List<Bulletin> bulletins) {
		this.bulletins = bulletins;
	}

	public Map<Long, List<String>> getTechTags() {
		return techTags;
	}

	public void setTechTags(Map<Long, List<String>> techTags) {
		this.techTags = techTags;
	}
	
}
