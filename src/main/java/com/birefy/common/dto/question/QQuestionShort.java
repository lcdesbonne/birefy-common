package com.birefy.common.dto.question;

public class QQuestionShort {
	private long id;
	private String title;
	private String detail;
	private long topicId;
	private String appUrl;
	
	public QQuestionShort() {
		
	}
	
	public QQuestionShort(long id, String title, String detail, long topicId, String appUrl) {
		this.id = id;
		this.title = title;
		this.detail = detail;
		this.topicId = topicId;
		this.appUrl = appUrl;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public long getTopicId() {
		return topicId;
	}

	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}

	public String getAppUrl() {
		return appUrl;
	}

	public void setAppUrl(String appUrl) {
		this.appUrl = appUrl;
	}
	

}
