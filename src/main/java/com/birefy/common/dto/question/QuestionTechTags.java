package com.birefy.common.dto.question;

import java.util.List;

public class QuestionTechTags {
	private Long questionId;
	private List<String> tagNames;
	
	public QuestionTechTags() {
	}
	
	public QuestionTechTags(Long questionId, List<String> tagNames) {
		this.questionId = questionId;
		this.tagNames = tagNames;
	}
	
	public Long getQuestionId() {
		return questionId;
	}
	
	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}
	
	public List<String> getTagNames() {
		return tagNames;
	}
	
	public void setTagNames(List<String> tagNames) {
		this.tagNames = tagNames;
	}
}
