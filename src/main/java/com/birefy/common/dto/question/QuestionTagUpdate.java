package com.birefy.common.dto.question;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QuestionTagUpdate implements Serializable{
	@JsonProperty("questionId")
	private Long questionId;
	@JsonProperty("tagNames")
	private List<String> tagNames;
	@JsonProperty("removedTags")
	private List<String> removedTags;
	
	public QuestionTagUpdate(Long questionId, List<String> tagNames, List<String> removedTags) {
		this.questionId = questionId;
		this.tagNames = tagNames;
		this.removedTags = removedTags;
	}
	
	public QuestionTagUpdate() {
		
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public List<String> getTagNames() {
		return tagNames;
	}

	public void setTagNames(List<String> tagNames) {
		this.tagNames = tagNames;
	}

	public List<String> getRemovedTags() {
		return removedTags;
	}

	public void setRemovedTags(List<String> removedTags) {
		this.removedTags = removedTags;
	}
	
	
}
