package com.birefy.common.dto.question;

public class QuestionIdAndSource {
	private Long id;
	private String sourceUrl;
	private Long memberId;
	
	public QuestionIdAndSource(Long id, String sourceUrl, Long memberId) {
		this.id = id;
		this.sourceUrl = sourceUrl;
		this.memberId = memberId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSourceUrl() {
		return sourceUrl;
	}
	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}
	
	public Long getMemberId() {
		return memberId;
	}
	
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
}
