package com.birefy.common.dto.question;

public class QQuestionShortExtended extends QQuestionShort {
	private boolean isReview;

	public QQuestionShortExtended(long id, String title, String detail, long topicId, String appUrl, boolean isReview) {
		super(id, title, detail, topicId, appUrl);
		this.isReview = isReview;
	}

	public boolean isReview() {
		return isReview;
	}

	public void setReview(boolean isReview) {
		this.isReview = isReview;
	}
}
