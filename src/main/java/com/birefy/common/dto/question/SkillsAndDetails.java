package com.birefy.common.dto.question;

import java.util.List;

public class SkillsAndDetails {
	private List<String> skillsList;
	private String details;
	
	public SkillsAndDetails(List<String> skillsList, String details) {
		this.skillsList = skillsList;
		this.details = details;
	}

	public List<String> getSkillsList() {
		return skillsList;
	}

	public void setSkillsList(List<String> skillsList) {
		this.skillsList = skillsList;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
}
