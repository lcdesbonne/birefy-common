package com.birefy.common.dto.question;

public class QuestionUrlUpdate {
	private Long questionId;
	private String url;
	
	public QuestionUrlUpdate() {
		
	}
	
	public QuestionUrlUpdate(Long questionId, String url) {
		this.questionId = questionId;
		this.url = url;
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
