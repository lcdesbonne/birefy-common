package com.birefy.common.dto.question;

public class QQuestionDTO extends QQuestionShort {
	private Boolean active;
	private String source;
	private Integer port;
	
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	public String getSource() {
		return this.source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	
	public Integer getPort() {
		return port;
	}
	
	public void setPort(Integer port) {
		this.port = port;
	}
	public QQuestionDTO() {
		
	}
	
	
	public QQuestionDTO(long id, String title, String detail, long topicId, String appUrl, Boolean active, String source, Integer port) {
		super(id, title, detail, topicId,appUrl);
		this.active = active;
		this.source = source;
		this.port = port;
	}
}
