package com.birefy.common.dto.question;

public class QQuestionTitleAndUrl {
	private String title;
	private String url;
	
	public QQuestionTitleAndUrl() {
		
	}
	
	public QQuestionTitleAndUrl(String title, String url) {
		this.url = url;
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
