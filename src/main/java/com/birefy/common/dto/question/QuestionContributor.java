package com.birefy.common.dto.question;

public class QuestionContributor {
	Long memberId;
	String details;
	String tidingId;
	
	public QuestionContributor() {
	}

	public QuestionContributor(Long memberId, String details, String tidingId) {
		this.memberId = memberId;
		this.details = details;
		this.tidingId = tidingId;
	}
	
	public Long getMemberId() {
		return memberId;
	}
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getTidingId() {
		return tidingId;
	}
	public void setTidingId(String tidingId) {
		this.tidingId = tidingId;
	}
}
