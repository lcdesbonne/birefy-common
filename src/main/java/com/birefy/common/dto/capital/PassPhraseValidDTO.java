package com.birefy.common.dto.capital;

import com.birefy.common.validation.PassphraseValidCheck;

public class PassPhraseValidDTO {
	@PassphraseValidCheck
	private String passphrase;
	
	public PassPhraseValidDTO() {
		
	}
	
	public String getPassphrase() {
		return passphrase;
	}
	
	public void setPassphrase(String passphrase) {
		this.passphrase = passphrase;
	}
}
