package com.birefy.common.dto.capital;

import java.time.LocalDateTime;
import java.util.List;

public class PayoutSummary {
	private List<SoldItem> breakdownOfSoldInventory;
	private Integer totalPayablePence;
	private LocalDateTime lastPayoutDate;
	
	public PayoutSummary(List<SoldItem> breakdownOfSoldInventory, Integer totalPayablePence,
			LocalDateTime lastPayoutDate) {
		this.breakdownOfSoldInventory = breakdownOfSoldInventory;
		this.totalPayablePence = totalPayablePence;
		this.lastPayoutDate = lastPayoutDate;
	}
	
	public PayoutSummary() {
	}

	public List<SoldItem> getBreakdownOfSoldInventory() {
		return breakdownOfSoldInventory;
	}
	public void setBreakdownOfSoldInventory(List<SoldItem> breakdownOfSoldInventory) {
		this.breakdownOfSoldInventory = breakdownOfSoldInventory;
	}
	public Integer getTotalPayablePence() {
		return totalPayablePence;
	}
	public void setTotalPayablePence(Integer totalPayablePence) {
		this.totalPayablePence = totalPayablePence;
	}

	public LocalDateTime getLastPayoutDate() {
		return lastPayoutDate;
	}

	public void setLastPayoutDate(LocalDateTime lastPayoutDate) {
		this.lastPayoutDate = lastPayoutDate;
	}
}
