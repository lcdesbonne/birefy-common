package com.birefy.common.dto.capital;

public class Purchase {
	private ChargeDetails chargeDetails;
	private String chargeDescription;
	private String paymentIntentId;
	
	public Purchase(ChargeDetails chargeDetails, String chargeDescription) {
		this.chargeDetails = chargeDetails;
		this.chargeDescription = chargeDescription;
	}
	
	public Purchase() {
	}

	public ChargeDetails getChargeDetails() {
		return chargeDetails;
	}

	public void setChargeDetails(ChargeDetails chargeDetails) {
		this.chargeDetails = chargeDetails;
	}

	public String getChargeDescription() {
		return chargeDescription;
	}

	public void setChargeDescription(String chargeDescription) {
		this.chargeDescription = chargeDescription;
	}
	
	public String getPaymentIntentId() {
		return paymentIntentId;
	}
	
	public void setPaymentIntentId(String paymentIntentId) {
		this.paymentIntentId = paymentIntentId;
	}
}
