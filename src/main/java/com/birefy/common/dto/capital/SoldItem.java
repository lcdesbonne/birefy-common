package com.birefy.common.dto.capital;

import com.birefy.common.dto.capital.ChargeDetails.MediaCategory;

public class SoldItem {
	private MediaCategory category;
	private String title;
	private Integer sales;
	private Integer pricePence;
	private Long inventoryId;
	
	public SoldItem(MediaCategory category, String title, Integer sales, Integer pricePence) {
		this.category = category;
		this.title = title;
		this.sales = sales;
		this.pricePence = pricePence;
	}
	
	public SoldItem() {
	}

	public MediaCategory getCategory() {
		return category;
	}
	public void setCategory(MediaCategory category) {
		this.category = category;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getSales() {
		return sales;
	}
	public void setSales(Integer sales) {
		this.sales = sales;
	}
	public Integer getPricePence() {
		return pricePence;
	}
	public void setPricePence(Integer pricePence) {
		this.pricePence = pricePence;
	}

	public Long getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(Long inventoryId) {
		this.inventoryId = inventoryId;
	}
	
}
