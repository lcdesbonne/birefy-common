package com.birefy.common.dto.capital;

import com.birefy.common.dto.capital.ChargeDetails.MediaCategory;

public class DigitalInventory {
	private long id;
	private MediaCategory category;
	
	public DigitalInventory() {
		
	}
	
	public DigitalInventory(long id, MediaCategory category) {
		id = this.id;
		category = this.category;
	}
	
	public MediaCategory getCategory() {
		return this.category;
	}
	
	public void setCategory(MediaCategory category) {
		this.category = category;
	}
	
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "{'id':" + id + ", 'category':" + category + "}";
	}
}
