package com.birefy.common.dto.capital;

import com.birefy.common.validation.PassphraseCheck;

public class PassPhraseDTO {
	@PassphraseCheck
	private String passphrase;
	
	public PassPhraseDTO() {
		
	}
	
	public String getPassphrase() {
		return passphrase;
	}
	
	public void setPassphrase(String passphrase) {
		this.passphrase = passphrase;
	}
}
