package com.birefy.common.dto.capital;

import java.time.LocalDateTime;

public class RevenueSummary implements Comparable {
	private Long id;
	private Long memberId;
	private Long productId;
	private String description;
	private String category;
	private Integer inventoryRequestPrice;
	private Integer birefyCharge;
	private LocalDateTime created;
	private Boolean resolved;
	
	
	public RevenueSummary(Long id, Long memberId, Long productId, String description, String category,
			Integer inventoryRequestPrice, Integer birefyCharge, LocalDateTime created, Boolean resolved) {
		this.id = id;
		this.memberId = memberId;
		this.productId = productId;
		this.description = description;
		this.category = category;
		this.inventoryRequestPrice = inventoryRequestPrice;
		this.birefyCharge = birefyCharge;
		this.created = created;
		this.resolved = resolved;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMemberId() {
		return memberId;
	}
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Integer getInventoryRequestPrice() {
		return inventoryRequestPrice;
	}
	public void setInventoryRequestPrice(Integer inventoryRequestPrice) {
		this.inventoryRequestPrice = inventoryRequestPrice;
	}
	public Integer getBirefyCharge() {
		return birefyCharge;
	}
	public void setBirefyCharge(Integer birefyCharge) {
		this.birefyCharge = birefyCharge;
	}
	public LocalDateTime getCreated() {
		return created;
	}
	public void setCreated(LocalDateTime created) {
		this.created = created;
	}
	public Boolean getResolved() {
		return resolved;
	}
	public void setResolved(Boolean resolved) {
		this.resolved = resolved;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int compareTo(Object o) {
		RevenueSummary other = (RevenueSummary) o;
		
		int result = 0;
		if (this.created.isBefore(other.getCreated())) {
			result = -1;
		}
		
		if (this.created.isAfter(other.getCreated())) {
			result = 1;
		}
		
		if (this.created.isEqual(other.getCreated())) {
			result =  0;
		}
		
		return result;
	}
}
