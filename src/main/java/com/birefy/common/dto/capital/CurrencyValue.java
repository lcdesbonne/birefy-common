package com.birefy.common.dto.capital;

public class CurrencyValue {
	private Integer value;
	private String currencyId;
	
	public CurrencyValue(Integer value, String currencyId) {
		this.value = value;
		this.currencyId = currencyId;
	}
	
	public CurrencyValue() {
		
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}
}
