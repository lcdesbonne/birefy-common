package com.birefy.common.dto.capital;

public class MemberPaymentDetails {
	private String bankAccountHolderName;
	private String name;
	private CountryCode country;
	private CountryCode bankAccountCountry;
	private String accountNumber;
	private String sortCode;
	private String secretKey;
	
	public String getBankAccountHolderName() {
		return bankAccountHolderName;
	}

	public void setBankAccountHolderName(String bankAccountHolderName) {
		this.bankAccountHolderName = bankAccountHolderName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CountryCode getCountry() {
		return country;
	}

	public void setCountry(CountryCode country) {
		this.country = country;
	}

	public CountryCode getBankAccountCountry() {
		return bankAccountCountry;
	}

	public void setBankAccountCountry(CountryCode bankAccountCountry) {
		this.bankAccountCountry = bankAccountCountry;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getSortCode() {
		return sortCode;
	}

	public void setSortCode(String sortCode) {
		this.sortCode = sortCode;
	}
	
	public String getSecretKey() {
		return secretKey;
	}
	
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}


	public enum CountryCode {
		GB
	}
}
