package com.birefy.common.dto.capital;

public class RevenuePayment {
	private long revenueId;
	private String paymentIntentId;
	
	public RevenuePayment(long revenueId, String paymentIntentId) {
		this.revenueId = revenueId;
		this.paymentIntentId = paymentIntentId;
	}
	
	public long getRevenueId() {
		return revenueId;
	}
	
	public void setRevenueId(long revenueId) {
		this.revenueId = revenueId;
	}
	
	public String getPaymentIntentId() {
		return paymentIntentId;
	}
	
	public void setPaymentIntentId(String paymentIntentId) {
		this.paymentIntentId = paymentIntentId;
	}
	
	@Override
	public String toString() {
		return "{ 'revenueId': " + this.revenueId + ", 'paymentIntentId': " + this.paymentIntentId + "}";
	}
}
