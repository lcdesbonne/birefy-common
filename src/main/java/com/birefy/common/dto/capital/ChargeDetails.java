package com.birefy.common.dto.capital;

import com.stripe.model.Token;

public class ChargeDetails {
	private Long customerMemberId;
	private Long supplierMemberId;
	private Long dealId;
	private Long mediaId;
	private MediaCategory mediaCategory;
	private Token source;
	private CurrencyValue value;
	
	public ChargeDetails(Long customerMemberId, Long supplierMemberId, Token source, CurrencyValue value) {
		this.customerMemberId = customerMemberId;
		this.supplierMemberId = supplierMemberId;
		this.source = source;
		this.value = value;
	}
	
	public ChargeDetails(Long customerMemberId, Long supplierMemberId, Long dealId, Long mediaId,
			MediaCategory mediaCategory, Token source, CurrencyValue value) {
		this.customerMemberId = customerMemberId;
		this.supplierMemberId = supplierMemberId;
		this.dealId = dealId;
		this.mediaId = mediaId;
		this.mediaCategory = mediaCategory;
		this.source = source;
		this.value = value;
	}

	public ChargeDetails() {
	}
	
	public Long getCustomerMemberId() {
		return customerMemberId;
	}
	
	public void setCustomerMemberId(Long customerMemberId) {
		this.customerMemberId = customerMemberId;
	}
	
	public Long getSupplierMemberId() {
		return supplierMemberId;
	}
	
	public void setSupplierMemberId(Long supplierMemberId) {
		this.supplierMemberId = supplierMemberId;
	}
	
	public Token getSource() {
		return source;
	}
	
	public void setSource(Token source) {
		this.source = source;
	}
	
	public CurrencyValue getValue() {
		return value;
	}
	
	public void setValue(CurrencyValue value) {
		this.value = value;
	}
	
	public Long getDealId() {
		return dealId;
	}

	public void setDealId(Long dealId) {
		this.dealId = dealId;
	}

	public Long getMediaId() {
		return mediaId;
	}

	public void setMediaId(Long mediaId) {
		this.mediaId = mediaId;
	}

	public MediaCategory getMediaCategory() {
		return mediaCategory;
	}

	public void setMediaCategory(MediaCategory mediaCategory) {
		this.mediaCategory = mediaCategory;
	}


	public enum MediaCategory {
		SOUND, PAINTING, DEAL
	}
}
