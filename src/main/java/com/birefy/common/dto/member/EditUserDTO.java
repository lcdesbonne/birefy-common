package com.birefy.common.dto.member;

import com.birefy.common.validation.EmailCheck;
import com.birefy.common.validation.NewTidingIdCheck;
import com.birefy.common.validation.PhoneNumberCheck;

public class EditUserDTO {
	String firstName;
	String lastName;
	String email;
	@PhoneNumberCheck
	String contactNumber;
	@NewTidingIdCheck
	String tidingId;
	
	public EditUserDTO() {
	}
	
	public EditUserDTO(String firstName, String lastName, String email, String contactNumber, String tidingId) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.contactNumber = contactNumber;
		this.tidingId = tidingId;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getTidingId() {
		return tidingId;
	}
	public void setTidingId(String tidingId) {
		this.tidingId = tidingId;
	}
	
}
