package com.birefy.common.dto.member;

import com.birefy.common.validation.EmailCheck;
import com.birefy.common.validation.NewTidingIdCheck;
import com.birefy.common.validation.PhoneNumberCheck;


public class MemberDTO {
	String firstName;
	String lastName;
	@EmailCheck
	String email;
	@PhoneNumberCheck
	String contactNumber;
	String password;
	@NewTidingIdCheck
	String tidingId;
	
	public MemberDTO() {
		
	}

	public MemberDTO(String firstName, String lastName, String email, String contactNumber, String password) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.contactNumber = contactNumber;
		this.password = password;
	}
	
	public MemberDTO(String firstName, String lastName, String email, String contactNumber, String password, String tidingId) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.contactNumber = contactNumber;
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTidingId() {
		return tidingId;
	}

	public void setTidingId(String tidingId) {
		this.tidingId = tidingId;
	}
}
