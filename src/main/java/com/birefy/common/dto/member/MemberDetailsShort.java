package com.birefy.common.dto.member;
//TODO perhaps delete this class
public class MemberDetailsShort {
	private String firstName;
	private String familyName;
	private Long id;
	
	public MemberDetailsShort(String firstName, String familyName, Long id) {
		this.firstName = firstName;
		this.familyName = familyName;
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
}
