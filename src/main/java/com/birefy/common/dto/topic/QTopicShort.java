package com.birefy.common.dto.topic;

public class QTopicShort {
	private long id;
	private String topicName;
	private String topicDetail;
	
	public QTopicShort() {
		
	}
	
	public QTopicShort(long id, String topicName, String topicDetail) {
		this.id = id;
		this.topicName = topicName;
		this.topicDetail = topicDetail;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getTopicDetail() {
		return topicDetail;
	}

	public void setTopicDetail(String topicDetail) {
		this.topicDetail = topicDetail;
	}
	
	
}
