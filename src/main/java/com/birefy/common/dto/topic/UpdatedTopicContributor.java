package com.birefy.common.dto.topic;

public class UpdatedTopicContributor {
	private Long topicId;
	private Long memberId;
	private String tidingId;
	
	public UpdatedTopicContributor(Long topicId, Long memberId, String tidingId) {
		this.topicId = topicId;
		this.memberId = memberId;
		this.tidingId = tidingId;
	}
	
	public UpdatedTopicContributor() {
	}
	
	public Long getTopicId() {
		return topicId;
	}
	
	public void setTopicId(Long topicId) {
		this.topicId = topicId;
	}
	
	public Long getMemberId() {
		return memberId;
	}
	
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	
	public String getTidingId() {
		return tidingId;
	}
	
	public void setTidingId(String tidingId) {
		this.tidingId = tidingId;
	}
}
