package com.birefy.common.dto.topic;

public class TopicContributor {
	Long memberId;
	String tidingId;
	
	public TopicContributor(Long memberId, String tidingId) {
		this.memberId = memberId;
		this.tidingId = tidingId;
	}
	
	public TopicContributor() {
	}
	
	public Long getMemberId() {
		return memberId;
	}
	
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	
	public String getTidingId() {
		return tidingId;
	}
	
	public void setTidingId(String tidingId) {
		this.tidingId = tidingId;
	}
}
