package com.birefy.common.dto.topic;

public class QTopicDTO extends QTopicShort {
	private Boolean active;
	
	public QTopicDTO(long id, String topicName, String topicDetail, Boolean active) {
		super(id, topicName, topicDetail);
		this.active = active;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	
	
}
