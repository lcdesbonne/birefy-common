package com.birefy.common.dto.topic;

public class QTagWithTopic {
	private long tagId;
	private String tagName;
	private long topicId;
	
	public QTagWithTopic() {
	}
	public QTagWithTopic(long tagId, String tagName, long topicId) {
		this.tagId = tagId;
		this.tagName = tagName;
		this.topicId = topicId;
	}
	public long getTagId() {
		return tagId;
	}
	public void setTagId(long tagId) {
		this.tagId = tagId;
	}
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	public long getTopicId() {
		return topicId;
	}
	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}

}
