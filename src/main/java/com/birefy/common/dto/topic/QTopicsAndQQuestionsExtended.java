package com.birefy.common.dto.topic;

import java.util.ArrayList;
import java.util.List;

import com.birefy.common.dto.question.QQuestionShortExtended;

public class QTopicsAndQQuestionsExtended {
	private List<QTopicShort> topics;
	private List<QQuestionShortExtended> questions;
	
	public QTopicsAndQQuestionsExtended() {
		this.topics = new ArrayList<>();
		this.questions = new ArrayList<>();
	}
	
	public QTopicsAndQQuestionsExtended(List<QTopicShort> topics, List<QQuestionShortExtended> questions) {
		this.topics = topics;
		this.questions = questions;
	}

	public List<QTopicShort> getTopics() {
		return topics;
	}

	public void setTopics(List<QTopicShort> topics) {
		this.topics = topics;
	}

	public List<QQuestionShortExtended> getQuestions() {
		return questions;
	}

	public void setQuestions(List<QQuestionShortExtended> questions) {
		this.questions = questions;
	}
	
	
}
