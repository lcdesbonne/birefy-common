package com.birefy.common.dto.topic;

import java.util.List;

public class TopicUpdate {
	private Long topicId;
	private String title;
	private String detail;
	private List<String> addedTags;
	private List<String> removedTags;
	private Boolean active;
	
	public TopicUpdate() {
	}

	public TopicUpdate(Long topicId, String title, String detail, List<String> addedTags, List<String> removedTags, Boolean active) {
		this.topicId = topicId;
		this.title = title;
		this.detail = detail;
		this.addedTags = addedTags;
		this.removedTags = removedTags;
		this.active = active;
	}

	public Long getTopicId() {
		return topicId;
	}

	public void setTopicId(Long topicId) {
		this.topicId = topicId;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public List<String> getAddedTags() {
		return addedTags;
	}

	public void setAddedTags(List<String> addedTags) {
		this.addedTags = addedTags;
	}

	public List<String> getRemovedTags() {
		return removedTags;
	}

	public void setRemovedTags(List<String> removedTags) {
		this.removedTags = removedTags;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
