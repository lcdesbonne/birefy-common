package com.birefy.common.dto.topic;

import java.util.List;

public class TopicAndTags {
	private Long topicId;
	private List<Long> tagIds;
	
	public TopicAndTags(Long topicId, List<Long> tagIds) {
		this.topicId = topicId;
		this.tagIds = tagIds;
	}
	
	public TopicAndTags() {
		
	}
	
	public Long getTopicId() {
		return this.topicId;
	}
	
	public void setTopicId(Long topicId) {
		this.topicId = topicId;
	}
	
	public List<Long> getTagIds() {
		return this.tagIds;
	}
	
	public void setTagIds(List<Long> tagIds) {
		this.tagIds = tagIds;
	}
}
