package com.birefy.common.dto.topic;

import java.util.ArrayList;
import java.util.List;

import com.birefy.common.dto.question.QQuestionShort;

public class QTopicsAndQQuestions {
	private List<QTopicShort> topics;
	private List<QQuestionShort> questions;
	
	public QTopicsAndQQuestions() {
		this.topics = new ArrayList<>();
		this.questions = new ArrayList<>();
	}
	
	public QTopicsAndQQuestions(List<QTopicShort> topics, List<QQuestionShort> questions) {
		this.topics = topics;
		this.questions = questions;
	}

	public List<QTopicShort> getTopics() {
		return topics;
	}

	public void setTopics(List<QTopicShort> topics) {
		this.topics = topics;
	}

	public List<QQuestionShort> getQuestions() {
		return questions;
	}

	public void setQuestions(List<QQuestionShort> questions) {
		this.questions = questions;
	}

}
