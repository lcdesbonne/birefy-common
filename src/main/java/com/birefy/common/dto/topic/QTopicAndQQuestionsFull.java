package com.birefy.common.dto.topic;

import java.util.ArrayList;
import java.util.List;

import com.birefy.common.dto.question.QQuestionDTO;

public class QTopicAndQQuestionsFull {
	private List<QTopicDTO> topics;
	private List<QQuestionDTO> questions;
	
	public QTopicAndQQuestionsFull() {
		this.topics = new ArrayList<>();
		this.questions = new ArrayList<>();
	}
	
	public QTopicAndQQuestionsFull(List<QTopicDTO> topics, List<QQuestionDTO> questions) {
		this.topics = topics;
		this.questions = questions;
	}

	public List<QTopicDTO> getTopics() {
		return topics;
	}

	public void setTopics(List<QTopicDTO> topics) {
		this.topics = topics;
	}

	public List<QQuestionDTO> getQuestions() {
		return questions;
	}

	public void setQuestions(List<QQuestionDTO> questions) {
		this.questions = questions;
	}

}
