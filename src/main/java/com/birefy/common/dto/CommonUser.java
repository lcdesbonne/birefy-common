package com.birefy.common.dto;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.birefy.common.security.Authority;

public class CommonUser implements UserDetails {
	private static final long serialVersionUID = 1L; //TODO understand why this is needed...
	
	private long id;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private List<Authority> authority;
	private boolean active;
	private boolean enabled;
	
	public CommonUser() {
		
	}
	
	public CommonUser(long id, String firstName, String lastName, String email, String password,
			List<Authority> authority, boolean active) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.authority = authority;
		this.active = active;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authority;
	}
	
	public void setAuthorities(List<Authority> authorities) {
		this.authority = authorities;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String getPassword() {
		return this.password;
	}
	
	public void setUserName(String email) {
		this.email = email;
	}

	@Override
	public String getUsername() {
		return this.email;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public boolean isAccountNonExpired() {
		return this.active;
	}

	@Override
	public boolean isAccountNonLocked() {
		return this.active;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		//TODO implement credential expiry
		return true;
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}
	
	public void setIsEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
