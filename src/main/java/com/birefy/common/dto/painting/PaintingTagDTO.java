package com.birefy.common.dto.painting;

public class PaintingTagDTO {
	private Long id;
	private String tag;
	
	public PaintingTagDTO() {
	}
	
	public PaintingTagDTO(Long id, String tag) {
		this.id = id;
		this.tag = tag;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
}
