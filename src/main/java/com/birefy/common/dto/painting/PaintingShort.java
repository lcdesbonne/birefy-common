package com.birefy.common.dto.painting;

import com.birefy.common.entity.Painting;
import com.birefy.common.entity.Painting.Category;

public class PaintingShort {
	private Long id;
	private String name;
	private String location;
	private Long memberId;
	private Painting.Category category;
	private Float price;
	
	public PaintingShort() {
		
	}
	
	public PaintingShort(Long id, String name, String location, Long memberId, Category category) {
		this.id = id;
		this.name = name;
		this.location = location;
		this.memberId = memberId;
		this.category = category;
	}
	
	public PaintingShort(Long id, String name, String location, Long memberId, Category category, Float price) {
		this.id = id;
		this.name = name;
		this.location = location;
		this.memberId = memberId;
		this.category = category;
		this.price = price;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Painting.Category getCategory() {
		return category;
	}

	public void setCategory(Painting.Category category) {
		this.category = category;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
}
