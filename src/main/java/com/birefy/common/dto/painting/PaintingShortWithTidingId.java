package com.birefy.common.dto.painting;

import com.birefy.common.entity.Painting.Category;

public class PaintingShortWithTidingId extends PaintingShort {
	private String tidingId;
	private Long channelId;
	
	public PaintingShortWithTidingId() {
		
	}
	
	public PaintingShortWithTidingId(Long id, String name, String location, Long memberId, Category category, String tidingId) {
		super(id, name,location,memberId, category);
		this.tidingId = tidingId;
	}
	
	public PaintingShortWithTidingId(PaintingShort paintingShort) {
		super(
				paintingShort.getId(),
				paintingShort.getName(),
				paintingShort.getLocation(),
				paintingShort.getMemberId(),
				paintingShort.getCategory(),
				paintingShort.getPrice());
	}
	
	public PaintingShortWithTidingId(PaintingShort paintingShort, String tidingId) {
		super(
				paintingShort.getId(),
				paintingShort.getName(),
				paintingShort.getLocation(),
				paintingShort.getMemberId(),
				paintingShort.getCategory(),
				paintingShort.getPrice());
		this.tidingId = tidingId;
	}

	public String getTidingId() {
		return tidingId;
	}

	public void setTidingId(String tidingId) {
		this.tidingId = tidingId;
	}

	public Long getChannelId() {
		return channelId;
	}

	public void setChannelId(Long channelId) {
		this.channelId = channelId;
	}
}
