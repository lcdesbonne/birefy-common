package com.birefy.common.dto.painting;

public class PaintingTagResult {
	private Long paintingId;
	private Long tagId;
	private String tag;
	
	
	public PaintingTagResult(Long paintingId, Long tagId, String tag) {
		this.paintingId = paintingId;
		this.tagId = tagId;
		this.tag = tag;
	}
	
	public Long getPaintingId() {
		return paintingId;
	}
	
	public void setPaintingId(Long paintingId) {
		this.paintingId = paintingId;
	}
	
	public Long getTagId() {
		return tagId;
	}
	
	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}
	
	public String getTag() {
		return tag;
	}
	
	public void setTag(String tag) {
		this.tag = tag;
	}
}
