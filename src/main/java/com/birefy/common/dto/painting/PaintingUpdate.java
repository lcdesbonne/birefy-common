package com.birefy.common.dto.painting;

import java.util.List;

import com.birefy.common.entity.Painting;
import com.birefy.common.entity.Painting.Category;

public class PaintingUpdate {
	private Long paintingId;
	private String newPaintingTitle;
	private Painting.Category newCategory;
	private List<PaintingTagDTO> addedTags;
	private List<PaintingTagDTO> removedTags;
	private Float price;
	
	public PaintingUpdate() {
		
	}

	public PaintingUpdate(Long paintingId, String newPaintingTitle, Category newCategory) {
		this.paintingId = paintingId;
		this.newPaintingTitle = newPaintingTitle;
		this.newCategory = newCategory;
	}

	public PaintingUpdate(Long paintingId, String newPaintingTitle, Category newCategory,
			List<PaintingTagDTO> addedTags, List<PaintingTagDTO> removedTags, Float price) {
		this.paintingId = paintingId;
		this.newPaintingTitle = newPaintingTitle;
		this.newCategory = newCategory;
		this.addedTags = addedTags;
		this.removedTags = removedTags;
		this.price = price;
	}

	public Long getPaintingId() {
		return paintingId;
	}

	public void setPaintingId(Long paintingId) {
		this.paintingId = paintingId;
	}

	public String getNewPaintingTitle() {
		return newPaintingTitle;
	}

	public void setNewPaintingTitle(String newPaintingTitle) {
		this.newPaintingTitle = newPaintingTitle;
	}

	public Painting.Category getNewCategory() {
		return newCategory;
	}

	public void setNewCategory(Painting.Category newCategory) {
		this.newCategory = newCategory;
	}

	public List<PaintingTagDTO> getAddedTags() {
		return addedTags;
	}

	public void setAddedTags(List<PaintingTagDTO> addedTags) {
		this.addedTags = addedTags;
	}

	public List<PaintingTagDTO> getRemovedTags() {
		return removedTags;
	}

	public void setRemovedTags(List<PaintingTagDTO> removedTags) {
		this.removedTags = removedTags;
	}

	public Float getPrice() {
		return price >= 0 ? price : 0;
	}

	public void setPrice(Float price) {
		this.price = price >= 0 ? price : 0;
	}
}
