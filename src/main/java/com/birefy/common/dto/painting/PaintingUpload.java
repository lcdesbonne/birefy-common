package com.birefy.common.dto.painting;

import java.io.IOException;
import java.util.List;

import javax.validation.constraints.NotEmpty;

import org.springframework.web.multipart.MultipartFile;

import com.birefy.common.entity.Painting;
import com.birefy.common.entity.Painting.Category;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PaintingUpload {
	private String title;
	private Painting.Category category;
	private MultipartFile paintingFile;
	private String tagString;
	private Float price;
	@NotEmpty
	private String software;
	
	public PaintingUpload() {
		
	}
	
	public PaintingUpload(String title, Category category, MultipartFile paintingFile, Float price, String software) {
		this.title = title;
		this.category = category;
		this.paintingFile = paintingFile;
		this.price = price;
		this.software = software;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Painting.Category getCategory() {
		return category;
	}

	public void setCategory(Painting.Category category) {
		this.category = category;
	}

	public MultipartFile getPaintingFile() {
		return paintingFile;
	}

	public void setPaintingFile(MultipartFile paintingFile) {
		this.paintingFile = paintingFile;
	}

	public String getTagString() {
		return tagString;
	}

	public void setTagString(String tagString) {
		this.tagString = tagString;
	}
	
	public List<PaintingTagDTO> getTags() throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(tagString, new TypeReference<List<PaintingTagDTO>>(){});
	}

	public Float getPrice() {
		return price != null ? price : 0f;
	}

	public void setPrice(Float price) {
		this.price = price >= 0 ? price : 0f;
	}
	
	public String getSoftware() {
		return software;
	}
	
	public void setSoftware(String software) {
		this.software = software;
	}
}
