package com.birefy.common.dto.sound;

public class SoundShort {
	private Long id;
	private Long memberId;
	private String location;
	private String category;
	private String name;
	private Float price;
	
	public SoundShort() {
		
	}
	
	public SoundShort(Long id, Long memberId, String location, String category, String name) {
		this.id = id;
		this.memberId = memberId;
		this.location = location;
		this.category = category;
		this.name = name;
	}
	
	public SoundShort(Long id, Long memberId, String category, String name, Float price) {
		this.id = id;
		this.memberId = memberId;
		this.category = category;
		this.name = name;
		this.price = price;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
}
