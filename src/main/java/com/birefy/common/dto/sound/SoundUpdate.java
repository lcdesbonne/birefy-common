package com.birefy.common.dto.sound;

import java.util.List;

import com.birefy.common.entity.Sound;
import com.birefy.common.entity.Sound.Category;

public class SoundUpdate {
	Long soundId;
	String newSoundName;
	Sound.Category newCategory;
	List<SoundTagDTO> addedTags;
	List<SoundTagDTO> removedTags;
	Float price;
	
	public SoundUpdate() {
		
	}
	
	public SoundUpdate(Long soundId, String newSoundName, Category newCategory) {
		this.soundId = soundId;
		this.newSoundName = newSoundName;
		this.newCategory = newCategory;
	}

	public Long getSoundId() {
		return soundId;
	}

	public void setSoundId(Long soundId) {
		this.soundId = soundId;
	}

	public String getNewSoundName() {
		return newSoundName;
	}

	public void setNewSoundName(String newSoundName) {
		this.newSoundName = newSoundName;
	}

	public Sound.Category getNewCategory() {
		return newCategory;
	}

	public void setNewCategory(Sound.Category newCategory) {
		this.newCategory = newCategory;
	}

	public List<SoundTagDTO> getAddedTags() {
		return addedTags;
	}

	public void setAddedTags(List<SoundTagDTO> addedTags) {
		this.addedTags = addedTags;
	}

	public List<SoundTagDTO> getRemovedTags() {
		return removedTags;
	}

	public void setRemovedTags(List<SoundTagDTO> removedTags) {
		this.removedTags = removedTags;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
}
