package com.birefy.common.dto.sound;

public class SoundNameAndCategory {
	private String soundName;
	private String category;
	
	public SoundNameAndCategory(String soundName, String category) {
		this.soundName = soundName;
		this.category = category;
	}
	
	public String getSoundName() {
		return soundName;
	}
	public void setSoundName(String soundName) {
		this.soundName = soundName;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}	
}
