package com.birefy.common.dto.sound;

public class SoundTagResult {
	private Long soundId;
	private Long tagId;
	private String tag;
	
	
	public SoundTagResult(Long soundId, Long tagId, String tag) {
		this.soundId = soundId;
		this.tagId = tagId;
		this.tag = tag;
	}
	
	public Long getSoundId() {
		return soundId;
	}
	
	public void setSoundId(Long soundId) {
		this.soundId = soundId;
	}
	
	public Long getTagId() {
		return tagId;
	}
	
	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}
	
	public String getTag() {
		return tag;
	}
	
	public void setTag(String tag) {
		this.tag = tag;
	}
}
