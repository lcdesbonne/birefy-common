package com.birefy.common.dto.sound;

public class SoundWithTidingId extends SoundShort {
	private String tidingId;
	private Long channelId;
	
	public SoundWithTidingId(Long id, Long memberId, String location, String category, String name) {
		super(id, memberId, location, category, name);
	}
	
	public SoundWithTidingId(Long id, Long memberId, String location, String category, String name, String tidingId) {
		super(id, memberId, location, category, name);
		this.tidingId = tidingId;
	}
	
	public SoundWithTidingId(SoundShort sound) {
		super(sound.getId(), sound.getMemberId(), sound.getCategory(), sound.getName(), sound.getPrice());
	}

	public String getTidingId() {
		return tidingId;
	}

	public void setTidingId(String tidingId) {
		this.tidingId = tidingId;
	}

	public Long getChannelId() {
		return channelId;
	}

	public void setChannelId(Long channelId) {
		this.channelId = channelId;
	}
}
