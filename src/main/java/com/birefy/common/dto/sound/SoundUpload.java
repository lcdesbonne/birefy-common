package com.birefy.common.dto.sound;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.birefy.common.entity.Sound;
import com.birefy.common.entity.Sound.Category;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SoundUpload {
	private String title;
	private Sound.Category category;
	private MultipartFile soundFile;
	private String tagsString;
	private Float price;
	private String software;
	
	public SoundUpload() {
		
	}
	
	public SoundUpload(String title, Category category, MultipartFile soundFile, List<SoundTagDTO> tags, Float price, String software) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		this.title = title;
		this.category = category;
		this.soundFile = soundFile;
		this.tagsString = objectMapper.writeValueAsString(tags);
		this.price = price;
		this.software = software;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Sound.Category getCategory() {
		return category;
	}

	public void setCategory(Sound.Category category) {
		this.category = category;
	}

	public MultipartFile getSoundFile() {
		return soundFile;
	}

	public void setSoundFile(MultipartFile soundFile) {
		this.soundFile = soundFile;
	}

	public List<SoundTagDTO> getTags() throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(tagsString, new TypeReference<List<SoundTagDTO>>(){});
	}

	public void setTags(List<SoundTagDTO> tags) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		this.tagsString = objectMapper.writeValueAsString(tags);
	}

	public String getTagsString() {
		return tagsString;
	}

	public void setTagsString(String tagsString) {
		this.tagsString = tagsString;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
	
	public String getSoftware() {
		return software;
	}
	
	public void setSoftware(String software) {
		this.software = software;
	}
	
}
