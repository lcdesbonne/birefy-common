package com.birefy.common.dto.tiding;

import java.util.List;

public class TagUpdateDTO {
	private long topicId;
	private List<String> newTagNames;
	private List<Long> existingTagIds;
	
	public TagUpdateDTO() {
		
	}
	
	public TagUpdateDTO(long topicId, List<String> newTagNames, List<Long> existingTopicIds) {
		this.topicId  = topicId;
		this.newTagNames = newTagNames;
		this.existingTagIds = existingTopicIds;
	}

	public long getTopicId() {
		return topicId;
	}

	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}

	public List<String> getNewTagNames() {
		return newTagNames;
	}

	public void setNewTagNames(List<String> newTagNames) {
		this.newTagNames = newTagNames;
	}

	public List<Long> getExistingTagIds() {
		return existingTagIds;
	}

	public void setExistingTagIds(List<Long> existingTopicIds) {
		this.existingTagIds = existingTopicIds;
	}
	
	
}
