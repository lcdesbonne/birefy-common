package com.birefy.common.dto.tiding;

public class NewGroupResponse {
	boolean success;
	TidingIdDTO tidingId;
	
	public NewGroupResponse(boolean success, TidingIdDTO tidingId) {
		this.success = success;
		this.tidingId = tidingId;
	}

	public NewGroupResponse() {
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public TidingIdDTO getTidingId() {
		return tidingId;
	}

	public void setTidingId(TidingIdDTO tidingId) {
		this.tidingId = tidingId;
	}
	
}
