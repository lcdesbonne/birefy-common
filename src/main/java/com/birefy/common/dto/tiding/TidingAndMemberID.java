package com.birefy.common.dto.tiding;

public class TidingAndMemberID {
	private Long memberId;
	private String tidingId;
	
	public TidingAndMemberID() {
	}
	
	public TidingAndMemberID(Long memberId, String tidingId) {
		this.memberId = memberId;
		this.tidingId = tidingId;
	}
	
	public Long getMemberId() {
		return memberId;
	}
	
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	
	public String getTidingId() {
		return tidingId;
	}
	
	public void setTidingId(String tidingId) {
		this.tidingId = tidingId;
	}
}
