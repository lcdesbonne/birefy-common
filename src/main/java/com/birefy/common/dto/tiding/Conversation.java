package com.birefy.common.dto.tiding;

import java.util.List;

import com.birefy.common.tidings.Tiding;

public class Conversation {
	private TidingIdDTO withTidingUser;
	private List<Tiding> tidings;
	private boolean isGroup;
	
	public Conversation(TidingIdDTO withTidingUser, List<Tiding> tidings) {
		this.withTidingUser = withTidingUser;
		this.tidings = tidings;
	}
	
	public Conversation(TidingIdDTO withTidingUser, List<Tiding> tidings, boolean isGroup) {
		this.withTidingUser = withTidingUser;
		this.tidings = tidings;
		this.isGroup = isGroup;
	}

	public TidingIdDTO getWithTidingUser() {
		return withTidingUser;
	}

	public void setWithTidingUser(TidingIdDTO withTidingUser) {
		this.withTidingUser = withTidingUser;
	}

	public List<Tiding> getTidings() {
		return tidings;
	}

	public void setTidings(List<Tiding> tidings) {
		this.tidings = tidings;
	}
	
	public void setIsGroup(boolean isGroup) {
		this.isGroup = isGroup;
	}
	
	public boolean isGroup() {
		return this.isGroup;
	}
}
