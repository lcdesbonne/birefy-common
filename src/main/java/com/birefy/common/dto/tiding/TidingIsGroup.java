package com.birefy.common.dto.tiding;

public class TidingIsGroup {
	private Long tidingId;
	private boolean isGroup;
	
	public TidingIsGroup(Long tidingId, boolean isGroup) {
		this.tidingId = tidingId;
		this.isGroup = isGroup;
	}

	public Long getTidingId() {
		return tidingId;
	}

	public void setTidingId(Long tidingId) {
		this.tidingId = tidingId;
	}

	public boolean isGroup() {
		return isGroup;
	}

	public void setGroup(boolean isGroup) {
		this.isGroup = isGroup;
	}
	
	
}
