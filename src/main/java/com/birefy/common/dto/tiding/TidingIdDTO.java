package com.birefy.common.dto.tiding;

public class TidingIdDTO {
	private String tidingId;
	private long channelId;
	
	public TidingIdDTO(String tidingId, long channelId) {
		this.tidingId = tidingId;
		this.channelId = channelId;
	}
	
	public TidingIdDTO(String tidingId) {
		this.tidingId = tidingId;
	}

	public String getTidingId() {
		return tidingId;
	}

	public void setTidingId(String tidingId) {
		this.tidingId = tidingId;
	}

	public long getChannelId() {
		return channelId;
	}

	public void setChannelId(long channelId) {
		this.channelId = channelId;
	}
}
