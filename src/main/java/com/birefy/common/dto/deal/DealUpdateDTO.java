package com.birefy.common.dto.deal;

import com.birefy.common.entity.Deal;

public class DealUpdateDTO {
	private Long dealId;
	private String customerDetails;
	private String supplierDetails;
	private Float price;
	
	public DealUpdateDTO() {
	}
	
	public DealUpdateDTO(Long dealId, String customerDetails, String supplierDetails, Float price) {
		this.customerDetails = customerDetails;
		this.supplierDetails = supplierDetails;
		this.price = price;
		this.dealId = dealId;
	}
	
	public Deal mapToDeal() {
		Deal deal = new Deal();
		deal.setId(dealId);
		deal.setCustomerDetails(customerDetails);
		deal.setSupplierDetails(supplierDetails);
		deal.setPrice(price);
		return deal;
	}
	
	public Long getDealId() {
		return dealId;
	}

	public void setDealId(Long dealId) {
		this.dealId = dealId;
	}

	public String getCustomerDetails() {
		return customerDetails;
	}
	
	public void setCustomerDetails(String customerDetails) {
		this.customerDetails = customerDetails;
	}
	
	public String getSupplierDetails() {
		return supplierDetails;
	}
	
	public void setSupplierDetails(String supplierDetails) {
		this.supplierDetails = supplierDetails;
	}
	
	public Float getPrice() {
		return price;
	}
	
	public void setPrice(Float price) {
		this.price = price;
	}
}
