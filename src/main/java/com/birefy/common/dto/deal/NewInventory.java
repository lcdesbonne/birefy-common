package com.birefy.common.dto.deal;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

import com.birefy.common.validation.FileCheck;


public class NewInventory {
	@NotNull
	private Long dealId;
	@NotNull
	private Integer inventoryTypeId;
	private String runCommands;
	@NotNull @NotEmpty
	private String title;
	@FileCheck
	private MultipartFile inventoryFile;
	
	public NewInventory(Long dealId, Integer inventoryTypeId, String runCommands, 
			String title, MultipartFile inventoryFile) {
		this.dealId = dealId;
		this.inventoryTypeId = inventoryTypeId;
		this.runCommands = runCommands;
		this.title = title;
		this.inventoryFile = inventoryFile;
	}

	public NewInventory() {
	}
	
	public Long getDealId() {
		return dealId;
	}

	public void setDealId(Long dealId) {
		this.dealId = dealId;
	}

	public Integer getInventoryTypeId() {
		return inventoryTypeId;
	}

	public void setInventoryTypeId(Integer inventoryTypeId) {
		this.inventoryTypeId = inventoryTypeId;
	}

	public String getRunCommands() {
		return runCommands;
	}

	public void setRunCommands(String runCommands) {
		this.runCommands = runCommands;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public MultipartFile getInventoryFile() {
		return inventoryFile;
	}

	public void setInventoryFile(MultipartFile inventoryFile) {
		this.inventoryFile = inventoryFile;
	}
}
