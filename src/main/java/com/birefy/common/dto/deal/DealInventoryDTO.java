package com.birefy.common.dto.deal;

public class DealInventoryDTO {
	public static final String CAN_DOWNLOAD_QUERY_HEADER = "can_download";
	private String title;
	private Long inventoryId;
	private Long authorId;
	private Long dealId;
	private InventoryTypeDTO type;
	private boolean canPreview;
	private boolean canDownload;
	private String runCommands;
	private String fileExtension;
	
	public DealInventoryDTO(String title, Long inventoryId, Long authorId, Long dealId, InventoryTypeDTO type, boolean canPreview,
			boolean canDownload, String runCommands, String fileExtension) {
		this.title = title;
		this.inventoryId = inventoryId;
		this.authorId = authorId;
		this.dealId = dealId;
		this.type = type;
		this.canPreview = canPreview;
		this.canDownload = canDownload;
		this.runCommands = runCommands;
		this.fileExtension = fileExtension;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public DealInventoryDTO() {
	}

	public Long getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(Long inventoryId) {
		this.inventoryId = inventoryId;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public Long getDealId() {
		return dealId;
	}

	public void setDealId(Long dealId) {
		this.dealId = dealId;
	}

	public InventoryTypeDTO getType() {
		return type;
	}

	public void setType(InventoryTypeDTO type) {
		this.type = type;
	}

	public boolean isCanPreview() {
		return canPreview;
	}

	public void setCanPreview(boolean canPreview) {
		this.canPreview = canPreview;
	}

	public boolean isCanDownload() {
		return canDownload;
	}

	public void setCanDownload(boolean canDownload) {
		this.canDownload = canDownload;
	}

	public String getRunCommands() {
		return runCommands;
	}

	public void setRunCommands(String runCommands) {
		this.runCommands = runCommands;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}
	
}
