package com.birefy.common.dto.deal;

public class InventoryUpdateDTO {
	private Long inventoryId;
	private String title;
	private boolean canPreview;
	
	public InventoryUpdateDTO(Long inventoryId, String title, boolean canPreview) {
		this.inventoryId = inventoryId;
		this.title = title;
		this.canPreview = canPreview;
	}
	
	public Long getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(Long inventoryId) {
		this.inventoryId = inventoryId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isCanPreview() {
		return canPreview;
	}

	public void setCanPreview(boolean canPreview) {
		this.canPreview = canPreview;
	}
}
