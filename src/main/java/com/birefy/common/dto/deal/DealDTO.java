package com.birefy.common.dto.deal;

import java.time.LocalDateTime;

import com.birefy.common.dto.tiding.TidingAndMemberID;

public class DealDTO {
	private Long id;
	private TidingAndMemberID  customerId;
	private TidingAndMemberID supplierId;
	private boolean supplierConfirm;
	private boolean customerConfirm;
	private String supplierDetails;
	private String customerDetails;
	private String creationDate;
	private boolean active;
	private LocalDateTime paid;
	private Float price;
	
	
	public DealDTO(Long id, TidingAndMemberID customerId, TidingAndMemberID supplierId, boolean supplierConfirm,
			boolean customerConfirm, String supplierDetails, String customerDetails, String creationDate,
			boolean active, LocalDateTime paid, Float price) {
		this.id = id;
		this.customerId = customerId;
		this.supplierId = supplierId;
		this.supplierConfirm = supplierConfirm;
		this.customerConfirm = customerConfirm;
		this.supplierDetails = supplierDetails;
		this.customerDetails = customerDetails;
		this.creationDate = creationDate;
		this.active = active;
		this.paid = paid;
		this.price = price;
	}


	public DealDTO() {
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public TidingAndMemberID getCustomerId() {
		return customerId;
	}


	public void setCustomerId(TidingAndMemberID customerId) {
		this.customerId = customerId;
	}


	public TidingAndMemberID getSupplierId() {
		return supplierId;
	}


	public void setSupplierId(TidingAndMemberID supplierId) {
		this.supplierId = supplierId;
	}


	public boolean isSupplierConfirm() {
		return supplierConfirm;
	}


	public void setSupplierConfirm(boolean supplierConfirm) {
		this.supplierConfirm = supplierConfirm;
	}


	public boolean isCustomerConfirm() {
		return customerConfirm;
	}


	public void setCustomerConfirm(boolean customerConfirm) {
		this.customerConfirm = customerConfirm;
	}


	public String getSupplierDetails() {
		return supplierDetails;
	}


	public void setSupplierDetails(String supplierDetails) {
		this.supplierDetails = supplierDetails;
	}


	public String getCustomerDetails() {
		return customerDetails;
	}


	public void setCustomerDetails(String customerDetails) {
		this.customerDetails = customerDetails;
	}


	public String getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}


	public boolean isActive() {
		return active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public LocalDateTime getPaid() {
		return paid;
	}


	public void setPaid(LocalDateTime paid) {
		this.paid = paid;
	}


	public Float getPrice() {
		return price;
	}


	public void setPrice(Float price) {
		this.price = price;
	}
	
}
