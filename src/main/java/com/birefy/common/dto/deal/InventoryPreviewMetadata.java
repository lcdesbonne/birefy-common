package com.birefy.common.dto.deal;

public class InventoryPreviewMetadata {
	private boolean isSoundFile;
	private String title;
	private Long id;
	private String contentUrl;
	
	public InventoryPreviewMetadata(boolean isSoundFile, String title, Long id, String contentUrl) {
		this.isSoundFile = isSoundFile;
		this.title = title;
		this.id = id;
		this.contentUrl = contentUrl;
	}
	
	public boolean isSoundFile() {
		return isSoundFile;
	}
	public void setSoundFile(boolean isSoundFile) {
		this.isSoundFile = isSoundFile;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getContentUrl() {
		return contentUrl;
	}

	public void setContentUrl(String contentUrl) {
		this.contentUrl = contentUrl;
	}
	
	
}
