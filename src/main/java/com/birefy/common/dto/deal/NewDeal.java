package com.birefy.common.dto.deal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.birefy.common.validation.DealDescriptionCheck;

public class NewDeal {
	@NotNull(message = "A customer must be provided")
	private Long customerId;
	@DealDescriptionCheck
	private String customerDetails;
	@NotNull(message = "A supplier must be provided")
	private Long supplierId;
	@DealDescriptionCheck
	private String supplierDetails;
	@Min(value = 0, message = "The price cannot be less than 0")
	@NotNull(message = "An initial price must be provided")
	private Float price;
	
	public NewDeal() {
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerDetails() {
		return customerDetails;
	}

	public void setCustomerDetails(String customerDetails) {
		this.customerDetails = customerDetails;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierDetails() {
		return supplierDetails;
	}

	public void setSupplierDetails(String supplierDetails) {
		this.supplierDetails = supplierDetails;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
	
}
