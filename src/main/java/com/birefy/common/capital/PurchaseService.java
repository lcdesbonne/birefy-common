package com.birefy.common.capital;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.birefy.common.authentication.BirefyPlatformAuthority;
import com.birefy.common.dto.capital.ChargeDetails;
import com.birefy.common.dto.capital.ChargeDetails.MediaCategory;
import com.birefy.common.dto.capital.CurrencyValue;
import com.birefy.common.dto.capital.Purchase;
import com.birefy.common.dto.capital.RevenuePayment;
import com.birefy.common.dto.painting.PaintingShort;
import com.birefy.common.dto.sound.SoundShort;
import com.birefy.common.entity.ImagePurchases;
import com.birefy.common.entity.Member;
import com.birefy.common.entity.Revenue;
import com.birefy.common.entity.SoundPurchases;
import com.birefy.common.repo.DealRepository;
import com.birefy.common.repo.ImagePurchasesRepository;
import com.birefy.common.repo.MemberRepository;
import com.birefy.common.repo.PaintingRepository;
import com.birefy.common.repo.QMemberQuestionRepository;
import com.birefy.common.repo.RevenueRepository;
import com.birefy.common.repo.SoundPurchasesRepository;
import com.birefy.common.repo.SoundRepository;
import com.birefy.common.service.PayoutService;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import com.stripe.model.PaymentIntent;
import com.stripe.model.Plan;
import com.stripe.model.Subscription;
import com.stripe.net.RequestOptions;
import com.stripe.param.PaymentIntentCreateParams;

/*
 * Purchase functionality removed from the current vision of the project.
 * May be reinstated in future.
 */
//@Service
@Deprecated
public class PurchaseService {
	public static final String INTERNAL_SUPPLIER_ID_KEY = "supplierId";
	public static final String INTERNAL_CUSTOMER_ID_KEY = "customerId";
	public static final String DEAL_ID_KEY = "dealId";
	public static final String MEDIA_ID_KEY = "mediaId";
	public static final String MEDIA_CATEGORY_KEY = "mediaCategory";
	
	@Autowired
	private QMemberQuestionRepository qMemberQuestionRepository;
	@Autowired
	private SoundPurchasesRepository soundPurchasesRepository;
	@Autowired
	private ImagePurchasesRepository imagePurchasesRepository;
	@Autowired
	private DealRepository dealRepository;
	@Autowired
	private SoundRepository soundRepository;
	@Autowired
	private PaintingRepository paintingRepository;
	@Autowired
	private PayoutService payoutService;
	@Autowired
	private MemberRepository memberRepository;
	@Autowired
	private RevenueRepository revenueRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseService.class);
	
	@Value("${stripe.api.key}")
	private String STRIPE_API_KEY;
	@Value("${birefy.charges.transaction}")
	private Integer applicationFee;
	@Value("${birefy.charges.subscription}")
	private Integer subscriptionFee;
	
	private final String INNOVATION_BROADCAST_SUBSCRIPTION = "innovation_broadcast";
	
	public boolean purchaseProduce(Purchase purchase) {
		Charge charge = createCharge(purchase.getChargeDetails(), purchase.getChargeDescription());
		
		return postProcessCharge(charge);
	}
	
	public String purchaseProduceByPaymentIntent(Purchase purchase) {
		PaymentIntent paymentIntent = createPaymentIntent(purchase.getChargeDetails(), purchase.getChargeDescription());
		
		String clientSecret = paymentIntent.getClientSecret();
		
		return clientSecret;
	}
	
	public Charge chargeDetailsRequest(String chargeId) {
		RequestOptions requestOptions = RequestOptions.builder().setApiKey(STRIPE_API_KEY).build();
		try {
			return Charge.retrieve(chargeId, requestOptions);
		} catch (StripeException e) {
			LOGGER.error("Error retrieving charge details with id: " + chargeId, e);
			throw new RuntimeException("Error retrieving charge details.");
		}
	}
	
	public PaymentIntent generateChargeDetails(Long mediaId, ChargeDetails.MediaCategory category, long customerId) {
		long supplierId = getOwnerOfProduce(mediaId, category);

		Map<String, CurrencyValue> valueDetails = getValueForDigitalProduce(mediaId, category, supplierId);
		
		Entry<String, CurrencyValue> entry = valueDetails.entrySet().stream().findFirst().get();
		
		ChargeDetails cDetails = new ChargeDetails();
		cDetails.setSupplierMemberId(supplierId);
		cDetails.setCustomerMemberId(customerId);
		
		if (category == ChargeDetails.MediaCategory.DEAL) {
			cDetails.setDealId(mediaId);
		} else {
			cDetails.setMediaId(mediaId);
		}
		
		cDetails.setMediaCategory(category);
		
		cDetails.setValue(entry.getValue());
		
		return createPaymentIntent(cDetails, entry.getKey());
	}
	
	private PaymentIntent createPaymentIntent(ChargeDetails chargeDetails, String chargeDescription) {
		Member customerDetails = memberRepository.selectFullMemberDetailsById(chargeDetails.getCustomerMemberId());
		CurrencyValue value = chargeDetails.getValue();
				
		Map<String, String> metadata = new HashMap<String, String>();
		metadata.put(INTERNAL_SUPPLIER_ID_KEY, chargeDetails.getSupplierMemberId().toString());
		metadata.put(INTERNAL_CUSTOMER_ID_KEY, chargeDetails.getCustomerMemberId().toString());
		
		if(Optional.ofNullable(chargeDetails.getDealId()).isPresent()) {
			metadata.put(DEAL_ID_KEY, chargeDetails.getDealId().toString());
		}
		
		if(Optional.ofNullable(chargeDetails.getMediaId()).isPresent()) {
			metadata.put(MEDIA_ID_KEY, chargeDetails.getMediaId().toString());
		}
		
		if(Optional.ofNullable(chargeDetails.getMediaCategory()).isPresent()) {
			metadata.put(MEDIA_CATEGORY_KEY, chargeDetails.getMediaCategory().toString());
		}
		
		PaymentIntentCreateParams chargeParams;
		if (memberRepository.getMemberAuthority(chargeDetails.getSupplierMemberId()) == BirefyPlatformAuthority.ROYAL) {
			chargeParams = PaymentIntentCreateParams.builder()
					.setAmount(Long.valueOf(value.getValue()))
					.setCurrency("gbp")
					.setDescription(chargeDescription)
					.setReceiptEmail(customerDetails.getEmail())
					.putAllMetadata(metadata)
					.build();
		} else {
			Optional<String> supplierAccountId = memberRepository.getMemberAccountId(chargeDetails.getSupplierMemberId());
			
			if (supplierAccountId.isEmpty()) {
				throw new RuntimeException("No account information on record for member with ID: " + chargeDetails.getSupplierMemberId());
			}
			
			chargeParams = PaymentIntentCreateParams.builder()
					.setAmount(Long.valueOf(value.getValue()))
					.setCurrency("gbp")
					.setDescription(chargeDescription)
					.setReceiptEmail(customerDetails.getEmail())
					.setApplicationFeeAmount(applicationFee.longValue())
					.setOnBehalfOf(supplierAccountId.get())
					.putAllMetadata(metadata)
					.build();
		}
		
		
		
		RequestOptions options = RequestOptions.builder()
				.setIdempotencyKey(UUID.randomUUID().toString())
				.setApiKey(STRIPE_API_KEY).build();
		
		try {
			return PaymentIntent.create(chargeParams, options);
		} catch (StripeException e) {
			LOGGER.error(e.getMessage());
			throw new RuntimeException("Failed to complete request");
		}
	}
	
	public PaymentStatus postProcessPaymentIntent(PaymentIntent paymentIntent) {
		boolean chargeSucceeded = paymentIntent.getStatus().equalsIgnoreCase("succeeded");
		
		Revenue revenue;
		try {
			RequestOptions options = RequestOptions.builder()
					.setApiKey(STRIPE_API_KEY).build();
			
			paymentIntent = PaymentIntent.retrieve(paymentIntent.getId(), options);
			
			List<Charge> charges = paymentIntent.getCharges().getData();
			
			chargeSucceeded = charges.stream().anyMatch(charge -> charge.getPaid());
			
			Charge mostRecentCharge = charges.get(0);
			
			Map<String, String> paymentMetadata = paymentIntent.getMetadata();
					
			//Create revenue record in the database
			revenue = new Revenue();
			revenue.setMemberId(Long.parseLong(paymentMetadata.get(INTERNAL_SUPPLIER_ID_KEY)));
			revenue.setResolved(false);
			revenue.setCategory(paymentMetadata.get(MEDIA_CATEGORY_KEY));
			revenue.setProductId(Long.parseLong(paymentMetadata.get(MEDIA_ID_KEY)));
			revenue.setDescription(paymentIntent.getDescription());
			revenue.setCreated(LocalDateTime.now());
			revenue.setIsPaymentConfirmed(chargeSucceeded);
			revenue.setPaymentId(paymentIntent.getId());
			revenue.setInventoryRequestPrice(mostRecentCharge.getAmount().intValue());
			revenue.setBirefyCharge(
					mostRecentCharge.getApplicationFeeAmount() == null ? 
							0 : mostRecentCharge.getApplicationFeeAmount().intValue());
			
		} catch (StripeException s) {
			LOGGER.error(s.getMessage());
			throw new RuntimeException("Error post processing payment with id: " + paymentIntent.getId());
		}
		
		try {
			revenueRepository.addNewRecord(revenue);	
		} catch (NoSuchFieldException | IllegalAccessException e) {
			LOGGER.error("Error saving charge record!");
			LOGGER.error("Manually enter the following record: " + revenue);
		}
		
		if (chargeSucceeded) {
			updateUserPurchases(paymentIntent);
		}
		
		return chargeSucceeded ? PaymentStatus.CLEARED : PaymentStatus.PENDING;
	}
	
	public void updateUserPurchases(PaymentIntent paymentIntent) {		
		Map<String, String> metadata = paymentIntent.getMetadata();
		if(metadata.containsKey(DEAL_ID_KEY)) {
			long dealId = Long.parseLong(metadata.get(DEAL_ID_KEY));
			boolean dealPaid = dealRepository.setDealPaidDate(dealId, LocalDateTime.now());
			if(!dealPaid) {
				LOGGER.error("Failed to set deal paid date for deal with id: "+dealId);
				throw new RuntimeException("Error confirming deal payment.");
			}
		} else if(metadata.containsKey(MEDIA_ID_KEY)) {
			long mediaId = Long.parseLong(metadata.get(MEDIA_ID_KEY));
			long customerMemberId = Long.parseLong(metadata.get(INTERNAL_CUSTOMER_ID_KEY));
			MediaCategory category = MediaCategory.valueOf(metadata.get(MEDIA_CATEGORY_KEY));
			
			switch (category) {
				case PAINTING: {
					ImagePurchases imagePurchase = new ImagePurchases(customerMemberId, mediaId);
					try {
						imagePurchasesRepository.addNewRecord(imagePurchase);
					} catch (NoSuchFieldException | IllegalAccessException e) {
						LOGGER.error("Error updating purchase for customer: "+customerMemberId+" for " +category + " " + mediaId);
						throw new RuntimeException("Error updating purchase table");
					}
					break;
				}
				case SOUND: {
					SoundPurchases soundPurchase = new SoundPurchases(customerMemberId, mediaId);
					try {
						soundPurchasesRepository.addNewRecord(soundPurchase);
					} catch (NoSuchFieldException | IllegalAccessException e) {
						LOGGER.error("Error updating purchase for customer: "+customerMemberId+" for " +category + " " + mediaId);
						throw new RuntimeException("Error updating purchase table");
					} 
					break;
				}
				default: {
					throw new RuntimeException("content type and purchase mismatch.");
				}
			}
		}
	}
	
	@Deprecated
	public Charge createCharge(ChargeDetails chargeDetails, String chargeDescription) {
		Map<String, Object> chargeParams = createPaymentDataMap(chargeDetails, chargeDescription);
		
		RequestOptions options = RequestOptions.builder()
				.setIdempotencyKey(UUID.randomUUID().toString())
				.setApiKey(STRIPE_API_KEY).build();
		
		try {
			return Charge.create(chargeParams, options);
		} catch (StripeException e) {
			LOGGER.error("Error creating charge with description :" + chargeDescription, e);
			throw new RuntimeException("Failed to complete request");
		}
	}
	
	public Map<String, Object> createPaymentDataMap(ChargeDetails chargeDetails, String chargeDescription) {
		Member customerDetails = memberRepository.selectFullMemberDetailsById(chargeDetails.getCustomerMemberId());
		
		CurrencyValue value = chargeDetails.getValue();
		
		Map<String, Object> chargeParams = new HashMap<String, Object>();
		chargeParams.put("amount", value.getValue());
		chargeParams.put("currency", value.getCurrencyId());
		chargeParams.put("description", chargeDescription);
		chargeParams.put("source", chargeDetails.getSource().getId());
		//chargeParams.put("application_fee", applicationFee); Application fee not applicable
		chargeParams.put("receipt_email", customerDetails.getEmail());
		
		Map<String, String> metadata = new HashMap<String, String>();
		metadata.put(INTERNAL_SUPPLIER_ID_KEY, chargeDetails.getSupplierMemberId().toString());
		metadata.put(INTERNAL_CUSTOMER_ID_KEY, chargeDetails.getCustomerMemberId().toString());
		
		if(Optional.ofNullable(chargeDetails.getDealId()).isPresent()) {
			metadata.put(DEAL_ID_KEY, chargeDetails.getDealId().toString());
		}
		
		if(Optional.ofNullable(chargeDetails.getMediaId()).isPresent()) {
			metadata.put(MEDIA_ID_KEY, chargeDetails.getMediaId().toString());
		}
		
		if(Optional.ofNullable(chargeDetails.getMediaCategory()).isPresent()) {
			metadata.put(MEDIA_CATEGORY_KEY, chargeDetails.getMediaCategory().toString());
		}
		
		chargeParams.put("metadata", metadata);
		
		return chargeParams;
	}
	
	public void configureSubscriptionCharge(Long memberId, String source) {
		Member member = memberRepository.selectFullMemberDetailsById(memberId);
		
		if (!member.isActive()) {
			throw new RuntimeException("User with id " + memberId + " is not active.");
		}
		String customerReferenceId = member.getSubscriptionReferenceId();
		
		RequestOptions options = RequestOptions.builder().setApiKey(STRIPE_API_KEY).build();
		
		Customer customer;
		if (StringUtils.isNotEmpty(customerReferenceId)) {
			try {
				customer = Customer.retrieve(customerReferenceId, options);
			} catch (StripeException e) {
				LOGGER.error("Unable to retrieve customer details for customer with reference id: "
						+ customerReferenceId, e);
				throw new RuntimeException("Unable to retrieve customer data.");
			}
		} else {
			Map<String, Object> params = new HashMap<String, Object>();
			Map<String, String> metadata = new HashMap<String, String>();
			
			params.put("description", "Customer for member "+member.getId());
			params.put("email", member.getEmail());
			params.put("source", source);
			
			metadata.put("memberId", member.getId().toString());
	
			try {
				customer = Customer.create(params, options);
			} catch (StripeException e) {
				LOGGER.error("Error creating customer data for member " + member.getId(), e);
				throw new RuntimeException("Unable to generate customer data.");
			}
		}
		
		//Calculate the number of running projects for the customer
		Integer numberOfSubscriptionProjects = qMemberQuestionRepository.subscriptionQuestionsForMember(member.getId());
		
		Map<String, Object> item = new HashMap<String, Object>();
		item.put("plan", retrieveInnovationBroadcastPlan());
		item.put("quantity", numberOfSubscriptionProjects);
		
		Map<String, Object> items = new HashMap<String, Object>();
		items.put("0", item);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("customer", customer.getId());
		params.put("items", items);
		
		try {
			Subscription.create(params, options);
		} catch (StripeException e) {
			LOGGER.error("Error creating subscription " + INNOVATION_BROADCAST_SUBSCRIPTION, e);
			throw new RuntimeException("Error creating subscription.");
		}
	}
	
	public void updateSubscriptionCharges() {
		//TODO need to charge based on a max number of projects --encourage people to do more projects
		
		//TODO create a job that can assess when subscriptions need to be updated
		
		//TODO adapt schema to be able to activate and deactivate questions based on subscription cancellations
		//TODO adapt questions based on perscription mispayments.
		
		//TODO need to create a service that deletes containers that are not active and require a subscription
	}
	
	@Deprecated
	public boolean postProcessCharge(Charge charge) {
		boolean success = true;
		Long productId = null;
		
		Map<String, String> metadata = charge.getMetadata();
		if(metadata.containsKey(DEAL_ID_KEY)) {
			long dealId = Long.parseLong(metadata.get(DEAL_ID_KEY));
			productId = dealId;
			boolean dealPaid = dealRepository.setDealPaidDate(dealId, LocalDateTime.now());
			if(!dealPaid) {
				LOGGER.error("Failed to set deal paid date for deal with id: "+dealId);
				throw new RuntimeException("Error confirming deal payment.");
			}
		} else if(metadata.containsKey(MEDIA_ID_KEY)) {
			long mediaId = Long.parseLong(metadata.get(MEDIA_ID_KEY));
			productId = mediaId;
			long customerMemberId = Long.parseLong(metadata.get(INTERNAL_CUSTOMER_ID_KEY));
			MediaCategory category = MediaCategory.valueOf(metadata.get(MEDIA_CATEGORY_KEY));
			
			switch (category) {
				case PAINTING: {
					ImagePurchases imagePurchase = new ImagePurchases(customerMemberId, mediaId);
					try {
						imagePurchasesRepository.addNewRecord(imagePurchase);
					} catch (NoSuchFieldException | IllegalAccessException e) {
						LOGGER.error("Error updating purchase for customer: "+customerMemberId+" for" +category + " " + mediaId);
						throw new RuntimeException("Error updating purchase table");
					}
					break;
				}
				case SOUND: {
					SoundPurchases soundPurchase = new SoundPurchases(customerMemberId, mediaId);
					try {
						soundPurchasesRepository.addNewRecord(soundPurchase);
					} catch (NoSuchFieldException | IllegalAccessException e) {
						LOGGER.error("Error updating purchase for customer: "+customerMemberId+" for" +category + " " + mediaId);
						throw new RuntimeException("Error updating purchase table");
					} 
					break;
				}
				default: {
					throw new RuntimeException("content type and purchase mismatch.");
				}
			}
		}
		
		//Update Revenue
		Revenue addedRevenue = new Revenue();
		addedRevenue.setMemberId(Long.valueOf(metadata.get(INTERNAL_SUPPLIER_ID_KEY)));
		addedRevenue.setProductId(productId);
		addedRevenue.setDescription(charge.getDescription());
		addedRevenue.setCategory(metadata.get(MEDIA_CATEGORY_KEY));
		addedRevenue.setInventoryRequestPrice(charge.getAmount().intValue() - applicationFee);
		addedRevenue.setBirefyCharge(applicationFee);
		addedRevenue.setCreated(LocalDateTime.now());
		addedRevenue.setResolved(false);
		
		success = payoutService.updateRevenue(addedRevenue);
		
		return success;
	}
	
	public Map<String, CurrencyValue> getValueForDigitalProduce(Long id, ChargeDetails.MediaCategory category, Long supplierId) {
		Map<String, CurrencyValue> value = new HashMap<>();
		switch(category) {
		case DEAL: {
			Map<String, Float> detailsAndPrice = dealRepository.dealDetailsAndPrice(id);
			
			Entry<String, Float> details = detailsAndPrice.entrySet().stream().findFirst().get();
			value.put(details.getKey(), new CurrencyValue(
					convertCurrencyToSmallestUnitAndAddApplicationFee(details.getValue(), supplierId),
					"gbp"
					));
			break;
		}
		case SOUND: {
			SoundShort sound = soundRepository.getSoundById(id);
			value.put(sound.getName(), new CurrencyValue(
					convertCurrencyToSmallestUnitAndAddApplicationFee(sound.getPrice(), supplierId),
					"gbp"
					));
			break;
		}
		case PAINTING: {
			PaintingShort painting = paintingRepository.getPaintingById(id);
			value.put(painting.getName(), new CurrencyValue(
					convertCurrencyToSmallestUnitAndAddApplicationFee(painting.getPrice(), supplierId),
					"gbp"
					));
			break;
		}
		}
		
		return value;
	}
	
	public Long getOwnerOfProduce(Long productId, ChargeDetails.MediaCategory category) {
		Long ownerId;
		switch (category) {
		case DEAL: {
			ownerId = dealRepository.dealSupplierId(productId);
			break;
		}
		case SOUND: {
			ownerId = soundRepository.soundOwnerId(productId);
			break;
		}
		case PAINTING: {
			ownerId = paintingRepository.paintingOwnerId(productId);
			break;
		}
		default: {
			throw new RuntimeException("Unrecognised media category.");
		}
		}
		
		return ownerId;
	}
	
	@Scheduled(cron = "0 0 6/6 * * ?")
	public void processPendingPayments() {
		LOGGER.info("Started processing of missed payments started...");
		
		List<RevenuePayment> unprocessedPayments = revenueRepository.findOutstandingPayments();
		
		LOGGER.info("Found " + unprocessedPayments.size() + " unprocessed payments.");
		
		RequestOptions options = RequestOptions.builder()
				.setApiKey(STRIPE_API_KEY).build();
		
		unprocessedPayments.forEach(rev -> {
			try {
				PaymentIntent pI = PaymentIntent.retrieve(rev.getPaymentIntentId(), options);
				
				List<Charge> charges = pI.getCharges().getData();
				
				boolean chargeSucceeded = charges.stream().anyMatch(charge -> charge.getPaid());
				
				if (chargeSucceeded) {
					updateUserPurchases(pI);
					revenueRepository.updateRevenuePaymentStatus(rev.getRevenueId(), chargeSucceeded);
				}
				
			} catch (StripeException s) {
				LOGGER.error(s.getMessage());
				LOGGER.error("Error processing revenue entry: " + rev.toString());
			}
		});
		
	}
	
	private Plan retrieveInnovationBroadcastPlan() {
		RequestOptions options = RequestOptions.builder().setApiKey(STRIPE_API_KEY).build();
		
		Plan innovationBroadcastPlan = null;
		try {
			innovationBroadcastPlan = Plan.retrieve(INNOVATION_BROADCAST_SUBSCRIPTION, options);
		} catch (StripeException s) {
			LOGGER.error("Unable to obtain " + INNOVATION_BROADCAST_SUBSCRIPTION + " plan.", s);
		}
		
		if (!Optional.ofNullable(innovationBroadcastPlan).isPresent()) {
			LOGGER.info("Attempting to create " + INNOVATION_BROADCAST_SUBSCRIPTION);
			Map<String, Object> productParams = new HashMap<String, Object>();
			productParams.put("name", "Innovation Skills Broadcast");
			
			Map<String, Object> planParameters = new HashMap<String, Object>();
			planParameters.put("id", INNOVATION_BROADCAST_SUBSCRIPTION);
			planParameters.put("amount", subscriptionFee);
			planParameters.put("product", productParams);
			planParameters.put("interval", "month");
			planParameters.put("currency", "gbp");
			planParameters.put("billing_scheme", "per_unit");
			
			try {
				innovationBroadcastPlan = Plan.create(planParameters, options);
			} catch (StripeException s) {
				LOGGER.error("Error creating " + INNOVATION_BROADCAST_SUBSCRIPTION + " plan", s);
				throw new RuntimeException("Error retrieving " + INNOVATION_BROADCAST_SUBSCRIPTION);
			}
		}
		
		return innovationBroadcastPlan;
	}
	
	private Integer convertCurrencyToSmallestUnitAndAddApplicationFee(Float currency, long supplierMemberId) {
		BirefyPlatformAuthority supplierAuthority = memberRepository.getMemberAuthority(supplierMemberId);
		
		Integer chargeValue;
		Float unitValue = currency * 100;
		
		if (supplierAuthority == BirefyPlatformAuthority.ROYAL) {
			chargeValue = unitValue.intValue() + applicationFee;
		} else {
			chargeValue = unitValue.intValue();
		}
		
		return chargeValue;
	}
}
