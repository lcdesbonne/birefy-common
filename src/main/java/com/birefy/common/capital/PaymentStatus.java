package com.birefy.common.capital;

public enum PaymentStatus {
	CLEARED, PENDING
}
