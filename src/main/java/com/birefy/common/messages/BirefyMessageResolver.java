package com.birefy.common.messages;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;

import freemarker.template.Template;
import freemarker.template.TemplateException;

//TODO implement using locale
public class BirefyMessageResolver implements MessageSource {
	private static final Logger LOGGER = LoggerFactory.getLogger(BirefyMessageResolver.class);
	
	private freemarker.template.Configuration emailMessagetemplateConfiguration;
	
	public BirefyMessageResolver(freemarker.template.Configuration templateSourceConfiguration) {
		this.emailMessagetemplateConfiguration = templateSourceConfiguration;
	}
	
	@Override
	public String getMessage(String code, Object[] args, String defaultMessage, Locale locale) {
		String message = new String();
		try {
			message = processMessage(code, args);
		} catch (IOException e) {
			LOGGER.warn("Unable to find template " + code, e);
			message = String.format(defaultMessage, args);
		} catch (TemplateException t) {
			LOGGER.error("Unable to process template.", t);
			throw new RuntimeException("Unable to execute task.");
		}
	
		return message;
	}

	@Override
	public String getMessage(String code, Object[] args, Locale locale) throws NoSuchMessageException {
		String message;
		try {
			message = processMessage(code, args);
		} catch (TemplateException | IOException e) {
			LOGGER.warn("Unable to find template " + code, e);
			throw new NoSuchMessageException(e.getMessage(), locale);
		} 	
		return message;
	}

	@Override
	public String getMessage(MessageSourceResolvable resolvable, Locale locale) throws NoSuchMessageException {
		String[] codes = resolvable.getCodes();
		String defaultMessage = resolvable.getDefaultMessage();
		Object[] arguments = resolvable.getArguments();
		
		String message;
		if (StringUtils.isNotEmpty(defaultMessage)) {
			StringBuilder messageBuilder = new StringBuilder();
			for(String code : codes) {
				messageBuilder.append(getMessage(code, arguments, defaultMessage, locale));
			}
			
			message = messageBuilder.toString();
		} else {
			StringBuilder messageBuilder = new StringBuilder();
			for(String code : codes) {
				messageBuilder.append(getMessage(code, arguments, locale));
			}
			
			message = messageBuilder.toString();
		}
		
		return message;
	}
	
	private String processMessage(String code, Object[] args) throws TemplateException, IOException {
		Template template = emailMessagetemplateConfiguration.getTemplate(code + ".ftl");
		
		//Construct property map
		Map<String, String> propertyMap = Stream.of(args).map(prop -> new MessageProperty(((MessageProperty) prop).getPropertyName(), ((MessageProperty) prop).getPropertyValue()))
				.collect(Collectors.toMap(MessageProperty::getPropertyName, MessageProperty::getPropertyValue));
		
		Writer stringWriter = new StringWriter();
		template.process(propertyMap, stringWriter);
		
		return stringWriter.toString();
	}

}
