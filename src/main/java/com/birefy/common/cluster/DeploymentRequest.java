package com.birefy.common.cluster;

import java.util.HashMap;
import java.util.Map;

public class DeploymentRequest {
	private String appName;
	private Integer port;
	private String dockerImage;
	private Long memberId;
	private Long questionId;
	
	public DeploymentRequest(String appName, String dockerImage, Integer port) {
		this.appName = appName;
		this.dockerImage = dockerImage;
		this.port = port;
	}

	public DeploymentRequest(String appName, Integer port, String dockerImage, Long memberId, Long questionId) {
		this.appName = appName;
		this.port = port;
		this.dockerImage = dockerImage;
		this.memberId = memberId;
		this.questionId = questionId;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getDockerImage() {
		return dockerImage;
	}

	public void setDockerImage(String dockerImage) {
		this.dockerImage = dockerImage;
	}
	
	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public Map<String, Object> toPropertyMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("qId", String.valueOf(questionId));
		map.put("port", String.valueOf(this.port));
		map.put("dockerImage", this.dockerImage);
		
		return map;
	}
}
