package com.birefy.common.cluster;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Component
public class ClusterUtilities {
	private static Logger LOGGER = LoggerFactory.getLogger(ClusterUtilities.class);
	
	@Autowired
	@Qualifier("freeMarkerTemplateProcessor")
	private Configuration freeMarkerTemplateProcessor;
	
	@Value("${technology.directories.deployments}")
	private String deploymentParentDirectory;
	
	@Value("${technology.deployment-filename}")
	private String deploymentFileName;
	
	@Value("${system.path.separator}")
	private String pathSeparator;
	
	@Value("${technology.directories.deletions}")
	private String deletionDirectory;
	
	@Value("${technology.deletion-filename}")
	private String deletionFileName;
	
	public void clearDeploymentFolder() {
		File parentDirectory = new File(deploymentParentDirectory);
		
		if(parentDirectory.exists() && parentDirectory.isDirectory()) {
			File[] containingFiles = parentDirectory.listFiles();
			for(File deployment : containingFiles) {
				deployment.delete();
			}
		}
	}
	
	public String generateDeploymentContent(DeploymentRequest deploymentRequest) throws IOException, TemplateException {
		Template template = freeMarkerTemplateProcessor.getTemplate("project-deployment.ftl");
		Map<String, Object> input = deploymentRequest.toPropertyMap();
		
		File parentDirectory = new File(deploymentParentDirectory);
		if(!parentDirectory.isDirectory()) {
			LOGGER.info("Creating directory: " + parentDirectory.getAbsolutePath());
			Files.createDirectory(parentDirectory.toPath());
		}
		
		File deploymentFile = deploymentFileNameCreator(deploymentRequest.getQuestionId(), deploymentRequest.getMemberId());
		Files.deleteIfExists(deploymentFile.toPath());
		
		//Write processed template to file
		try (Writer fileWriter = new FileWriter(deploymentFile)) {
			LOGGER.info("Writing content to file: " + deploymentFile.getAbsolutePath());
			template.process(input, fileWriter);
		} 
		
		return deploymentFile.getAbsolutePath();
	}
	
	public String deleteProjectContent(Long questionId) throws IOException, TemplateException  {
		Template template = freeMarkerTemplateProcessor.getTemplate("project-delete.ftl");
		Map<String, Object> input = new HashMap<String, Object>();
		input.put("questionId", questionId);
		
		File deletionDirectory = new File(this.deletionDirectory);
		if(!deletionDirectory.isDirectory()) {
			Files.createDirectory(deletionDirectory.toPath());
		}
		
		String deletionFileName = String.format(this.deletionFileName, questionId.toString());
		
		File deletionFile = new File(this.deletionDirectory + pathSeparator + deletionFileName);
		
		try (Writer fileWriter = new FileWriter(deletionFile)) {
			template.process(input, fileWriter);
		}
		
		return deletionFile.getName();
	}
	
	/**
	 * The name of the file will include the ID of the related question
	 * @param questionId
	 * @return
	 */
	private File deploymentFileNameCreator(Long questionId, Long memberId) {
		File deploymentFile = new File(
				deploymentParentDirectory +
				pathSeparator +
				String.format(deploymentFileName, questionId.toString())
				);
		return deploymentFile;
	}

}
