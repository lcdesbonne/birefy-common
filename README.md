Built using java and spring boot birefy is a platform designed to share creative ideas and promote collaboration.
Requirements for full functionality are...
 - MySQL database
 - Java 11 or higher
Deployment has been tested using kubernetes with the application running in a Docker container. 

To run the dummy-example branch (branch with a lot of features disabled in order to show quick/simple
visualization of the platform)
Run: mvn:spring-boot:run -Dspring-boot.run.profiles=develop